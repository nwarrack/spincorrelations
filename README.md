# Branch notes
"master" works with round 5 nTuples.  
"com03" works with round 3 nTuples. It and was created from master just prior to changing master to work with rount 5 nTuples.  


# Release/tag notes
tag: N.X.X is code that works with the "round N" common dilepton ntuples  

[ml-3.0.2](https://gitlab.cern.ch/nwarrack/spincorrelations/-/releases/ml-3.0.2): created ml iputs from rnd3 ntups (`tester_06_2023Feb28_190530.root`) from branch: "com03"  

# What does this code do?
There are a number of executable python scripts, `create_something.py`, most of which require arguments. You can find out about the arguments by doing something like
```bash
./create_something.py -h
```
  
`create_hists.py` will loop over nTuples and save histograms of various things (as .root files)  
`create_plots.py` will use .root files with histograms to produce pdf plots.  
`create_migration_plot.py` will use .root files histograms to produce migration matrix plots  
`create_ml_inputs.py` will create root nTuples for some [machine learning code](https://gitlab.com/neilwarrack/spinformation) I've written to look into ttbar reco methods. 

# create_hists.py
The `create_hists.py` is the main script which loops over events and fills and saves histograms, but before you can use it you must setup the environment and tell the scripts where your files are...
## Cloning the repository and setting up your environment
To copy this repository to your workspace do the following:
``` bash
git clone https://gitlab.cern.ch/nwarrack/spincorrelations.git
cd spincorrelations
source setup.sh
```
*The last command (`source setup.sh`) sets up your environment and you are required to do this every time you use this code!*

Before the scripts can work you need to tell them where your files are...
## Pointing the scripts to the data files
The scripts in this repo use samples, which are each comprised of a set of files, the scripts need to know where the files are located (either on the grid, or locally - both work!). The paths of the files are saved in a directory called `user/addresses`. If you haven't got this directory then you need to create it by folloing [these instructions](https://gitlab.cern.ch/nwarrack/spincorrelations/blob/master/bash/README.md "bash/README.md").


Once the `addresses` directory is make you can use the various "create_----.py" scripts to do various things like loop over the files and make root files with historgrams in them (using create_hists.py) and then make pdf plots (using create_plots.py). For help you can always use the `-h` argument, like this:
```bash
./create_hists.py -h
```
Using the `-h` argument as above will show you information about the arguments that the script needs to work. It might show something like:  
> 
> usage: create_hists.py [-h] -p PERIOD -o OBJECT [OBJECT ...] -c CHANNEL [CHANNEL ...]  
>  
Which takes the following options:  
`PERIOD` = `16a`/`16d`/`16e`  
`OBJECT` = `met`/`lep`/`top`/`jet`/`spin`  
`CHANNEL` = `ee`/`emu`/`mumu`  
<!--- this used to be: object = `el`/`mu`/`lep`/`jet`/`met`/`nu`/`top`/`tt`/`spin`  -->

Note:  
`16a` = period MC16a = 2015 + 2016  
`16d` = period MC16d = 2017  
`16e` = period MC16e = 2018  
*You can only specify _one_ period, but you can supply a space-seperated list of objects and you can supply a space-separated list of channels.*  

Note:
Specifying an object will produce all associated histograms for that object. These are often: signal, backgrounds and data which are all configured as defined in [the histconfig directory](https://gitlab.cern.ch/nwarrack/spincorrelations/tree/master/histconfig "histconfig").  
`lep` = lepton = electrons and muons  
`top` = top quarks and antiquarks (produces separate plots for each)  
`spin` = spin sensitive observables (ttbar correlations and top/tbar polarisations)  
`met` = 'full event' variables (missing transverse momentum 'met_met', and its direction 'met_phi')  
`jet` = jets
<!-- `el` = electron -->
<!-- `mu` = muon  -->
<!-- `tt` = ttbar system  -->
<!-- `nu` = neutrinos and anti-neutrinos  -->

Note:  
Combining channels and periods can be done at the plotting stage with `create_plots.py`.

# Examples
To make all the histograms needed to plot the spin correlations in the emu channel for preiod MC16e, you can run:
```bash
./create_hists.py --period 16e --object spin --channel emu -w 666
```
or, equivalently:
```bash
./create_hists.py -p 16e -o spin -c emu -w 666
```
The above command would make some histograms (in root file format).


# create_plots.py

Once you have used the `create_hists.py` script you will have root files, saved in the `user/Hists/16X` directory, where `X`=`a`, `d` or `e`. Each `.root` file contains all hists associated with an observable and a channel and will have a name like `emu_NW_ckk_nominal.root` (which is the `emu` channel for the `C_kk` spin correlation sensitive variable using the Neutrino Weighter ttbar reconstruction method and using the `nominal` weight for each event. If the file has a number at the end (e.g. emu_NW_ckk_nominal_173.root`) then it means the EFT ttbar samples were used with the Wilson Coeff index 173 (where index meanings are give, for example, [here](https://gitlab.cern.ch/nwarrack/spincorrelations/-/blob/master/datalists/eftlist_21.2.109.txt "example of WC indicies list (for tuples made with 21.6.109)")).


You can then plot all the hists you just saved, using some something like:
```bash
# if you used `./create_hists.py -p 16e -o spin -c emu` you can plot them with:
./create_plots.py -p 16e -o spin -c emu -s nominal -a plot
```

`PERIOD` = `16a`,`16d`,`16e` or `all`.  
`OBJECT` = `met`/`lep`/`top`/`jet`/`spin`  
`CHANNEL` = `ee`/`emu`/`mumu`/`all`  
`ACTION` = `plot`/`rmbck`/`truthcomp`/`NNLOcomp`/`resolution`/`eft`/`mcplot`  

Use the period/channel arg 'all' to combine all three periods/channels.  

Type `./create_plots.py -h` for full discription of arguments.

 
_NB: you need the extra systematics (`-s`) argument and the 'action' argument (`-a`)._  
_NB: When creating the hists using create_hists.py the systematics are controled by a list inside the create_hists.py script, rather than as an argument._  




# create_migration_plot_2eft.py
This script last worked with uproot4 which was the latest at the time. It may work with newer uproots, check out https://github.com/conda-forge/uproot-feedstock for info.  
This is how Installed and used uproot4 (2021):
```bash
wget -nv http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
bash miniconda.sh -b -p $HOME/miniconda
source $HOME/miniconda/etc/profile.d/conda.sh
conda config --add channels conda-forge
conda install uproot uproot-base 
conda create -n myuproot4env python=3.7 uproot
```

Firstly make some hists:
```bash
source setup.sh
./create_hists.py --period 16e --object spin --channel emu -w 666 # i.e. SM
```
Then move the output hists (user/Hists) to somewhere safe. Then:
```bash
./create_hists.py --period 16e --object spin --channel emu -w 117
```
Then, again, move the output to somewhere safe. Then, in a new shell (to stop clashes with conda and python2 setup used for everthing else):
```
conda activate myuproot4env # to go into new virtual env
./create_migration_plot_2eft.py -c emu -v ckk -w 117 -e False
conda deactivate # to get out of virtual env
```

# Refereneces
- [Cross-sections and K-factors](https://atlas-groupdata.web.cern.ch/atlas-groupdata/dev/AnalysisTop/TopDataPreparation/)
- [Uproot](https://github.com/conda-forge/uproot-feedstock)
