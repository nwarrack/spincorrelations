# Create your directory of addresses (required!)
Each sample is made up of many root files and creating the `addresses/` directory allows scripts in this repository to find those files. It doesn't matter if they are downloaded locally or replicated somewhere on the grid, if they are on the grid you need to know which grid site (or "RSE") the files are replicated at. If you don't know the RSE where the files are replicated then you can request replicas at a chosen RSE by using the `mkreplicas.sh` scrip in this directory. If you want to use downloaded files then you can download the files using `downloaddata.sh` and use those instead of on-grid files.

*Firstly, set up Rucio (remeber to set up your environment first, using `setup.sh` from the main directory)*
```bash
cd bash
lsetup rucio
```
## Option 1) Running over downloaded files
*Note: If you want to use files on the grid, skip to Option 2.*

You can use the default list of samples (see the [`datalists/`](https://gitlab.cern.ch/nwarrack/spincorrelations/-/tree/master/datalists) directory) to download the corresponding data by first making an empty directory for your data:
```bash
mkdir /absolute/path/to/your/data
```
Then running the `downloaddata.sh` bash script:
```bash
./downloaddata.sh ../datalists/samples_AB212109_com_01.py /absolute/path/to/your/data
```
Or, just run rucio directly on a smaple using its DID (dataset ID). For example the following would download two random files from the dataset `group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10724_p4514.AB-21.2.197-MC16eSignal-v21_out_root` to the directory `/eos/user/n/nwarrack/data/samples_AB212197_com_05`:
```bash
rucio download --nrandom 2 group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10724_p4514.AB-21.2.197-MC16eSignal-v21_out_root --dir /eos/user/n/nwarrack/data/samples_AB212197_com_05
```

Once the files are downloaded, you can create the required `addresses/` directory by doing:
```bash
./mkaddresses.sh ../datalists/samples_AB212109_com_01.py /absolute/path/to/your/data
```


## Option 2) Running over files on the grid
You need to locate the grid site ("RSE") where your files are replicated. You can check that your files are fully replicated at a specific RSE by doing something like:
```bash
./checkreplicas.sh ../datalists/samples_AB212197_com_05.py CERN-PROD_PHYS-TOP
```
The above command would check to see that all the samples in `datalists/samples_AB212197_com_05.py` were fully replicated at the CERN-PROD_PHYS-TOP grid site

*Note: If your files are not yet replicated at your desired RSE then you can make replicas by doing something like:*
```bash 
./mkreplicas.sh ../datalists/samples_desiredDampleList.py MY-DESIRED-RSE
```
*Note: If you are not sure which is your favourite RSE you could check for RSEs in, say, Glasgow, Scotland by doing:*
```bash
rucio list-rses --expression 'tier=2' | grep "GLASGOW"
```
Once the samples are fully replicated at the RSE of your choice then you can make the `addresses` directory by doing something like:
```bash
./mkaddresses ../datalists/samples_AB212197_com_05.py CERN-PROD_PHYS-TOP
```
*Note: type `mkaddresses.sh -h` for more info.*