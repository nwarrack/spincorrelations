#!/usr/bin/env bash

if [ "$1" == "-h" ]; then
    printf "
 This script checks the status of replicas at a specified grid site

 Usage: ./`basename $0` SAMPLES RSE

 where:
   SAMPLES = path/to/your/samplesfile.py
   DATADIR = NAME_OF_GRID_SITE

 EXAMPLE:
    `basename $0` ../datalists/samples_AB212109_com_01.py UKI-SCOTGRID-GLASGOW_LOCALGROUPDISK

 NOTES:
   If you have not requested replicas then for more info type: mkreplicas.sh -h\n
   If you can't remember your RSE, try checking, for example:
   rucio list-rses --expression 'tier=2' | grep \"GLASGOW\"\n\n"
    exit 0
fi


# abort if incorrect number of arguments
[[ $# -eq 2 ]] || { echo >&2 "This script requires 2 arguments - for help type: ./`basename $0` -h"; exit 1;}

# check to see if rucio is set up
type rucio >/dev/null 2>&1 || { echo >&2 "Type: \"lsetup rucio\" and try again..."; exit 1; }

SAMPLE_SCRIPT=$1

# check if the input datalist exists
[[ -f $SAMPLE_SCRIPT ]] || { printf >&2 "The file $SAMPLE_SCRIPT doesn't exist. Exiting\n"; exit 0; }

# create sample list name
SAMPLE_LIST=$(basename ${SAMPLE_SCRIPT%.py}.txt)

# create the sample list (just a copy of the python script)
cp $SAMPLE_SCRIPT $PWD/../user/$SAMPLE_LIST

# remove everything from the sample list except for the names of the samples
cd ../user/
sed -i -e '/txt/!d' -e 's/^[ \t]*//' -e '/^#/d' -e 's/[^\"]*\"//' -e 's/[\"].*//' -e 's/.txt//' $SAMPLE_LIST

RSE=$2

# define boolian for recording replication status
FULLY_REPLICATED=true


## Check replicas using rucio
while IFS="" read -r p || [ -n "$p" ]
do

    echo "Checking sample: $p"
    files=$(rucio list-files --csv $p | wc -l)
    replicas=$(rucio list-file-replicas --rse $RSE --pfns $p | wc -l)
        
    if [ "$files" != "$replicas" ]; then
	echo " WARNING: not fully replicated to $RSE"
	echo " files    = $files"
	echo " replicas = $replicas"
	FULLY_REPLICATED=false
    elif [ "$files" == 0 ]; then
	echo " WARNING: files not found!"
	FULLY_REPLICATED=false
    else
	echo "Replicas complete!"
    fi
    echo ""
done < $SAMPLE_LIST

echo "___________________________________________________"
echo "Finished checking for replicas on $RSE!"
echo "___________________________________________________"
echo "------------------ YOUR RESULTS: ------------------"
if [ "$FULLY_REPLICATED" = false ] ; then
    echo "WARNING: Some files have not been fully replicated or they don't exist."
else
    echo "SUCCESS! All replicas complete!"
fi
echo "___________________________________________________"
