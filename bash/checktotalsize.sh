#!/usr/bin/env bash


# abort (with help) if incorrect arguments are given.
[[ $# -eq 1 ]] || { echo >&2 "

 This is a script to calculate the total size of a list of samples
 
 usage: 
   ./checktotalsize.sh SAMPLES

 where:
   SAMPLE = path/to/your/samplesfile.py

 EXAMPLE:
   ./checktotalsize.sh ../datalists/samples_AB212109_com_01.py";

    exit 1; }


# Check if rucio is setup
type rucio >/dev/null 2>&1 || { echo >&2 "Type: \"lsetup rucio\" and try again..."; exit 1; }

# abort if script not run in correct location
[[ -f mkreplicas.sh ]] || { echo >&2 "Execute this script from its location"; exit 1; }

SAMPLE_SCRIPT=$1

# check if the input datalist exists
[[ -f $SAMPLE_SCRIPT ]] || { printf >&2 "The file $SAMPLE_SCRIPT doesn't exist. Exiting\n"; exit 0; }

# create sample list name
SAMPLE_LIST=$(basename ${SAMPLE_SCRIPT%.py}.txt)

# create the sample list (just a copy of the python script)
cp $SAMPLE_SCRIPT $PWD/../user/$SAMPLE_LIST

# remove everything from the sample list except for the names of the samples
cd ../user/
sed -i -e '/txt/!d' -e 's/^[ \t]*//' -e '/^#/d' -e 's/[^\"]*\"//' -e 's/[\"].*//' -e 's/.txt//' $SAMPLE_LIST

# create a file to fill with output
FILES_LIST=$PWD/../user/$(basename ${SAMPLE_LIST%.txt}_filesoutput.txt)
touch $FILES_LIST

# loop over samples list and request replicas for each sample
while IFS="" read -r p || [ -n "$p" ]
do

    rucio list-files $p >> $FILES_LIST
    echo "getting file sizes for: $p"

done < $SAMPLE_LIST

sed -i '/size/!d' $FILES_LIST    # remove all lines except those with "size"
sed -i 's/Total//g' $FILES_LIST  # remove "Total"
sed -i 's/size//g' $FILES_LIST   # remove "size"
sed -i 's/://g' $FILES_LIST      # remove all colons
sed -i "s/^[ t]*//" $FILES_LIST  # remove all leading whitespace


echo ""
echo ""
echo "----------- RESULTS ----------------"
echo ""
paste $FILES_LIST $SAMPLE_LIST
echo ""
