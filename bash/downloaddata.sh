#!/usr/bin/env bash

if [ "$1" == "-h" ]; then
    printf "
 This is a script to download the files from the grid into
 a local directory (if the files already exist it will not
 re-download anything).

 Usage: ./`basename $0` SAMPLES DATADIR

 where:
   SAMPLES = path/to/samples.py
   DATADIR = path/to/directory

 EXAMPLE:
    `basename $0` ../datalists/samples_AB212109_com_01.py path/to/empty/dir\n"
    exit 0
fi

# Abort if incorrect arguments are given.
[[ $# -eq 2 ]] || { echo >&2 "This script requires 2 arguments - for help type: ./`basename $0` -h"; exit 1;}


# Check if rucio is setup
type rucio >/dev/null 2>&1 || { echo >&2 "You need Rucio to download from the grid, type: \"lsetup rucio\" and try again..."; exit 1; }


# set desired path to empty directory where data will be stored
DATAPATH=$2

# make $DATAPATH if it does not exist
[[ -d "$DATAPATH" ]] || {
    echo >&2 "The directory $DATAPATH does not exist. Creating it..."
    mkdir -p $DATAPATH || { >&2 echo "you do not have write permission - aborting"; exit 0; }
}


# print some helpful info
echo "path to data: $DATAPATH"


# make copy of input list in target directory and cd to target directory
SAMPLE_SCRIPT=$1

# check if the input datalist exists
[[ -f $SAMPLE_SCRIPT ]] || { printf >&2 "The file $SAMPLE_SCRIPT doesn't exist. Exiting\n"; exit 0; }

# create sample list name
SAMPLE_LIST=$(basename ${SAMPLE_SCRIPT%.py}.txt)

# create the sample list (just a copy of the python script)
cp $SAMPLE_SCRIPT $PWD/../user/$SAMPLE_LIST

# remove everything from the sample list except for the names of the samples
cd ../user/
sed -i -e '/txt/!d' -e 's/^[ \t]*//' -e '/^#/d' -e 's/[^\"]*\"//' -e 's/[\"].*//' -e 's/.txt//' $SAMPLE_LIST

cp $SAMPLE_LIST $DATAPATH
cd $DATAPATH


# download file using rucio
while IFS="" read -r p || [ -n "$p" ]
do
    printf '%s\n' "$p"
    rucio download "$p"
    #rucio download --rse UKI-SCOTGRID-GLASGOW_LOCALGROUPDISK "$p" # <- option to use specific RSE

done < $(basename $SAMPLE_LIST)
