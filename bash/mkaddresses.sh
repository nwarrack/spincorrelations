#!/usr/bin/env bash


if [ "$1" == "-h" ]; then
    printf "
 This script produces a directory of addresses (paths to your local or on-grid data files)

 Usage: ./`basename $0` SAMPLES LOCATION

 where:
   SAMPLES  = path to appropriate samplesfile.py (they live in the datalists directory)
   LOCATION = YOUR_CHOSEN_GRID_SITE or an absolure path to your files

 EXAMPLE (using GRID files):
    `basename $0` ../datalists/samples_AB212109_com_01.py CERN-PROD_PHYS-TOP

    NOTE: If your samples are not replicated at the grid site you should use mkreplicas.sh!

 EXAMPLE (using LOCAL files):
    `basename $0` ../datalists/samples_AB212109_com_01.py /home/your/data/directory\n"
    exit 0
fi

# Abort if environment is not setup (i.e. setup.sh from the spincorrelations directory!)
[[ -v SPINHOME ]] || { printf >&2 "Your environment is not set up! Go to the spincorrelations/ directory and type \"source setup.sh\"\n"; exit 0; }

# abort if incorrect number of arguments
[[ $# -eq 2 ]] || { printf >&2 "This script requires two arguments. For help type:\n./`basename $0` -h\n"; exit 0;}

# Abort if script not run in correct location
[[ -f $(basename $0) ]] || { printf >&2 "Execute this script from within its own 'bash' directory:\n$(dirname $(realpath -s $0))/\n"; exit 0; }

# abort if addresses directory already exists
[[ ! -d ../user/addresses ]] || { echo >&2 "The directory spincorrelations/user/addresses already exists. Move it or delete it."; exit 0; }

# store the datalist
SAMPLE_SCRIPT=$1

# check if the input datalist exists
[[ -f $SAMPLE_SCRIPT ]] || { printf >&2 "The file $SAMPLE_SCRIPT doesn't exist. Exiting\n"; exit 0; }

# if using local files...
if [[ $2 == /* ]]
then
    LOCATION=$2
    # Check the argument directory exists
    [[ -d $LOCATION ]] || { printf "$LOCATION doesn't exist. Exiting\n"; exit 0; }


else # use grid files...
    echo "checking for rucio"
    # check to see if rucio is set up
    type rucio >/dev/null 2>&1 || { echo >&2 "Type: \"lsetup rucio\" and try again..."; exit 0; }

    GRIDSITE=$2
    printf "using grid site (RSE): $GRIDSITE\n"
fi

# create sample list name
SAMPLE_LIST=$(basename ${SAMPLE_SCRIPT%.py}.txt)

# create the sample list and remove everything from the sample list except for the names of the samples
cp $SAMPLE_SCRIPT $PWD/../user/$SAMPLE_LIST
cd ../user/
sed -i -e '/txt/!d' -e 's/^[ \t]*//' -e '/^#/d' -e 's/[^\"]*\"//' -e 's/[\"].*//' -e 's/.txt//' $SAMPLE_LIST


# make and fill the addresses directory
mkdir -p ../user/addresses


if [[ $2 == /* ]] # files are local
then 
    printf "\n\nworking with local files...\n"    
    # Define an iterator
    Ctr=0

    while IFS="" read -r p || [ -n "$p" ]
    do
        # increment the counter
        Ctr=$((Ctr+1))

        # create the name of the `file list` (it will be named after the sample)
        FILE_LIST_NAME=$(sed "${Ctr}q;d" $SAMPLE_LIST)

        # create absolute path to the data
        DATA_PATH=$LOCATION/$p/

        # store the (local) paths of the downloaded data
        echo "Preparing $FILE_LIST_NAME.txt"
        ls $DATA_PATH > $PWD/../user/addresses/$FILE_LIST_NAME.txt

        # Prepend the absolute path prefix to each line
        sed -i "s@^@${DATA_PATH}@" $PWD/../user/addresses/$FILE_LIST_NAME.txt

        # check to see if the file has something in it, print warning if it doesn't
        [[ -s $PWD/../user/addresses/$FILE_LIST_NAME.txt ]] || echo "WARNING: This file is empty: $PWD/../user/addresses/$FILE_LIST_NAME.txt"
    
    done < $SAMPLE_LIST
    printf "\nDone.\n"


else # use files on the grid

    # store user directory location
    USER=$PWD/../user

    # define boolian for recording any rucio errors
    errors=false

    # loop over samples list and make lists of the root files for each sample
    while IFS="" read -r p || [ -n "$p" ]
    do
        echo "making list of addresses: addresses/$p.txt" | tee -a $USER/mkinputlists.log

        rucio list-file-replicas --rse $GRIDSITE --protocols root --pfns "$p" > $USER/addresses/$p.txt 2>&1 | tee -a $USER/mkinputlists.log

        # do a (not very robust!) check for rucio errors (by searching for the word 'error')
        if grep -i -q error $USER/addresses/$p.txt; then

	        echo "ERROR!!! Error found in file: $USER/addresses/$p.txt" | tee -a $USER/mkinputlists.log;
	        errors=true

	        # copy the entire file into the log
	        echo "---- BEGIN: contents of addresses/$p ----" >> $USER/mkinputlists.log
	        cat addresses/$p.txt >> $USER/mkinputlists.log
	        echo "---- END: contents of addresses/$p ----" >> $USER/mkinputlists.log

	        # save an example of a faulty file for final user output
	        example_string=$p
        fi

        # check to see if the file has something in it
        [[ -s $USER/addresses/$p.txt ]] || echo "WARNING: This file is empty: $USER/addresses/$p.txt"

    done < $SAMPLE_LIST

    # suggest a re-try if errors are found
    if [ "$errors" = true ] ; then
        echo "ERROR: found errors! Check mkinputlists.log"
        echo "ERROR: To manually try again for a single file do something like:"
        echo "ERROR:   rucio list-file-replicas --rse $2 --protocols root --pfns $example_string > $PWD/../user/addresses/$example_string"
    else
    
    #    for file in ../user/addresses/*;
    #    do
    #        # get rid of the string: root://eosatlas.cern.ch:1094/
    #        sed -i 's/root:\/\/eosatlas.cern.ch:1094\///g' $file
    #    done

    echo "Done!"
    fi
fi
