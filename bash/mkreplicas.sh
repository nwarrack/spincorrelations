#!/usr/bin/env bash


# abort (with help) if incorrect arguments are given.
[[ $# -eq 2 ]] || { echo >&2 "

 This is a script to make replicas of samples on a specified grid site
 
 usage: 
   ./mkreplicas.sh SAMPLES RSE

 where:
   SAMPLE = path/to/your/samplesfile.py
   RSE     = your desired grid-site

 EXAMPLE:
   ./mkreplicas.sh ../datalists/samples_AB212109_com_01.py UKI-SCOTGRID-GLASGOW-CEPH_LOCALGROUPDISK

 NOTES:
   1) To find an RSE, for example at the Glasgow site, you can do something like:
      rucio list-rses --expression 'tier=2' | grep \"GLASGOW\"

   2) To check if your replicas exist you can run checkreplicas.sh

   3) Re-requesting replicas again does not request MORE replicas, so it does no harm ;)";
    exit 1; }


# Check if rucio is setup
type rucio >/dev/null 2>&1 || { echo >&2 "Type: \"lsetup rucio\" and try again..."; exit 1; }

# abort if script not run in correct location
[[ -f mkreplicas.sh ]] || { echo >&2 "Execute this script from its location"; exit 1; }

SAMPLE_SCRIPT=$1

# check if the input datalist exists
[[ -f $SAMPLE_SCRIPT ]] || { printf >&2 "The file $SAMPLE_SCRIPT doesn't exist. Exiting\n"; exit 0; }

# create sample list name
SAMPLE_LIST=$(basename ${SAMPLE_SCRIPT%.py}.txt)

# create the sample list (just a copy of the python script)
cp $SAMPLE_SCRIPT $PWD/../user/$SAMPLE_LIST

# remove everything from the sample list except for the names of the samples
cd ../user/
sed -i -e '/txt/!d' -e 's/^[ \t]*//' -e '/^#/d' -e 's/[^\"]*\"//' -e 's/[\"].*//' -e 's/.txt//' $SAMPLE_LIST

RSE=$2

## Request replicas from rucio
# loop over samples list and request replicas for each sample
while IFS="" read -r p || [ -n "$p" ]
do

    #printf '%s\n' "$p"
    printf "rucio add-rule --lifetime 20000000 $p 1 $RSE\n"


    # a basic check to see that the container has files
    nfiles=$(rucio list-files --csv $p | wc -l)
    if [ "$nfiles" == 0 ]; then
	echo " WARNING: No files found!"
    else
        rucio add-rule --lifetime 20000000 $p 1 $RSE
    fi

    echo "" # for output readability

done < $SAMPLE_LIST


printf  "
To check if your replicas exist yet use the checkreplicas.sh script:
 
  ./checkreplicas.sh $INLIST $RSE

For more info type:

  ./checkreplicas.sh -h\n"
