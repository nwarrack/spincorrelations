you can run some tasks on the lxplus batch system by doing something like
```bash
./get_proxy.sh # requires grid certificate
condor_submit lxplus_condor.job
condor_q # shows job status
```
