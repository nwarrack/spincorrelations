#!/usr/bin/env bash

echo "starting condor job..."

# Set (machine-specific) location of spincorrelations directory and go there
SPINCORDIR="/afs/cern.ch/work/n/nwarrack/spincorrelations"
[[ -d $SPINCORDIR && cd $SPINCORDIR ]] || {
    echo "$SPINCORDIR does not exist!"
    echo "...that's going to be a problem."
    echo "...maybe check the SPINCORDIR definition in ${0}"
    echo "...exiting"
    exit 1
}

# Get proxy ('my_proxy_file' can be made with spincorrelations/batch/getproxy.sh)
echo "getting proxy..."
export X509_USER_PROXY=~/my_proxy_file

# Setup
echo "setting up environment..."
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
source setup.sh

# run the target 
python create_hists.py -$6  --period $1 --object $2 --channel $3 --wilsoncoef $4 --systematic $5 

# print some stuff
echo "finished condor job"
exit 0
