#! /bin/bash

new_proxy=false

# to force renewal of proxy, uncomment:
# new_proxy=true

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo "...setupATLAS done!"

lsetup rucio

export X509_USER_PROXY=~/my_proxy_file
time=`voms-proxy-info -timeleft`
timeH=$((time/3600))

echo "current time:"
date +'%r'
echo "Current proxy has $timeH hours remaining"


if (( timeH < 24 )); then
    echo 'Current proxy has less than 24hrs'
    echo 'Obtaining new one...'
    new_proxy=true
fi


if [[ "$new_proxy" = true ]]; then
    voms-proxy-init -voms atlas -valid 96:0 -out ~/my_proxy_file # maximum allowed validity
fi
