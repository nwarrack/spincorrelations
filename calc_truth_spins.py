#!/usr/bin/env python

# system imports
from os import path
import argparse

# 3rd party imports
from ROOT import TH1D, TVector3

# project imports
from histtools import sample_collector_signalmconly, data_lumi, save_histogram, file_collector, nevents_from_list, true_dilepton
from histconfig import histo_lists
from spin import boosted_objects_truth, calc_cos_thetas, calc_spin_obs

## Import the list of samples ##
#from datalists import samples_AB212109_com_01 as samplelist
#from datalists import samples_AB212109_com_01_justMC16e as samplelist
from datalists import samples_AB212109_com_01_justMC16e_testStripped as samplelist
#from datalists import samples_AB212109_com_01_singleMC16a_signal_sample as samplelist
#from datalists import samples_AB212109_com_01_MC16aSignalOnly_KINNW as samplelist
#from datalists import samples_AB212109_com_01_MC16eSignalOnly_KINNW as samplelist
#from datalists import samples_AT21284_KIN_01 as samplelist # <- an old AT version
#from datalists import samples_AT21258_KIN_01 as samplelist # <- an older AT version


# define required arguments and 'help' printout
parser = argparse.ArgumentParser(description='create root files of hists of spin sensitive observables at truth level only')
parser.add_argument('-p', '--period', help='PERIOD  = 16a/16d/16e', required=True)
parser.add_argument('-c', '--channel',help='CHANNEL = ee/emu/mumu', required=True)

# get arguments
args     = parser.parse_args()
period   = args.period
channel  = args.channel

def check_t_lep(t_lep):
    """
    This is designed to catch a bug related to the lep mass not being saved in
    truth tree. Leave it in for now...
    """
    if t_lep.X() == 0.0 and t_lep.Y() == 0 and t_lep.Z() == 0:
        print "ERROR: something is wrong!"


# Define a new file path
current_dir   = path.dirname(path.abspath(__file__))
new_file_path = path.join(current_dir, "user/")


# Get the input samples (names only)
sample_collection = sample_collector_signalmconly(period, samplelist)
root_files = []

# Define some lists, dicts, bools, etc.
z_axis = TVector3(0,0,1)
spin_obs = []
spin_hists = {}
quickexit = False

for hist in histo_lists["truspin"]:

    observable = hist[0]
    xaxis      = hist[1]
    bins       = hist[4]
    xmin       = hist[5]
    xmax       = hist[6]
    varname    = hist[7]

    # Make hist
    histname   = period + "_" + channel + "_" + varname + "_truth"
    h = TH1D(histname, xaxis, bins, xmin, xmax)

    # Make dict of histograms
    spin_hists[observable] = h

    # create list position to store calculated observable
    # on an event-by-event basis.
    spin_obs.append([observable,0.0])



for sample in sample_collection:

    file_list     = sample[0]
    x_sec         = sample[1]
    k_factor      = sample[2]
    sample_type   = sample[4]
    period        = sample[5]

    sf = data_lumi[period]/(nevents_from_list(file_list)/(x_sec*k_factor))

    # Get the data and mc
    root_files[:] = file_collector(open(file_list, mode='r'))

    #print root_files
    #root_files.append(TFile.Open("/path/to/input/file.root","READ")) #<-options to do ONLY ONE file

    for f in root_files:

        if quickexit == True: continue

        print "NEW FILE..."
        # Loop over events
        evntCtr        = 0
        skippedEvntCtr = 0



        for event in f.truth:

            if quickexit == True: continue
            if evntCtr == 99: # debuging 'exit'
                quickexit = True

            evntCtr += 1
            if evntCtr % 10000 == 0:
                print "INFO:", evntCtr, "events processed"

            # skip event with strange truth info
            if event.d_t_afterFSR_m == -999:
                skippedEvntCtr += 1
                continue
            if event.d_tbar_afterFSR_m == -999:
                skippedEvntCtr += 1
                continue
            if event.d_l_m == -999:
                skippedEvntCtr += 1
                continue

            # Define dilepton (these are the prompt leptons!)
            if not true_dilepton(event, "emu", False): # true turns on print out info
                continue

            # Define weight
            weight = event.weight_mc*event.weight_pileup

            # Calculate cos thetas in position 'a' (top) and
            # 'b' (anti-top) using boosted objects
            b_t, b_tbar, b_t_lep, b_tbar_lep = boosted_objects_truth(event)
            check_t_lep(b_t_lep)
            cta, ctb = calc_cos_thetas(b_t, b_tbar, b_t_lep, b_tbar_lep, z_axis)

            # calc spin observables from cos thetas and fill hists
            for obs in spin_obs:
                spin_hists[obs[0]].Fill(calc_spin_obs(obs[0], cta, ctb), weight)


        # end event loop
        print "events :", evntCtr
        print "skipped:", skippedEvntCtr
    # end file loop

    # Scale and save hists
    for obs in spin_obs:
        spin_hists[obs[0]].Scale(sf)
        save_histogram(spin_hists[obs[0]],channel,obs[0],"truth",period)
