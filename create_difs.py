#!/usr/bin/env python
import os
channel = ["ee","emu","mumu"]

var = [
    # correlations
    "ckk","cnn","crr",
    "crk","ckr",
    "cnr","crn",
    "cnk","ckn",
    # polarizations
    "bkminus", "bkplus",
    "brminus", "brplus",
    "bnminus", "bnplus",
    # cross-correlations
    "crkmkr","crkpkr",
    "cnrmrn","cnrprn",
    "cnkmkn","cnkpkn",
]


for c in channel:
    for v in var:
        print c, v
        os.system('./create_migration_plot_2.py -c ' + c + ' -v ' + v)
