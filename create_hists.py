#!/usr/bin/env python

"""
This script creates differential distributions for observables. The
histogram plots for the physics objects are defined (bins, ranges,
etc) in plotting/bin/objects/. To choose which systematic trees and
which channels to plot edit this file. You can only plot one period
at a time (MC16a, MC16d or MC16e)
"""

import os
import sys
import argparse

# Exit if setup has not been run
if "SPINHOME" not in os.environ:
    print "Environment variable \'SPINHOME\' doesn't exist. "
    print "You need to source setup.sh to create it! Aborting."
    exit()

# define required arguments and 'help' printout
parser = argparse.ArgumentParser(description='create root files of hists - one file per observable')
parser.add_argument('-p', '--period', help='PERIOD = 16a/16d/16e', required=True)
parser.add_argument('-o', '--object', nargs='+', help='OBJECT = lep/top/spin/met (space-separated list accepted!)', required=True)
parser.add_argument('-c', '--channel', nargs='+', help='CHANNEL = ee/emu/mumu (space-separated list accepted!)', required=True)
parser.add_argument('-s', '--systematic', nargs='+', help='SYSTEMATIC = nominal/blah', required=True)
parser.add_argument('-w', '--wilsoncoef', type=int, help='EFTWC = 0/117/etc.. (only one! use 666 for non-EFT)', required=True)
parser.add_argument('-t', '--truth', default=False, action='store_true')
parser.add_argument('-i', '--ignorebackgrounds', default=False, action='store_true')
parser.add_argument('-d', '--debugmode', default=False, action='store_true')
parser.add_argument('-l', '--log', default=False, action='store_true')

# get arguments
args = parser.parse_args()
period  = args.period
objects = args.object
channels = args.channel
wc = args.wilsoncoef
save_tr = args.truth
ig_bckg = args.ignorebackgrounds
info    = args.log
debug   = args.debugmode
systematics = args.systematic


if info:
    from datetime import datetime
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print "\"", ' '.join(str(i) for i in sys.argv), "\" executed at", current_time 
    print "save truth-level stuff :", save_tr
    print "ignore backgrounds     :", ig_bckg
    print "print info             :", info
    print "debug mode             :", debug
    print "systematic(s)          :", systematics
    print "object(s)              :", objects
    print "channel(s)             :", channels
    print "wilson coeff           :", wc
    print "MC16 period(s)         :", period


# Check the dirtectory exists where hists will be saved
if os.path.exists("./user/Hists"):
    savedir = "./user/Hists/" + period + "/"
else:
    print "./user/Hists does not exist. Source setup.sh to create it!"
    exit()

# third party imports
from ROOT import gROOT, TH1, TH1D, TStopwatch

if info:
    # start a timer
    timer = TStopwatch()
    timer.Start()

# project imports:
from histtools import hist_collection, sample_collector, sample_collector_signalmconly, sample_collector_eftonly, sample_collector_dataandsignalonly, compile_histograms, sum_histos, save_histograms
from debug import print_yields, print_overflow_yields
from histconfig import histo_lists


if debug:
    print "\n________ settings from create_hists.py __________"
    print "1) Print info to terminal:"
    print "   info =", info
    print "1) Debug mode:"
    print "   debug =", debug
    print "2) Save truth or particle level info for ttbar:"
    print "   save_tr =", save_tr
    print "3) Ignore backgrounds (and data):"
    print "   ig_bckg =", ig_bckg
    print "4) Systematic trees which will be looped over:"
    print "   systematics =", systematics
    print "5) Wilson Coeff:"
    if wc == 666:
        print "   wc =", wc, "(i.e. ignoring EFT stuff)"
    else:
        print "   wc =", wc
        print "___________________________________________________\n"




# Set some root functionality
gROOT.SetStyle("ATLAS")
gROOT.ForceStyle()
TH1.SetDefaultSumw2()
gROOT.SetBatch(True)

## Import list of samples with their cross sections, k-factors, etc., etc....
#from datalists import samples_AB212179_com_03 as samplelist  # rnd3 (multiple ttbar recos)
#from datalists import samples_AB212197_com_04 as samplelist # rnd4 (one successful ttbar reco); use this to test eft stuff event though it could be faulty (round 5 fixed the fault)
#from datalists import samples_AB212197_com_05 as samplelist # rnd5 (one successful ttbar reco); does not include EFT yet!
from datalists import samples_AB212197_com_05_16e_localEFT as samplelist

# EFT handling
if wc == 666: # wc=666 is a hacky way to choose EFT/non-EFT hist creation from command line stuff)
    if ig_bckg:
        sample_collection = sample_collector_signalmconly(period, samplelist)
    else:
        sample_collection = sample_collector(period, samplelist)
else:
    if ig_bckg:
        sample_collection = sample_collector_eftonly(period, samplelist)
    else:
        print "don't know how to include EFT signal and backgrounds - aborting"
        exit

# Create list for 'hist_collection' objects
hists = []

# Make list of class 'hist', where each entry stores everything required for one data/mc comparison plot.
for systematic in systematics:
    for obj in objects:
        for hist_config in histo_lists[obj]:
            for channel in channels:
                # make the class object 'h'
                h = hist_collection(channel, period, systematic, hist_config, wc)

                # add hists that are not automatically added
                h.add_sshist()    # records same sign events
                h.add_truthhist() # records ttbar truth (no cuts, mc and pileup weights only)
                h.add_NNLOhist()  # records NNLO rescaled ttbar values
                h.add_migration() # records truth-reco migrations
                h.add_ttbarrecowhenpl() # records the reco quantities when event exists at particle-level
                h.add_ttbarrecowhentr() # records the reco quantities when event exists at truth-level
                hists.append(h)

    # check there are no resolution hists required without truth!
    for h in hists:
        if "resolution" in h.variable and not save_tr:
            print "ERROR: You asked for some 'resolution' histograms, but did not set 'save_tr' in the user config section of 'create_hists.py'.\nSee create_hists.py or your 'histconfig' directory."
            exit()

    # print some info to terminal
    if info: print "Compiling", len(hists), "sets of histograms:", len(hists)/len(channels), "observable(s) in", len(channels), "channel(s)"

    # The main function to perform loop over samples
    compile_histograms(wc, hists, sample_collection, period, systematic, save_tr, info, debug)

    # print some yields info
    if info:
        hist_tot = TH1D()
        print_yields(hists[0], sum_histos(hists[0].master, hist_tot))
        print_overflow_yields(hists[0].master)


    # Save all the histograms to (per-variable) root files
    for hist in hists:
        save_eft = False
        if wc == 666:
            print "Saving:", savedir + hist.master["ttbar"].GetName().replace(period + "_", "", 1) + ".root"
        else:
            save_eft = True
            print "Saving:", savedir + hist.master["EFT"].GetName().replace(period + "_", "", 1) + ".root"
        #print "Mean (ttbar)     :", round( hist.master["ttbar"].GetMean(), 4)
        #print " RMS  (ttbar)     :", round( hist.master["ttbar"].GetRMS() , 4)

        save_histograms(save_eft,
                        hist.master,
                        hist.channel,
                        hist.varname,
                        hist.systematic,
                        hist.period)

        ## Migration matrix examination
        # mig_proj_x = TH1D("migprojx", "x-axis-name", hist.bins, hist.xmin, hist.xmax )
        # mig_proj_y = TH1D("migprojx", "x-axis-name", hist.bins, hist.xmin, hist.xmax )
        # mig_proj_x = hist.master["migration"].ProjectionX()
        # mig_proj_y = hist.master["migration"].ProjectionY()
        # for i in range(hist.bins):
        #     print "bin-x-proj", i+1, ":", mig_proj_x.GetBinContent(i+1)
        # for i in range(hist.bins):
        #     print "bin-y-proj", i+1, ":", mig_proj_y.GetBinContent(i+1)
        gROOT.cd()

# report on the timer
if info:
    timer.Stop()
    print "Loop over samples - DONE!" 
    print "Time (real) :", round( timer.RealTime()/60, 2), "m"
    print "Time (CPU)  :", round( timer.CpuTime()/60, 2) , "m"

print "\nDone!\n\nTo make pdf plots use create_plots.py\n"
