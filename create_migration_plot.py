#!/usr/bin/env python3

"""
Python3 code which depends on uproot4 and numpy.
Uproot is a package that uses numpy arrays to
work with root files - it DOES NOT depend on
ROOT! This code plots migration matrices for
ttbar observables. It takes as it's input a 2D
root histogram.
"""

import matplotlib.pyplot as plt
import numpy as np
import uproot
import argparse

# define input arguments
parser = argparse.ArgumentParser()
parser.add_argument('-r', '--reco', help='RECO = NW/KIN', required=True)
parser.add_argument('-w', '--wc', help='WC = 0/119', required=True)
parser.add_argument('-v', '--variable', help='variable = ckk/bkplus/etc..', required=True)

args = parser.parse_args()
reco = args.reco
wc   = args.wc
var  = args.variable

# project imports:
from plot_py3.heatmap import heatmap, heatmap_annotate


# choose your file:

channel = "emu"
period = "16e"
WC = 'WC' + wc

rootfile_path = './user/Hists_mc16e_eft04_' + WC + '/' + period + '/'

rootfile_name = channel + '_' + reco + "_" + var + '_nominal.root'

print("opening:", rootfile_path + rootfile_name)
migmatrix = uproot.open(rootfile_path + rootfile_name)["migration"]

# choose your 'save as' name
uniqueName = var + "_" + channel + "_" + reco + "_" + WC
print("save as: ", uniqueName)

# get the truth hist
ttbar_truth = uproot.open(rootfile_path + rootfile_name)["ttbar_truth"]
#ttbar_rwhenpl = uproot.open(rootfile_path + rootfile_name)["ttbar_rwhenpl"]
#ttbar_rwhenpl = uproot.open(rootfile_path + rootfile_name)["ttbar_truth"]


mig_np = migmatrix.to_numpy()
ttb_np = ttbar_truth.to_numpy()
print("ttbar truth (AKA pl)", ttb_np[0])
edges = ttb_np[1]
#print(edges)
# Get values and bin edges
vals = mig_np[0]
#print(vals)
size = len(edges) - 1
#print("size:", size)

# Normalize to truth columns
ones = np.ones(size, np.float)

#tots = np.matmul(vals,ones.T) # <- row-normalized
tots = np.matmul(ones,vals) # <- column-normalized
print("tots:", tots)
#tots = [1,1,1,1,1,1,1,1,1,1]

# Make an array of repeated 'tots' arrays, for easy iteration
tots_matrix = np.array([tots,]*size, np.float) # <- column-normalized
#tots_matrix = np.array([tots,]*size, np.float).transpose() #<- row-normalized

# Make empty array to hold normalized values
normed_vals = np.zeros((size, size), np.float)


with np.nditer([normed_vals, vals, tots_matrix], op_flags=['readwrite'], order='C') as it:
    for nv,v,t in it:
        #print("normed val:",nv)
        #print("val       :",v)
        #print("totes_mat :",t)
        nv[...] = v/t


# # Make sure normalization worked
# unitytest = np.matmul(ones, normed_vals)
# print(unitytest)

# # Save migrations array as file (see NOTES below to re-use it)
# import pickle
# with open('user/Mig_' + uniqueName, 'wb') as fp:
#     pickle.dump(normed_vals, fp)

# # Show image
# plt.figure(figsize=(10, 16))
# plt.imshow(normed_vals)
# plt.show()


# Get a list of bin edge values
binvals = []
for i in range(size):
#    binvals.append(round(ttbar_truth.edges[i],2)) # why round()??
    binvals.append(round(edges[i],2))
#print(binvals)
# Plot the heatmap
#fig, ax = plt.subplots(figsize=(18, 14))
fig, ax = plt.subplots(figsize=(12, 10))
#im, cbar = heatmap(normed_vals, reversed(binvals), binvals, ax=ax, cmap="YlGn", cbarlabel="probability")
im, cbar = heatmap(np.fliplr(normed_vals), reversed(binvals), binvals, ax=ax, cmap="YlGn", cbarlabel="probability of truth-level ttbar event in reconstruction bin range")

texts = heatmap_annotate(im, threshold = 1, fontsize=19)
if reco == "NW":
    titlestring = var + ', ' + channel + ' channel; MC' + period + '; reco: NW; WC:' + wc
    if wc == "0":
        titlestring = var + ', ' + channel + ' channel; MC' + period + '; reco: NW ; Standard Model'
elif reco == "KIN":
    titlestring = var + ', ' + channel + ' channel, MC' + period + ', reco: KIN'
plt.title(titlestring, fontsize = 18)
plt.xlabel('Truth (value of bin\'s lower edge)', fontsize = 15)
plt.ylabel(reco +' reco', fontsize = 15)

plt.plot(ax.get_xlim(), ax.get_ylim(), ls=":", c="darkgrey")
#plt.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")
#plt.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c="yellowgreen")
#plt.plot(ax.get_ylim(), ax.get_xlim(), ls=":", c="darkgrey")

fig.tight_layout()


# Save the image
plt.savefig('./user/mig_plots/' + uniqueName + '.png')


# Show image
#plt.show()
