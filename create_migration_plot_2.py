#!/usr/bin/env python3

"""
Python3 code which depends on uproot and numpy.
Uproot is a package that uses numpy arrays to to work with root files - it DOES NOT depend on ROOT!
This code plots migration matrices for ttbar observables. It takes as it's input a 2D root histogram.
"""

import matplotlib.pyplot as plt
import numpy as np
import pickle
import uproot
import argparse

# define required arguments and 'help' printout
parser = argparse.ArgumentParser(description='create difference of two migration matrices in terms of probability')
parser.add_argument('-v', '--variable', help='VARIABLE = e.g: ckk', required=True)
parser.add_argument('-c', '--channel',  help='CHANNEL  = ee/emu/mumu', required=True)
# get arguments
args = parser.parse_args()
channel = args.channel
var     = args.variable

# project imports:
from plot_py3.heatmap import heatmap, heatmap_annotate

path = 'user/Hists/16a/'

rootfile_name_NW  = channel + '_NW_'  + var+ '_nominal.root'
rootfile_name_KIN = channel + '_KIN_' + var+ '_nominal.root'

KINhists = uproot.open(path + rootfile_name_KIN)
NWhists  = uproot.open(path + rootfile_name_NW)

uniqueName = var + '_' + channel + '_KIN_vs_NW'

# for getting bin values and defining a "score" for the matrix R:
# score = sum over j from 1 to N of (R_{jj}*W_i)
# where N is the number of bins and W=N*(x_i)/(sum over i from 1 to N of x_i)
# where x are the truth value in those bins

ttbar_truth = NWhists['ttbar_truth'] # note: we're taking 'truth' from KIN (could be different from NW)!
truth_vals = ttbar_truth.values
normed_truth_vals = [i*len(truth_vals)/np.sum(truth_vals) for i in truth_vals]

size = len(KINhists['migration'].values)

# print(hists.keys())
# print(hists['migration'].numpy())
# print(ttbar_truth.title)
# print(ttbar_truth.values)
# print(ttbar_truth.edges)
# print(ttbar_truth.hepdata())

def make_matrix(hists, size):

    # Get values and bin edges
    vals = hists['migration'].values
    bins = hists['migration'].edges

    # Normalize to truth columns
    ones = np.ones(size, np.float)
    #tots = np.matmul(vals,ones.T) # <- row-normalized
    tots = np.matmul(ones,vals) # <- column-normalized
    
    # Make an array of repeated 'tots' arrays, for easy iteration
    tots_matrix = np.array([tots,]*size, np.float) # <- column-normalized
    #tots_matrix = np.array([tots,]*size, np.float).transpose() #<- row-normalized
    
    # Make empty array to hold normalized values
    normed_vals = np.zeros((size, size), np.float)

    with np.nditer([normed_vals, vals, tots_matrix], op_flags=['readwrite'], order='C') as it:
        for nv,v,t in it:
            nv[...] = v/t

    return normed_vals


# # Make sure normalization worked
# unitytest = np.matmul(ones, normed_vals)
# print(unitytest)

normed_KIN = make_matrix(KINhists, size)
normed_NW  = make_matrix(NWhists, size)

normed_dif_x100 = 100*(normed_KIN - normed_NW)
normed_dif_x100_rounded = round(np.trace(normed_dif_x100),3)
print('  |__>    percent improvement: ', normed_dif_x100_rounded)
#print('sum of diagonal elements :', round(np.trace(normed_dif_x100),3))

score = 0
for i in range(len(normed_truth_vals)):
    score += normed_dif_x100[i,i]*normed_truth_vals[i]*len(normed_truth_vals)

score = round(score, 3)
print('  |__>    score: ', score)


# Save migrations array as file (see NOTES below to re-use it)
#with open('user/Comp_mig_' + uniqueName, 'wb') as fp:
#     pickle.dump(normed_dif_x100, fp)

#plt.figure(figsize=(10, 16))
#plt.imshow(normed_dif_x100)
#plt.show()


# Get a list of bin edge values
binvals = []
for i in range(size):
    binvals.append(round(ttbar_truth.edges[i],2))




# Plot the heatmap
#fig, ax = plt.subplots(figsize=(18, 14))
fig, ax = plt.subplots(figsize=(12, 10))
#im, cbar = heatmap(normed_dif_x100, reversed(binvals), binvals, ax=ax, cmap="YlGn", cbarlabel="probability")
#im, cbar = heatmap(np.fliplr(normed_dif_x100), reversed(binvals), binvals, ax=ax, cmap="rainbow", cbarlabel="probability of truth-level ttbar event in reconstruction bin range")
cbar_kwargs_prob = {}
cbar_kwargs_perc = {}

cbar_kwargs_prob["ticks"] = [-0.05,-0.04,-0.03,-0.02,-0.01, 0.0,0.01,0.02,0.03,0.04,0.05]
cbar_kwargs_perc["ticks"] = [i*100 for i in cbar_kwargs_prob["ticks"]]

#im, cbar = heatmap(np.fliplr(normed_dif_x100), reversed(binvals), binvals, ax=ax, cmap="RdYlGn", cbar_kw=cbar_kwargs, cbarlabel='P(weight > 0.' + dif2 + ')-P(weight > 0.'+dif1+')')
#im, cbar = heatmap(np.fliplr(normed_dif_x100), reversed(binvals), binvals, ax=ax, cmap="RdYlGn", cbar_kw=cbar_kwargs_perc, vmin=-5, vmax=5,cbarlabel='P(weight > 0.' + dif2 + ')-P(weight > 0.'+dif1+')')
im, cbar = heatmap(np.fliplr(normed_dif_x100), reversed(binvals), binvals, ax=ax, cmap="RdYlGn", cbar_kw=cbar_kwargs_perc, vmin=-5, vmax=5,cbarlabel='Percentage change')

#texts = heatmap_annotate(im, threshold = 1)
texts = heatmap_annotate(im, threshold=5, fontsize=19) # threshold = where black text turns to white text

title_top = 'From NW to KIN (' + var + ' ' + channel + ' ' + 'MC16a)'
#title_bottom = '(Diag. sum: ' + str(normed_dif_x100_rounded) + '%. Weighted diag sum: ' + str(score) + '%\')'
title_bottom = '(Diag. sum: ' + str(normed_dif_x100_rounded) + '%)'
plt.title(title_top + '\n' + title_bottom, fontsize = 18)
plt.xlabel('Truth (value of bin\'s lower edge)', fontsize = 15)
plt.ylabel('Reconstruction', fontsize = 15)

plt.plot(ax.get_xlim(), ax.get_ylim(), ls=":", c="darkgrey")
#plt.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")
#plt.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c="yellowgreen")
#plt.plot(ax.get_ylim(), ax.get_xlim(), ls=":", c="darkgrey")

fig.tight_layout()


# Save the image
plt.savefig('./user/mig_plots/dif_' + uniqueName + '.png')


# Show image
#plt.show()
