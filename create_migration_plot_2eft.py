#!/usr/bin/env python3

"""
Python3 code which depends on uproot and numpy.
Uproot is a package that uses numpy arrays to to work with root files - it DOES NOT depend on ROOT!
This code plots migration matrices for ttbar observables. It takes as it's input a 2D root histogram.
"""

import matplotlib.pyplot as plt
import numpy as np
import pickle
import uproot
import argparse
from math import sqrt

# define required arguments and 'help' printout
parser = argparse.ArgumentParser(description='create difference of two migration matrices in terms of probability')
parser.add_argument('-p', '--period',   help='PERIOD   = 16a/16d/16e', required=True)
parser.add_argument('-v', '--variable', help='VARIABLE = e.g: ckk',    required=True)
parser.add_argument('-c', '--channel',  help='CHANNEL  = ee/emu/mumu', required=True)
parser.add_argument('-w', '--wci',      help='WCINDEX  = 0/117/etc..', required=True)
parser.add_argument('-e', '--errors',   help='ERRORS   = True/False',  required=True)


# get arguments
args = parser.parse_args()
period  = args.period
channel = args.channel
var     = args.variable
wci     = args.wci
printer = args.errors

# project imports:
from plot_py3.heatmap import heatmap, heatmap_annotate


def make_matrix(hists, size):

    # Get values and bin edges
    migmatrix = hists["migration"].to_numpy()
    errors    = hists["migration"].errors()
    vals      = migmatrix[0]
    bins      = migmatrix[1]

    #print("vals:", vals)
    #print(bins)

    # Normalize to truth columns
    ones = np.ones(size, np.float)
    #tots = np.matmul(vals,ones.T) # <- row-normalized
    tots = np.matmul(ones,vals) # <- column-normalized
    
    # Make an array of repeated 'tots' arrays, for easy iteration
    tots_matrix = np.array([tots,]*size, np.float) # <- column-normalized
    #tots_matrix = np.array([tots,]*size, np.float).transpose() #<- row-normalized
    
    # Make empty array to hold normalized values
    normed_vals   = np.zeros((size, size), np.float)
    normed_errors = np.zeros((size, size), np.float)
    with np.nditer([normed_vals, vals, tots_matrix], op_flags=['readwrite'], order='C') as it:
        for nv,v,t in it:
            #print("nv:", nv)
            #print("v :", v)
            #print("t :", t)
            nv[...] = v/t

    with np.nditer([normed_errors, errors, tots_matrix], op_flags=['readwrite'], order='C') as it:
        for nv,v,t in it:
            nv[...] = v/t

    return normed_vals, normed_errors


# get the path to the root files (files of hists)
path = 'user/Hists/' + period + '/'

# get the file names with different recos
NW_EFT = channel + '_NW_'  + var + '_nominal_' + wci + '.root'
SN_EFT = channel + '_SN_'  + var + '_nominal_' + wci + '.root'
EM_EFT = channel + '_EM_'  + var + '_nominal_' + wci + '.root'

NW_SM  = channel + '_NW_'  + var + '_nominal_0.root'
SN_SM  = channel + '_SN_'  + var + '_nominal_0.root'
EM_SM  = channel + '_EM_'  + var + '_nominal_0.root'

SM_hists = []
SM_hists.append(uproot.open( path + NW_SM ))
SM_hists.append(uproot.open( path + SN_SM ))
SM_hists.append(uproot.open( path + EM_SM ))

EFT_hists = []
EFT_hists.append(uproot.open( path + NW_EFT ))
EFT_hists.append(uproot.open( path + SN_EFT ))
EFT_hists.append(uproot.open( path + EM_EFT ))


for i in range(len(SM_hists)):

    uniqueName = var + '_' + channel + '_SMVsEFTWC' + WC

    # for getting bin values and defining a "score" for the matrix R:
    # score = sum over j from 1 to N of (R_{jj}*W_i)
    # where N is the number of bins and W=N*(x_i)/(sum over i from 1 to N of x_i)
    # where x are the truth value in those bins

    ttbar_truth = SM_hists[i]['ttbar_truth']
    truth_vals  = ttbar_truth.values()
    print("truth vals:", truth_values)

    for hist in SM_hists[i].keys():

        print(hist, ":", SM_hists[i][hist].values())

    normed_truth_vals = [j*len(truth_vals)/np.sum(truth_vals) for j in truth_vals]

    size = len(SM_hists[i]['migration'].values())
    print("size:", size)
    print(SM_hists[i].keys())
    print(SM_hists[i]['migration'].to_numpy())




    # # Make sure normalization worked
    ones = np.ones(size, np.float)
    unitytest = np.matmul(ones, normed_vals)
    print(unitytest)

    exit()

    normed_SM, errors_SM   = make_matrix(SM_hists,  size)
    normed_EFT, errors_EFT = make_matrix(EFT_hists, size)

    #normed_dif_x100 = 100*(normed_SM - normed_EFT)
    normed_dif_x100 = 100*(normed_EFT - normed_SM)

    #print("errors_SM", errors_SM)
    errors = np.ones((size, size), np.float)
    errors = errors*100
    for i in range(len(errors_SM)):
        for j in range(len(errors_SM)):
            errors[i][j] = errors[i][j]*sqrt(errors_SM[i][j]*errors_SM[i][j] + errors_EFT[i][j]*errors_EFT[i][j])
    #print("errors:", errors)
        
    
    #normed_errors_x100 = 100*(sqrt(errors_EFT^2 + errors_SM^2))
    #print("normed errors", normed_errors_x100)

    normed_dif_x100_rounded = round(np.trace(normed_dif_x100),3)
    #print('  |__>    percent improvement: ', normed_dif_x100_rounded)
    #print('sum of diagonal elements :', round(np.trace(normed_dif_x100),3))

    high_val = 0
    for row in normed_dif_x100:
        for i in range(len(row)):
            if abs(row[i]) > high_val:
                high_val = abs(row[i])
    #print("high val:", round(high_val, 0))
    high_val = round(high_val, 0)

    if high_val < 0.05: hih_val = 0.05

    score = 0
    for i in range(len(normed_truth_vals)):
        score += normed_dif_x100[i,i]*normed_truth_vals[i]*len(normed_truth_vals)

    score = round(score, 3)
    #print('  |__>    score: ', score)


    # Save migrations array as file (see NOTES below to re-use it)
    with open('user/Comp_mig_' + uniqueName, 'wb') as fp:
        pickle.dump(normed_dif_x100, fp)

    #plt.figure(figsize=(10, 16))
    #plt.imshow(normed_dif_x100)
    #plt.show()


    # Get a list of bin edge values
    binvals = []
    ttbar_truth_forEdges = ttbar_truth.to_numpy()
    for i in range(size):
        binvals.append(round(ttbar_truth_forEdges[1][i],2))
    print("binvals:", binvals)



# Plot the heatmap
#fig, ax = plt.subplots(figsize=(18, 14))
fig, ax = plt.subplots(figsize=(12, 10))
#im, cbar = heatmap(normed_dif_x100, reversed(binvals), binvals, ax=ax, cmap="YlGn", cbarlabel="probability")
#im, cbar = heatmap(np.fliplr(normed_dif_x100), reversed(binvals), binvals, ax=ax, cmap="rainbow", cbarlabel="probability of truth-level ttbar event in reconstruction bin range")
cbar_kwargs_prob = {}
cbar_kwargs_perc = {}

cr = -1*high_val/100 # colour range in percent (min)
cr_max = high_val/100
inc = (cr_max - cr)/10 # increment 

#cbar_kwargs_prob["ticks"] = [-0.05,-0.04,-0.03,-0.02,-0.01, 0.0,0.01,0.02,0.03,0.04,0.05]
cbar_kwargs_prob["ticks"] = [cr+inc*0,cr+inc*1,cr+inc*2,cr+inc*3,cr+inc*4,cr+inc*5,cr+inc*6,cr+inc*7,cr+inc*8,cr+inc*9,cr+inc*10]
cbar_kwargs_perc["ticks"] = [i*100 for i in cbar_kwargs_prob["ticks"]]

#im, cbar = heatmap(np.fliplr(normed_dif_x100), reversed(binvals), binvals, ax=ax, cmap="RdYlGn", cbar_kw=cbar_kwargs, cbarlabel='P(weight > 0.' + dif2 + ')-P(weight > 0.'+dif1+')')
#im, cbar = heatmap(np.fliplr(normed_dif_x100), reversed(binvals), binvals, ax=ax, cmap="RdYlGn", cbar_kw=cbar_kwargs_perc, vmin=-5, vmax=5,cbarlabel='P(weight > 0.' + dif2 + ')-P(weight > 0.'+dif1+')')
#im, cbar = heatmap(np.fliplr(normed_dif_x100), reversed(binvals), binvals, ax=ax, cmap="RdYlGn", cbar_kw=cbar_kwargs_perc, vmin=-5, vmax=5,cbarlabel='Percentage points change')
im, cbar = heatmap(np.fliplr(normed_dif_x100), reversed(binvals), binvals, ax=ax, cmap="RdYlGn", cbar_kw=cbar_kwargs_perc, vmin=cr*100, vmax=cr_max*100,cbarlabel='Percentage points change')
stringbinvals = np.array2string(np.fliplr(normed_dif_x100))
stringerrors = np.array2string(errors)
# for i in range(len(stringerrors)):
#     for j in range(len(stringerrors)):
#         print(stringerrors[i][j])
#cellstrings = stringbinvals + "(" + stringerrors + ")"
#print("cells:", cellstrings)
#print("stringerrors:", stringerrors)
#texts = heatmap_annotate(im, threshold = 1)
if printer == "True":
    texts = heatmap_annotate(im, threshold=5000, fontsize=19, data=errors) # threshold = where black text turns to white text
    
    title_top = 'Errors from SM to WC:' + WC + ' (' + var + ' ' + channel + ' ' + 'MC16e)'
else:
    texts = heatmap_annotate(im, threshold=cr_max*50, fontsize=19)
    title_top = 'From SM to WC:' + WC + ' (' + var + ' ' + channel + ' ' + 'MC16e)'
#title_bottom = '(Diag. sum: ' + str(normed_dif_x100_rounded) + '%. Weighted diag sum: ' + str(score) + '%\')'
#title_bottom = '(Diag. sum: ' + str(normed_dif_x100_rounded) + '%)'
#title_bottom = '(117=ctgi_m5p0, 118=ctgi_p5p0, 236=ctg_m0p8, 237=ctg_p0p8)'
#plt.title(title_top + '\n' + title_bottom, fontsize = 18)
plt.title(title_top, fontsize = 18)
plt.xlabel('Truth (value of bin\'s lower edge)', fontsize = 15)
plt.ylabel('Reconstruction', fontsize = 15)

plt.plot(ax.get_xlim(), ax.get_ylim(), ls=":", c="darkgrey")
#plt.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")
#plt.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c="yellowgreen")
#plt.plot(ax.get_ylim(), ax.get_xlim(), ls=":", c="darkgrey")

fig.tight_layout()


# Save the image
if printer == "True":
    plt.savefig('./user/mig_plots/dif_' + uniqueName + '_err.png')
    print("made: ./user/mig_plots/dif_" + uniqueName + "_err.png")
else:
    plt.savefig('./user/mig_plots/dif_' + uniqueName + '.png')
    print("made: ./user/mig_plots/dif_" + uniqueName + ".png")


# Show image
#plt.show()
