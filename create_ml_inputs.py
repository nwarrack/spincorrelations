#!/usr/bin/env python

"""
This script produces a root file with a "features" tree. Thie tree
containsthe truth level spin observables in their own branches. It
will also save other variables in the same "features" tree. This
code was written for use with machine learning algorithms.
Specifically these ones:
https://gitlab.com/neilwarrack/spinformation
"""

## System imports
import sys
import math
import re
import argparse
import time
from os import path, mkdir




## Check arguments
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--period'    , help='PERIOD = 16a/16d/16e', required=True)
parser.add_argument('-s', '--systematic', help='SYSTEMATIC = treename', required=True)
args       = parser.parse_args()
systematic = args.systematic
period     = args.period

## 3rd party imports
from ROOT import gROOT, TFile, TVector3, TChain, TTree, TLorentzVector

## Project imports
from histtools import sample_collector_signalmconly, file_collector, file_chainer, true_dilepton
from ml import create_branch, create_ptEtaPhi_branches
from spin import make_lorentzvector_mass, get_PtEtaPhiM
from spin import boosted_objects, boosted_objects_truth, boosted_objects
from spin import cos_theta, calc_spin_obs, calc_cos_thetas
from debug import print_lepton_errors
from histconfig import histo_lists

spin_observables = ["c_nn", "c_rr", "c_kk", "c_rk", "c_kr", "c_nr", "c_rn", "c_nk", "c_kn", "c_nk_p_kn","c_nr_p_rn","c_rk_p_kr","c_nk_m_kn","c_nr_m_rn","c_rk_m_kr","b_kplus","b_nplus","b_rplus","b_kminus","b_nminus","b_rminus"]

# Directly import your samplelist which is probably something like:
# spincorrelations/datalists/samples_AB212197_com_05.py
#
#s = samples_AB212109_com_01
#s = samples_AB212169_com_02_mc16e_justSignalandEFT
#s = samples_AB212179_com_03_mc16e
#s = samples_AB212197_com_05_16eNominalSignalOnly
#s = "samples_AB212197_com_05_16eNominalSignalOnly"
s = "samples_AB212179_com_03_16eNomSigOnly"


def is_safe(s):
    #test if s is a valid argument
    return True or False
if is_safe(s):
    exec('from datalists import %s as samplelist'%s)


debug = True # set True for some to-screen printouts
stopshort = False # set True to to stop event processing short (at event No. = stopN)
stopN = 100

if debug:
    print " --- Debug mode ---", debug
if stopshort:
    print "INFO: event loop will stop after", stopN, "events."


# make new output file in 'user/'
current_dir = path.dirname(path.abspath(__file__))
#new_file_path = path.join(current_dir, "user/ml_inputs/samples_AB212179_com_03_mc16e/test_01/")
#new_file_path = path.join(current_dir, "../spinformation/data/KINNW_MC16e_nomSignal_mlpreped_temp/")
mldir = path.join(current_dir + "/user/ml_inputs")
outputdir = mldir


# get the input signal MC
sample_collection = sample_collector_signalmconly(period, samplelist)
if debug: print "DEBUG: number of samples:", len(sample_collection)

input_files = []

sampleCtr = 0
reco_codeCtr = 0
for sample in sample_collection:

    sampleCtr      += 1
    input_files[:] = []
    filelistname   = sample[0]
    x_sec          = sample[1]
    k_factor       = sample[2]
    sample_type    = sample[4]
    period         = sample[5]

    # get the input_files list
    input_files = file_collector(open(filelistname, mode='r'))
    #input_files.append(TFile.Open("/home/wn/spinformation/data/KINNW_MC16e_nomSignal_loose/user.rlysak.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10724_p4031.AB-21.2.109-MC16eSignalNominal-v01_out_root/out_proc_0.root","READ"))
    #input_files.append(TFile.Open("/nfs/atlas/nwarrack/SpinCors/NWKIN_outputs_02_fullFiles/out_proc_0.root","READ"))

    if debug:
        print "DEBUG: Creating ML input files in directory: ", outputdir
        print "DEBUG: Number of files in sample No.", sampleCtr, "is:", len(input_files)

    # Create new file and tree
    #file_name = str(f.GetName()).split('/')[-1]
    #new_file_name = new_file_path + file_name[:-5] + "_" + "copy.root"
    
    # Create timestamp for file naming so you can repetitively run whilst
    # debugging and not have to delete/rename files so often...
    ct = time.ctime()
    ct = ct.split()
    y = ct[4]
    m = ct[1]
    d = ct[2]
    t = ct[3].replace(':','')
    timestamp = "_" + y + m + d + "_" + t


    # name output file
    outfiletag = "tester_06"
    outfiletype = ".root"
    outfilename = outfiletag + timestamp + outfiletype
    outfile = path.join(outputdir + "/", outfilename)
    if path.exists(outfile):
        print "WARNING:", outfile, "already exists. Exiting."
        exit()
    
    # print output file location
    print "INFO: New output file:", outfile



    # make new root file and trees to fill
    new_file = TFile(outfile, "RECREATE")
    new_tree = TTree("features", "spinFeatures")


    ## create truth branches ##
    # tops
    t_pt, t_eta, t_phi                   = create_ptEtaPhi_branches(new_tree, "t"       ,"d")
    tbar_pt, tbar_eta, tbar_phi          = create_ptEtaPhi_branches(new_tree, "tbar"    ,"d")
    t_m    = create_branch(new_tree, "t_m", "d")
    tbar_m = create_branch(new_tree, "tbar_m"    , "d")        

    # leptons
    l_pt, l_eta, l_phi                   = create_ptEtaPhi_branches(new_tree, "l"       ,"d")
    lbar_pt, lbar_eta, lbar_phi          = create_ptEtaPhi_branches(new_tree, "lbar"    ,"d")
    l_m     = create_branch(new_tree, "l_m", "d")
    lbar_m  = create_branch(new_tree, "lbar_m" , "d")

    #neutrinos
    nu_t_pt, nu_t_eta, nu_t_phi          = create_ptEtaPhi_branches(new_tree, "nu_top"  ,"d")
    nu_tbar_pt, nu_tbar_eta, nu_tbar_phi = create_ptEtaPhi_branches(new_tree, "nu_tbar" ,"d")
    nu_t_m    = create_branch(new_tree, "nu_t_m"  , "d")
    nu_tbar_m = create_branch(new_tree, "nu_tbar_m" , "d")

    # bjets
    b_pt, b_eta, b_phi                   = create_ptEtaPhi_branches(new_tree, "b"       ,"d")
    bbar_pt, bbar_eta, bbar_phi          = create_ptEtaPhi_branches(new_tree, "bbar"    ,"d")
    b_m        = create_branch(new_tree, "b1_m"      , "d")
    bbar_m     = create_branch(new_tree, "b2_m"      , "d")

    # ttbar
    ttbar_pt, ttbar_eta, ttbar_phi       = create_ptEtaPhi_branches(new_tree, "ttbar"   ,"d")
    ttbar_m    = create_branch(new_tree, "ttbar_m"   , "d")

    # met
    met   = create_branch(new_tree, "met"  , "d")
    met_x = create_branch(new_tree, "met_x", "d")
    met_y = create_branch(new_tree, "met_y", "d")

    # spin sensitive observables
    spin_obs = []
    for ob in spin_observables:
        branch = create_branch(new_tree, ob, "d")
        spin_obs.append([ob,branch])

    # deltas
    t_delta_phi   = create_branch(new_tree, "t_delta_phi", "d")
    t_delta_eta   = create_branch(new_tree, "t_delta_eta", "d")
    lep_delta_phi = create_branch(new_tree, "lep_delta_phi", "d")
    lep_delta_eta = create_branch(new_tree, "lep_delta_eta", "d")
    lep_dphi_bypt = create_branch(new_tree, "lep_dphi_bypt", "d")        

    # other cos thetas, dot products and unit dot products
    costheta_leps_a_k = create_branch(new_tree, "costheta_leps_a_k", "d")
    costheta_leps_a_n = create_branch(new_tree, "costheta_leps_a_n", "d")
    costheta_leps_a_r = create_branch(new_tree, "costheta_leps_a_r", "d")
    costheta_leps_b_k = create_branch(new_tree, "costheta_leps_b_k", "d")
    costheta_leps_b_n = create_branch(new_tree, "costheta_leps_b_n", "d")
    costheta_leps_b_r = create_branch(new_tree, "costheta_leps_b_r", "d")

    dp_ll       = create_branch(new_tree, "dp_ll", "d")
    dp_bb       = create_branch(new_tree, "dp_bb", "d")
    dp_blep     = create_branch(new_tree, "dp_blep", "d")
    dp_bbarlep  = create_branch(new_tree, "dp_bbarlep", "d")
    dp_blbar    = create_branch(new_tree, "dp_blbar", "d")
    dp_bbarlbar = create_branch(new_tree, "dp_bbarlbar", "d")

    dp_u_ll       = create_branch(new_tree, "dp_u_ll", "d")
    dp_u_bb       = create_branch(new_tree, "dp_u_bb", "d")
    dp_u_blep     = create_branch(new_tree, "dp_u_blep", "d")
    dp_u_bbarlep  = create_branch(new_tree, "dp_u_bbarlep", "d")
    dp_u_blbar    = create_branch(new_tree, "dp_u_blbar", "d")
    dp_u_bbarlbar = create_branch(new_tree, "dp_u_bbarlbar", "d")


    ## create detector-level reco branches ##

    # reco met
    reco_met     = create_branch(new_tree, "reco_met", "d")
    reco_met_phi = create_branch(new_tree, "reco_met_phi", "d")

    # reco lepton
    reco_lep_p_pt, reco_lep_p_eta, reco_lep_p_phi = create_ptEtaPhi_branches(new_tree, "reco_lep_p", "d")
    reco_lep_n_pt, reco_lep_n_eta, reco_lep_n_phi = create_ptEtaPhi_branches(new_tree, "reco_lep_n", "d")
    reco_lep_p_e = create_branch(new_tree, "reco_lep_p_e", "d")
    reco_lep_n_e = create_branch(new_tree, "reco_lep_n_e", "d")

    # reco jets (first three in pT order)
    reco_jet_0_pt, reco_jet_0_eta, reco_jet_0_phi = create_ptEtaPhi_branches(new_tree, "reco_jet_0", "d")
    reco_jet_0_e       = create_branch(new_tree, "reco_jet_0_e", "d")
    #reco_jet_0_mv2c10  = create_branch(new_tree, "reco_jet_bweight", "d")
    reco_jet_1_pt, reco_jet_1_eta, reco_jet_1_phi = create_ptEtaPhi_branches(new_tree, "reco_jet_1", "d")
    reco_jet_1_e       = create_branch(new_tree, "reco_jet_1_e", "d")
    #reco_jet_1_mv2c10  = create_branch(new_tree, "reco_jet_1_bweight", "d")
    reco_jet_2_pt, reco_jet_2_eta, reco_jet_2_phi = create_ptEtaPhi_branches(new_tree, "reco_jet_2", "d")
    reco_jet_2_e       = create_branch(new_tree, "reco_jet_2_e", "d")
    #reco_jet_2_mv2c10  = create_branch(new_tree, "reco_jet_2_bweight", "d")
    reco_njets = create_branch(new_tree, "reco_njets", "i")

    # reco deltas
    reco_lep_delta_phi = create_branch(new_tree, "reco_lep_delta_phi", "d")
    reco_lep_delta_eta = create_branch(new_tree, "reco_lep_delta_eta", "d")
    reco_jet_delta_phi = create_branch(new_tree, "reco_jet_delta_phi", "d")
    reco_jet_delta_eta = create_branch(new_tree, "reco_jet_delta_eta", "d")
    reco_jetlep_delta_phi_0 = create_branch(new_tree, "reco_jetlep_delta_phi_0", "d")
    reco_jetlep_delta_phi_1 = create_branch(new_tree, "reco_jetlep_delta_phi_1", "d")

    # reco lep costhetas
    costheta_reco_leps_a_k = create_branch(new_tree, "costheta_reco_leps_a_k", "d")
    costheta_reco_leps_a_r = create_branch(new_tree, "costheta_reco_leps_a_r", "d")
    costheta_reco_leps_a_n = create_branch(new_tree, "costheta_reco_leps_a_n", "d")
    costheta_reco_leps_b_k = create_branch(new_tree, "costheta_reco_leps_b_k", "d")
    costheta_reco_leps_b_r = create_branch(new_tree, "costheta_reco_leps_b_r", "d")
    costheta_reco_leps_b_n = create_branch(new_tree, "costheta_reco_leps_b_n", "d")


    ## NW reco variables ##

    # NW tops
    NW_t_pt, NW_t_eta, NW_t_phi    = create_ptEtaPhi_branches(new_tree, "NW_top"  , "d")
    NW_tbar_pt, NW_tbar_eta, NW_tbar_phi = create_ptEtaPhi_branches(new_tree, "NW_tbar" , "d")
    NW_t_m  = create_branch(new_tree, "NW_t_m" , "d")
    NW_tbar_m = create_branch(new_tree, "NW_tbar_m", "d")

    # NW b jets
    #NW_b_pt, NW_b_eta, NW_b_phi          = create_ptEtaPhi_branches(new_tree, "NW_b"  , "d")
    #NW_bbar_pt, NW_bbar_eta, NW_bbar_phi = create_ptEtaPhi_branches(new_tree, "NW_bbar"  , "d")
    #NW_b_e    = create_branch(new_tree, "NW_b_e" , "d")
    #NW_bbar_e = create_branch(new_tree, "NW_bbar_e", "d")

    # NW neutrinos
    NW_nu_pt, NW_nu_eta, NW_nu_phi          = create_ptEtaPhi_branches(new_tree, "NW_nu"  , "d")
    NW_nubar_pt, NW_nubar_eta, NW_nubar_phi = create_ptEtaPhi_branches(new_tree, "NW_nubar"  , "d")
    NW_nu_m    = create_branch(new_tree, "NW_nu_m" , "d")
    NW_nubar_m = create_branch(new_tree, "NW_nubar_m", "d")

    # NW deltas
    NW_t_delta_phi = create_branch(new_tree, "NW_t_delta_phi", "d")
    NW_t_delta_eta = create_branch(new_tree, "NW_t_delta_eta", "d")
    NW_b_delta_phi   = create_branch(new_tree, "NW_b_delta_phi", "d")
    NW_b_delta_eta   = create_branch(new_tree, "NW_b_delta_eta", "d")
    NW_truth_topdeltaR   = create_branch(new_tree, "NW_truth_topdeltaR", "d")
    NW_truth_tbardeltaR  = create_branch(new_tree, "NW_truth_tbardeltaR", "d")

    # NW weight & success
    NW_weight      = create_branch(new_tree, "NW_weight", "d")
    NW_success     = create_branch(new_tree, "NW_success", "i")

    # NW spin cors
    NW_spin_obs = []
    for ob in spin_observables:
        branch = create_branch(new_tree, "NW_" + ob, "d")
        NW_spin_obs.append([ob,branch])


    ## EM reco variables ##

    # EM tops
    EM_t_pt, EM_t_eta, EM_t_phi    = create_ptEtaPhi_branches(new_tree, "EM_top"  , "d")
    EM_tbar_pt, EM_tbar_eta, EM_tbar_phi = create_ptEtaPhi_branches(new_tree, "EM_tbar" , "d")
    EM_t_m  = create_branch(new_tree, "EM_t_m" , "d")
    EM_tbar_m = create_branch(new_tree, "EM_tbar_m", "d")

    # EM neutrinos
    EM_nu_pt, EM_nu_eta, EM_nu_phi          = create_ptEtaPhi_branches(new_tree, "EM_nu"  , "d")
    EM_nubar_pt, EM_nubar_eta, EM_nubar_phi = create_ptEtaPhi_branches(new_tree, "EM_nubar"  , "d")
    EM_nu_m    = create_branch(new_tree, "EM_nu_m" , "d")
    EM_nubar_m = create_branch(new_tree, "EM_nubar_m", "d")

    # EM deltas
    EM_t_delta_phi = create_branch(new_tree, "EM_t_delta_phi", "d")
    EM_t_delta_eta = create_branch(new_tree, "EM_t_delta_eta", "d")
    EM_b_delta_phi   = create_branch(new_tree, "EM_b_delta_phi", "d")
    EM_b_delta_eta   = create_branch(new_tree, "EM_b_delta_eta", "d")
    EM_truth_topdeltaR   = create_branch(new_tree, "EM_truth_topdeltaR", "d")
    EM_truth_tbardeltaR  = create_branch(new_tree, "EM_truth_tbardeltaR", "d")

    # EM success
    EM_success     = create_branch(new_tree, "EM_success", "i")

    # EM spin cors
    EM_spin_obs = []
    for ob in spin_observables:
        branch = create_branch(new_tree, "EM_" + ob, "d")
        EM_spin_obs.append([ob, branch])

    ## EM reco variables ##

    # SN tops
    SN_t_pt, SN_t_eta, SN_t_phi    = create_ptEtaPhi_branches(new_tree, "SN_top"  , "d")
    SN_tbar_pt, SN_tbar_eta, SN_tbar_phi = create_ptEtaPhi_branches(new_tree, "SN_tbar" , "d")
    SN_t_m  = create_branch(new_tree, "SN_t_m" , "d")
    SN_tbar_m = create_branch(new_tree, "SN_tbar_m", "d")

    # SN neutrinos
    SN_nu_pt, SN_nu_eta, SN_nu_phi          = create_ptEtaPhi_branches(new_tree, "SN_nu"  , "d")
    SN_nubar_pt, SN_nubar_eta, SN_nubar_phi = create_ptEtaPhi_branches(new_tree, "SN_nubar"  , "d")
    SN_nu_m    = create_branch(new_tree, "SN_nu_m" , "d")
    SN_nubar_m = create_branch(new_tree, "SN_nubar_m", "d")

    # SN deltas
    SN_t_delta_phi = create_branch(new_tree, "SN_t_delta_phi", "d")
    SN_t_delta_eta = create_branch(new_tree, "SN_t_delta_eta", "d")
    SN_b_delta_phi   = create_branch(new_tree, "SN_b_delta_phi", "d")
    SN_b_delta_eta   = create_branch(new_tree, "SN_b_delta_eta", "d")
    SN_truth_topdeltaR   = create_branch(new_tree, "SN_truth_topdeltaR", "d")
    SN_truth_tbardeltaR  = create_branch(new_tree, "SN_truth_tbardeltaR", "d")

    # SN success
    SN_success     = create_branch(new_tree, "SN_success", "i")

    # SN spin cors
    SN_spin_obs = []
    for ob in spin_observables:
        branch = create_branch(new_tree, "SN_" + ob, "d")
        SN_spin_obs.append([ob, branch])


    #######################################
    # reco codes:                         #
    # 0 = SN succeeded ; EM failed        #
    # 1 = SN failed    ; EM succeeded     #
    # 2 = SN           ; EM succeeded     #
    #######################################
    reco_code = create_branch(new_tree, "reco_code", "i")

    #######################################
    # spin_obs_pref:                      #
    # 0 = only one successful reco method #
    # 1 = both successful, EM prefered    #
    # 2 = both successful, SN prefered    #
    #######################################
    spin_obs_pref = []
    for sp_obs in spin_observables:
        branch = create_branch(new_tree, "pref_" + sp_obs, "i")
        spin_obs_pref.append([sp_obs,branch])


    # Difference in spin values
    diff_NWtruth = []
    for sp_obs in spin_observables:
        branch = create_branch(new_tree, "diff_NWtruth_" + sp_obs, "d")
        diff_NWtruth.append([sp_obs,branch])




    ##################################################
    ## Examine events and fill new trees if desired ##
    ##################################################

    ## Loop through input files and save event properties (loop over files is
    ## better than chaining which can consume too much memory when building
    ## the truth index
    for f in input_files:

        # (re)set counters
        reco_codeCtr = 0
        errorCtr = 0
        evntCtr  = 0
        savedCtr = 0
        funnyEventCtr = 0
        unique_evntCtr = 0
        duplic_evntCtr = 0

        # set some re-used variables
        pi = 3.1415926535
        done_list = []
        z_axis = TVector3(0,0,1)

        # Get the trees we need
        syst  = f.Get(systematic)
        truth = f.Get("truth")

        if debug:
            print "DEBUG: Input file: ", str(f).split()[2]
            print "DEBUG: truth entries: ", truth.GetEntries()
            print "DEBUG:",systematic, "entries: ", syst.GetEntries()


        # build the index between the systematic tree and the truth tree
        truth.BuildIndex("runNumber", "eventNumber")


        for event in syst:

            evntCtr += 1


            # cop out if debugging

            if stopshort and evntCtr > (stopN - 1): break

            
            if evntCtr % 10000 == 0:
                print evntCtr, "events processed (", round(100.0*savedCtr/evntCtr,2), "% saved)."

            # get truth event
            truth.GetEntryWithIndex(event.runNumber, event.eventNumber)

            # define dilepton (this is the prompt leptons!)
            if not true_dilepton(truth, "emu", False): # True/False turns on/pff printout
                funnyEventCtr += 1
                continue

            # skip non OS events
            if event.d_lep_charge[0]*event.d_lep_charge[1] > 0:
                continue

            # skip events where any of the three recos fail
            if not (event.d_NW_weight >= 0.0 and event.d_SN_t_pt >= 0.0 and event.d_EM_t_pt >= 0.0):
                #print "skip event"
                continue
            #else:
            #    print "process event"


            ## Fill trees


            ## Save reconstructed kinematics ##

            # Leptons
            if event.d_lep_charge[0] > 0:
                p = 0
                n = 1
            else:
                p = 1
                n = 0
            reco_lep_p_pt[0]  = event.d_lep_pt[p]
            reco_lep_p_eta[0] = event.d_lep_eta[p]
            reco_lep_p_phi[0] = event.d_lep_phi[p]
            reco_lep_p_e[0]   = event.d_lep_e[p]
            reco_lep_n_pt[0]  = event.d_lep_pt[n]
            reco_lep_n_eta[0] = event.d_lep_eta[n]
            reco_lep_n_phi[0] = event.d_lep_phi[n]
            reco_lep_n_e[0]   = event.d_lep_e[n]

            lep_p = TLorentzVector()
            lep_p.SetPtEtaPhiE(event.d_lep_pt[p], event.d_lep_eta[p], event.d_lep_phi[p], event.d_lep_e[p])
            lep_n = TLorentzVector()
            lep_n.SetPtEtaPhiE(event.d_lep_pt[n], event.d_lep_eta[n], event.d_lep_phi[n], event.d_lep_e[n])

            # these are inspired by spin cors, but not driven by theory themselves
            costheta_reco_leps_a_k[0] = cos_theta(TVector3(lep_p.Vect()), TVector3(lep_n.Vect()), z_axis, "k", "a")
            costheta_reco_leps_a_n[0] = cos_theta(TVector3(lep_p.Vect()), TVector3(lep_n.Vect()), z_axis, "n", "a")
            costheta_reco_leps_a_r[0] = cos_theta(TVector3(lep_p.Vect()), TVector3(lep_n.Vect()), z_axis, "r", "a")
            costheta_reco_leps_b_k[0] = cos_theta(TVector3(lep_p.Vect()), TVector3(lep_n.Vect()), z_axis, "k", "b")
            costheta_reco_leps_b_n[0] = cos_theta(TVector3(lep_p.Vect()), TVector3(lep_n.Vect()), z_axis, "n", "b")
            costheta_reco_leps_b_r[0] = cos_theta(TVector3(lep_p.Vect()), TVector3(lep_n.Vect()), z_axis, "r", "b")


            # lepton deltas (pt ordered)
            reco_lep_delta_phi[0] = event.d_lep_phi[0] - event.d_lep_phi[1]
            if reco_lep_delta_phi[0] < 0.0:
                reco_lep_delta_phi[0] += 2*pi
            reco_lep_delta_eta[0] = event.d_lep_eta[0] - event.d_lep_eta[1]

            # met
            reco_met[0]       = event.d_met_met
            reco_met_phi[0]   = event.d_met_phi

            # jets
            reco_njets[0]        = len(event.d_jet_pt)
            reco_jet_0_pt[0]     = event.d_jet_pt[0]
            reco_jet_0_eta[0]    = event.d_jet_eta[0]
            reco_jet_0_phi[0]    = event.d_jet_phi[0]
            reco_jet_0_e[0]      = event.d_jet_e[0]
            #eco_jet_0_mv2c10[0] = event.d_jet_mv2c10[0]
            reco_jet_1_pt[0]     = event.d_jet_pt[1]
            reco_jet_1_eta[0]    = event.d_jet_eta[1]
            reco_jet_1_phi[0]    = event.d_jet_phi[1]
            reco_jet_1_e[0]      = event.d_jet_e[1]
            #eco_jet_1_mv2c10[0] = event.d_jet_mv2c10[1]
            if reco_njets[0] > 2:
                reco_jet_2_pt[0]     = event.d_jet_pt[2]
                reco_jet_2_eta[0]    = event.d_jet_eta[2]
                reco_jet_2_phi[0]    = event.d_jet_phi[2]
                reco_jet_2_e[0]      = event.d_jet_e[2]
                #eco_jet_2_mv2c10[0] = event.d_jet_mv2c10[2]

            # jet deltas (pt ordered)
            reco_jet_delta_phi[0] = event.d_jet_phi[0] - event.d_jet_phi[1]
            if reco_jet_delta_phi[0] < 0.0:
                reco_jet_delta_phi[0] += 2*pi
            reco_jet_delta_eta[0] = event.d_jet_eta[0] - event.d_jet_eta[1]

            # jet0-lepton0 deltas
            reco_jetlep_delta_phi_0[0] = event.d_jet_phi[0] - event.d_lep_phi[0]
            if reco_jetlep_delta_phi_0[0] < 0.0:
                reco_jetlep_delta_phi_0[0] += 2*pi

            # jet1-lepton1 deltas
            reco_jetlep_delta_phi_1[0] = event.d_jet_phi[1] - event.d_lep_phi[1]
            if reco_jetlep_delta_phi_1[0] < 0.0:
                reco_jetlep_delta_phi_1[0] += 2*pi


            ## Neutrino Weighter (NW) ##

            # record success of reco
            NW_weight[0] = event.d_NW_weight
            if NW_weight[0] >= 0:
                NW_success[0] = 1
            else:
                NW_success[0] = 0


            NW_top          = make_lorentzvector_mass(event, "d_NW_t")
            NW_tbar         = make_lorentzvector_mass(event, "d_NW_tbar")
            NW_t_pt[0], NW_t_eta[0], NW_t_phi[0], NW_t_m[0] = get_PtEtaPhiM(NW_top)
            NW_tbar_pt[0], NW_tbar_eta[0], NW_tbar_phi[0], NW_tbar_m[0] = get_PtEtaPhiM(NW_tbar)

            # NW neutrinos
            NW_nu_pt[0]    = event.d_NW_nu_pt
            NW_nu_eta[0]   = event.d_NW_nu_eta
            NW_nu_phi[0]   = event.d_NW_nu_phi
            NW_nu_m[0]     = event.d_NW_nu_m
            NW_nubar_pt[0]   = event.d_NW_nubar_pt
            NW_nubar_eta[0]  = event.d_NW_nubar_eta
            NW_nubar_phi[0]  = event.d_NW_nubar_phi
            NW_nubar_m[0]    = event.d_NW_nubar_m

            # NW spin cors and polz
            b_t, b_tbar, b_t_lep, b_tbar_lep = boosted_objects(event, "NW")
            cta_NW = {}
            cta_NW["k"] = cos_theta(b_t, b_t_lep, z_axis, "k", "a")
            cta_NW["n"] = cos_theta(b_t, b_t_lep, z_axis, "n", "a")
            cta_NW["r"] = cos_theta(b_t, b_t_lep, z_axis, "r", "a")
            ctb_NW = {}
            ctb_NW["k"] = cos_theta(b_tbar, b_tbar_lep, z_axis, "k", "b")
            ctb_NW["n"] = cos_theta(b_tbar, b_tbar_lep, z_axis, "n", "b")
            ctb_NW["r"] = cos_theta(b_tbar, b_tbar_lep, z_axis, "r", "b")

            
            for obs in NW_spin_obs:
                obs[1][0] = calc_spin_obs(obs[0], cta_NW, ctb_NW)
                #print obs[1][0]

            ## Elipse Method (EM) ##

            # record success of reco
            if event.d_EM_t_pt >= 0:
                EM_success[0] = 1
            else:
                EM_success[0] = 0


            EM_top          = make_lorentzvector_mass(event, "d_EM_t")
            EM_tbar         = make_lorentzvector_mass(event, "d_EM_tbar")
            EM_t_pt[0], EM_t_eta[0], EM_t_phi[0], EM_t_m[0] = get_PtEtaPhiM(EM_top)
            EM_tbar_pt[0], EM_tbar_eta[0], EM_tbar_phi[0], EM_tbar_m[0] = get_PtEtaPhiM(EM_tbar)

            # EM neutrinos
            EM_nu_pt[0]    = event.d_EM_nu_pt
            EM_nu_eta[0]   = event.d_EM_nu_eta
            EM_nu_phi[0]   = event.d_EM_nu_phi
            EM_nu_m[0]     = event.d_EM_nu_m
            EM_nubar_pt[0]   = event.d_EM_nubar_pt
            EM_nubar_eta[0]  = event.d_EM_nubar_eta
            EM_nubar_phi[0]  = event.d_EM_nubar_phi
            EM_nubar_m[0]    = event.d_EM_nubar_m

            # EM spin cors and polz
            b_t, b_tbar, b_t_lep, b_tbar_lep = boosted_objects(event, "EM")
            cta_EM = {}
            cta_EM["k"] = cos_theta(b_t, b_t_lep, z_axis, "k", "a")
            cta_EM["n"] = cos_theta(b_t, b_t_lep, z_axis, "n", "a")
            cta_EM["r"] = cos_theta(b_t, b_t_lep, z_axis, "r", "a")
            ctb_EM = {}
            ctb_EM["k"] = cos_theta(b_tbar, b_tbar_lep, z_axis, "k", "b")
            ctb_EM["n"] = cos_theta(b_tbar, b_tbar_lep, z_axis, "n", "b")
            ctb_EM["r"] = cos_theta(b_tbar, b_tbar_lep, z_axis, "r", "b")


            for obs in EM_spin_obs:
                obs[1][0] = calc_spin_obs(obs[0], cta_EM, ctb_EM)


            ## Sonnenshein Method (SN) ##

            if event.d_SN_t_pt >= 0:
                SN_success[0] = 1
            else:
                SN_success[0] = 0
            #print "SN_success[0]  :", SN_success[0]

            SN_top          = make_lorentzvector_mass(event, "d_SN_t")
            SN_tbar         = make_lorentzvector_mass(event, "d_SN_tbar")
            SN_t_pt[0], SN_t_eta[0], SN_t_phi[0], SN_t_m[0] = get_PtEtaPhiM(SN_top)
            SN_tbar_pt[0], SN_tbar_eta[0], SN_tbar_phi[0], SN_tbar_m[0] = get_PtEtaPhiM(SN_tbar)

            # SN neutrinos
            SN_nu_pt[0]    = event.d_SN_nu_pt
            SN_nu_eta[0]   = event.d_SN_nu_eta
            SN_nu_phi[0]   = event.d_SN_nu_phi
            SN_nu_m[0]     = event.d_SN_nu_m
            SN_nubar_pt[0]   = event.d_SN_nubar_pt
            SN_nubar_eta[0]  = event.d_SN_nubar_eta
            SN_nubar_phi[0]  = event.d_SN_nubar_phi
            SN_nubar_m[0]    = event.d_SN_nubar_m

            # SN spin cors and polz
            b_t, b_tbar, b_t_lep, b_tbar_lep = boosted_objects(event, "SN")

            
            
            cta_SN = {}
            cta_SN["k"] = cos_theta(b_t, b_t_lep, z_axis, "k", "a")
            cta_SN["n"] = cos_theta(b_t, b_t_lep, z_axis, "n", "a")
            cta_SN["r"] = cos_theta(b_t, b_t_lep, z_axis, "r", "a")
            ctb_SN = {}
            ctb_SN["k"] = cos_theta(b_tbar, b_tbar_lep, z_axis, "k", "b")
            ctb_SN["n"] = cos_theta(b_tbar, b_tbar_lep, z_axis, "n", "b")
            ctb_SN["r"] = cos_theta(b_tbar, b_tbar_lep, z_axis, "r", "b")


            #if math.isnan(cta_SN["k"]):
            #    print "NaN found! cta_SN[k] in event:", evntCtr 
            #    print "b_t =", b_t.X(), b_t.Y(), b_t.Z()
            #    print "b_t_lep =", b_t_lep
            #    print "b_tbar =", b_tbar
            #    print "b_tbar_lep =", b_tbar_lep
            #    print "SN top pt, eta, phi, m:", SN_t_pt[0], SN_t_eta[0], SN_t_phi[0], SN_t_m[0]
            #    print "mass top = ", event.d_SN_t_m



            for obs in SN_spin_obs:
                obs[1][0] = calc_spin_obs(obs[0], cta_SN, ctb_SN)


            # Truth spin cors and polz
            b_t, b_tbar, b_t_lep, b_tbar_lep = boosted_objects_truth(truth)
            cta, ctb = calc_cos_thetas(b_t, b_tbar, b_t_lep, b_tbar_lep, z_axis)
            for obs in spin_obs:
                obs[1][0] = calc_spin_obs(obs[0], cta, ctb)


            # compare spin cors (KIN/NW/truth) to each other
            #for i in range(len(spin_obs)):

                # Get values from arrays
                # nw  = NW_spin_obs[i][1][0]
                
                # # Case 1: both reco methods succeeded
                # if reco_code[0] == 2:
                #     diff_NWKIN[i][1][0]    = nw  - kin
                #     diff_NWtruth[i][1][0]  = nw  - tru
                #     diff_KINtruth[i][1][0] = kin - tru
                #     if abs(kin - tru) < abs(nw - tru):
                #         spin_obs_pref[i][1][0] = 0
                #     else:
                #         spin_obs_pref[i][1][0] = 1
                # elif reco_code[0] == 0: # KIN was successful, but not NW
                #     spin_obs_pref[i][1][0] = 2
                #     diff_KINtruth[i][1][0] = kin - tru
                # elif reco_code[0] == 1: # NW was successful, but not KIN
                #     spin_obs_pref[i][1][0] = 2
                #     diff_NWtruth[i][1][0]  = nw - tru
                
            if SN_success[0] and NW_success[0] and EM_success[0]:
                reco_code[0] = 1
                reco_codeCtr += 1
            else:
                reco_code[0] = 0

            ### Save truth level info ###

            # b kinomatics
            b    = make_lorentzvector_mass(truth, "d_b")
            bbar = make_lorentzvector_mass(truth, "d_bbar")
            b_pt[0], b_eta[0], b_phi[0], b_m[0]             = get_PtEtaPhiM(b)
            bbar_pt[0], bbar_eta[0], bbar_phi[0], bbar_m[0] = get_PtEtaPhiM(bbar)

            # neutrinos
            nu_t     = make_lorentzvector_mass(truth, "d_nu")
            nu_tbar  = make_lorentzvector_mass(truth, "d_nubar")
            nu_t_pt[0], nu_t_eta[0], nu_t_phi[0], nu_t_m[0]     = get_PtEtaPhiM(nu_t)
            nu_tbar_pt[0], nu_tbar_eta[0], nu_tbar_phi[0], nu_tbar_m[0] = get_PtEtaPhiM(nu_tbar)

            # met
            nunubar  = nu_t + nu_tbar
            met[0]   = nunubar.Pt()
            met_x[0] = nunubar.X()
            met_y[0] = nunubar.Y()

            # top and ttbar system
            t     = make_lorentzvector_mass(truth, "d_t_afterFSR")
            tbar  = make_lorentzvector_mass(truth, "d_tbar_afterFSR")
            ttbar = t + tbar
            t_pt[0], t_eta[0], t_phi[0], t_m[0]         = get_PtEtaPhiM(t)
            tbar_pt[0], tbar_eta[0], tbar_phi[0], tbar_m[0]     = get_PtEtaPhiM(tbar)
            ttbar_pt[0], ttbar_eta[0], ttbar_phi[0], ttbar_m[0] = get_PtEtaPhiM(ttbar)

            # leptons
            lbar_id  = truth.d_l_pdgid
            l_id = truth.d_lbar_pdgid
            lbar    = make_lorentzvector_mass(truth, "d_l")
            l = make_lorentzvector_mass(truth, "d_lbar")
            lbar_pt[0], lbar_eta[0], lbar_phi[0], lbar_m[0]     = get_PtEtaPhiM(lbar)
            l_pt[0], l_eta[0], l_phi[0], l_m[0] = get_PtEtaPhiM(l)
            # count number of events without 2 truth level leptons
            if debug:
                errorCtr += print_lepton_errors(truth, l_id, "emu")
                errorCtr += print_lepton_errors(truth, lbar_id, "emu")


            ## Detas
            # truth
            lep_delta_phi[0] = abs(l_phi[0] - lbar_phi[0])
            if lep_delta_phi[0] > 3.1415926535:
                lep_delta_phi[0] = 2*pi - lep_delta_phi[0]

            t_delta_phi[0] = abs(tbar_phi[0] - t_phi[0])
            if t_delta_phi[0] > 3.1415926535:
                t_delta_phi[0] = 2*pi - t_delta_phi[0]

            if lbar.Pt() > l.Pt():
                lep_dphi_bypt[0] = lbar.Phi() - l.Phi()
            else:
                lep_dphi_bypt[0] = l.Phi() - lbar.Phi()
            if lep_dphi_bypt[0] < 0.0:
                lep_dphi_bypt[0] = lep_dphi_bypt[0] + 2*pi

            lep_delta_eta[0] = abs(l_eta[0] - lbar_eta[0])
            t_delta_eta[0] = abs(tbar_eta[0] - t_eta[0])



            # dot products
            costheta_leps_a_k[0] = cos_theta(TVector3(lbar.Vect()), TVector3(l.Vect()), z_axis, "k", "a")
            costheta_leps_a_n[0] = cos_theta(TVector3(lbar.Vect()), TVector3(l.Vect()), z_axis, "n", "a")
            costheta_leps_a_r[0] = cos_theta(TVector3(lbar.Vect()), TVector3(l.Vect()), z_axis, "r", "a")
            costheta_leps_b_k[0] = cos_theta(TVector3(lbar.Vect()), TVector3(l.Vect()), z_axis, "k", "b")
            costheta_leps_b_n[0] = cos_theta(TVector3(lbar.Vect()), TVector3(l.Vect()), z_axis, "n", "b")
            costheta_leps_b_r[0] = cos_theta(TVector3(lbar.Vect()), TVector3(l.Vect()), z_axis, "r", "b")

            v3_lbar = TVector3(lbar.Vect())
            v3_lep  = TVector3(l.Vect())
            v3_b    = TVector3(b.Vect())
            v3_bbar = TVector3(bbar.Vect())

            v3_u_lbar = v3_lbar.Unit()
            v3_u_lep  = v3_lep.Unit()
            v3_u_b    = v3_b.Unit()
            v3_u_bbar = v3_bbar.Unit()

            dp_u_ll[0]       = v3_u_lep.Dot(v3_u_lbar)
            dp_u_bb[0]       = v3_u_b.Dot(v3_u_bbar)
            dp_u_blep[0]     = v3_u_b.Dot(v3_u_lep)
            dp_u_bbarlep[0]  = v3_u_bbar.Dot(v3_u_lep)
            dp_u_blbar[0]    = v3_u_b.Dot(v3_u_lbar)
            dp_u_bbarlbar[0] = v3_u_bbar.Dot(v3_u_lbar)

            dp_ll[0]       = v3_lep.Dot(v3_lbar)
            dp_bb[0]       = v3_b.Dot(v3_bbar)
            dp_blep[0]     = v3_b.Dot(v3_lep)
            dp_bbarlep[0]  = v3_bbar.Dot(v3_lep)
            dp_blbar[0]    = v3_b.Dot(v3_lbar)
            dp_bbarlbar[0] = v3_bbar.Dot(v3_lbar)


            ## compare NW and KIN to truth
            NW_truth_topdeltaR[0]  = NW_top.DeltaR(t)
            NW_truth_tbardeltaR[0]  = NW_tbar.DeltaR(tbar)

            # Save
            savedCtr += 1
            new_tree.Fill()

        # # save one file for every file input
        # if debug: print "DEBUG: writing new tree"
        # new_tree.Write()
        # if debug: print "DEBUG: closing new file"
        # new_file.Close()

        print "INFO: ******* STATS per input file *******"
        print "INFO: Events considered :", evntCtr
        print "INFO: Events saved      :", savedCtr
        print "INFO: Funny events      :", funnyEventCtr
        print "INFO: ************************************"
        print "INFO: Reco_code = 1     :", round(100.0*reco_codeCtr/savedCtr,2), "% of saved events have reco_code = 1 (i.e. success for NW, SN and EM reco methods)"

    if debug: print "DEBUG: writing new tree"
    new_tree.Write()
    if debug: print "DEBUG: closing new file"
    new_file.Close()

    print "Done!"
