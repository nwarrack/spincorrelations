#!/usr/bin/env python

"""
This scrip uses root files which contain histograms and produces plots from them
"""


# system imports:
import argparse

# project import:
from debug import test_plot_arg

# define input arguments and 'help' printouts (use "./create_plots.py -h")
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--period', nargs='+', help='PERIOD = 16a/16d/16e/all (space-separated list accepted!)', required=True)
parser.add_argument('-o', '--object', nargs='+', help='OBJECT = lep/jet/met/top/spin/costheta (space-separated list accepted!)', required=True)
parser.add_argument('-c', '--channel', nargs='+', help='CHANNEL = ee/emu/mumu/all (space-separated list accepted!)', required=True)
parser.add_argument('-s', '--systematic', help='SYSTEMATIC = tree-name', required=True)
parser.add_argument('-a', '--action', help='ACTION = plot/rmbckg/truthcomp/NNLOcomp/resolution/eft/mcplot/mig', required=True)

# get the arguments
args = parser.parse_args()
action     = args.action
objv       = args.object
periods    = args.period
channels   = args.channel
systematic = args.systematic

# check the arguments
if not test_plot_arg(action,    "action"): exit()
if not test_plot_arg(objv,      "object",  True): exit()
if not test_plot_arg(periods,   "period",  True): exit()
if not test_plot_arg(channels,  "channel", True): exit()

# third part imports:
from ROOT import TFile, TH1D

# project imports:
from plot import make_dict, combine_dicts, make_datamc_plot, make_signal_comparison_plot, make_resolution_plot, make_eft_plot, make_simple_mc_plot, make_mig_plot
from histtools import data_lumi
from histconfig import histo_lists
from debug import make_readable

info = True # False will stop printouts
histdir="./user/Hists"

# default values
NO_LEGEND             = 0
LEGEND_TOP_LEFT       = 1
LEGEND_TOP_RIGHT      = 2

hist_counter   = 0
plots_produced = 0
file_failures  = 0
file_success   = []

# create a dictionary to hold the histograms to be plotted
master = {}

# create a combanatoric list of plots requested
plots = []
for period in periods:
    for channel in channels:
        for obj in objv:
            for histconfig in histo_lists[obj]:
                plots.append([period, channel, histconfig]) # note: histconfig is itself a list

ctr = 0
print plots


for plot in plots:

    ctr = ctr + 1
    #if ctr > 1: continue

    # unpack plot to make readable
    period      = plot[0]
    channel     = plot[1]
    histconfig  = plot[2]
    variable_id = histconfig[7]
    variable    = histconfig[0]

    if info:
        print "INFO:   New hist ID:", variable_id
        print "INFO:   Observable :", variable
        print "INFO:   Period     :", make_readable(period)
        print "INFO:   Channel    :", make_readable(channel)

    hist_counter += 1

    ## Combine channels and/or periods if needed, before plotting
    master.clear()

    if period == "all" and channel == "all":

        # make file name strings
        fn_ee   = "ee_" + variable_id + "_" + systematic + ".root"
        fn_16a_ee   = histdir + "/16a/" + fn_ee
        fn_16d_ee   = histdir + "/16d/" + fn_ee
        fn_16e_ee   = histdir + "/16e/" + fn_ee
        fn_emu  = "emu_" + variable_id + "_" + systematic + ".root"
        fn_16a_emu  = histdir + "/16a/" + fn_emu
        fn_16d_emu  = histdir + "/16d/" + fn_emu
        fn_16e_emu  = histdir + "/16e/" + fn_emu
        fn_mumu = "mumu_" + variable_id + "_" + systematic + ".root"
        fn_16a_mumu = histdir + "/16a/" + fn_mumu
        fn_16d_mumu = histdir + "/16d/" + fn_mumu
        fn_16e_mumu = histdir + "/16e/" + fn_mumu

        # open files
        f_16a_ee   = TFile.Open(fn_16a_ee,"READ")
        f_16d_ee   = TFile.Open(fn_16d_ee,"READ")
        f_16e_ee   = TFile.Open(fn_16e_ee,"READ")
        f_16a_emu  = TFile.Open(fn_16a_emu,"READ")
        f_16d_emu  = TFile.Open(fn_16d_emu,"READ")
        f_16e_emu  = TFile.Open(fn_16e_emu,"READ")
        f_16a_mumu = TFile.Open(fn_16a_mumu,"READ")
        f_16d_mumu = TFile.Open(fn_16d_mumu,"READ")
        f_16e_mumu = TFile.Open(fn_16e_mumu,"READ")

        try:
            # Throw a python error is files are missing
            temphist = f_16a_ee.Get("ttbar").Clone()
            temphist.Add(f_16d_ee.Get("ttbar"))
            temphist.Add(f_16e_ee.Get("ttbar"))
            temphist.Add(f_16a_emu.Get("ttbar"))
            temphist.Add(f_16d_emu.Get("ttbar"))
            temphist.Add(f_16e_emu.Get("ttbar"))
            temphist.Add(f_16a_mumu.Get("ttbar"))
            temphist.Add(f_16d_mumu.Get("ttbar"))
            temphist.Add(f_16e_mumu.Get("ttbar"))
            del temphist
            file_success.append([period+"_"+channel+"_"+variable_id, "success"])
        except:
            print "ERROR: Problem with one of the following input files:"
            print fn_16a_ee
            print fn_16d_ee
            print fn_16e_ee
            print fn_16a_emu
            print fn_16d_emu
            print fn_16e_emu
            print fn_16a_mumu
            print fn_16d_mumu
            print fn_16e_mumu
            print "WARNING: Continuing..."
            file_success.append([period+"_"+channel+"_"+variable_id, "failed "])
            file_failures += 1
            continue

        # make dictionaries of the histograms for each file
        d_16a_ee   = make_dict(f_16a_ee)
        d_16d_ee   = make_dict(f_16d_ee)
        d_16e_ee   = make_dict(f_16e_ee)
        d_16a_emu  = make_dict(f_16a_emu)
        d_16d_emu  = make_dict(f_16d_emu)
        d_16e_emu  = make_dict(f_16e_emu)
        d_16a_mumu = make_dict(f_16a_mumu)
        d_16d_mumu = make_dict(f_16d_mumu)
        d_16e_mumu = make_dict(f_16e_mumu)

        # combine the channels
        d_mc16a = combine_dicts(d_16a_ee,d_16a_emu,d_16a_mumu)
        d_mc16d = combine_dicts(d_16d_ee,d_16d_emu,d_16d_mumu)
        d_mc16e = combine_dicts(d_16e_ee,d_16e_emu,d_16e_mumu)

        # combine the periods
        master  = combine_dicts(d_mc16a, d_mc16d, d_mc16e)


    elif period != "all" and channel == "all":

        # make file name string
        fn = variable_id + "_" + systematic + ".root"
        fn_ee   = histdir + "/" + period + "/ee_" + fn
        fn_emu  = histdir + "/" + period + "/emu_" + fn
        fn_mumu = histdir + "/" + period + "/mumu_" + fn

        # open files
        f_ee   = TFile.Open(fn_ee,"READ")
        f_emu  = TFile.Open(fn_emu,"READ")
        f_mumu = TFile.Open(fn_mumu,"READ")

        try:
            # Throw a python error if files are missing
            temphist = f_ee.Get("ttbar").Clone()
            temphist.Add(f_emu.Get("ttbar"))
            temphist.Add(f_mumu.Get("ttbar"))
            del temphist
            file_success.append([period+"_"+channel+"_"+variable_id, "success"])
        except:
            print "ERROR: Problem with one of the following input files:"
            print fn_ee
            print fn_emu
            print fn_mumu
            print "WARNING: Continuing..."
            file_success.append([period+"_"+channel+"_"+variable_id, "failed "])
            file_failures += 1
            continue

        # make dictionaries of hists
        d_ee   = make_dict(f_ee)
        d_emu  = make_dict(f_emu)
        d_mumu = make_dict(f_mumu)

        # combine the periods
        master = combine_dicts(d_ee, d_emu, d_mumu)

    elif period == "all" and channel != "all":

        # make file name string
        fn = channel + "_" + variable_id + "_" + systematic + ".root"
        fn_16a  = histdir + "/16a/" + fn
        fn_16d  = histdir + "/16d/" + fn
        fn_16e  = histdir + "/16e/" + fn

        # open files
        f_16a = TFile.Open(fn_16a,"READ")
        f_16d = TFile.Open(fn_16d,"READ")
        f_16e = TFile.Open(fn_16e,"READ")
        try:
            # Throw a python error if files are missing
            temphist = f_16a.Get("ttbar").Clone()
            temphist.Add(f_16d.Get("ttbar"))
            temphist.Add(f_16e.Get("ttbar"))
            del temphist
            file_success.append([period+"_"+channel+"_"+variable_id, "success"])
        except:
            print "ERROR: Problem with one of the following input files:"
            print fn_16a
            print fn_16d
            print fn_16e
            print "WARNING: Continuing..."
            file_success.append([period+"_"+channel+"_"+variable_id, "failed "])
            file_failures += 1
            continue

        # make dictionaries of hists
        d_16a = make_dict(f_16a)
        d_16d = make_dict(f_16d)
        d_16e = make_dict(f_16e)

        # combine the periods
        master = combine_dicts(d_16a, d_16d, d_16e)

    else: # arg 'all' not used for period OR channel

        fn = histdir + "/" + period + "/" + channel + "_" + variable_id + "_" + systematic + ".root"
        f = TFile.Open(fn,"READ")

        try:
            # Throw error if file is missing
            temphist = f.Get("ttbar").Clone() #<- this will throw error!
            del temphist
            file_success.append([period+"_"+channel+"_"+variable_id, "success"])
        except:
            print "ERROR: There is a problem with input file:", fn
            print "WARNING: Continuing..."
            file_success.append([period+"_"+channel+"_"+variable_id, "failed "])
            file_failures += 1
            continue

        master = make_dict(f)


    # set option to draw mean line (for spin sensitive observables)
    if variable.startswith("c_") or variable.startswith("b_"):
        DRAW_MEAN_VALUES   = True
    else:
        DRAW_MEAN_VALUES   = False


    ### Execute the plotting functions depending on 'action' argument
 
    ## New attempt to hakily plot a migration matrix in com05 branch (i.e.
    ## using common dilepton nTupes from production round 5)
    if action == "mig":

        plots_produced += make_mig_plot(master,
                                        channel,
                                        histconfig,
                                        systematic,
                                        data_lumi[period],
                                        period)


    ## Standard Data Vs MC plot (where MC = sig + bckg)
    if action == "plot":


        DRAW_UNCERTAINTY_BAND = True
        NORMALISE_MC_TO_DATA  = False
        PLOT_NNLO_REWEIGHT    = False
        REMOVE_BACKGROUNDS    = False

        plots_produced += make_datamc_plot(master,
                                           channel,
                                           histconfig,
                                           systematic,
                                           DRAW_UNCERTAINTY_BAND,
                                           DRAW_MEAN_VALUES,
                                           PLOT_NNLO_REWEIGHT,
                                           REMOVE_BACKGROUNDS,
                                           NORMALISE_MC_TO_DATA,
                                           LEGEND_TOP_RIGHT,
                                           data_lumi[period],
                                           period)


    # Standard Data Vs MC plot, but with backgrounds removed
    if action == "rmbckg":

        syst_rmbckg = systematic + "_rmbckg" # for naming, hacky! TODO: tidy up!

        DRAW_UNCERTAINTY_BAND = True
        NORMALISE_MC_TO_DATA  = False
        COMPARE_TRUTH_RECO    = False
        PLOT_NNLO_REWEIGHT    = False
        REMOVE_BACKGROUNDS    = True


        plots_produced += make_datamc_plot(master,
                                           channel,
                                           histconfig,
                                           syst_rmbckg,
                                           DRAW_UNCERTAINTY_BAND,
                                           DRAW_MEAN_VALUES,
                                           PLOT_NNLO_REWEIGHT,
                                           REMOVE_BACKGROUNDS,
                                           NORMALISE_MC_TO_DATA,
                                           LEGEND_TOP_RIGHT,
                                           data_lumi[period],
                                           period)


    ## a simple ttbar mc plot
    if action == "mcplot":

        plots_produced += make_simple_mc_plot(master,
                                              channel,
                                              histconfig,
                                              systematic,
                                              LEGEND_TOP_RIGHT,
                                              DRAW_MEAN_VALUE,
                                              data_lumi[period],
                                              period)


    # reco lev dists for reweighted ttbar EFT samples
    if action == "eft":

        plots_produced += make_eft_plot(master,
                                        channel,
                                        histconfig,
                                        systematic,
                                        LEGEND_TOP_RIGHT,
                                        data_lumi[period],
                                        period)



    if action == "resolution":

        plots_produced += make_resolution_plot(master,
                                               channel,
                                               histconfig,
                                               systematic,
                                               LEGEND_TOP_RIGHT,
                                               data_lumi[period],
                                               period)




    # Truth Vs Reco plot
    if action == "truthcomp":

        syst_truth = systematic + "_truth" # for naming, hacky! TODO: tidy up!

        COMPARE_TRUTH_RECO = True
        COMPARE_NNLO       = False

        plots_produced += make_signal_comparison_plot(master,
                                                      channel,
                                                      histconfig,
                                                      syst_truth,
                                                      DRAW_MEAN_VALUES,
                                                      COMPARE_TRUTH_RECO,
                                                      COMPARE_NNLO,
                                                      LEGEND_TOP_RIGHT,
                                                      data_lumi[period],
                                                      period)

    # NNLO reweight plot using NNLO(randomTop_pT)
    if action   == "NNLOcomp":

        syst_NNLO = systematic + "_NNLO" # for naming, hacky! TODO: tidy up!

        COMPARE_TRUTH_RECO = False
        COMPARE_NNLO       = True

        plots_produced += make_signal_comparison_plot(master,
                                                      channel,
                                                      histconfig,
                                                      syst_NNLO,
                                                      DRAW_MEAN_VALUES,
                                                      COMPARE_TRUTH_RECO,
                                                      COMPARE_NNLO,
                                                      LEGEND_TOP_RIGHT,
                                                      data_lumi[period],
                                                      period)

print "\nDone!"

# print-out about file access faults
if file_failures > 0:
    print "\nWARNING:", file_failures, "/", hist_counter, "requested plots encountered problems related to file access.\n\n ---------------------------\n - File access -   hist\n ---------------------------"
    for i in range(file_failures):
        print " -  ", file_success[i][1], "  -", file_success[i][0]

# Final print-out (includes file access fault info again)
if hist_counter - plots_produced > 0:
    print "\nWARNING: Only", plots_produced, "/", hist_counter, "plots were produced!"
    if file_failures > 0:
        print "\nINFO: From the", hist_counter, "requests,", file_failures, "encountered file-access errors."
    print "\nINFO: From the", hist_counter, "requests,", hist_counter - plots_produced - file_failures, "encountered errors in the plotting functions.\n"
else:
    print "\nINFO: All",hist_counter, "plots successfully passed to plotting script\n"
