from os import environ

def readXsecK(file):
    f     =  open(file,'r')
    lines =  f.readlines()
    d     = {}
    for line in lines:
        l = line.split()
        if l==[] or l[0][0]=='#': continue
        d[l[0]]=[float(l[1]),float(l[2])]
    return d

# get location of spincorrelations repo (set in the setup.sh script)
spinhome = environ['SPINHOME']
 
path       = spinhome + "/user/addresses/"
xsec_file  = spinhome + "/datalists/XSection-MC15-13TeV.data"
xsec_K     = readXsecK(xsec_file)


samples = [      #file                #Xsec                  #Kfactor          #run number         #sys_type
    # Signal
    [path + "user.rlysak.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10724_p4031.AB-21.2.109-MC16eSignalNominal-v01_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16e"]
]
