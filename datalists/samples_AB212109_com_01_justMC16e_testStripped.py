from os import environ

## Please note, the full run 2 samples made with AnalysisBase 21.2.109 (AKA "round 1")
## have dissapeared from the orginal grid site but the mc16e subset, listed
## here is replicated fully to the following Glasgow grid site:
## UKI-SCOTGRID-GLASGOW-CEPH_DATADISK
## see "rucio list-file-replicas --help" for help

def readXsecK(file):
    f     =  open(file,'r')
    lines =  f.readlines()
    d     = {}
    for line in lines:
        l = line.split()
        if l==[] or l[0][0]=='#': continue
        d[l[0]]=[float(l[1]),float(l[2])]
    return d

# get location of spincorrelations repo (set in the setup.sh script)
spinhome = environ['SPINHOME']
 
path       = spinhome + "/user/addresses/"
xsec_file  = spinhome + "/datalists/XSection-MC15-13TeV.data"
xsec_K     = readXsecK(xsec_file)


samples = [      #file                #Xsec                  #Kfactor          #run number         #sample-type   #period
##########16e
##Signal
    [path + "user.rlysak.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10724_p4031.AB-21.2.109-MC16eSignalNominal-v01_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16e"],
##SingleTop
    [path + "user.fisopkov.410648.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10724_p4031.AB-21.2.109-MC16eBckgSingleTop-v01_out_root.txt",xsec_K['410648'][0], xsec_K['410648'][1], 410648, "singletop","16e"],
##Diboson
    [path + "user.thmegy.364250.Sherpa.DAOD_TOPQ1.e5894_s3126_r10724_p4029.AB-21.2.109-MC16eBckgDibosons-v01_out_root.txt",xsec_K['364250'][0], xsec_K['364250'][1], 364250, "diboson","16e"],
##Zjets_ee
    [path + "user.fisopkov.364114.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p4029.AB-21.2.109-MC16eBckgZjets-v01_out_root.txt", xsec_K['364114'][0], xsec_K['364114'][1], 364114, "zjets","16e"],
    [path + "user.thmegy.364197.Sherpa.DAOD_TOPQ1.e5340_s3126_r10724_p4029.AB-21.2.109-MC16eBckgWjets-v01_out_root.txt", xsec_K['364197'][0], xsec_K['364197'][1], 364197, "wjets","16e"],
###Fakes
    [path + "user.fisopkov.410470.PhPy8EG.DAOD_TOPQ1.e6337_s3126_r10724_p4031.AB-21.2.109-MC16eBckgFakes_ttbar-v01_out_root.txt", xsec_K['410470'][0], xsec_K['410470'][1], 410470, "fakes","16e"], 
### Data
    [path + "group.phys-top.periodAllYear.physics_Main.DAOD_TOPQ2.grp18_v01_p4030.AB-21.2.109-DATA18-v02_out_root.txt",-1.0, 1.0, "data", "data","16e"],
]
