from os import environ

def readXsecK(file):
    f     =  open(file,'r')
    lines =  f.readlines()
    d     = {}
    for line in lines:
        l = line.split()
        if l==[] or l[0][0]=='#': continue
        d[l[0]]=[float(l[1]),float(l[2])]
    return d

# get location of spincorrelations repo (set in the setup.sh script)
spinhome = environ['SPINHOME']
 
path       = spinhome + "/user/addresses/"
xsec_file  = spinhome + "/datalists/XSection-MC16-13TeV.data"
xsec_K     = readXsecK(xsec_file)


samples = [
### EFT
    [path + "user.nwarrack.504488.MGPy8.DAOD_TOPQ1.e8276_a875_r10724_p4346.AB-21.2.158-MC16eEFT-eftv02_out_root.txt", xsec_K['504488'][0], xsec_K['504488'][1], 504488, "ttbar","16e"],
]
