from os import environ

# Function for reading x-secs from file
def readXsecK(file):
    f     =  open(file,'r')
    lines =  f.readlines()
    d     = {}
    for line in lines:
        l = line.split()
        if l==[] or l[0][0]=='#': continue
        d[l[0]]=[float(l[1]),float(l[2])]
    return d

# get location of spincorrelations repo (set in the setup.sh script)
spinhome = environ['SPINHOME']

# set path to files containing the whereabouts of the actual data files (grid or local)
path       = spinhome + "/user/addresses/"

# get the cross sections and k-factors
xsec_file  = spinhome + "/datalists/XSection-MC16-13TeV_AB212169_com_02.data"
xsec_K     = readXsecK(xsec_file)


# Put the sample locations, cross sections, mc/data types, etc, in a list
samples = [

    # Format:
    #file  #Xsec  #Kfactor  #run number  #sample-type  #period

## 16e Signal
    [path + "group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10724_p4346.AB-21.2.169-MC16eSignalttbar-v04_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16e"],

## 16e EFT
    [path + "group.phys-top.504488.MGPy8.DAOD_TOPQ1.e8276_a875_r10724_p4346.AB-21.2.169-MC16eEFT-v04_out_root.txt", xsec_K['504488'][0], xsec_K['504488'][1], 504488, "EFT","16e"]
]
