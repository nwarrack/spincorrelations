from os import environ

# Function for reading x-secs from file
def readXsecK(file):
    f     =  open(file,'r')
    lines =  f.readlines()
    d     = {}
    for line in lines:
        l = line.split()
        if l==[] or l[0][0]=='#': continue
        d[l[0]]=[float(l[1]),float(l[2])]
    return d

# get location of spincorrelations repo (set in the setup.sh script)
spinhome = environ['SPINHOME']

# set path to files containing the whereabouts of the actual data files (grid or local)
path       = spinhome + "/user/addresses/"

# get the cross sections and k-factors
xsec_file  = spinhome + "/datalists/XSection-MC16-13TeV_AB212179_com_03.data"
xsec_K     = readXsecK(xsec_file)


# Put the sample locations, cross sections, mc/data types, etc, in a list
samples = [

    # Format:
    #[0]file  #[1]Xsec  #[2]Kfactor  #[3]run-number  #[4]sample-type #[5]period


######################## MC16a ##########################

## Nominal Signal ##
    [path + "group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r9364_p4514.AB-21.2.179-Signalttbar-v02_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16a"],


## EFT ##
    [path + "group.phys-top.504488.MGPy8.DAOD_TOPQ1.e8276_a875_r9364_p4346.AB-21.2.179-SignalEFT-v01_out_root.txt", xsec_K['504488'][0], xsec_K['504488'][1], 504488, "ttbar","16a"],


## ttX ##
# ttW aMC@NLO
    [path + "group.phys-top.410155.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_s3126_r9364_r9315_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410155'][0], xsec_K['410155'][1], 410155, "ttx","16a"],
# ttZ aMC@NLO (ttZnunu)
    [path + "group.phys-top.410156.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_s3126_r9364_r9315_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410156'][0], xsec_K['410156'][1], 410156, "ttx","16a"],
# ttZ aMC@NLO (ttZqq)
    [path + "group.phys-top.410157.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_s3126_r9364_r9315_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410157'][0], xsec_K['410157'][1], 410157, "ttx","16a"],
# ttH Powheg (allhad)
    [path + "group.phys-top.346343.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r9364_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['346343'][0], xsec_K['346343'][1], 346343, "ttx","16a"],
# ttH Powheg (semilep)
    [path + "group.phys-top.346344.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r9364_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['346344'][0], xsec_K['346344'][1], 346344, "ttx","16a"],
# ttH Powheg (dilep)
    [path + "group.phys-top.346345.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r9364_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['346345'][0], xsec_K['346345'][1], 346345, "ttx","16a"],
# ttWW Magraph + py8
    [path + "group.phys-top.410081.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4111_e5984_s3126_r10724_r10726_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410081'][0], xsec_K['410081'][1], 410081, "ttx","16a"],
# 4tops Madgraph + py8
    [path + "group.phys-top.410080.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4111_s3126_r9364_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410080'][0], xsec_K['410080'][1], 410080, "ttx","16a"],
# 3tops Madgraph + py8
    [path + "group.phys-top.304014.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4324_s3126_r9364_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['304014'][0], xsec_K['304014'][1], 304014, "ttx","16a"],


## Single Top ##
# Wt DR dilepton top
    [path + "group.phys-top.410648.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r9364_p4514.AB-21.2.179-BckgSingleTop-v01_out_root.txt",xsec_K['410648'][0], xsec_K['410648'][1], 410648, "singletop","16a"],
# Wt DR dilepton tbar
    [path + "group.phys-top.410649.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r9364_p4514.AB-21.2.179-BckgSingleTop-v01_out_root.txt",xsec_K['410649'][0], xsec_K['410649'][1], 410649, "singletop","16a"],


## Diboson ##
# (WWlvlv)
    [path + "group.phys-top.361600.PowhegPy8EG.DAOD_TOPQ1.e4616_s3126_r9364_p4344.AB-21.2.179-BckgDiboson-v03_out_root.txt",xsec_K['361600'][0], xsec_K['361600'][1], 361600, "diboson","16a"],
# (WZlvll)
    [path + "group.phys-top.361601.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r9364_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361601'][0], xsec_K['361601'][1], 361601, "diboson","16a"],
# (WZqqll)
    [path + "group.phys-top.361607.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r9364_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361607'][0], xsec_K['361607'][1], 361607, "diboson","16a"],
# (ZZllll)
    [path + "group.phys-top.361603.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r9364_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361603'][0], xsec_K['361603'][1], 361603, "diboson","16a"],
# (ZZvvll)
    [path + "group.phys-top.361604.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r9364_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361604'][0], xsec_K['361604'][1], 361604, "diboson","16a"],
# (ZZqqll)
    [path + "group.phys-top.361610.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r9364_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361610'][0], xsec_K['361610'][1], 361610, "diboson","16a"],
    

## Z + jets ##
# Zee Sherpa CvetoBveto
    [path + "group.phys-top.700127.Sh.DAOD_TOPQ1.e8118_s3126_r9364_r9315_p4512.AB-21.2.179-BckgDrellYan-v02_out_root",xsec_K['700127'][0], xsec_K['700127'][1], 700127, "zjets","16a"],
# Zee Sherpa CfilterBveto
    [path + "group.phys-top.700126.Sh.DAOD_TOPQ1.e8118_s3126_r9364_r9315_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700126'][0], xsec_K['700126'][1], 700126, "zjets","16a"],
# Zee Sherpa Bfilter
    [path + "group.phys-top.700125.Sh.DAOD_TOPQ1.e8118_s3126_r9364_r9315_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700125'][0], xsec_K['700125'][1], 700125, "zjets","16a"],
# Zmumu Sherpa CvetoBveto
    [path + "group.phys-top.700130.Sh.DAOD_TOPQ1.e8118_s3126_r9364_r9315_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700130'][0], xsec_K['700130'][1], 700130, "zjets","16a"],
# Zmumu Sherpa CfilterBveto
    [path + "group.phys-top.700129.Sh.DAOD_TOPQ1.e8118_s3126_r9364_r9315_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700129'][0], xsec_K['700129'][1], 700129, "zjets","16a"],
# Zmumu Sherpa Bfilter
    [path + "group.phys-top.700128.Sh.DAOD_TOPQ1.e8118_s3126_r9364_r9315_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700128'][0], xsec_K['700128'][1], 700128, "zjets","16a"],
# Ztautau Sherpa CvetoBveto
    [path + "group.phys-top.700133.Sh.DAOD_TOPQ1.e8118_s3126_r9364_r9315_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700133'][0], xsec_K['700133'][1], 700133, "zjets","16a"],
# Ztautau Sherpa CfilterBveto
    [path + "group.phys-top.700132.Sh.DAOD_TOPQ1.e8118_s3126_r9364_r9315_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700132'][0], xsec_K['700132'][1], 700132, "zjets","16a"],
# Ztautau Sherpa Bfilter
    [path + "group.phys-top.700131.Sh.DAOD_TOPQ1.e8118_s3126_r9364_r9315_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700131'][0], xsec_K['700131'][1], 700131, "zjets","16a"],

#ZjetsLowMass_ee
    [path + "group.phys-top.364204.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364204'][0], xsec_K['364204'][1], 364204, "zjets","16a"],
    [path + "group.phys-top.364205.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364205'][0], xsec_K['364205'][1], 364205, "zjets","16a"],
    [path + "group.phys-top.364206.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364206'][0], xsec_K['364206'][1], 364206, "zjets","16a"],
    [path + "group.phys-top.364207.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364207'][0], xsec_K['364207'][1], 364207, "zjets","16a"], 
    [path + "group.phys-top.364208.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364208'][0], xsec_K['364208'][1], 364208, "zjets","16a"], 
    [path + "group.phys-top.364209.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364209'][0], xsec_K['364209'][1], 364209, "zjets","16a"],
#ZjetsLowMass_mumu
    [path + "group.phys-top.364198.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364198'][0], xsec_K['364198'][1], 364198, "zjets","16a"],
    [path + "group.phys-top.364199.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364199'][0], xsec_K['364199'][1], 364199, "zjets","16a"],
    [path + "group.phys-top.364200.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364200'][0], xsec_K['364200'][1], 364200, "zjets","16a"],
    [path + "group.phys-top.364201.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364201'][0], xsec_K['364201'][1], 364201, "zjets","16a"],
    [path + "group.phys-top.364202.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364202'][0], xsec_K['364202'][1], 364202, "zjets","16a"],
    [path + "group.phys-top.364203.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364203'][0], xsec_K['364203'][1], 364203, "zjets","16a"],
#ZjetsLowMass_tautau
    [path + "group.phys-top.364210.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364210'][0], xsec_K['364210'][1], 364210, "zjets","16a"],
    [path + "group.phys-top.364211.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364211'][0], xsec_K['364211'][1], 364211, "zjets","16a"],
    [path + "group.phys-top.364212.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364212'][0], xsec_K['364212'][1], 364212, "zjets","16a"],
    [path + "group.phys-top.364213.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364213'][0], xsec_K['364213'][1], 364213, "zjets","16a"],
    [path + "group.phys-top.364214.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364214'][0], xsec_K['364214'][1], 364214, "zjets","16a"],
    [path + "group.phys-top.364215.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364215'][0], xsec_K['364215'][1], 364215, "zjets","16a"],


## W + jets ##
# Wjets_enu Sherpa Bfilter
    [path + "group.phys-top.700143.Sh.DAOD_TOPQ1.e8118_s3126_r9364_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700143'][0], xsec_K['700143'][1], 700143, "wjets","16a"],
# Wjets_enu Sherpa Cfilter Bveto
    [path + "group.phys-top.700144.Sh.DAOD_TOPQ1.e8118_s3126_r9364_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700144'][0], xsec_K['700144'][1], 700144, "wjets","16a"],
# Wjets_enu Sherpa Cveto Bveto
    [path + "group.phys-top.700145.Sh.DAOD_TOPQ1.e8118_s3126_r9364_p4512.AB-21.2.179-BckgFakes-v03_out_root.txt", xsec_K['700145'][0], xsec_K['700145'][1], 700145, "wjets","16a"],
# Wjets_munu Sherpa Bfilter
    [path + "group.phys-top.700146.Sh.DAOD_TOPQ1.e8118_s3126_r9364_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700146'][0], xsec_K['700146'][1], 700146, "wjets","16a"],
# Wjets_munu Sherpa Cfilter Bveto
    [path + "group.phys-top.700147.Sh.DAOD_TOPQ1.e8118_s3126_r9364_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700147'][0], xsec_K['700147'][1], 700147, "wjets","16a"],
# Wjets_munu Sherpa Cveto Bveto
    [path + "group.phys-top.700148.Sh.DAOD_TOPQ1.e8118_s3126_r9364_p4512.AB-21.2.179-BckgFakes-v03_out_root.txt", xsec_K['700148'][0], xsec_K['700148'][1], 700148, "wjets","16a"],
# Wjets_taunu Sherpa Bfilter
    [path + "group.phys-top.700149.Sh.DAOD_TOPQ1.e8118_s3126_r9364_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700149'][0], xsec_K['700149'][1], 700149, "wjets","16a"],
# Wjets_taunu Sherpa Cfilter B veto
    [path + "group.phys-top.700150.Sh.DAOD_TOPQ1.e8118_s3126_r9364_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700150'][0], xsec_K['700150'][1], 700150, "wjets","16a"],
# Wjets_taunu Sherpa Cveto Bveto
    [path + "group.phys-top.700151.Sh.DAOD_TOPQ1.e8118_s3126_r9364_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700151'][0], xsec_K['700151'][1], 700151, "wjets","16a"],


## Fakes ##
# non-all-hadronic
    [path + "group.phys-top.410470.PhPy8EG.DAOD_TOPQ1.e6337_s3126_r9364_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410470'][0], xsec_K['410470'][1], 410470, "fakes","16a"], 
# s-chan top
    [path + "group.phys-top.410644.PowhegPythia8EvtGen.DAOD_TOPQ1.e6527_s3126_r9364_p4514.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['410644'][0], xsec_K['410644'][1], 410644, "fakes","16a"],
# s-chan tbar  
    [path + "group.phys-top.410645.PowhegPythia8EvtGen.DAOD_TOPQ1.e6527_s3126_r9364_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410645'][0], xsec_K['410645'][1], 410645, "fakes","16a"],
# Wt DR inclusive top
    [path + "group.phys-top.410646.PowhegPythia8EvtGen.DAOD_TOPQ1.e6552_s3126_r9364_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410646'][0], xsec_K['410646'][1], 410646, "fakes","16a"],
# Wt DR inclusive tbar
    [path + "group.phys-top.410647.PowhegPythia8EvtGen.DAOD_TOPQ1.e6552_s3126_r9364_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410647'][0], xsec_K['410647'][1], 410647, "fakes","16a"], 
# t-chan top
    [path + "group.phys-top.410658.PhPy8EG.DAOD_TOPQ1.e6671_s3126_r9364_p4514.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['410658'][0], xsec_K['410658'][1], 410658, "fakes","16a"],
# t-chan tbar
    [path + "group.phys-top.410659.PhPy8EG.DAOD_TOPQ1.e6671_s3126_r9364_p4514.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['410659'][0], xsec_K['410659'][1], 410659, "fakes","16a"],


## Signal Systematics ##
# what is this??
    [path + "group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r9364_p4514.AB-21.2.179-Signalttbar-v02_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "signalsyst","16a"], 
# aMC@NLO Pythia8
    [path + "group.phys-top.410465.aMcAtNloPy8EvtGen.DAOD_TOPQ1.e6762_a875_r9364_p4514.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['410465'][0], xsec_K['410465'][1], 410465, "signalsyst","16a"], 
# Powheg + Herwig
    [path + "group.phys-top.410558.PowhegHerwig7EvtGen.DAOD_TOPQ1.e6366_a875_r9364_p4514.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['410558'][0], xsec_K['410558'][1], 410558, "signalsyst","16a"],
# (FAILED) ME off (AF2) (check AF2????)
    [path + "group.phys-top.411289.PhPy8EG.DAOD_TOPQ1.e7948_a875_r9364_p4346.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['411289'][0], xsec_K['411289'][1], 411289, "signalsyst","16a"], 
# (FAILED) ME off (check AF2????)
    [path + "group.phys-top.411289.PhPy8EG.DAOD_TOPQ1.e7948_e5984_a875_r9364_r9315_p4346.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['411289'][0], xsec_K['411289'][1], 411289, "signalsyst","16a"], 
# Sherpa
    [path + "group.phys-top.700124.Sh.DAOD_TOPQ1.e8253_e7400_s3126_r9364_r9315_p4434.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['700124'][0], xsec_K['700124'][1], 700124, "signalsyst","16a"], 

### Data ###
    [path + "group.phys-top.AllYear.physics_Main.DAOD_TOPQ2.grp15_v01_p4173.AB-21.2.179-Data-v01_out_root.txt",-1.0, 1.0, "data", "data","16a"],
    [path + "group.phys-top.AllYear.physics_Main.DAOD_TOPQ2.grp16_v01_p4173.AB-21.2.179-Data-v01_out_root.txt",-1.0, 1.0, "data", "data","16a"],




############################# MC16d ###############################

## Nominal Signal ##
    [path + "group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10201_p4514.AB-21.2.179-Signalttbar-v02_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16d"],


## EFT ##
    [path + "group.phys-top.504488.MGPy8.DAOD_TOPQ1.e8276_a875_r10201_p4346.AB-21.2.179-SignalEFT-v01_out_root.txt", xsec_K['504488'][0], xsec_K['504488'][1], 504488, "ttbar","16d"],


## ttX ##
# ttW aMC@NLO
    [path + "group.phys-top.410155.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_e5984_s3126_r10201_r10210_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410155'][0], xsec_K['410155'][1], 410155, "ttx","16d"],
# ttZ aMC@NLO (ttZnunu)
    [path + "group.phys-top.410156.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_e5984_s3126_r10201_r10210_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410156'][0], xsec_K['410156'][1], 410156, "ttx","16d"],
# ttZ aMC@NLO (ttZqq)
    [path + "group.phys-top.410157.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_e5984_s3126_r10201_r10210_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410157'][0], xsec_K['410157'][1], 410157, "ttx","16d"],
# ttH Powheg (allhad)
    [path + "group.phys-top.346343.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r10201_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['346343'][0], xsec_K['346343'][1], 346343, "ttx","16d"],
# ttH Powheg (semilep)
    [path + "group.phys-top.346344.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r10201_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['346344'][0], xsec_K['346344'][1], 346344, "ttx","16d"],
# ttH Powheg (dilep)
    [path + "group.phys-top.346345.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r10201_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['346345'][0], xsec_K['346345'][1], 346345, "ttx","16d"],
# ttWW Magraph + py8
    [path + "group.phys-top.410081.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4111_e5984_s3126_r10201_r10210_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410081'][0], xsec_K['410081'][1], 410081, "ttx","16d"],


## Multi-top ##
# 4tops Madgraph + py8
    [path + "group.phys-top.410080.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4111_s3126_r10201_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410080'][0], xsec_K['410080'][1], 410080, "mt","16d"],
# 3tops Madgraph + py8
    [path + "group.phys-top.304014.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4324_s3126_r10201_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['304014'][0], xsec_K['304014'][1], 304014, "mt","16d"],


## Single Top ##
# Wt DR dilepton top
    [path + "group.phys-top.410648.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10201_p4514.AB-21.2.179-BckgSingleTop-v01_out_root.txt",xsec_K['410648'][0], xsec_K['410648'][1], 410648, "singletop","16d"],
# Wt DR dilepton tbar
    [path + "group.phys-top.410649.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10201_p4514.AB-21.2.179-BckgSingleTop-v01_out_root.txt",xsec_K['410649'][0], xsec_K['410649'][1], 410649, "singletop","16d"],


## Diboson ##
# (WWlvlv)
    [path + "group.phys-top.361600.PowhegPy8EG.DAOD_TOPQ1.e4616_s3126_r10201_p4344.AB-21.2.179-BckgDiboson-v03_out_root.txt",xsec_K['361600'][0], xsec_K['361600'][1], 361600, "diboson","16d"],
# (WZlvll)
    [path + "group.phys-top.361601.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10201_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361601'][0], xsec_K['361601'][1], 361601, "diboson","16d"],
# (WZqqll)
    [path + "group.phys-top.361607.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10201_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361607'][0], xsec_K['361607'][1], 361607, "diboson","16d"],
# (ZZllll)
    [path + "group.phys-top.361603.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10201_p4344.AB-21.2.179-BckgDiboson-v03_out_root.txt",xsec_K['361603'][0], xsec_K['361603'][1], 361603, "diboson","16d"],
# (ZZvvll)
    [path + "group.phys-top.361604.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10201_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361604'][0], xsec_K['361604'][1], 361604, "diboson","16d"],
# (ZZqqll)
    [path + "group.phys-top.361610.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10201_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361610'][0], xsec_K['361610'][1], 361610, "diboson","16d"],
    

## Z + jets ##
# Zee Sherpa CvetoBveto
    [path + "group.phys-top.700127.Sh.DAOD_TOPQ1.e8118_s3126_r10201_r10210_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700127'][0], xsec_K['700127'][1], 700127, "zjets","16d"],
# Zee Sherpa CfilterBveto
    [path + "group.phys-top.700126.Sh.DAOD_TOPQ1.e8118_s3126_r10201_r10210_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700126'][0], xsec_K['700126'][1], 700126, "zjets","16d"],
# Zee Sherpa Bfilter
    [path + "group.phys-top.700125.Sh.DAOD_TOPQ1.e8118_s3126_r10201_r10210_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700125'][0], xsec_K['700125'][1], 700125, "zjets","16d"],
# Zmumu Sherpa CvetoBveto
    [path + "group.phys-top.700130.Sh.DAOD_TOPQ1.e8118_s3126_r10201_r10210_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700130'][0], xsec_K['700130'][1], 700130, "zjets","16d"],
# Zmumu Sherpa CfilterBveto
    [path + "group.phys-top.700129.Sh.DAOD_TOPQ1.e8118_s3126_r10201_r10210_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700129'][0], xsec_K['700129'][1], 700129, "zjets","16d"],
# Zmumu Sherpa Bfilter
    [path + "group.phys-top.700128.Sh.DAOD_TOPQ1.e8118_s3126_r10201_r10210_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700128'][0], xsec_K['700128'][1], 700128, "zjets","16d"],
# Ztautau Sherpa CvetoBveto
    [path + "group.phys-top.700133.Sh.DAOD_TOPQ1.e8118_s3126_r10201_r10210_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700133'][0], xsec_K['700133'][1], 700133, "zjets","16d"],
# Ztautau Sherpa CfilterBveto
    [path + "group.phys-top.700132.Sh.DAOD_TOPQ1.e8118_s3126_r10201_r10210_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700132'][0], xsec_K['700132'][1], 700132, "zjets","16d"],
# Ztautau Sherpa Bfilter
    [path + "group.phys-top.700131.Sh.DAOD_TOPQ1.e8118_s3126_r10201_r10210_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700131'][0], xsec_K['700131'][1], 700131, "zjets","16d"],

#ZjetsLowMass_ee
    [path + "group.phys-top.364204.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364204'][0], xsec_K['364204'][1], 364204, "zjets","16d"],
    [path + "group.phys-top.364205.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364205'][0], xsec_K['364205'][1], 364205, "zjets","16d"],
    [path + "group.phys-top.364206.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364206'][0], xsec_K['364206'][1], 364206, "zjets","16d"],
    [path + "group.phys-top.364207.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364207'][0], xsec_K['364207'][1], 364207, "zjets","16d"], 
    [path + "group.phys-top.364208.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364208'][0], xsec_K['364208'][1], 364208, "zjets","16d"], 
    [path + "group.phys-top.364209.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364209'][0], xsec_K['364209'][1], 364209, "zjets","16d"],
#ZjetsLowMass_mumu
    [path + "group.phys-top.364198.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364198'][0], xsec_K['364198'][1], 364198, "zjets","16d"],
    [path + "group.phys-top.364199.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364199'][0], xsec_K['364199'][1], 364199, "zjets","16d"],
    [path + "group.phys-top.364200.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364200'][0], xsec_K['364200'][1], 364200, "zjets","16d"],
    [path + "group.phys-top.364201.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364201'][0], xsec_K['364201'][1], 364201, "zjets","16d"],
    [path + "group.phys-top.364202.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364202'][0], xsec_K['364202'][1], 364202, "zjets","16d"],
    [path + "group.phys-top.364203.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364203'][0], xsec_K['364203'][1], 364203, "zjets","16d"],
#ZjetsLowMass_tautau
    [path + "group.phys-top.364210.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364210'][0], xsec_K['364210'][1], 364210, "zjets","16d"],
    [path + "group.phys-top.364211.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364211'][0], xsec_K['364211'][1], 364211, "zjets","16d"],
    [path + "group.phys-top.364212.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364212'][0], xsec_K['364212'][1], 364212, "zjets","16d"],
    [path + "group.phys-top.364213.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364213'][0], xsec_K['364213'][1], 364213, "zjets","16d"],
    [path + "group.phys-top.364214.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364214'][0], xsec_K['364214'][1], 364214, "zjets","16d"],
    [path + "group.phys-top.364215.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364215'][0], xsec_K['364215'][1], 364215, "zjets","16d"],


## W + jets ##
# Wjets_enu Sherpa Bfilter
    [path + "group.phys-top.700143.Sh.DAOD_TOPQ1.e8118_s3126_r10201_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700143'][0], xsec_K['700143'][1], 700143, "wjets","16d"],
# Wjets_enu Sherpa Cfilter Bveto
    [path + "group.phys-top.700144.Sh.DAOD_TOPQ1.e8118_s3126_r10201_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700144'][0], xsec_K['700144'][1], 700144, "wjets","16d"],
# Wjets_enu Sherpa Cveto Bveto
    [path + "group.phys-top.700145.Sh.DAOD_TOPQ1.e8118_s3126_r10201_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700145'][0], xsec_K['700145'][1], 700145, "wjets","16d"],
# Wjets_munu Sherpa Bfilter
    [path + "group.phys-top.700146.Sh.DAOD_TOPQ1.e8118_s3126_r10201_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700146'][0], xsec_K['700146'][1], 700146, "wjets","16d"],
# Wjets_munu Sherpa Cfilter Bveto
    [path + "group.phys-top.700147.Sh.DAOD_TOPQ1.e8118_s3126_r10201_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700147'][0], xsec_K['700147'][1], 700147, "wjets","16d"],
# Wjets_munu Sherpa Cveto Bveto
    [path + "group.phys-top.700148.Sh.DAOD_TOPQ1.e8118_s3126_r10201_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700148'][0], xsec_K['700148'][1], 700148, "wjets","16d"],
# Wjets_taunu Sherpa Bfilter
    [path + "group.phys-top.700149.Sh.DAOD_TOPQ1.e8118_s3126_r10201_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700149'][0], xsec_K['700149'][1], 700149, "wjets","16d"],
# Wjets_taunu Sherpa Cfilter B veto
    [path + "group.phys-top.700150.Sh.DAOD_TOPQ1.e8118_s3126_r10201_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700150'][0], xsec_K['700150'][1], 700150, "wjets","16d"],
# Wjets_taunu Sherpa Cveto Bveto
    [path + "group.phys-top.700151.Sh.DAOD_TOPQ1.e8118_s3126_r10201_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700151'][0], xsec_K['700151'][1], 700151, "wjets","16d"],


## Fakes ##
# non-all-hadronic
    [path + "group.phys-top.410470.PhPy8EG.DAOD_TOPQ1.e6337_s3126_r10201_p4514.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['410470'][0], xsec_K['410470'][1], 410470, "fakes","16d"], 
# s-chan top
    [path + "group.phys-top.410644.PowhegPythia8EvtGen.DAOD_TOPQ1.e6527_s3126_r10201_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410644'][0], xsec_K['410644'][1], 410644, "fakes","16d"],
# s-chan tbar  
    [path + "group.phys-top.410645.PowhegPythia8EvtGen.DAOD_TOPQ1.e6527_s3126_r10201_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410645'][0], xsec_K['410645'][1], 410645, "fakes","16d"],
# Wt DR inclusive top
    [path + "group.phys-top.410646.PowhegPythia8EvtGen.DAOD_TOPQ1.e6552_s3126_r10201_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410646'][0], xsec_K['410646'][1], 410646, "fakes","16d"],
# Wt DR inclusive tbar
    [path + "group.phys-top.410647.PowhegPythia8EvtGen.DAOD_TOPQ1.e6552_s3126_r10201_p4514.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['410647'][0], xsec_K['410647'][1], 410647, "fakes","16d"], 
# t-chan top
    [path + "group.phys-top.410658.PhPy8EG.DAOD_TOPQ1.e6671_s3126_r10201_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410658'][0], xsec_K['410658'][1], 410658, "fakes","16d"],
# t-chan tbar
    [path + "group.phys-top.410659.PhPy8EG.DAOD_TOPQ1.e6671_s3126_r10201_p4514.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['410659'][0], xsec_K['410659'][1], 410659, "fakes","16d"],


## Signal Systematics ##
# what is this??
    [path + "group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_a875_r10201_p4514.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "signalsyst","16d"], 
# aMC@NLO Pythia8
    [path + "group.phys-top.410465.aMcAtNloPy8EvtGen.DAOD_TOPQ1.e6762_a875_r10201_p4514.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['410465'][0], xsec_K['410465'][1], 410465, "signalsyst","16d"], 
# Powheg + Herwig
    [path + "group.phys-top.410558.PowhegHerwig7EvtGen.DAOD_TOPQ1.e6366_a875_r10201_p4514.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['410558'][0], xsec_K['410558'][1], 410558, "signalsyst","16d"],
# (FAILED) ME off (AF2) (check which is AF2!)
    [path + "group.phys-top.411289.PhPy8EG.DAOD_TOPQ1.e7948_e5984_a875_r10201_r10210_p4346.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['411289'][0], xsec_K['411289'][1], 411289, "signalsyst","16d"], 
# (FAILED) ME off (check which is AF2!)
    [path + "group.phys-top.411289.PhPy8EG.DAOD_TOPQ1.e7948_a875_r10201_p4346.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['411289'][0], xsec_K['411289'][1], 411289, "signalsyst","16d"], 
# Sherpa
    [path + "group.phys-top.700124.Sh.DAOD_TOPQ1.e8253_e7400_s3126_r10201_r10210_p4434.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['700124'][0], xsec_K['700124'][1], 700124, "signalsyst","16d"], 

### Data ###
    [path + "group.phys-top.AllYear.physics_Main.DAOD_TOPQ2.grp17_v01_p4173.AB-21.2.179-Data-v01_out_root.txt",-1.0, 1.0, "data", "data","16d"],



##################### MC16e ################################



## Nominal Signal ##
    [path + "group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10724_p4514.AB-21.2.179-Signalttbar-v02_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16e"],


## EFT ##
    [path + "group.phys-top.504488.MGPy8.DAOD_TOPQ1.e8276_a875_r10724_p4346.AB-21.2.179-SignalEFT-v01_out_root.txt", xsec_K['504488'][0], xsec_K['504488'][1], 504488, "ttbar","16e"],


## ttX ##
# ttW aMC@NLO
    [path + "group.phys-top.410155.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_e5984_s3126_r10724_r10726_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410155'][0], xsec_K['410155'][1], 410155, "ttx","16e"],
# ttZ aMC@NLO (ttZnunu)
    [path + "group.phys-top.410156.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_e5984_s3126_r10724_r10726_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410156'][0], xsec_K['410156'][1], 410156, "ttx","16e"],
# ttZ aMC@NLO (ttZqq)
    [path + "group.phys-top.410157.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_e5984_s3126_r10724_r10726_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410157'][0], xsec_K['410157'][1], 410157, "ttx","16e"],
# ttH Powheg (allhad)
    [path + "group.phys-top.346343.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r10724_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['346343'][0], xsec_K['346343'][1], 346343, "ttx","16e"],
# ttH Powheg (semilep)
    [path + "group.phys-top.346344.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r10724_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['346344'][0], xsec_K['346344'][1], 346344, "ttx","16e"],
# ttH Powheg (dilep)
    [path + "group.phys-top.346345.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r10724_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['346345'][0], xsec_K['346345'][1], 346345, "ttx","16e"],
# ttWW Magraph + py8
    [path + "group.phys-top.410081.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4111_e5984_s3126_r10724_r10726_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410081'][0], xsec_K['410081'][1], 410081, "ttx","16e"],


## Multi-top ##
# 4tops Madgraph + py8
    [path + "group.phys-top.410080.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4111_s3126_r10724_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['410080'][0], xsec_K['410080'][1], 410080, "mt","16e"],
# 3tops Madgraph + py8
    [path + "group.phys-top.304014.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4324_s3126_r10724_p4514.AB-21.2.179-BckgttX-v01_out_root.txt", xsec_K['304014'][0], xsec_K['304014'][1], 304014, "mt","16e"],


## Single Top ##
# Wt DR dilepton top
    [path + "group.phys-top.410648.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10724_p4514.AB-21.2.179-BckgSingleTop-v03_out_root.txt",xsec_K['410648'][0], xsec_K['410648'][1], 410648, "singletop","16e"],
# Wt DR dilepton tbar
    [path + "group.phys-top.410649.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10724_p4514.AB-21.2.179-BckgSingleTop-v01_out_root.txt",xsec_K['410649'][0], xsec_K['410649'][1], 410649, "singletop","16e"],


## Diboson ##
# (WWlvlv)
    [path + "group.phys-top.361600.PowhegPy8EG.DAOD_TOPQ1.e4616_s3126_r10724_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361600'][0], xsec_K['361600'][1], 361600, "diboson","16e"],
# (WZlvll)
    [path + "group.phys-top.361601.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10724_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361601'][0], xsec_K['361601'][1], 361601, "diboson","16e"],
# (WZqqll)
    [path + "group.phys-top.361607.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10724_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361607'][0], xsec_K['361607'][1], 361607, "diboson","16e"],
# (ZZllll)
    [path + "group.phys-top.361603.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10724_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361603'][0], xsec_K['361603'][1], 361603, "diboson","16e"],
# (ZZvvll)
    [path + "group.phys-top.361604.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10724_p4344.AB-21.2.179-BckgDiboson-v01_out_root.txt",xsec_K['361604'][0], xsec_K['361604'][1], 361604, "diboson","16e"],
# (ZZqqll)
    [path + "group.phys-top.361610.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10724_p4344.AB-21.2.179-BckgDiboson-v03_out_root.txt",xsec_K['361610'][0], xsec_K['361610'][1], 361610, "diboson","16e"],
    

## Z + jets ##
# Zee Sherpa CvetoBveto
    [path + "group.phys-top.700127.Sh.DAOD_TOPQ1.e8118_s3126_r10724_r10726_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700127'][0], xsec_K['700127'][1], 700127, "zjets","16e"],
# Zee Sherpa CfilterBveto
    [path + "group.phys-top.700126.Sh.DAOD_TOPQ1.e8118_s3126_r10724_r10726_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700126'][0], xsec_K['700126'][1], 700126, "zjets","16e"],
# Zee Sherpa Bfilter
    [path + "group.phys-top.700125.Sh.DAOD_TOPQ1.e8118_s3126_r10724_r10726_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700125'][0], xsec_K['700125'][1], 700125, "zjets","16e"],
# Zmumu Sherpa CvetoBveto
    [path + "group.phys-top.700130.Sh.DAOD_TOPQ1.e8118_s3126_r10724_r10726_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700130'][0], xsec_K['700130'][1], 700130, "zjets","16e"],
# Zmumu Sherpa CfilterBveto
    [path + "group.phys-top.700129.Sh.DAOD_TOPQ1.e8118_s3126_r10724_r10726_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700129'][0], xsec_K['700129'][1], 700129, "zjets","16e"],
# Zmumu Sherpa Bfilter
    [path + "group.phys-top.700128.Sh.DAOD_TOPQ1.e8118_s3126_r10724_r10726_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700128'][0], xsec_K['700128'][1], 700128, "zjets","16e"],
# Ztautau Sherpa CvetoBveto
    [path + "group.phys-top.700133.Sh.DAOD_TOPQ1.e8118_s3126_r10724_r10726_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700133'][0], xsec_K['700133'][1], 700133, "zjets","16e"],
# Ztautau Sherpa CfilterBveto
    [path + "group.phys-top.700132.Sh.DAOD_TOPQ1.e8118_s3126_r10724_r10726_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700132'][0], xsec_K['700132'][1], 700132, "zjets","16e"],
# Ztautau Sherpa Bfilter
    [path + "group.phys-top.700131.Sh.DAOD_TOPQ1.e8118_s3126_r10724_r10726_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt",xsec_K['700131'][0], xsec_K['700131'][1], 700131, "zjets","16e"],

#ZjetsLowMass_ee
    [path + "group.phys-top.364204.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364204'][0], xsec_K['364204'][1], 364204, "zjets","16e"],
    [path + "group.phys-top.364205.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364205'][0], xsec_K['364205'][1], 364205, "zjets","16e"],
    [path + "group.phys-top.364206.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364206'][0], xsec_K['364206'][1], 364206, "zjets","16e"],
    [path + "group.phys-top.364207.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364207'][0], xsec_K['364207'][1], 364207, "zjets","16e"], 
    [path + "group.phys-top.364208.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364208'][0], xsec_K['364208'][1], 364208, "zjets","16e"], 
    [path + "group.phys-top.364209.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364209'][0], xsec_K['364209'][1], 364209, "zjets","16e"],
#ZjetsLowMass_mumu
    [path + "group.phys-top.364198.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364198'][0], xsec_K['364198'][1], 364198, "zjets","16e"],
    [path + "group.phys-top.364199.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364199'][0], xsec_K['364199'][1], 364199, "zjets","16e"],
    [path + "group.phys-top.364200.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364200'][0], xsec_K['364200'][1], 364200, "zjets","16e"],
    [path + "group.phys-top.364201.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364201'][0], xsec_K['364201'][1], 364201, "zjets","16e"],
    [path + "group.phys-top.364202.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364202'][0], xsec_K['364202'][1], 364202, "zjets","16e"],
    [path + "group.phys-top.364203.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364203'][0], xsec_K['364203'][1], 364203, "zjets","16e"],
#ZjetsLowMass_tautau
    [path + "group.phys-top.364210.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364210'][0], xsec_K['364210'][1], 364210, "zjets","16e"],
    [path + "group.phys-top.364211.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364211'][0], xsec_K['364211'][1], 364211, "zjets","16e"],
    [path + "group.phys-top.364212.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364212'][0], xsec_K['364212'][1], 364212, "zjets","16e"],
    [path + "group.phys-top.364213.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364213'][0], xsec_K['364213'][1], 364213, "zjets","16e"],
    [path + "group.phys-top.364214.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364214'][0], xsec_K['364214'][1], 364214, "zjets","16e"],
    [path + "group.phys-top.364215.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.179-BckgDrellYan-v02_out_root.txt", xsec_K['364215'][0], xsec_K['364215'][1], 364215, "zjets","16e"],


## W + jets ##
# Wjets_enu Sherpa Bfilter
    [path + "group.phys-top.700143.Sh.DAOD_TOPQ1.e8118_s3126_r10724_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700143'][0], xsec_K['700143'][1], 700143, "wjets","16e"],
# Wjets_enu Sherpa Cfilter Bveto
    [path + "group.phys-top.700144.Sh.DAOD_TOPQ1.e8118_s3126_r10724_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700144'][0], xsec_K['700144'][1], 700144, "wjets","16e"],
# Wjets_enu Sherpa Cveto Bveto
    [path + "group.phys-top.700145.Sh.DAOD_TOPQ1.e8118_s3126_r10724_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700145'][0], xsec_K['700145'][1], 700145, "wjets","16e"],
# Wjets_munu Sherpa Bfilter
    [path + "group.phys-top.700146.Sh.DAOD_TOPQ1.e8118_s3126_r10724_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700146'][0], xsec_K['700146'][1], 700146, "wjets","16e"],
# Wjets_munu Sherpa Cfilter Bveto
    [path + "group.phys-top.700147.Sh.DAOD_TOPQ1.e8118_s3126_r10724_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700147'][0], xsec_K['700147'][1], 700147, "wjets","16e"],
# Wjets_munu Sherpa Cveto Bveto
    [path + "group.phys-top.700148.Sh.DAOD_TOPQ1.e8118_s3126_r10724_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700148'][0], xsec_K['700148'][1], 700148, "wjets","16e"],
# Wjets_taunu Sherpa Bfilter
    [path + "group.phys-top.700149.Sh.DAOD_TOPQ1.e8118_s3126_r10724_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700149'][0], xsec_K['700149'][1], 700149, "wjets","16e"],
# Wjets_taunu Sherpa Cfilter B veto
    [path + "group.phys-top.700150.Sh.DAOD_TOPQ1.e8118_s3126_r10724_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700150'][0], xsec_K['700150'][1], 700150, "wjets","16e"],
# Wjets_taunu Sherpa Cveto Bveto
    [path + "group.phys-top.700151.Sh.DAOD_TOPQ1.e8118_s3126_r10724_p4512.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['700151'][0], xsec_K['700151'][1], 700151, "wjets","16e"],


## Fakes ##
# non-all-hadronic
    [path + "group.phys-top.410470.PhPy8EG.DAOD_TOPQ1.e6337_s3126_r10724_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410470'][0], xsec_K['410470'][1], 410470, "fakes","16e"], 
# s-chan top
    [path + "group.phys-top.410644.PowhegPythia8EvtGen.DAOD_TOPQ1.e6527_s3126_r10724_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410644'][0], xsec_K['410644'][1], 410644, "fakes","16e"],
# s-chan tbar  
    [path + "group.phys-top.410645.PowhegPythia8EvtGen.DAOD_TOPQ1.e6527_s3126_r10724_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410645'][0], xsec_K['410645'][1], 410645, "fakes","16e"],
# Wt DR inclusive top
    [path + "group.phys-top.410646.PowhegPythia8EvtGen.DAOD_TOPQ1.e6552_s3126_r10724_p4514.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['410646'][0], xsec_K['410646'][1], 410646, "fakes","16e"],
# Wt DR inclusive tbar
    [path + "group.phys-top.410647.PowhegPythia8EvtGen.DAOD_TOPQ1.e6552_s3126_r10724_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410647'][0], xsec_K['410647'][1], 410647, "fakes","16e"], 
# t-chan top
    [path + "group.phys-top.410658.PhPy8EG.DAOD_TOPQ1.e6671_s3126_r10724_p4514.AB-21.2.179-BckgFakes-v01_out_root.txt", xsec_K['410658'][0], xsec_K['410658'][1], 410658, "fakes","16e"],
# t-chan tbar
    [path + "group.phys-top.410659.PhPy8EG.DAOD_TOPQ1.e6671_s3126_r10724_p4514.AB-21.2.179-BckgFakes-v02_out_root.txt", xsec_K['410659'][0], xsec_K['410659'][1], 410659, "fakes","16e"],


## Signal Systematics ##
# what is this??
    [path + "group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_a875_r10724_p4514.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "signalsyst","16e"], 
# aMC@NLO Pythia8
    [path + "group.phys-top.410465.aMcAtNloPy8EvtGen.DAOD_TOPQ1.e6762_a875_r10724_p4514.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['410465'][0], xsec_K['410465'][1], 410465, "signalsyst","16e"], 
# Powheg + Herwig
    [path + "group.phys-top.410558.PowhegHerwig7EvtGen.DAOD_TOPQ1.e6366_a875_r10724_p4514.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['410558'][0], xsec_K['410558'][1], 410558, "signalsyst","16e"],
    # (FAILED FILES???) ME off (AF2) (check AF2!)
    [path + "group.phys-top.411289.PhPy8EG.DAOD_TOPQ1.e7948_a875_r10724_p4346.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['411289'][0], xsec_K['411289'][1], 411289, "signalsyst","16e"], 
    # (FAILED FILES???) ME off (check AF2!)
    [path + "group.phys-top.411289.PhPy8EG.DAOD_TOPQ1.e7948_e5984_a875_r10724_r10726_p4346.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['411289'][0], xsec_K['411289'][1], 411289, "signalsyst","16e"], 
# Sherpa
    [path + "group.phys-top.700124.Sh.DAOD_TOPQ1.e8253_e7400_s3126_r10724_r10726_p4434.AB-21.2.179-SignalModelling-v01_out_root.txt", xsec_K['700124'][0], xsec_K['700124'][1], 700124, "signalsyst","16e"], 

### Data ###
    [path + "group.phys-top.AllYear.physics_Main.DAOD_TOPQ2.grp18_v01_p4173.AB-21.2.179-Data-v01_out_root.txt",-1.0, 1.0, "data", "data","16e"]
]
