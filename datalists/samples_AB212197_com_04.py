from os import environ

# Function for reading x-secs from file
def readXsecK(file):
    f     =  open(file,'r')
    lines =  f.readlines()
    d     = {}
    for line in lines:
        l = line.split()
        if l==[] or l[0][0]=='#': continue
        d[l[0]]=[float(l[1]),float(l[2])]
    return d

# get location of spincorrelations repo (set in the setup.sh script)
spinhome = environ['SPINHOME']

# set path to files containing the whereabouts of the actual data files (grid or local)
path       = spinhome + "/user/addresses/"

# get the cross sections and k-factors
xsec_file  = spinhome + "/datalists/XSection-MC16-13TeV_AB212179_com_03.data"
xsec_K     = readXsecK(xsec_file)


# Put the sample locations, cross sections, mc/data types, etc, in a list
samples = [

    # Format:
    #[0]file  #[1]Xsec  #[2]Kfactor  #[3]run-number  #[4]sample-type #[5]period

##################### MC16e ################################


## Nominal Signal ##
    [path + "group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10724_p4514.AB-21.2.197-MC16eSignal-v02_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16e"],


## EFT ##
#madgraph pythia8
[path + "group.phys-top.504488.MGPy8.DAOD_TOPQ1.e8276_a875_r10724_p4514.AB-21.2.197-MC16eModelling-v10_out_root.txt", xsec_K['504488'][0], xsec_K['504488'][1], 504488, "ttbar","16e"],


# ## ttX ##
# # ttW aMC@NLO
#     [path + "group.phys-top.410155.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_e5984_s3126_r10724_r10726_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410155'][0], xsec_K['410155'][1], 410155, "ttx","16e"],
# # ttZ aMC@NLO (ttZnunu)
#     [path + "group.phys-top.410156.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_e5984_s3126_r10724_r10726_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410156'][0], xsec_K['410156'][1], 410156, "ttx","16e"],
# # ttZ aMC@NLO (ttZqq)
#     [path + "group.phys-top.410157.aMcAtNloPythia8EvtGen.DAOD_TOPQ1.e5070_e5984_s3126_r10724_r10726_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410157'][0], xsec_K['410157'][1], 410157, "ttx","16e"],
# # ttH Powheg (allhad)
#     [path + "group.phys-top.346343.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['346343'][0], xsec_K['346343'][1], 346343, "ttx","16e"],
# # ttH Powheg (semilep)
#     [path + "group.phys-top.346344.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['346344'][0], xsec_K['346344'][1], 346344, "ttx","16e"],
# # ttH Powheg (dilep)
#     [path + "group.phys-top.346345.PhPy8EG.DAOD_TOPQ1.e7148_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['346345'][0], xsec_K['346345'][1], 346345, "ttx","16e"],
# # ttWW Magraph + py8
#     [path + "group.phys-top.410081.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4111_e5984_s3126_r10724_r10726_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410081'][0], xsec_K['410081'][1], 410081, "ttx","16e"],


# ## Multi-top ##
# # 4tops Madgraph + py8
#     [path + "group.phys-top.410080.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4111_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410080'][0], xsec_K['410080'][1], 410080, "mt","16e"],
# # 3tops Madgraph + py8
#     [path + "group.phys-top.304014.MadGraphPythia8EvtGen.DAOD_TOPQ1.e4324_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['304014'][0], xsec_K['304014'][1], 304014, "mt","16e"],


# ## Single Top ##
# # Wt DR dilepton top
#     [path + "group.phys-top.410648.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v04_out_root.txt",xsec_K['410648'][0], xsec_K['410648'][1], 410648, "singletop","16e"],
# # Wt DR dilepton tbar
#     [path + "group.phys-top.410649.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt",xsec_K['410649'][0], xsec_K['410649'][1], 410649, "singletop","16e"],


# ## Diboson ##
# # (WW->lvlv)
#     [path + "group.phys-top.361600.PowhegPy8EG.DAOD_TOPQ1.e4616_s3126_r10724_p4344.AB-21.2.197-MC16eBkgrd-v03_out_root.txt",xsec_K['361600'][0], xsec_K['361600'][1], 361600, "diboson","16e"],
# # (WZ->lvll)
#     [path + "group.phys-top.361601.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10724_p4344.AB-21.2.197-MC16eBkgrd-v03_out_root.txt",xsec_K['361601'][0], xsec_K['361601'][1], 361601, "diboson","16e"],
# # (WZ->qqll)
#     [path + "group.phys-top.361607.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10724_p4344.AB-21.2.197-MC16eBkgrd-v04_out_root.txt",xsec_K['361607'][0], xsec_K['361607'][1], 361607, "diboson","16e"],
# # (ZZ->llll)
#     [path + "group.phys-top.361603.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10724_p4344.AB-21.2.197-MC16eBkgrd-v03_out_root.txt",xsec_K['361603'][0], xsec_K['361603'][1], 361603, "diboson","16e"],
# # (ZZ->vvll)
#     [path + "group.phys-top.361604.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10724_p4344.AB-21.2.197-MC16eBkgrd-v03_out_root.txt",xsec_K['361604'][0], xsec_K['361604'][1], 361604, "diboson","16e"],
# # (ZZ->qqll)
#     [path + "group.phys-top.361610.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10724_p4344.AB-21.2.197-MC16eBkgrd-v03_out_root.txt",xsec_K['361610'][0], xsec_K['361610'][1], 361610, "diboson","16e"],
# # (llll) (Sherpa 222)
#     [path + "group.phys-top.364250.Sherpa.DAOD_TOPQ1.e5894_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364250'][0], xsec_K['364250'][1], 364250, "diboson","16e"], #<-new     
# # (lllv) (Sherpa 222)
#     [path + "group.phys-top.364253.Sherpa.DAOD_TOPQ1.e5916_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v04_out_root.txt", xsec_K['364253'][0], xsec_K['364253'][1], 364253, "diboson","16e"], #<-new 
#     # (llvv) (Sherpa 222)
#     [path + "group.phys-top.364254.Sherpa.DAOD_TOPQ1.e5916_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364254'][0], xsec_K['364254'][1], 364254, "diboson","16e"], #<-new 
#     # (lvvv) (Sherpa 222)
#     [path + "group.phys-top.364255.Sherpa.DAOD_TOPQ1.e5916_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364255'][0], xsec_K['364255'][1], 364255, "diboson","16e"], #<-new 
#     # (llll): Sherpa 222 lowMll Pt Comp
#     [path + "group.phys-top.364288.Sherpa.DAOD_TOPQ1.e6096_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364288'][0], xsec_K['364288'][1], 364288, "diboson","16e"], #<-new 
# # (llll): Sherpa 222 lowMll Pt Comp
#     [path + "group.phys-top.364289.Sherpa.DAOD_TOPQ1.e6133_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364289'][0], xsec_K['364289'][1], 364289, "diboson", "16e"], #<-new
#     # (llvv): Sherpa222 lowMll Pt Comp
# [path + "group.phys-top.364290.Sherpa.DAOD_TOPQ1.e6096_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364290'][0], xsec_K['364290'][1], 364290, "diboson","16e"], #<-new 


# ## Z + jets ##
# # Zee Sherpa CvetoBveto
# [path + "group.phys-top.700322.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700322'][0], xsec_K['700322'][1], 700322, "zjets","16e"], #<-new 
# # Zee Sherpa CfilterBveto
# [path + "group.phys-top.700321.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700321'][0], xsec_K['700321'][1], 700321, "zjets","16e"], #<-new 
# # Zee Sherpa Bfilter
# [path + "group.phys-top.700320.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700320'][0], xsec_K['700320'][1], 700320, "zjets","16e"], #<-new 
# # Zmumu Sherpa CvetoBveto
# [path + "group.phys-top.700325.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700325'][0], xsec_K['700325'][1], 700325, "zjets","16e"], #<-new 
# # Zmumu Sherpa CfilterBveto
# [path + "group.phys-top.700324.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700324'][0], xsec_K['700324'][1], 700324, "zjets","16e"], #<-new 
# # Zmumu Sherpa Bfilter
# [path + "group.phys-top.700323.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700323'][0], xsec_K['700323'][1], 700323, "zjets","16e"], #<-new 
# # Ztautau Sherpa CvetoBveto
# [path + "group.phys-top.700328.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700328'][0], xsec_K['700328'][1], 700328, "zjets","16e"], #<-new 
# # Ztautau Sherpa Bfilter
# [path + "group.phys-top.700326.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700326'][0], xsec_K['700326'][1], 700326, "zjets","16e"], #<-new 
# # Ztautau Sherpa CfilterBveto
# [path + "group.phys-top.700327.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700327'][0], xsec_K['700327'][1], 700327, "zjets","16e"], #<-new 

# #ZjetsLowMass_ee
#     [path + "group.phys-top.364204.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364204'][0], xsec_K['364204'][1], 364204, "zjets","16e"],
#     [path + "group.phys-top.364205.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364205'][0], xsec_K['364205'][1], 364205, "zjets","16e"],
#     [path + "group.phys-top.364206.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364206'][0], xsec_K['364206'][1], 364206, "zjets","16e"],
#     [path + "group.phys-top.364207.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364207'][0], xsec_K['364207'][1], 364207, "zjets","16e"], 
#     [path + "group.phys-top.364208.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364208'][0], xsec_K['364208'][1], 364208, "zjets","16e"], 
#     [path + "group.phys-top.364209.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364209'][0], xsec_K['364209'][1], 364209, "zjets","16e"],
# #ZjetsLowMass_mumu
#     [path + "group.phys-top.364198.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364198'][0], xsec_K['364198'][1], 364198, "zjets","16e"],
#     [path + "group.phys-top.364199.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364199'][0], xsec_K['364199'][1], 364199, "zjets","16e"],
#     [path + "group.phys-top.364200.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364200'][0], xsec_K['364200'][1], 364200, "zjets","16e"],
#     [path + "group.phys-top.364201.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364201'][0], xsec_K['364201'][1], 364201, "zjets","16e"],
#     [path + "group.phys-top.364202.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364202'][0], xsec_K['364202'][1], 364202, "zjets","16e"],
#     [path + "group.phys-top.364203.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364203'][0], xsec_K['364203'][1], 364203, "zjets","16e"],
# #ZjetsLowMass_tautau
#     [path + "group.phys-top.364210.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364210'][0], xsec_K['364210'][1], 364210, "zjets","16e"],
#     [path + "group.phys-top.364211.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364211'][0], xsec_K['364211'][1], 364211, "zjets","16e"],
#     [path + "group.phys-top.364212.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364212'][0], xsec_K['364212'][1], 364212, "zjets","16e"],
#     [path + "group.phys-top.364213.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364213'][0], xsec_K['364213'][1], 364213, "zjets","16e"],
#     [path + "group.phys-top.364214.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364214'][0], xsec_K['364214'][1], 364214, "zjets","16e"],
#     [path + "group.phys-top.364215.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['364215'][0], xsec_K['364215'][1], 364215, "zjets","16e"],


# ## W + jets ##
# # Wjets_enu Sherpa Bfilter
# [path + "group.phys-top.700338.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700338'][0], xsec_K['700338'][1], 700338, "wjets","16e"], #<-new 
# # Wjets_enu Sherpa Cfilter Bveto
# [path + "group.phys-top.700339.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700339'][0], xsec_K['700339'][1], 700339, "wjets","16e"], #<-new 
# # Wjets_enu Sherpa Cveto Bveto
# [path + "group.phys-top.700340.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700340'][0], xsec_K['700340'][1], 700340, "wjets","16e"], #<-new 
# # Wjets_munu Sherpa Bfilter
# [path + "group.phys-top.700341.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700341'][0], xsec_K['700341'][1], 700341, "wjets","16e"], #<-new 
# # Wjets_munu Sherpa Cfilter Bveto
# [path + "group.phys-top.700342.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700342'][0], xsec_K['700342'][1], 700342, "wjets","16e"], #<-new 
# # Wjets_munu Sherpa Cveto Bveto
# [path + "group.phys-top.700343.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700343'][0], xsec_K['700343'][1], 700343, "wjets","16e"], #<-new 
# # Wjets_taunu Sherpa L Bfilter
# [path + "group.phys-top.700344.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v04_out_root.txt", xsec_K['700344'][0], xsec_K['700344'][1], 700344, "wjets","16e"], #<-new 
# # Wjets_taunu Sherpa L Cfilter Bveto
# [path + "group.phys-top.700345.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700345'][0], xsec_K['700345'][1], 700345, "wjets","16e"], #<-new 
# # Wjets_taunu Sherpa L Cveto Bveto
# [path + "group.phys-top.700346.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700346'][0], xsec_K['700346'][1], 700346, "wjets","16e"], #<-new 
# # Wjets_taunu Sherpa H Bfilter
# [path + "group.phys-top.700347.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700347'][0], xsec_K['700347'][1], 700347, "wjets","16e"], #<-new 
# # Wjets_taunu Sherpa H Cfilter Bveto
# [path + "group.phys-top.700348.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700348'][0], xsec_K['700348'][1], 700348, "wjets","16e"], #<-new 
# # Wjets_taunu Sherpa H Cveto Bveto
# [path + "group.phys-top.700349.Sh.DAOD_TOPQ1.e8351_s3126_r10724_p4512.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['700349'][0], xsec_K['700349'][1], 700349, "wjets","16e"], #<-new 

# ## Fakes ##
# # non-all-hadronic
#     [path + "group.phys-top.410470.PhPy8EG.DAOD_TOPQ1.e6337_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410470'][0], xsec_K['410470'][1], 410470, "fakes","16e"], 
# # s-chan top
#     [path + "group.phys-top.410644.PowhegPythia8EvtGen.DAOD_TOPQ1.e6527_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410644'][0], xsec_K['410644'][1], 410644, "fakes","16e"],
# # s-chan tbar  
#     [path + "group.phys-top.410645.PowhegPythia8EvtGen.DAOD_TOPQ1.e6527_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410645'][0], xsec_K['410645'][1], 410645, "fakes","16e"],
# # Wt DR inclusive top
#     [path + "group.phys-top.410646.PowhegPythia8EvtGen.DAOD_TOPQ1.e6552_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410646'][0], xsec_K['410646'][1], 410646, "fakes","16e"],
# # Wt DR inclusive tbar
#     [path + "group.phys-top.410647.PowhegPythia8EvtGen.DAOD_TOPQ1.e6552_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410647'][0], xsec_K['410647'][1], 410647, "fakes","16e"], 
# # t-chan top
#     [path + "group.phys-top.410658.PhPy8EG.DAOD_TOPQ1.e6671_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410658'][0], xsec_K['410658'][1], 410658, "fakes","16e"],
# # t-chan tbar
#     [path + "group.phys-top.410659.PhPy8EG.DAOD_TOPQ1.e6671_s3126_r10724_p4514.AB-21.2.197-MC16eBkgrd-v03_out_root.txt", xsec_K['410659'][0], xsec_K['410659'][1], 410659, "fakes","16e"],


# ## Signal Systematics ##
#     [path + "group.phys-top.410472.PhPy8EG.DAOD_TOPQ1.e6348_a875_r10724_p4514.AB-21.2.197-MC16eModelling-v02_out_root.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "signalsyst","16e"], 
# # aMC@NLO Pythia8
#     [path + "group.phys-top.410465.aMcAtNloPy8EvtGen.DAOD_TOPQ1.e6762_a875_r10724_p4514.AB-21.2.197-MC16eModelling-v02_out_root.txt", xsec_K['410465'][0], xsec_K['410465'][1], 410465, "signalsyst","16e"], 
# # Powheg + Herwig704
#     [path + "group.phys-top.410558.PowhegHerwig7EvtGen.DAOD_TOPQ1.e6366_a875_r10724_p4514.AB-21.2.197-MC16eModelling-v02_out_root.txt", xsec_K['410558'][0], xsec_K['410558'][1], 410558, "signalsyst","16e"],
# # Powheg + Herwig713
#     [path + "group.phys-top.411234.PowhegHerwig7EvtGen.DAOD_TOPQ1.e7580_a875_r10724_p4514.AB-21.2.197-MC16eModelling-v02_out_root.txt", xsec_K['411234'][0], xsec_K['411234'][1], 411234, "signalsyst","16e"], #<-new
# # Powheg Pythia8 ME off (AF2)
#      [path + "group.phys-top.411289.PhPy8EG.DAOD_TOPQ1.e7948_a875_r10724_p4346.AB-21.2.197-MC16eModelling-v02_out_root.txt", xsec_K['411289'][0], xsec_K['411289'][1], 411289, "signalsyst","16e"], #<-new
# # aMC@NLO + Herwig713
#     [path + "group.phys-top.412117.aMcAtNloHerwig7EvtGen.DAOD_TOPQ1.e7620_a875_r10724_p4514.AB-21.2.197-MC16eModelling-v02_out_root.txt", xsec_K['412117'][0], xsec_K['412117'][1], 412117, "signalsyst", "16e"], #<-new 

### Data ### 
    [path + "group.phys-top.AllYear.physics_Main.DAOD_TOPQ2.grp18_v01_p4513.AB-21.2.197-Data18-v01_out_root.txt",-1.0, 1.0, "data", "data","16e"]
]
