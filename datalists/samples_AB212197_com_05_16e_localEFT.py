from os import environ

# Function for reading x-secs from file
def readXsecK(file):
    f     =  open(file,'r')
    lines =  f.readlines()
    d     = {}
    for line in lines:
        l = line.split()
        if l==[] or l[0][0]=='#': continue
        d[l[0]]=[float(l[1]),float(l[2])]
    return d

# get location of spincorrelations repo (set in the setup.sh script)
spinhome = environ['SPINHOME']

# set path to files containing the whereabouts of the actual data files (grid or local)
path       = spinhome + "/user/addresses/"

# get the cross sections and k-factors
xsec_file  = spinhome + "/datalists/XSection-MC16-13TeV_AB212179_com_03.data"
xsec_K     = readXsecK(xsec_file)


# Put the sample locations, cross sections, mc/data types, etc, in a list
samples = [

    # Format:
    #[0]file  #[1]Xsec  #[2]Kfactor  #[3]run-number  #[4]sample-type #[5]period

##################### MC16e ################################


## EFT ##
# todo make rnd 5 EFT?
    [path + "loc.504488.MGPy8_ttbar_dilept.txt", xsec_K['504488'][0], xsec_K['504488'][1], 504488, "ttbar","16e"],


]
