from xsecs import readXsecK
import ROOT
from ROOT import TChain
from os import environ

# get location of spincorrelations repo (set in the setup.sh script)
spinhome = environ['SPINHOME']
 
path = spinhome + "/data/addresses/"
xsec_file  = spinhome + "/data/bin/data_tools/data_lists/XSection-MC15-13TeV.data"
 

 
xsec_K = readXsecK(xsec_file)


samples = [      #file                #Xsec                  #Kfactor          #run number         #sys_type

#########16a
    [path + "user.pavol:user.pavol.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r9364_p3629.AT-21.2.58-MC16aSignal-14May_reco.txt",xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16a"],
    [path + "user.pavol:user.pavol.410648.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt",xsec_K['410648'][0], xsec_K['410648'][1], 410648, "singletop","16a"],
    [path + "user.pavol:user.pavol.410649.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt",xsec_K['410649'][0], xsec_K['410649'][1], 410649, "singletop","16a"],
    [path + "user.pavol:user.pavol.361600.PowhegPy8EG.DAOD_TOPQ1.e4616_s3126_r9364_p3526.AT-21.2.58-MC16aBckg-v2_reco.txt",xsec_K['361600'][0], xsec_K['361600'][1], 361600, "diboson","16a"],
    [path + "user.pavol:user.pavol.361601.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r9364_p3526.AT-21.2.58-MC16aBckg-v2_reco.txt",xsec_K['361601'][0], xsec_K['361601'][1], 361601, "diboson","16a"],
    [path + "user.pavol:user.pavol.361603.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r9364_p3526.AT-21.2.58-MC16aBckg-v2_reco.txt",xsec_K['361603'][0], xsec_K['361603'][1], 361603, "diboson","16a"],
    [path + "user.pavol:user.pavol.361604.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r9364_p3526.AT-21.2.58-MC16aBckg-v2_reco.txt",xsec_K['361604'][0], xsec_K['361604'][1], 361604, "diboson","16a"],
    [path + "user.pavol:user.pavol.361607.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r9364_p3526.AT-21.2.58-MC16aBckg-v2_reco.txt",xsec_K['361607'][0], xsec_K['361607'][1], 361607, "diboson","16a"],
    [path + "user.pavol:user.pavol.361610.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r9364_p3526.AT-21.2.58-MC16aBckg-v2_reco.txt",xsec_K['361610'][0], xsec_K['361610'][1], 361610, "diboson","16a"],
    [path + "user.pavol:user.pavol.364121.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364121'][0], xsec_K['364121'][1], 364121, "zjets","16a"],
    #[path + "user.pavol:user.pavol.364205.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364205'][0], xsec_K['364205'][1], 364205, "zjets","16a"],#empty dataset
    #[path + "user.pavol:user.pavol.364214.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364214'] [0], xsec_K['364214'][1], 364214, "zjets","16a"],#empty dataset
    [path + "user.pavol:user.pavol.364198.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364198'][0], xsec_K['364198'][1], 364198, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364207.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364207'][0], xsec_K['364207'][1], 364207, "zjets","16a"],
    [path + "user.pavol:user.pavol.364126.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt",  xsec_K['364126'][0], xsec_K['364126'][1], 364126, "zjets","16a"],
    #[path + "user.pavol:user.pavol.364210.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364210'][0], xsec_K['364210'][1], 364210, "zjets","16a"],#empty dataset
    [path + "user.pavol:user.pavol.364138.Sherpa.DAOD_TOPQ1.e5313_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364138'][0], xsec_K['364138'][1], 364138, "zjets","16a"],
    [path + "user.pavol:user.pavol.364127.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364127'][0], xsec_K['364127'][1], 364127, "zjets","16a"],
    [path + "user.pavol:user.pavol.364103.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364103'][0], xsec_K['364103'][1], 364103, "zjets","16a"],
    [path + "user.pavol:user.pavol.364212.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364212'][0], xsec_K['364212'][1], 364212, "zjets","16a"],
    #[path + "user.pavol:user.pavol.364124.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364124'][0], xsec_K['364124'][1], 364124, "zjets","16a"], #empty dataset
    [path + "user.pavol:user.pavol.364209.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364209'][0], xsec_K['364209'][1], 364209, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364213.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364213'][0], xsec_K['364213'][1], 364213, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364215.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364215'][0], xsec_K['364215'][1], 364215, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364101.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364101'][0], xsec_K['364101'][1], 364101, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364133.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364133'][0], xsec_K['364133'][1], 364133, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364100.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364100'][0], xsec_K['364100'][1], 364100, "zjets","16a"],
    #[path + "user.pavol:user.pavol.364211.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364211'][0], xsec_K['364211'][1], 364211, "zjets","16a"],#empty dataset
    [path + "user.pavol:user.pavol.364139.Sherpa.DAOD_TOPQ1.e5313_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364139'][0], xsec_K['364139'][1], 364139, "zjets","16a"],
    [path + "user.pavol:user.pavol.364131.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364131'][0], xsec_K['364131'][1], 364131, "zjets","16a"],
    [path + "user.pavol:user.pavol.364200.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364200'][0], xsec_K['364200'][1], 364200, "zjets","16a"],
    [path + "user.pavol:user.pavol.364132.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364132'][0], xsec_K['364132'][1], 364132, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364102.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364102'][0], xsec_K['364102'][1], 364102, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364117.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364117'][0], xsec_K['364117'][1], 364117, "zjets","16a"],  
    [path + "user.pavol:user.pavol.364116.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364116'][0], xsec_K['364116'][1], 364116, "zjets","16a"],
    [path + "user.pavol:user.pavol.364105.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364105'][0], xsec_K['364105'][1], 364105, "zjets","16a"],
    [path + "user.pavol:user.pavol.364112.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364112'][0], xsec_K['364112'][1], 364112, "zjets","16a"],
    [path + "user.pavol:user.pavol.364108.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364108'][0], xsec_K['364108'][1], 364108, "zjets","16a"],
    [path + "user.pavol:user.pavol.364208.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364208'][0], xsec_K['364208'][1], 364208, "zjets","16a"],
    [path + "user.pavol:user.pavol.364119.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364119'][0], xsec_K['364119'][1], 364119, "zjets","16a"],
    [path + "user.pavol:user.pavol.364125.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364125'][0], xsec_K['364125'][1], 364125, "zjets","16a"],
    [path + "user.pavol:user.pavol.364106.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364106'][0], xsec_K['364106'][1], 364106, "zjets","16a"],
    [path + "user.pavol:user.pavol.364123.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364123'][0], xsec_K['364123'][1], 364123, "zjets","16a"],
    [path + "user.pavol:user.pavol.364111.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364111'][0], xsec_K['364111'][1], 364111, "zjets","16a"],
    [path + "user.pavol:user.pavol.364130.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364130'][0], xsec_K['364130'][1], 364130, "zjets","16a"],
    [path + "user.pavol:user.pavol.364140.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364140'][0], xsec_K['364140'][1], 364140, "zjets","16a"],
    [path + "user.pavol:user.pavol.364201.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364201'][0], xsec_K['364201'][1], 364201, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364202.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364202'][0], xsec_K['364202'][1], 364202, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364120.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364120'][0], xsec_K['364120'][1], 364120, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364109.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364109'][0], xsec_K['364109'][1], 364109, "zjets","16a"],
    [path + "user.pavol:user.pavol.364114.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364114'][0], xsec_K['364114'][1], 364114, "zjets","16a"],
    [path + "user.pavol:user.pavol.364141.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364141'][0], xsec_K['364141'][1], 364141, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364134.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364134'][0], xsec_K['364134'][1], 364134, "zjets","16a"], 
    [path + "user.pavol:user.pavol.364107.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364107'][0], xsec_K['364107'][1], 364107, "zjets","16a"],
    [path + "user.pavol:user.pavol.364135.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364135'][0], xsec_K['364135'][1], 364135, "zjets","16a"],
    [path + "user.pavol:user.pavol.364122.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364122'][0], xsec_K['364122'][1], 364122, "zjets","16a"],
    [path + "user.pavol:user.pavol.364137.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364137'][0], xsec_K['364137'][1], 364137, "zjets","16a"],
    [path + "user.pavol:user.pavol.364115.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364115'][0], xsec_K['364115'][1], 364115, "zjets","16a"],
    [path + "user.pavol:user.pavol.364104.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364104'][0], xsec_K['364104'][1], 364104, "zjets","16a"],
    [path + "user.pavol:user.pavol.364203.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364203'][0], xsec_K['364203'][1], 364203, "zjets","16a"],
    #[path + "user.pavol:user.pavol.364199.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364199'][0], xsec_K['364199'][1], 364199, "zjets","16a"],#empty dataset
    [path + "user.pavol:user.pavol.364113.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364113'][0], xsec_K['364113'][1], 364113, "zjets","16a"],
    [path + "user.pavol:user.pavol.364204.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364204'][0], xsec_K['364204'][1], 364204, "zjets","16a"],
    [path + "user.pavol:user.pavol.364136.Sherpa.DAOD_TOPQ1.e5307_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364136'][0], xsec_K['364136'][1], 364136, "zjets","16a"],
    [path + "user.pavol:user.pavol.364118.Sherpa.DAOD_TOPQ1.e5299_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364118'][0], xsec_K['364118'][1], 364118, "zjets","16a"],
    [path + "user.pavol:user.pavol.364206.Sherpa.DAOD_TOPQ1.e5421_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364206'][0], xsec_K['364206'][1], 364206, "zjets","16a"],
    [path + "user.pavol:user.pavol.364110.Sherpa.DAOD_TOPQ1.e5271_s3126_r9364_p3629.AT-21.2.58-MC16aBckg-v2_reco.txt", xsec_K['364110'][0], xsec_K['364110'][1], 364110, "zjets","16a"],



##########16d
    [path + "user.pavol:user.pavol.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10201_p3629.AT-21.2.58-MC16dSignal-14May_v3_reco.txt", xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16d"],
    [path + "user.pavol:user.pavol.410648.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt",xsec_K['410648'][0], xsec_K['410648'][1], 410648, "singletop","16d"],
    [path + "user.pavol:user.pavol.410649.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt",xsec_K['410649'][0], xsec_K['410649'][1], 410649, "singletop","16d"],
    [path + "user.pavol:user.pavol.361600.PowhegPy8EG.DAOD_TOPQ1.e4616_s3126_r10201_p3526.AT-21.2.58-MC16dBckg-v2_reco.txt",xsec_K['361600'][0], xsec_K['361600'][1], 361600, "diboson","16d"],
    [path + "user.pavol:user.pavol.361601.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10201_p3526.AT-21.2.58-MC16dBckg-v2_reco.txt",xsec_K['361601'][0], xsec_K['361601'][1], 361601, "diboson","16d"],
    [path + "user.pavol:user.pavol.361603.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10201_p3526.AT-21.2.58-MC16dBckg-v2_reco.txt",xsec_K['361603'][0], xsec_K['361603'][1], 361603, "diboson","16d"],
    [path + "user.pavol:user.pavol.361604.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10201_p3526.AT-21.2.58-MC16dBckg-v2_reco.txt",xsec_K['361604'][0], xsec_K['361604'][1], 361604, "diboson","16d"],
    [path + "user.pavol:user.pavol.361607.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10201_p3526.AT-21.2.58-MC16dBckg-v2_reco.txt",xsec_K['361607'][0], xsec_K['361607'][1], 361607, "diboson","16d"],
    [path + "user.pavol:user.pavol.361610.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10201_p3526.AT-21.2.58-MC16dBckg-v2_reco.txt",xsec_K['361610'][0], xsec_K['361610'][1], 361610, "diboson","16d"],
    [path + "user.pavol:user.pavol.364139.Sherpa.DAOD_TOPQ1.e5313_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364139'][0], xsec_K['364139'][1], 364139, "zjets","16d"],
    [path + "user.pavol:user.pavol.364119.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364119'][0], xsec_K['364119'][1], 364119, "zjets","16d"],
    [path + "user.pavol:user.pavol.364102.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364102'][0], xsec_K['364102'][1], 364102, "zjets","16d"],
    [path + "user.pavol:user.pavol.364214.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364214'][0], xsec_K['364214'][1], 364214, "zjets","16d"],
    [path + "user.pavol:user.pavol.364202.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364202'][0], xsec_K['364202'][1], 364202, "zjets","16d"],
    #[path + "user.pavol:user.pavol.364211.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364211'][0], xsec_K['364211'][1], 364211, "zjets","16d"],#empty dataset
    [path + "user.pavol:user.pavol.364100.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364100'][0], xsec_K['364100'][1], 364100, "zjets","16d"],
    [path + "user.pavol:user.pavol.364125.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364125'][0], xsec_K['364125'][1], 364125, "zjets","16d"],
    [path + "user.pavol:user.pavol.364116.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364116'][0], xsec_K['364116'][1], 364116, "zjets","16d"],
    [path + "user.pavol:user.pavol.364199.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364199'][0], xsec_K['364199'][1], 364199, "zjets","16d"],
    [path + "user.pavol:user.pavol.364131.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364131'][0], xsec_K['364131'][1], 364131, "zjets","16d"],
    [path + "user.pavol:user.pavol.364121.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364121'][0], xsec_K['364121'][1], 364121, "zjets","16d"], 
    [path + "user.pavol:user.pavol.364207.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364207'][0], xsec_K['364207'][1], 364207, "zjets","16d"], 
    [path + "user.pavol:user.pavol.364115.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364115'][0], xsec_K['364115'][1], 364115, "zjets","16d"],
    [path + "user.pavol:user.pavol.364135.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364135'][0], xsec_K['364135'][1], 364135, "zjets","16d"],
    [path + "user.pavol:user.pavol.364130.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364130'][0], xsec_K['364130'][1], 364130, "zjets","16d"],
    [path + "user.pavol:user.pavol.364132.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364132'][0], xsec_K['364132'][1], 364132, "zjets","16d"],
    [path + "user.pavol:user.pavol.364108.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364108'][0], xsec_K['364108'][1], 364108, "zjets","16d"],
    [path + "user.pavol:user.pavol.364215.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364215'][0], xsec_K['364215'][1], 364215, "zjets","16d"],
    [path + "user.pavol:user.pavol.364205.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364205'][0], xsec_K['364205'][1], 364205, "zjets","16d"],
    [path + "user.pavol:user.pavol.364198.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364198'][0], xsec_K['364198'][1], 364198, "zjets","16d"],
    [path + "user.pavol:user.pavol.364118.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364118'][0], xsec_K['364118'][1], 364118, "zjets","16d"],
    #[path + "user.pavol:user.pavol.364210.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364210'][0], xsec_K['364210'][1], 364210, "zjets","16d"],#empty dataset
    [path + "user.pavol:user.pavol.364203.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364203'][0], xsec_K['364203'][1], 364203, "zjets","16d"],
    [path + "user.pavol:user.pavol.364103.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364103'][0], xsec_K['364103'][1], 364103, "zjets","16d"],
    [path + "user.pavol:user.pavol.364114.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364114'][0], xsec_K['364114'][1], 364114, "zjets","16d"],
    [path + "user.pavol:user.pavol.364126.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364126'][0], xsec_K['364126'][1], 364126, "zjets","16d"],
    [path + "user.pavol:user.pavol.364140.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364140'][0], xsec_K['364140'][1], 364140, "zjets","16d"],
    [path + "user.pavol:user.pavol.364213.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364213'][0], xsec_K['364213'][1], 364213, "zjets","16d"],
    [path + "user.pavol:user.pavol.364206.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364206'][0], xsec_K['364206'][1], 364206, "zjets","16d"],
    [path + "user.pavol:user.pavol.364200.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364200'][0], xsec_K['364200'][1], 364200, "zjets","16d"],
    [path + "user.pavol:user.pavol.364123.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364123'][0], xsec_K['364123'][1], 364123, "zjets","16d"],
    [path + "user.pavol:user.pavol.364122.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364122'][0], xsec_K['364122'][1], 364122, "zjets","16d"],
    [path + "user.pavol:user.pavol.364106.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364106'][0], xsec_K['364106'][1], 364106, "zjets","16d"],
    [path + "user.pavol:user.pavol.364137.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364137'][0], xsec_K['364137'][1], 364137, "zjets","16d"],
    [path + "user.pavol:user.pavol.364209.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364209'][0], xsec_K['364209'][1], 364209, "zjets","16d"],
    [path + "user.pavol:user.pavol.364136.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364136'][0], xsec_K['364136'][1], 364136, "zjets","16d"],
    [path + "user.pavol:user.pavol.364124.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364124'][0], xsec_K['364124'][1], 364124, "zjets","16d"],
    [path + "user.pavol:user.pavol.364110.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364110'][0], xsec_K['364110'][1], 364110, "zjets","16d"],
    [path + "user.pavol:user.pavol.364105.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364105'][0], xsec_K['364105'][1], 364105, "zjets","16d"],
    [path + "user.pavol:user.pavol.364101.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364101'][0], xsec_K['364101'][1], 364101, "zjets","16d"],
    [path + "user.pavol:user.pavol.364107.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364107'][0], xsec_K['364107'][1], 364107, "zjets","16d"],
    [path + "user.pavol:user.pavol.364208.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364208'][0], xsec_K['364208'][1], 364208, "zjets","16d"],
    [path + "user.pavol:user.pavol.364120.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364120'][0], xsec_K['364120'][1], 364120, "zjets","16d"],
    [path + "user.pavol:user.pavol.364109.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364109'][0], xsec_K['364109'][1], 364109, "zjets","16d"], 
    [path + "user.pavol:user.pavol.364133.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364133'][0], xsec_K['364133'][1], 364133, "zjets","16d"],
    [path + "user.pavol:user.pavol.364138.Sherpa.DAOD_TOPQ1.e5313_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364138'][0], xsec_K['364138'][1], 364138, "zjets","16d"],
    [path + "user.pavol:user.pavol.364127.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364127'][0], xsec_K['364127'][1], 364127, "zjets","16d"],
    [path + "user.pavol:user.pavol.364117.Sherpa.DAOD_TOPQ1.e5299_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364117'][0], xsec_K['364117'][1], 364117, "zjets","16d"],
    [path + "user.pavol:user.pavol.364112.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364112'][0], xsec_K['364112'][1], 364112, "zjets","16d"],
    [path + "user.pavol:user.pavol.364104.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364104'][0], xsec_K['364104'][1], 364104, "zjets","16d"],
    [path + "user.pavol:user.pavol.364111.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364111'][0], xsec_K['364111'][1], 364111, "zjets","16d"],
    [path + "user.pavol:user.pavol.364134.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364134'][0], xsec_K['364134'][1], 364134, "zjets","16d"],
    #[path + "user.pavol:user.pavol.364204.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364204'][0], xsec_K['364204'][1], 364204, "zjets","16d"],#empty dataset
    [path + "user.pavol:user.pavol.364201.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364201'][0], xsec_K['364201'][1], 364201, "zjets","16d"],
    [path + "user.pavol:user.pavol.364113.Sherpa.DAOD_TOPQ1.e5271_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364113'][0], xsec_K['364113'][1], 364113, "zjets","16d"], 
    [path + "user.pavol:user.pavol.364141.Sherpa.DAOD_TOPQ1.e5307_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364141'][0], xsec_K['364141'][1], 364141, "zjets","16d"],
    [path + "user.pavol:user.pavol.364212.Sherpa.DAOD_TOPQ1.e5421_s3126_r10201_p3629.AT-21.2.58-MC16dBckg-v2_reco.txt", xsec_K['364212'][0], xsec_K['364212'][1], 364212, "zjets","16d"],
    
    
    
    ##########16e
    [path + "user.pavol:user.pavol.410472.PhPy8EG.DAOD_TOPQ1.e6348_s3126_r10724_p3629.AT-21.2.58-MC16eSignal-14May_reco.txt",xsec_K['410472'][0], xsec_K['410472'][1], 410472, "ttbar","16e"],
    [path + "user.pavol:user.pavol.410648.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt",xsec_K['410648'][0], xsec_K['410648'][1], 410648, "singletop","16e"],
    [path + "user.pavol:user.pavol.410649.PowhegPythia8EvtGen.DAOD_TOPQ1.e6615_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt",xsec_K['410649'][0], xsec_K['410649'][1], 410649, "singletop","16e"],
    [path + "user.pavol:user.pavol.361600.PowhegPy8EG.DAOD_TOPQ1.e4616_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt",xsec_K['361600'][0], xsec_K['361600'][1], 361600, "diboson","16e"],
    [path + "user.pavol:user.pavol.361601.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt",xsec_K['361601'][0], xsec_K['361601'][1], 361601, "diboson","16e"],
    [path + "user.pavol:user.pavol.361603.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt",xsec_K['361603'][0], xsec_K['361603'][1], 361603, "diboson","16e"],
    [path + "user.pavol:user.pavol.361604.PowhegPy8EG.DAOD_TOPQ1.e4475_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt",xsec_K['361604'][0], xsec_K['361604'][1], 361604, "diboson","16e"],
    [path + "user.pavol:user.pavol.361607.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt",xsec_K['361607'][0], xsec_K['361607'][1], 361607, "diboson","16e"],
    [path + "user.pavol:user.pavol.361610.PowhegPy8EG.DAOD_TOPQ1.e4711_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt",xsec_K['361610'][0], xsec_K['361610'][1], 361610, "diboson","16e"],
    [path + "user.pavol:user.pavol.364138.Sherpa.DAOD_TOPQ1.e5313_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364138'][0], xsec_K['364138'][1], 364138, "zjets","16e"],
    [path + "user.pavol:user.pavol.364115.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364115'][0], xsec_K['364115'][1], 364115, "zjets","16e"],
    [path + "user.pavol:user.pavol.364130.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364130'][0], xsec_K['364130'][1], 364130, "zjets","16e"],
    [path + "user.pavol:user.pavol.364214.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364214'][0], xsec_K['364214'][1], 364214, "zjets","16e"],
    [path + "user.pavol:user.pavol.364141.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364141'][0], xsec_K['364141'][1], 364141, "zjets","16e"],
    [path + "user.pavol:user.pavol.364117.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364117'][0], xsec_K['364117'][1], 364117, "zjets","16e"],
    [path + "user.pavol:user.pavol.364136.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364136'][0], xsec_K['364136'][1], 364136, "zjets","16e"],
    [path + "user.pavol:user.pavol.364100.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364100'][0], xsec_K['364100'][1], 364100, "zjets","16e"],
    [path + "user.pavol:user.pavol.364140.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364140'][0], xsec_K['364140'][1], 364140, "zjets","16e"],
    [path + "user.pavol:user.pavol.364113.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364113'][0], xsec_K['364113'][1], 364113, "zjets","16e"],
    [path + "user.pavol:user.pavol.364109.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364109'][0], xsec_K['364109'][1], 364109, "zjets","16e"],
    [path + "user.pavol:user.pavol.364126.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364126'][0], xsec_K['364126'][1], 364126, "zjets","16e"],
    #[path + "user.pavol:user.pavol.364204.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364204'][0], xsec_K['364204'][1], 364204, "zjets","16e"],#empty dataset
    #[path + "user.pavol:user.pavol.364211.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364211'][0], xsec_K['364211'][1], 364211, "zjets","16e"],#empty dataset
    [path + "user.pavol:user.pavol.364203.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364203'][0], xsec_K['364203'][1], 364203, "zjets","16e"],
    [path + "user.pavol:user.pavol.364132.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364132'][0], xsec_K['364132'][1], 364132, "zjets","16e"],
    [path + "user.pavol:user.pavol.364133.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364133'][0], xsec_K['364133'][1], 364133, "zjets","16e"],
    [path + "user.pavol:user.pavol.364125.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364125'][0], xsec_K['364125'][1], 364125, "zjets","16e"], 
    [path + "user.pavol:user.pavol.364205.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364205'][0], xsec_K['364205'][1], 364205, "zjets","16e"], 
    [path + "user.pavol:user.pavol.364102.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364102'][0], xsec_K['364102'][1], 364102, "zjets","16e"],
    [path + "user.pavol:user.pavol.364207.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364207'][0], xsec_K['364207'][1], 364207, "zjets","16e"],
    [path + "user.pavol:user.pavol.364112.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364112'][0], xsec_K['364112'][1], 364112, "zjets","16e"],
    [path + "user.pavol:user.pavol.364201.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364201'][0], xsec_K['364201'][1], 364201, "zjets","16e"],
    [path + "user.pavol:user.pavol.364121.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364121'][0], xsec_K['364121'][1], 364121, "zjets","16e"],
    [path + "user.pavol:user.pavol.364120.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364120'][0], xsec_K['364120'][1], 364120, "zjets","16e"],
    [path + "user.pavol:user.pavol.364119.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364119'][0], xsec_K['364119'][1], 364119, "zjets","16e"],
    [path + "user.pavol:user.pavol.364131.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364131'][0], xsec_K['364131'][1], 364131, "zjets","16e"],   
    [path + "user.pavol:user.pavol.364209.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364209'][0], xsec_K['364209'][1], 364209, "zjets","16e"],
    [path + "user.pavol:user.pavol.364114.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364114'][0], xsec_K['364114'][1], 364114, "zjets","16e"],
    #[path + "user.pavol:user.pavol.364198.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364198'][0], xsec_K['364198'][1], 364198, "zjets","16e"], #empty dataset
    [path + "user.pavol:user.pavol.364127.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364127'][0], xsec_K['364127'][1], 364127, "zjets","16e"],
    [path + "user.pavol:user.pavol.364105.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364105'][0], xsec_K['364105'][1], 364105, "zjets","16e"],
    [path + "user.pavol:user.pavol.364135.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364135'][0], xsec_K['364135'][1], 364135, "zjets","16e"],
    [path + "user.pavol:user.pavol.364110.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364110'][0], xsec_K['364110'][1], 364110, "zjets","16e"],
    [path + "user.pavol:user.pavol.364123.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364123'][0], xsec_K['364123'][1], 364123, "zjets","16e"],
    [path + "user.pavol:user.pavol.364200.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364200'][0], xsec_K['364200'][1], 364200, "zjets","16e"],
    [path + "user.pavol:user.pavol.364137.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364137'][0], xsec_K['364137'][1], 364137, "zjets","16e"],
    [path + "user.pavol:user.pavol.364101.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364101'][0], xsec_K['364101'][1], 364101, "zjets","16e"],
    [path + "user.pavol:user.pavol.364116.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364116'][0], xsec_K['364116'][1], 364116, "zjets","16e"],
    [path + "user.pavol:user.pavol.364139.Sherpa.DAOD_TOPQ1.e5313_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364139'][0], xsec_K['364139'][1], 364139, "zjets","16e"],
    [path + "user.pavol:user.pavol.364212.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364212'][0], xsec_K['364212'][1], 364212, "zjets","16e"],
    [path + "user.pavol:user.pavol.364124.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364124'][0], xsec_K['364124'][1], 364124, "zjets","16e"],
    [path + "user.pavol:user.pavol.364107.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364107'][0], xsec_K['364107'][1], 364107, "zjets","16e"],
    [path + "user.pavol:user.pavol.364106.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364106'][0], xsec_K['364106'][1], 364106, "zjets","16e"],
    [path + "user.pavol:user.pavol.364213.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364213'][0], xsec_K['364213'][1], 364213, "zjets","16e"],
    [path + "user.pavol:user.pavol.364111.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364111'][0], xsec_K['364111'][1], 364111, "zjets","16e"],
    [path + "user.pavol:user.pavol.364206.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364206'][0], xsec_K['364206'][1], 364206, "zjets","16e"],
    [path + "user.pavol:user.pavol.364104.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364104'][0], xsec_K['364104'][1], 364104, "zjets","16e"],
    [path + "user.pavol:user.pavol.364199.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364199'][0], xsec_K['364199'][1], 364199, "zjets","16e"],
    [path + "user.pavol:user.pavol.364118.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364118'][0], xsec_K['364118'][1], 364118, "zjets","16e"],
    [path + "user.pavol:user.pavol.364202.Sherpa.DAOD_TOPQ1.e5421_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364202'][0], xsec_K['364202'][1], 364202, "zjets","16e"],
    [path + "user.pavol:user.pavol.364122.Sherpa.DAOD_TOPQ1.e5299_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364122'][0], xsec_K['364122'][1], 364122, "zjets","16e"],
    [path + "user.pavol:user.pavol.364134.Sherpa.DAOD_TOPQ1.e5307_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364134'][0], xsec_K['364134'][1], 364134, "zjets","16e"],
    [path + "user.pavol:user.pavol.364108.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364108'][0], xsec_K['364108'][1], 364108, "zjets","16e"],
    [path + "user.pavol:user.pavol.364103.Sherpa.DAOD_TOPQ1.e5271_s3126_r10724_p3629.AT-21.2.58-MC16eBckg-v2_reco.txt", xsec_K['364103'][0], xsec_K['364103'][1], 364103, "zjets","16e"], 
    
    
    
    
    
    
    
    [path + "user.pavol:user.pavol.AllYear.physics_Main.DAOD_TOPQ2.grp15_v02_p3541_p3553.AT-21.2.58-DATA15-v2_reco_reco.txt",-1.0, 1.0, "data", "data","16a"],
    [path + "user.pavol:user.pavol.AllYear.physics_Main.DAOD_TOPQ2.grp16_v02_p3541_p3553.AT-21.2.58-DATA16-v2_reco_reco.txt",-1.0, 1.0, "data", "data","16a"],
    [path + "user.pavol:user.pavol.AllYear.physics_Main.DAOD_TOPQ2.grp17_v03_p3541_p3553.AT-21.2.58-DATA17-v2_reco_reco.txt",-1.0, 1.0, "data", "data","16d"],
    [path + "user.pavol:user.pavol.AllYear.physics_Main.DAOD_TOPQ2.grp18_v02_p3544_p3553_p3583.AT-21.2.58-DATA18-v2_reco_reco.txt",-1.0, 1.0, "data", "data","16e"],
    
    [path + "user.pavol:user.pavol.AllYear.physics_Main.DAOD_TOPQ2.grp15_v02_p3541_p3553.AT-21.2.58-DATA15-v2_reco_reco.txt",-1.0, 1.0, "fakes", "fakes","16a"],
    [path + "user.pavol:user.pavol.AllYear.physics_Main.DAOD_TOPQ2.grp16_v02_p3541_p3553.AT-21.2.58-DATA16-v2_reco_reco.txt",-1.0, 1.0, "fakes", "fakes","16a"],
    [path + "user.pavol:user.pavol.AllYear.physics_Main.DAOD_TOPQ2.grp17_v03_p3541_p3553.AT-21.2.58-DATA17-v2_reco_reco.txt",-1.0, 1.0, "fakes", "fakes","16d"],
    [path + "user.pavol:user.pavol.AllYear.physics_Main.DAOD_TOPQ2.grp18_v02_p3544_p3553_p3583.AT-21.2.58-DATA18-v2_reco_reco.txt",-1.0, 1.0, "fakes", "fakes","16e"],
    




    #[path_to_files + "ttbar_410250.root",  xsec_K['410250'][0] , xsec_K['410250'][1] ,   14996424, "ttbar"],
    #[path_to_files + "ttbar_410251.root",  xsec_K['410251'][0] , xsec_K['410251'][1] ,   14996424, "ttbar"],
    #[path_to_files + "ttbar_410480.root",  xsec_K['410480'][0] , xsec_K['410480'][1] ,   14996424, "ttbar"],
    #[path_to_files + "ttbar_410470.root",  xsec_K['410470'][0] , xsec_K['410470'][1] ,   14996424, "ttbar", 410470],
    #["dcap://se-iep-grid.saske.sk/pnfs/saske.sk/data/atlas/atlaslocalgroupdisk/rucio/user/fisopkov/85/8b/user.fisopkov.16507634._000020.output.root",  xsec_K['410472'][0] , xsec_K['410472'][1] ,   410472, "ttbar"], 
    #["dcap://se-iep-grid.saske.sk/pnfs/saske.sk/data/atlas/atlaslocalgroupdisk/rucio/user/fisopkov/10/ac/user.fisopkov.16507634._000017.output.root ",  xsec_K['410472'][0] , xsec_K['410472'][1] ,   410472, "ttbar"], 
   

    ###[path_to_files + "dibosons_364250.root",  xsec_K['364250'][0] , xsec_K['364250'][1] ,   255000, "diboson", 364250],
    ##[path_to_files + "dibosons_364288.root",  xsec_K['364288'][0] , xsec_K['364288'][1] ,   255000, "diboson", 364288],
    ###[path_to_files + "dibosons_364289.root",  xsec_K['364289'][0] , xsec_K['364289'][1] ,   255000, "diboson", 364289],
    #[path_to_files + "dibosons_361066.root",  xsec_K['361066'][0] , xsec_K['361066'][1] ,   255000, "diboson"],
    #[path_to_files + "dibosons_361067.root",  xsec_K['361067'][0] , xsec_K['361067'][1] ,   255000, "diboson"],
    #[path_to_files + "dibosons_361068.root",  xsec_K['361068'][0] , xsec_K['361068'][1] ,   255000, "diboson"],


    ###[path_to_files + "singletop_410013.root", xsec_K['410013'][0] , xsec_K['410013'][1] ,   404499, "singletop", 410013],
    #[path_to_files + "singletop_410014.root", xsec_K['410014'][0] , xsec_K['410014'][1] ,   404499, "singletop"],

    #[path_to_files + "Zjets_361106.root", xsec_K['361106'][0] , xsec_K['361106'][1] ,   404499, "zjets"],
    #[path_to_files + "Zjets_361107.root", xsec_K['361107'][0] , xsec_K['361107'][1] ,   404499, "zjets"],
    #[path_to_files + "Zjets_361108.root", xsec_K['361108'][0] , xsec_K['361108'][1] ,   404499, "zjets"],

    #[path_to_files + "data2015.root",   -1.0 ,   -1.0, 17246096, "data"],
    #[path_to_files + "data2016.root",   -1.0 ,   -1.0, 17246096, "data"],
    ###[path_to_files + "data_2015_2016.root",   -1.0 ,   -1.0, 17246096, "data", "data"],
    
    #[path_to_files + "fakes_15.root", -1.0 ,   -1.0, 17246096, "fakes"] ,
    #[path_to_files + "fakes2016.root", -1.0 ,   -1.0, 17246096, "fakes"] ,
    #[path_to_files + "fakes1516.root",-1.0 ,   -1.0, 17246096, "fakes"]
    #[path_to_files + "fakes_2.root",-1.0 ,   -1.0, 17246096, "fakes"]
    ]

