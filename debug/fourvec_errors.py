def print_lepton_errors(event, lep_id, acceptance, quiet = None):

    """ function that returns 1 if the lep_id is NOT in 'acceptance' and 0 if it is. In this way the function is an 'error counter' of sorts """

    lep_ids = []
    if "e" in acceptance:
        lep_ids.append(11)
        lep_ids.append(-11)
    if "mu" in acceptance:
        lep_ids.append(13)
        lep_ids.append(-13)
    if "tau" in acceptance:
        lep_ids.append(15)
        lep_ids.append(-15)
    
    if lep_id not in lep_ids:
        if quiet == None:
            print "ERROR: 'lepton' not a lepton!"
            print "ERROR:    -- pgdID      :", lep_id
            print "ERROR:    -- eventNumber:", event.eventNumber
            print "ERROR:    -- weight_mc  :", event.weight_mc
        return 1
    else:
        return 0
