##
## code for print-to-screen user friendliness
##

# define acceptable rgs
arg_ac = ["plot", "rmbckg", "truthcomp", "NNLOcomp", "resolution", "mcplot", "mig"]
arg_ob = ["lep", "jet", "met", "top", "spin", "costheta"]
arg_pr = ["16a", "16d", "16e"]
arg_ch = ["ee", "emu", "mumu"]


def print_yields(collection, total_hist):


    hists = collection.master

    print "*-*-*-*-*-*-*-*-* YIELD OUTPUT *-*-*-*-*-*-*-*-*-* "
    print collection.period, collection.systematic, collection.channel, collection.variable, collection.varname
    print '%50s' % ("-----------------------------------------------")
    print '%10s %3s %7s %7s %3s %7s' % ("SAMPLE","|","YIELD","ERROR","|", "ENTRIES")
    print '%50s' % ("-----------------------------------------------")
    print '%10s %3s %7s %7s %3s %7s' % ("ttbar",     "|",str(round(hists["ttbar"].Integral(),2)),     str(round(getScaledErrors(hists["ttbar"]),2)),    "|",str(hists["ttbar"].GetEntries()))
    #print '%10s %3s %7s %7s %3s %7s' % ("ttbarrecowhenpl","|",str(round(hists["ttbarrecowhenpl"].Integral(),2)),str(round(getScaledErrors(hists["ttbarrecowhenpl"]),2)),"|",str(hists["ttbarrecowhenpl"].GetEntries()))
    #print '%10s %3s %7s %7s %3s %7s' % ("ttbarrecowhentr","|",str(round(hists["ttbarrecowhentr"].Integral(),2)),str(round(getScaledErrors(hists["ttbarrecowhentr"]),2)),"|",str(hists["ttbarrecowhentr"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("truth",     "|",str(round(hists["truth"].Integral(),2)),     str(round(getScaledErrors(hists["truth"]),2)),    "|",str(hists["truth"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("signalsyst","|",str(round(hists["signalsyst"].Integral(),2)),str(round(getScaledErrors(hists["signalsyst"]),2)),    "|",str(hists["signalsyst"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("singletop", "|",str(round(hists["singletop"].Integral(),2)), str(round(getScaledErrors(hists["singletop"]),2)),"|",str(hists["singletop"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("diboson",   "|",str(round(hists["diboson"].Integral(),2)),   str(round(getScaledErrors(hists["diboson"]),2)),  "|",str(hists["diboson"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("zjets",     "|",str(round(hists["zjets"].Integral(),2)),     str(round(getScaledErrors(hists["zjets"]),2)),    "|",str(hists["zjets"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("wjets",     "|",str(round(hists["wjets"].Integral(),2)),     str(round(getScaledErrors(hists["wjets"]),2)),    "|",str(hists["wjets"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("fakes",     "|",str(round(hists["fakes"].Integral(),2)),     str(round(getScaledErrors(hists["fakes"]),2)),    "|",str(hists["fakes"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("ss",        "|",str(round(hists["ss"].Integral(),2)),        str(round(getScaledErrors(hists["ss"]),2)),       "|",str(hists["ss"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("total exp.","|",str(round(total_hist.Integral(),2)),         str(round(getScaledErrors(total_hist),2)),        "|",str(total_hist.GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("data",      "|",str(round(hists["data"].Integral(),2)),      str(round(getScaledErrors(hists["data"]),2)),     "|",str(hists["data"].GetEntries()))
    print '%50s' % ("-----------------------------------------------")


def print_yields_from_dict(hists, total_hist):


    print "*-*-*-*-*-*-*-*-* YIELD OUTPUT *-*-*-*-*-*-*-*-*-* "
    print '%50s' % ("-----------------------------------------------")
    print '%10s %3s %7s %7s %3s %7s' % ("SAMPLE","|","YIELD","ERROR","|", "ENTRIES")
    print '%50s' % ("-----------------------------------------------")
    print '%10s %3s %7s %7s %3s %7s' % ("ttbar",     "|",str(round(hists["ttbar"].Integral(),2)),    str(round(getScaledErrors(hists["ttbar"]),2)),    "|",str(hists["ttbar"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("singletop", "|",str(round(hists["singletop"].Integral(),2)),str(round(getScaledErrors(hists["singletop"]),2)),"|",str(hists["singletop"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("diboson",   "|",str(round(hists["diboson"].Integral(),2)),  str(round(getScaledErrors(hists["diboson"]),2)),  "|",str(hists["diboson"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("zjets",     "|",str(round(hists["zjets"].Integral(),2)),    str(round(getScaledErrors(hists["zjets"]),2)),    "|",str(hists["zjets"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("wjets",     "|",str(round(hists["wjets"].Integral(),2)),    str(round(getScaledErrors(hists["wjets"]),2)),    "|",str(hists["wjets"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("fakes",     "|",str(round(hists["fakes"].Integral(),2)),    str(round(getScaledErrors(hists["fakes"]),2)),    "|",str(hists["fakes"].GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("total exp.","|",str(round(total_hist.Integral(),2)),        str(round(getScaledErrors(total_hist),2)),        "|",str(total_hist.GetEntries()))
    print '%10s %3s %7s %7s %3s %7s' % ("data",      "|",str(round(hists["data"].Integral(),2)),     str(round(getScaledErrors(hists["data"]),2)),     "|",str(hists["data"].GetEntries()))
    print '%50s' % ("-----------------------------------------------")


def print_overflow_yields(master):
    if master["ttbar"].GetBinContent(master["ttbar"].GetNbinsX()+1) != 0:
        print "INFO: There are", master["ttbar"].GetBinContent(master["ttbar"].GetNbinsX()+1), "events in the overflow bin for ttbar"
    if master["fakes"].GetBinContent(master["fakes"].GetNbinsX()+1) != 0:
        print "INFO: There are", master["fakes"].GetBinContent(master["fakes"].GetNbinsX()+1), "events in the overflow bin for fakes"
    if master["ss"].GetBinContent(master["ss"].GetNbinsX()+1) != 0:
        print "INFO: There are", master["ss"].GetBinContent(master["ss"].GetNbinsX()+1), "events in the overflow bin for ss (same-sign events)"
    if master["diboson"].GetBinContent(master["diboson"].GetNbinsX()+1) != 0:
        print "INFO: There are", master["diboson"].GetBinContent(master["diboson"].GetNbinsX()+1), "events in the overflow bin for diboson"
    if master["singletop"].GetBinContent(master["singletop"].GetNbinsX()+1) != 0:
        print "INFO: There are", master["singletop"].GetBinContent(master["singletop"].GetNbinsX()+1), "events in the overflow bin for singletop"
    if master["zjets"].GetBinContent(master["zjets"].GetNbinsX()+1) != 0:
        print "INFO: There are", master["zjets"].GetBinContent(master["zjets"].GetNbinsX()+1), "events in the overflow bin for zjets"
    if master["wjets"].GetBinContent(master["wjets"].GetNbinsX()+1) != 0:
        print "INFO: There are", master["wjets"].GetBinContent(master["wjets"].GetNbinsX()+1), "events in the overflow bin for wjets"
    if master["data"].GetBinContent(master["data"].GetNbinsX()+1) != 0:
        print "INFO: There are", master["data"].GetBinContent(master["data"].GetNbinsX()+1), "events in the overflow bin for data"


def make_readable(instring):
    # a function to make things readable for printing to screen
    # it turns coded words like 'lep' into 'lepton'.

    readable_dict = {
        "lep"   : "lepton",
        "el"    : "electron",
        "mu"    : "muon",
        "met"   : "missing transverse momentum and/or ht",
        "nu"    : "neutrino",
        "top"   : "top quark",
        "tt"    : "ttbar system",
        "all"   : "all combined",
    }

    try:
        return readable_dict[instring]
    except:
        return instring

def test_plot_arg(arg, option, input_is_list = False):

    """ function to check the arguments given to create_plots.py"""
    import copy
    # add 'all' to period and channel lists
    new_arg_pr = copy.copy(arg_pr)
    new_arg_pr.append("all")
    new_arg_ch = copy.copy(arg_ch)
    new_arg_ch.append("all")

    # make dict of list
    acceptable_args = {
        "period"   : new_arg_pr,
        "channel"  : new_arg_ch,
        "action"   : arg_ac,
        "object"   : arg_ob
    }

    if input_is_list:
        for a in arg:
            if a in acceptable_args[option]:
                pass
            else:
                print "ERROR: \"", a, "\" is an invalid argument for:", option
                print "       Choose from one of the following:"
                print "      ", acceptable_args[option]
                return False
    else:
        if arg in acceptable_args[option]:
            pass
        else:
            print "ERROR: \"", arg, "\" is an invalid argument for:", option
            print "       Choose from one of the following:"
            print "      ", acceptable_args[option]
            return False

    return True

def getScaledErrors(hist):

    """ Function to return the scaled entries of a histogram """

    from ROOT import TH1, TH1D, TH1F
    import math
    TH1.SetDefaultSumw2()

    scaled_error = 0;
    number = hist.GetNbinsX()
    i = 0
    while i <= number:

        scaled_error += pow(hist.GetBinError(i),2)
        i = i + 1

#    if scaled_error==0: print "WARNING no error in histogram ",hist
    return math.sqrt(scaled_error)

def print_component_entries(component_hists, debug):
    if debug:
        print "DEBUG: __Number_of_entries__"
        for hist in component_hists:
            # if "NNLO" in hist:
            #     continue
            # if "ss" in hist:
            #     continue
            # if "truth" in hist:
            #     continue
            # if "ttbar" in hist:
            #     continue
            print "DEBUG:", hist, ":" , component_hists[hist].GetEntries()


def print_component_integrals(component_hists, debug):
    if debug:
        print "DEBUG: __hist_integrals__"
        for hist in component_hists:
            print "DEBUG:", hist, ":" , round(component_hists[hist].Integral(),2)
