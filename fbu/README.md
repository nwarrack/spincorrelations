# Fully Bayesian Unfolding

This directory holds everything you need to perform Fully Bayesian Unfolding (FBU) given some data plots and some truth-to-reco migration matrices.

The code is based on the fbu python package [here](https://github.com/gerbaudo/fbu).

Download this package and check that it runs by using the instructions below and the minimal test script `test_fbu.py`.

_If you have already done this once, skip to the end and follow the re-setup instructions_


```bash
# Download miniconda for linux
# (for Mac use https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh)
wget -nv http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh

# Install miniconda:
bash miniconda.sh -b -p $HOME/miniconda

# Activate Conda (add this to your bashrc):
source $HOME/miniconda/etc/profile.d/conda.sh

# Create your virtual environment and install python3.6.3
conda create --name fbuenv python=3.6.3

# Activate your virtual environment
conda activate fbuenv

# Install pymc3, etc. into your environment
conda install -c conda-forge pymc3
conda install mkl
conda install numpy
conda install mkl-service

# avoid warnings with this (add this to your bashrc)
export MKL_THREADING_LAYER=GNU

# Clone the fbu package
git clone https://github.com/pyFBU/fbu.git

# Run the test script
./test_fbu.py

# If you see no errors - things work fine :)
# If you see warnings related to "theano.gof.compilelock" try running again and see if they disappear
# IF you see:
# WARNING (theano.tensor.blas): Using NumPy C-API based implementation for BLAS functions.
# Then don't worry, as far as I can tell it isn't doing anything to change the functionality of pyFBU
```

### Re-setup 
Once you have setup the virtual env as above, you can log out and log back in again and re-setup like this:
```bash
conda activate fbuenv
./test_fbu.py
```

### Deactivating your virtual environment
```bash
# To deactivate the virtual environment once you're finished:
conda deactivate
```

_If you see any errors, let me know via the email (neil.warrack@cern.ch) or by opening an issue._