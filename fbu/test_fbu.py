#!/usr/bin/env python
import platform

# from net: "PyFBU is tested on Python 3.6.3 within Anaconda 4.3.30 and depends on PyMC 3."
print("python version:", platform.python_version())


from fbu import fbu
myfbu = fbu.PyFBU()
myfbu.data = [100,150]
myfbu.response = [[0.08,0.02], #first truth bin
                  [0.02,0.08]] #second truth bin
myfbu.lower = [0,0]
myfbu.upper = [3000,3000]
myfbu.run()
trace = myfbu.trace
print( trace )


from matplotlib import pyplot as plt
plt.hist(trace[1],
         bins=20,alpha=0.85,
         density=True)
plt.ylabel('probability')
plt.savefig('../user/testplot')
print("testplot.png created in ../user/")
print("Done.")
