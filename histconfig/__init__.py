from . import jets, leptons, electrons, met, tops, spincorrelations, truth_spincorrelations, costhetas

# Dictionary of object lists (each list contains observables such as pT, eta, phi, etc.)
histo_lists = {
    "jet"      : jets.jet_observables,
    "lep"      : leptons.lepton_observables,
    "elec"     : electrons.electron_observables,
    "met"      : met.met_observables,
    "top"      : tops.top_observables,
    "spin"     : spincorrelations.spin_observables,
    "truspin"  : truth_spincorrelations.truth_spin_observables,
    "costheta" : costhetas.costheta_observables,
}
