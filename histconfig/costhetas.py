from ROOT import TMath

costheta_observables = [

    #|Varible    |X-axis title                |Log    |maxY  |bins|--range--| file-id |

    ["cos_kaxis_p" ,    "cos theta k axis +",  True,  1000,   8,    0, 1.0,  "cos_kaxis_p" ],
    ["cos_kaxis_m" ,    "cos theta k axis -",  True,  1000,   8,    0, 1.0,  "cos_kaxis_m" ],
    ["cos_naxis_p" ,    "cos theta n axis +",  True,  1000,   8,    0, 1.0,  "cos_naxis_p" ],
    ["cos_naxis_m" ,    "cos theta n axis -",  True,  1000,   8,    0, 1.0,  "cos_naxis_m" ],
    ["cos_raxis_p" ,    "cos theta r axis +",  True,  1000,   8,    0, 1.0,  "cos_raxis_p" ],
    ["cos_raxis_m" ,    "cos theta r axis -",  True,  1000,   8,    0, 1.0,  "cos_raxis_m" ],
 
]
