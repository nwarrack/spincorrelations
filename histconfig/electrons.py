from ROOT import TMath
pi = TMath.Pi()



electron_observables = [

    #Varible           X-axis title                      Log     maxY   bins      ---range---    filename   
    # project method
    # ["el_pt/1000",     "All Electron p_{T} [GeV]",       True,  1000,  25,       0.,  600.,  "el_pt_log"    ],
    # ["el_e/1000",      "All Electron E [GeV]",           True,  1000,  25,       0.,  1000., "el_e_log"     ],
    # ["el_eta",         "All Electron #eta",              False,   0.00,   20,     -2.5,   2.5,  "el_eta"       ],
    # ["el_phi/pi",      "All Electron #phi/#pi [rad/#pi]",False,   0.00,   20,      -1,    1,    "el_phi"       ],

    # event loop method
#    ["el_pt",          "Leading electron p_{T} [GeV]",       True,  1.0001,  200,       0.,  600.,  "el1_pt"    ],
    ["el_pt",          "Leading electron p_{T} [GeV]",       True,  1.0001,  200,       0.,  1000.,  "el0_pt"    ],
    #["el_e",           "Leading electron E [GeV]",           True,  1.0001,1000,  25,       0.,  1000., "el1_e"     ],
    #["el_eta",         "Leading electron #eta",              False,   0.00, 1.8,  20,     -2.5,   2.5,  "el1_eta"   ],
    #["el_phi",         "Leading electron #phi/#pi [rad/#pi]",False,   0.00, 1.8,  20,      -1,    1,    "el1_phi"   ],
]

