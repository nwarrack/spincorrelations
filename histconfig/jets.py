jet_observables = [

    #|Varible     |X-axis title                       |Log |maxY|bins|---range---| file_id  |
    
    # Leading jets
    ["d_jet_pt",  "Leading jet p_{T} [GeV]",           True, 1000,  25,    0,  400, "jet0_pt" ],
    ["d_jet_eta", "Leading jet #eta",                  False, 1.8,  20, -2.5,  2.5, "jet0_eta"],
    ["d_jet_phi", "Leading jet #phi/#pi [rad/#pi]",    False, 1.8,  20,   -1,    1, "jet0_phi"],

    # Sub-leading jets
    ["d_jet_pt",  "Sub-leading jet p_{T} [GeV]",       True, 1000,  25,    0,  400, "jet1_pt" ],
    ["d_jet_eta", "Sub-leading jet #eta",              False, 1.8,  20, -2.5,  2.5, "jet1_eta"],
    ["d_jet_phi", "Sub-leading jet #phi/#pi [rad/#pi]",False, 1.8,  20,   -1,    1, "jet1_phi"],

    # Leading b-jets
    ["d_jet_pt",  "Leading b-jet p_{T} [GeV]",           True, 1000,  25,    0,  400, "jetb0_pt" ],
    ["d_jet_eta", "Leading b-jet #eta",                  False, 1.8,  20, -2.5,  2.5, "jetb0_eta"],
    ["d_jet_phi", "Leading b-jet #phi/#pi [rad/#pi]",    False, 1.8,  20,   -1,    1, "jetb0_phi"],

    # Sub-leading b-jets
    ["d_jet_pt",  "Sub-leading b-jet p_{T} [GeV]",       True, 1000,  25,    0,  400, "jetb1_pt" ],
    ["d_jet_eta", "Sub-leading b-jet #eta",              False, 1.8,  20, -2.5,  2.5, "jetb1_eta"],
    ["d_jet_phi", "Sub-leading b-jet #phi/#pi [rad/#pi]",False, 1.8,  20,   -1,    1, "jetb1_phi"],
 ]
