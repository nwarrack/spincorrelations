lepton_observables = [

    #|Varible     |X-axis title                           |Log |maxY|bins|---range---| file_id  |

    # Leading leptons
    ["d_lep_pt",  "Leading Lepton p_{T} [GeV]",           True, 1000,  80,    0,  400, "lep_pt_0" ],
    ["d_lep_eta", "Leading Lepton #eta",                  False, 1.8,  20, -2.5,  2.5, "lep_eta_0"],
    ["d_lep_phi", "Leading Lepton #phi/#pi [rad/#pi]",    False, 1.8,  20,   -1,    1, "lep_phi_0"],


    # Sub-leading leptons
    ["d_lep_pt",  "Sub-leading Lepton p_{T} [GeV]",       True, 1000,  80,    0,  400, "lep_pt_1" ],
    ["d_lep_eta", "Sub-leading Lepton #eta",              False, 1.8,  20, -2.5,  2.5, "lep_eta_1"],
    ["d_lep_phi", "Sub-leading Lepton #phi/#pi [rad/#pi]",False, 1.8,  20,   -1,    1, "lep_phi_1"],


    # dilepton deltas
    ["d_lep_phi", "Lepton Delta #phi [rad/#pi]",          False, 1.8,  20,  0,    1, "lep_delta_phi"],
    ["d_lep_eta", "Lepton Delta #eta",                    True, 1000,  20,  0,    5, "lep_delta_eta"],


    # dilepton mass and pT
    ["lep_pair_pt", "Lepton pair p_{T} [GeV]",              True, 1000,  20,  0,  500, "lep_pair_pt"],    
    ["lep_pair_m",  "Lepton pair mass [GeV]",               True, 1000,  20,  0,  500, "lep_pair_m" ],
]
