from ROOT import TMath
pi = TMath.Pi()


muon_observables = [

    #Varible           X-axis title                      Log     minY  maxY   bins     ---range---  filename   
    # projection method
    # ["mu_pt/1000",     "All Muon p_{T} [GeV]",           True,  1.0001,1000,  25,       0.,  600., "mu_pt_log" ],
    # ["mu_e/1000",      "All Muon E [GeV]",               True,  1.0001,1000,  25,       0., 1000., "mu_e_log"  ],
    # ["mu_eta",         "All Muon #eta",                  False,   0.00, 1.8,  20,     -2.5,   2.5, "mu_eta"    ],
    # ["mu_phi/pi",      "All Muon #phi/#pi [rad/#pi]",    False,   0.00, 1.8,  20,      -1.,    1., "mu_phi"    ],

    # event_loop method
    ["mu_pt",         "Leading muon p_{T} [GeV]",               True,  1.0001,1000,  25,       0.,  600., "mu1_pt" ],
    #["mu_eta",        "Leading muon #eta",                      False,   0.00, 1.8,  20,     -2.5,   2.5, "mu1_eta" ],
    #["mu_phi",        "Leading muon #phi/#pi [rad/#pi]",        False,   0.00, 1.8,  20,      -1.,    1., "mu1_phi" ],
    #["mu_e",          "Leading muon Energy [GeV]",              True,  1.0001,1000,  25,       0., 1000., "mu1_e"   ],

    #["mu_pt",         "Sub-leading muon p_{T} [GeV]",            True,  1.0001,1000,  25,       0.,  600., "mu2_pt" ],
    # ["mu_eta",        "Sub-leading muon #eta",                  False,   0.00, 1.8,  20,     -2.5,   2.5, "mu2_eta" ],
    #["mu_phi",        "Sub-leading muon #phi/#pi [rad/#pi]",    False,   0.00, 1.8,  20,      -1.,    1., "mu2_phi" ],
    # ["mu_e",          "Sub-leading muon Energy [GeV]",          True,  1.0001,1000,  25,       0., 1000., "mu2_e"   ],

    # ["mu_pt/1000",     "All muon p_{T} [GeV]",                  True,  1.0001,1000,  25,       0.,  600., "mu_pt" ],
    # ["mu_e/1000",      "All muon E [GeV]",                      True,  1.0001,1000,  25,       0., 1000., "mu_e"  ],
    # ["mu_eta",         "All muon #eta",                         False,   0.00, 1.8,  20,     -2.5,   2.5, "mu_eta"],
    # ["mu_phi/pi",      "All muon #phi/#pi [rad/#pi]",           False,   0.00, 1.8,  20,      -1.,    1., "mu_phi"],
]
