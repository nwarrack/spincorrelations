from ROOT import TMath
pi = TMath.Pi()


neutrino_observables = [

    #Varible           X-axis title                      Log     minY  maxY   bins      ---range---    filename   

    # Project method
    # ["nu_pt/1000",     "#nu p_{T} [GeV]",                True,  1.0001,1000,  25,       0.,  1000.,  "nu_pt_log"    ],
    # ["nu_eta",         "#nu #eta",                       False,   0.00, 1.8,  36,     -4.5,    4.5,  "nu_eta"       ],
    # ["nu_phi/pi",      "#nu #phi/#pi [rad/#pi]",         False,   0.00, 1.8,  20,      -1.,     1.,  "nu_phi"       ],
    # ["nu_E/1000",      "#nu E [GeV]",                    True,  1.0001,1000,  40,       0.,  2000.,  "nu_E_log"     ],

    # ["nubar_pt/1000",  "#bar{#nu} p_{T} [GeV]",          True,  1.0001,1000,  25,       0.,  1000.,  "nubar_pt_log" ],
    # ["nubar_eta",      "#bar{#nu} #eta",                 False,   0.00, 1.8,  36,     -4.5,    4.5,  "nubar_eta"    ],
    # ["nubar_phi/pi",   "nu #phi/#pi [rad/#pi]",          False,   0.00, 1.8,  20,      -1.,     1.,  "nubar_phi"    ],
    # ["nubar_E/1000",   "#bar{#nu} E [GeV]",              True,  1.0001,1000,  40,       0.,  2000.,  "nubar_E_log"  ],


    # event loop method
    ["nu_pt",     "#nu p_{T} [GeV]",                True,  1.0001,1000,  25,       0.,  1000.,  "nu_pt"    ],
    ["nu_eta",         "#nu #eta",                       False,   0.00, 1.8,  36,     -4.5,    4.5,  "nu_eta"       ],
    ["nu_phi",      "#nu #phi/#pi [rad/#pi]",         False,   0.00, 1.8,  20,      -1.,     1.,  "nu_phi"       ],
    ["nu_E",      "#nu E [GeV]",                    True,  1.0001,1000,  40,       0.,  2000.,  "nu_e"     ],

    ["nubar_pt",  "#bar{#nu} p_{T} [GeV]",          True,  1.0001,1000,  25,       0.,  1000.,  "nubar_pt" ],
    ["nubar_eta",      "#bar{#nu} #eta",                 False,   0.00, 1.8,  36,     -4.5,    4.5,  "nubar_eta"    ],
    ["nubar_phi",   "nu #phi/#pi [rad/#pi]",          False,   0.00, 1.8,  20,      -1.,     1.,  "nubar_phi"    ],
    ["nubar_E",   "#bar{#nu} E [GeV]",              True,  1.0001,1000,  40,       0.,  2000.,  "nubar_e"  ],
]

