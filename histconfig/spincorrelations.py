spin_observables = [

    # 'log' and 'ymax' only used for plotting

    #|Varible    | X-axis title                   | Log |ymax|bins|-range-|fileID|

    ## Neutrino Weighter (NW) reco
    # spin correlations
    ["NW_c_nn", "cos#theta_{n}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "NW_cnn" ],
    ["NW_c_rr", "cos#theta_{r}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "NW_crr" ],
    ["NW_c_kk", "cos#theta_{k}^{+}cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "NW_ckk" ],
    ["NW_c_rk", "cos#theta_{r}^{+}cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "NW_crk" ],
    ["NW_c_kr", "cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "NW_ckr" ],
    ["NW_c_nr", "cos#theta_{n}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "NW_cnr" ],
    ["NW_c_rn", "cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "NW_crn" ],
    ["NW_c_nk", "cos#theta_{n}^{+}cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "NW_cnk" ],
    ["NW_c_kn", "cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "NW_ckn" ],
    # spin cross correlations
    ["NW_c_nk_p_kn", "cos#theta_{n}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "NW_cnkpkn"],
    ["NW_c_nr_p_rn", "cos#theta_{n}^{+}cos#theta_{r}^{-}+cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "NW_cnrprn"],
    ["NW_c_rk_p_kr", "cos#theta_{r}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "NW_crkpkr"],
    ["NW_c_nk_m_kn", "cos#theta_{n}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "NW_cnkmkn"],
    ["NW_c_nr_m_rn", "cos#theta_{n}^{+}cos#theta_{r}^{-}-cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "NW_cnrmrn"],
    ["NW_c_rk_m_kr", "cos#theta_{r}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "NW_crkmkr"],
    # spin polarizations
    ["NW_b_kplus",   "cos#theta_{k}^{+}", False, 1.8, 4, -1, 1, "NW_bkplus" ],
    ["NW_b_nplus",   "cos#theta_{n}^{+}", False, 1.8, 4, -1, 1, "NW_bnplus" ],
    ["NW_b_rplus",   "cos#theta_{r}^{+}", False, 1.8, 4, -1, 1, "NW_brplus" ],
    ["NW_b_kminus",  "cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "NW_bkminus"],
    ["NW_b_nminus",  "cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "NW_bnminus"],
    ["NW_b_rminus",  "cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "NW_brminus"],

    ## Son. (SN) reco
    # spin correlations
    ["SN_c_nn", "cos#theta_{n}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "SN_cnn" ],
    ["SN_c_rr", "cos#theta_{r}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "SN_crr" ],
    ["SN_c_kk", "cos#theta_{k}^{+}cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "SN_ckk" ],
    ["SN_c_rk", "cos#theta_{r}^{+}cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "SN_crk" ],
    ["SN_c_kr", "cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "SN_ckr" ],
    ["SN_c_nr", "cos#theta_{n}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "SN_cnr" ],
    ["SN_c_rn", "cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "SN_crn" ],
    ["SN_c_nk", "cos#theta_{n}^{+}cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "SN_cnk" ],
    ["SN_c_kn", "cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "SN_ckn" ],
    # spin cross correlations
    ["SN_c_nk_p_kn", "cos#theta_{n}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "SN_cnkpkn"],
    ["SN_c_nr_p_rn", "cos#theta_{n}^{+}cos#theta_{r}^{-}+cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "SN_cnrprn"],
    ["SN_c_rk_p_kr", "cos#theta_{r}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "SN_crkpkr"],
    ["SN_c_nk_m_kn", "cos#theta_{n}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "SN_cnkmkn"],
    ["SN_c_nr_m_rn", "cos#theta_{n}^{+}cos#theta_{r}^{-}-cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "SN_cnrmrn"],
    ["SN_c_rk_m_kr", "cos#theta_{r}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "SN_crkmkr"],
    # spin polarizations
    ["SN_b_kplus",   "cos#theta_{k}^{+}", False, 1.8, 4, -1, 1, "SN_bkplus" ],
    ["SN_b_nplus",   "cos#theta_{n}^{+}", False, 1.8, 4, -1, 1, "SN_bnplus" ],
    ["SN_b_rplus",   "cos#theta_{r}^{+}", False, 1.8, 4, -1, 1, "SN_brplus" ],
    ["SN_b_kminus",  "cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "SN_bkminus"],
    ["SN_b_nminus",  "cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "SN_bnminus"],
    ["SN_b_rminus",  "cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "SN_brminus"],

    ## Elipse Method (EM) reco
    # spin correlations
    ["EM_c_nn", "cos#theta_{n}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "EM_cnn" ],
    ["EM_c_rr", "cos#theta_{r}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "EM_crr" ],
    ["EM_c_kk", "cos#theta_{k}^{+}cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "EM_ckk" ],
    ["EM_c_rk", "cos#theta_{r}^{+}cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "EM_crk" ],
    ["EM_c_kr", "cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "EM_ckr" ],
    ["EM_c_nr", "cos#theta_{n}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "EM_cnr" ],
    ["EM_c_rn", "cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "EM_crn" ],
    ["EM_c_nk", "cos#theta_{n}^{+}cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "EM_cnk" ],
    ["EM_c_kn", "cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "EM_ckn" ],
    # spin cross correlations
    ["EM_c_nk_p_kn", "cos#theta_{n}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "EM_cnkpkn"],
    ["EM_c_nr_p_rn", "cos#theta_{n}^{+}cos#theta_{r}^{-}+cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "EM_cnrprn"],
    ["EM_c_rk_p_kr", "cos#theta_{r}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "EM_crkpkr"],
    ["EM_c_nk_m_kn", "cos#theta_{n}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "EM_cnkmkn"],
    ["EM_c_nr_m_rn", "cos#theta_{n}^{+}cos#theta_{r}^{-}-cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "EM_cnrmrn"],
    ["EM_c_rk_m_kr", "cos#theta_{r}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "EM_crkmkr"],
    # spin polarization
    ["EM_b_kplus",   "cos#theta_{k}^{+}", False, 1.8, 4, -1, 1, "EM_bkplus" ],
    ["EM_b_nplus",   "cos#theta_{n}^{+}", False, 1.8, 4, -1, 1, "EM_bnplus" ],
    ["EM_b_rplus",   "cos#theta_{r}^{+}", False, 1.8, 4, -1, 1, "EM_brplus" ],
    ["EM_b_kminus",  "cos#theta_{k}^{-}", False, 1.8, 4, -1, 1, "EM_bkminus"],
    ["EM_b_nminus",  "cos#theta_{n}^{-}", False, 1.8, 4, -1, 1, "EM_bnminus"],
    ["EM_b_rminus",  "cos#theta_{r}^{-}", False, 1.8, 4, -1, 1, "EM_brminus"],

    # ## RESOLUTION PLOTS
    # # # spin correlations resoluions (NW)
    # ["NW_c_nn_resolution", "cos#theta_{n}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "NW_cnn_resolution" ],
    # ["NW_c_rr_resolution", "cos#theta_{r}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "NW_crr_resolution" ],
    # ["NW_c_kk_resolution", "cos#theta_{k}^{+}cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "NW_ckk_resolution" ],
    # ["NW_c_rk_resolution", "cos#theta_{r}^{+}cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "NW_crk_resolution" ],
    # ["NW_c_kr_resolution", "cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "NW_ckr_resolution" ],
    # ["NW_c_nr_resolution", "cos#theta_{n}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "NW_cnr_resolution" ],
    # ["NW_c_rn_resolution", "cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "NW_crn_resolution" ],
    # ["NW_c_nk_resolution", "cos#theta_{n}^{+}cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "NW_cnk_resolution" ],
    # ["NW_c_kn_resolution", "cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "NW_ckn_resolution" ],
    # # spin cross correlations resolution (NW)
    # ["NW_c_nk_p_kn_resolution", "cos#theta_{n}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "NW_cnkpkn_resolution"],
    # ["NW_c_nr_p_rn_resolution", "cos#theta_{n}^{+}cos#theta_{r}^{-}+cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "NW_cnrprn_resolution"],
    # ["NW_c_rk_p_kr_resolution", "cos#theta_{r}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "NW_crkpkr_resolution"],
    # ["NW_c_nk_m_kn_resolution", "cos#theta_{n}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "NW_cnkmkn_resolution"],
    # ["NW_c_nr_m_rn_resolution", "cos#theta_{n}^{+}cos#theta_{r}^{-}-cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "NW_cnrmrn_resolution"],
    # ["NW_c_rk_m_kr_resolution", "cos#theta_{r}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "NW_crkmkr_resolution"],
    # # spin polarizations resolutions (NW)
    # ["NW_b_kplus_resolution",   "cos#theta_{k}^{+}", False, 1.8, 8, -2, 2, "NW_bkplus_resolution" ],
    # ["NW_b_nplus_resolution",   "cos#theta_{n}^{+}", False, 1.8, 8, -2, 2, "NW_bnplus_resolution" ],
    # ["NW_b_rplus_resolution",   "cos#theta_{r}^{+}", False, 1.8, 8, -2, 2, "NW_brplus_resolution" ],
    # ["NW_b_kminus_resolution",  "cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "NW_bkminus_resolution"],
    # ["NW_b_nminus_resolution",  "cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "NW_bnminus_resolution"],
    # ["NW_b_rminus_resolution",  "cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "NW_brminus_resolution"],

    # # spin correlations resoluions (SN)
    # ["SN_c_nn_resolution", "cos#theta_{n}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "SN_cnn_resolution" ],
    # ["SN_c_rr_resolution", "cos#theta_{r}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "SN_crr_resolution" ],
    # ["SN_c_kk_resolution", "cos#theta_{k}^{+}cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "SN_ckk_resolution" ],
    # ["SN_c_rk_resolution", "cos#theta_{r}^{+}cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "SN_crk_resolution" ],
    # ["SN_c_kr_resolution", "cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "SN_ckr_resolution" ],
    # ["SN_c_nr_resolution", "cos#theta_{n}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "SN_cnr_resolution" ],
    # ["SN_c_rn_resolution", "cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "SN_crn_resolution" ],
    # ["SN_c_nk_resolution", "cos#theta_{n}^{+}cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "SN_cnk_resolution" ],
    # ["SN_c_kn_resolution", "cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "SN_ckn_resolution" ],
    # # spin cross correlations resolution (SN)
    # ["SN_c_nk_p_kn_resolution", "cos#theta_{n}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "SN_cnkpkn_resolution"],
    # ["SN_c_nr_p_rn_resolution", "cos#theta_{n}^{+}cos#theta_{r}^{-}+cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "SN_cnrprn_resolution"],
    # ["SN_c_rk_p_kr_resolution", "cos#theta_{r}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "SN_crkpkr_resolution"],
    # ["SN_c_nk_m_kn_resolution", "cos#theta_{n}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "SN_cnkmkn_resolution"],
    # ["SN_c_nr_m_rn_resolution", "cos#theta_{n}^{+}cos#theta_{r}^{-}-cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "SN_cnrmrn_resolution"],
    # ["SN_c_rk_m_kr_resolution", "cos#theta_{r}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "SN_crkmkr_resolution"],
    # # spin polarizations resolutions (SN)
    # ["SN_b_kplus_resolution",   "cos#theta_{k}^{+}", False, 1.8, 8, -2, 2, "SN_bkplus_resolution" ],
    # ["SN_b_nplus_resolution",   "cos#theta_{n}^{+}", False, 1.8, 8, -2, 2, "SN_bnplus_resolution" ],
    # ["SN_b_rplus_resolution",   "cos#theta_{r}^{+}", False, 1.8, 8, -2, 2, "SN_brplus_resolution" ],
    # ["SN_b_kminus_resolution",  "cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "SN_bkminus_resolution"],
    # ["SN_b_nminus_resolution",  "cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "SN_bnminus_resolution"],
    # ["SN_b_rminus_resolution",  "cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "SN_brminus_resolution"],

    # # spin correlations resoluions (EM)
    # ["EM_c_nn_resolution", "cos#theta_{n}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "EM_cnn_resolution" ],
    # ["EM_c_rr_resolution", "cos#theta_{r}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "EM_crr_resolution" ],
    # ["EM_c_kk_resolution", "cos#theta_{k}^{+}cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "EM_ckk_resolution" ],
    # ["EM_c_rk_resolution", "cos#theta_{r}^{+}cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "EM_crk_resolution" ],
    # ["EM_c_kr_resolution", "cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "EM_ckr_resolution" ],
    # ["EM_c_nr_resolution", "cos#theta_{n}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "EM_cnr_resolution" ],
    # ["EM_c_rn_resolution", "cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "EM_crn_resolution" ],
    # ["EM_c_nk_resolution", "cos#theta_{n}^{+}cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "EM_cnk_resolution" ],
    # ["EM_c_kn_resolution", "cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "EM_ckn_resolution" ],
    # # spin cross correlations resolution (EM)
    # ["EM_c_nk_p_kn_resolution", "cos#theta_{n}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "EM_cnkpkn_resolution"],
    # ["EM_c_nr_p_rn_resolution", "cos#theta_{n}^{+}cos#theta_{r}^{-}+cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "EM_cnrprn_resolution"],
    # ["EM_c_rk_p_kr_resolution", "cos#theta_{r}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "EM_crkpkr_resolution"],
    # ["EM_c_nk_m_kn_resolution", "cos#theta_{n}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "EM_cnkmkn_resolution"],
    # ["EM_c_nr_m_rn_resolution", "cos#theta_{n}^{+}cos#theta_{r}^{-}-cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "EM_cnrmrn_resolution"],
    # ["EM_c_rk_m_kr_resolution", "cos#theta_{r}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "EM_crkmkr_resolution"],
    # # spin polarizations resolutions (EM)
    # ["EM_b_kplus_resolution",   "cos#theta_{k}^{+}", False, 1.8, 8, -2, 2, "EM_bkplus_resolution" ],
    # ["EM_b_nplus_resolution",   "cos#theta_{n}^{+}", False, 1.8, 8, -2, 2, "EM_bnplus_resolution" ],
    # ["EM_b_rplus_resolution",   "cos#theta_{r}^{+}", False, 1.8, 8, -2, 2, "EM_brplus_resolution" ],
    # ["EM_b_kminus_resolution",  "cos#theta_{k}^{-}", False, 1.8, 8, -2, 2, "EM_bkminus_resolution"],
    # ["EM_b_nminus_resolution",  "cos#theta_{n}^{-}", False, 1.8, 8, -2, 2, "EM_bnminus_resolution"],
    # ["EM_b_rminus_resolution",  "cos#theta_{r}^{-}", False, 1.8, 8, -2, 2, "EM_brminus_resolution"],
]
