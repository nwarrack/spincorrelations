from ROOT import TMath

top_observables = [

    #|Varible    |X-axis title                |Log    |maxY  |bins|--range--| file-id |

    # Best reco
    # Top
    ["t_pt" ,    "top p_{T} [GeV]",              True,  1000,  6,    0, 800,  "top_pt"   ],
    ["t_eta",    "top #eta",                     False,  1.8,  10,   -5, 5,    "top_eta"  ],
    ["t_phi",    "top #phi/#pi [rad/#pi]",       False,  1.8,  10,   -1, 1,    "top_phi"  ],

    # tbar
    ["tbar_pt" , "tbar p_{T} [GeV]",              False,  1000, 10,    0, 800,  "tbar_pt"  ],
    ["tbar_eta", "tbar #eta",                     False,  1.8,  10,   -5, 5,    "tbar_eta" ],
    ["tbar_phi", "tbar #phi/#pi [rad/#pi]",       False,  1.8,  10,   -1, 1,    "tbar_phi" ],

    # tt_bar
    ["ttbar_pt" , "ttbar p_{T} [GeV]",            False,  1000, 10,    0, 1000,  "ttbar_pt"  ],
    ["ttbar_pt" , "ttbar mass",                   False,  1000, 10,    0, 3000,  "ttbar_m"   ],
    ["ttbar_eta", "ttbar #eta",                   False,  1.8,  10,   -5, 5,     "ttbar_eta" ],
    ["ttbar_phi", "ttbar #phi/#pi [rad/#pi]",     False,  1.8,  10,   -1, 1,     "ttbar_phi" ],
 
]

