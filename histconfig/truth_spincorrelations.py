truth_spin_observables = [

    # NB: 'log' and 'ymax' only used for plotting

    #|Varible    | X-axis title                   | Log |ymax|bins|-range-|fileID|

    # # spin correlations
    ["c_nn", "cos#theta_{n}^{+}cos#theta_{n}^{-}", False, 1.8, 10, -1, 1, "cnn" ],
    ["c_rr", "cos#theta_{r}^{+}cos#theta_{r}^{-}", False, 1.8, 10, -1, 1, "crr" ],
    ["c_kk", "cos#theta_{k}^{+}cos#theta_{k}^{-}", False, 1.8, 10, -1, 1, "ckk" ],
    ["c_rk", "cos#theta_{r}^{+}cos#theta_{k}^{-}", False, 1.8, 10, -1, 1, "crk" ],
    ["c_kr", "cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 10, -1, 1, "ckr" ],
    ["c_nr", "cos#theta_{n}^{+}cos#theta_{r}^{-}", False, 1.8, 10, -1, 1, "cnr" ],
    ["c_rn", "cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 10, -1, 1, "crn" ],
    ["c_nk", "cos#theta_{n}^{+}cos#theta_{k}^{-}", False, 1.8, 10, -1, 1, "cnk" ],
    ["c_kn", "cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 10, -1, 1, "ckn" ],
    # spin cross correlations
    ["c_nk_p_kn", "cos#theta_{n}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 10, -1, 1, "cnkpkn"],
    ["c_nr_p_rn", "cos#theta_{n}^{+}cos#theta_{r}^{-}+cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 10, -1, 1, "cnrprn"],
    ["c_rk_p_kr", "cos#theta_{r}^{+}cos#theta_{k}^{-}+cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 10, -1, 1, "crkpkr"],
    ["c_nk_m_kn", "cos#theta_{n}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{n}^{-}", False, 1.8, 10, -1, 1, "cnkmkn"],
    ["c_nr_m_rn", "cos#theta_{n}^{+}cos#theta_{r}^{-}-cos#theta_{r}^{+}cos#theta_{n}^{-}", False, 1.8, 10, -1, 1, "cnrmrn"],
    ["c_rk_m_kr", "cos#theta_{r}^{+}cos#theta_{k}^{-}-cos#theta_{k}^{+}cos#theta_{r}^{-}", False, 1.8, 10, -1, 1, "crkmkr"],
    # spin polarizations
    ["b_kplus",   "cos#theta_{k}^{+}", False, 1.8, 10, -1, 1, "bkplus" ],
    ["b_nplus",   "cos#theta_{n}^{+}", False, 1.8, 10, -1, 1, "bnplus" ],
    ["b_rplus",   "cos#theta_{r}^{+}", False, 1.8, 10, -1, 1, "brplus" ],
    ["b_kminus",  "cos#theta_{k}^{-}", False, 1.8, 10, -1, 1, "bkminus"],
    ["b_nminus",  "cos#theta_{n}^{-}", False, 1.8, 10, -1, 1, "bnminus"],
    ["b_rminus",  "cos#theta_{r}^{-}", False, 1.8, 10, -1, 1, "brminus"],
]
