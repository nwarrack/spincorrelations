ttbar_observables = [

    #|Varible        |X-axis title          |Log   |maxY  |bins |-range-|  file-id
    ["ttbar_pt",     "t#bar{t} p_{T} [GeV]", True,  1000,  25,   0, 1000,  "ttbar_pt"  ],
    ["ttbar_mass",   "t#bar{t} mass [GeV]",  True , 1000,  30,   0, 3000,  "ttbar_mass"],
]
