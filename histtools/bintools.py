def add_overflow_to_last_bin(hist):

        if hist.GetBinContent(hist.GetNbinsX()+1) !=0:

            hist.SetBinContent(hist.GetNbinsX(),hist.GetBinContent(hist.GetNbinsX()) + hist.GetBinContent(hist.GetNbinsX()+1))


def divide_by_bin_width(histogram):

    """ Function to divide the entries in an histogram by bin width """

    from ROOT import TH1
    TH1.SetDefaultSumw2()
    
    nbins = histogram.GetNbinsX()
    bin_counter = 1

    while bin_counter <= nbins:
        x_low = histogram.GetXaxis().GetBinLowEdge(bin_counter)
        x_high = histogram.GetXaxis().GetBinUpEdge(bin_counter)
        bin_width = abs(x_high - x_low)
        if not (bin_width == 0):
            bin_content = histogram.GetBinContent(bin_counter)/bin_width
            if not histogram.GetBinContent(bin_counter) == 0:                
                bin_error = histogram.GetBinError(bin_counter)/histogram.GetBinContent(bin_counter)
            else:
                bin_error = 0.0
            histogram.SetBinContent(bin_counter, bin_content)
            histogram.SetBinError(bin_counter, bin_error*bin_content)
        bin_counter = bin_counter + 1
    
    return histogram

# def multiply_by_bin_width(histogram):

#     """ Function to multiply the entries in an histogram by bin width """

#     from ROOT import TH1
#     TH1.SetDefaultSumw2()
    
#     nbins = histogram.GetNbinsX()
#     bin_counter = 1

#     while bin_counter <= nbins:
#         x_low = histogram.GetXaxis().GetBinLowEdge(bin_counter)
#         x_high = histogram.GetXaxis().GetBinUpEdge(bin_counter)
#         bin_width = abs(x_high - x_low)
#         if not (bin_width == 0):
#             bin_content = histogram.GetBinContent(bin_counter)*bin_width
#             if not histogram.GetBinContent(bin_counter) == 0:                
#                 bin_error = histogram.GetBinError(bin_counter)/histogram.GetBinContent(bin_counter)
#             else:
#                 bin_error = 0.0
#             histogram.SetBinContent(bin_counter, bin_content)
#             histogram.SetBinError(bin_counter, bin_error*bin_content)
#         bin_counter = bin_counter + 1
    
#     return histogram
