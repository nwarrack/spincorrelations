EFT_STRING = "504488.MGPy8"

def file_collector(file_list):

    """ function makes a python list (!!) of open root files. Use with caution as method can be slow for big samples with lots of files! """

    from ROOT import TFile
    file_collection = []
    for line in file_list:
        if line.startswith("#"):
            continue
        file_collection.append(TFile.Open(line[:-1],"READ"))
    file_list.seek(0)
    return file_collection


def file_chainer(lofiles, systematic, sample_type):

    from ROOT import TFile, TChain

    chain = TChain(str(systematic))

    for line in lofiles:
        if line.startswith("#"):
            continue
        chain.AddFile(line[:-1])

    lofiles.seek(0)

    return chain


def sample_collector(period, samplelist):

    sample_collection = []
    
    for sample in samplelist.samples:
        if sample[5] == period and EFT_STRING not in sample[0] and "signalsyst" not in sample[4]:
            sample_collection.append(sample)

    return sample_collection


def sample_collector_mconly(period, samplelist):

    sample_collection = []

    for sample in samplelist.samples:

        if sample[5] == period and "data" not in sample[4] and "fake" not in sample[4] and EFT_STRING not in sample[0]:
            sample_collection.append(sample)

    return sample_collection

def sample_collector_eftonly(period, samplelist):

    sample_collection = []

    for sample in samplelist.samples:
        if sample[5] == period and EFT_STRING in sample[0]:
            sample_collection.append(sample)

    return sample_collection


def sample_collector_signalmconly(period, samplelist):

    sample_collection = []

    for sample in samplelist.samples:

        if sample[5] == period and "ttbar" in sample[4] and EFT_STRING not in sample[0]:
            sample_collection.append(sample)

    return sample_collection


def sample_collector_dataandsignalonly(period, samplelist):

    sample_collection = []

    for sample in samplelist.samples:

        if sample[5] == period and ("data" in sample[4] or "ttbar" in sample[4]) and EFT_STRING not in sample[0]:
            sample_collection.append(sample)

    return sample_collection
