def NNLOcorrection(top_pt):

    """
    Function for NNLO reweighting based on the following theory predictions for pt of random top in ttbar:
    NLO pred : [2.54306067, 5.12898782, 4.03037755, 1.93539626, 0.70716282, 0.25751486, 0.08337643, 0.00633206]
    NNLO pred: [2.682306  , 5.225875  , 3.99730202, 1.87610646, 0.67020173, 0.24182458, 0.07781366, 0.00579703]
    Bins: [   0.,   50.,  100.,  160.,  225.,  300.,  360.,  475., 1000.]
    """

    if top_pt >= 0 and top_pt < 50:       return 1.0547550169143232
    elif top_pt >= 50 and top_pt < 100:   return 1.0188901169977822
    elif top_pt >= 100 and top_pt < 160:  return 0.9917934412869087
    elif top_pt >= 160 and top_pt < 225:  return 0.9693655499778634
    elif top_pt >= 225 and top_pt < 300:  return 0.9477332674248911
    elif top_pt >= 300 and top_pt < 360:  return 0.9390703899573019
    elif top_pt >= 360 and top_pt < 475:  return 0.9332812642613747
    elif top_pt >= 475 and top_pt < 1000: return 0.9155045909230172
    else:
        return 1 # no corrections avaliable for random top pt > 1000GeV

def construct_mcweight(e, event_type, WC_index, customweight = None):

    if WC_index == 666: # i.e. if the sample is not a ttbar EFT sample
        if "postsym" in event_type: # event is post ATLAS simulation event
            if customweight is None:
                #return e.weight_mc*e.weight_pileup*e.weight_leptonSF*e.weight_globalLeptonTriggerSF*e.weight_bTagSF_DL1r_85*e.weight_jvt
                #return e.weight_mc*e.weight_pileup*e.weight_leptonSF*e.weight_globalLeptonTriggerSF*e.weight_bTagSF_DL1r_77*e.weight_jvt
                return e.weight_mc*e.weight_pileup*e.weight_leptonSF*e.weight_globalLeptonTriggerSF*e.weight_jvt
            else:
                return e.weight_mc*e.weight_pileup*e.weight_leptonSF*e.weight_globalLeptonTriggerSF*e.weight_bTagSF_DL1r_85*e.weight_jvt*customweight

        elif "particleLevel" in event_type or "truth" in event_type:
            if customweight is None:
                return e.weight_mc
            else:
                return e.weight_mc*customweight
        else:
            print "what's happening to these events??"

    else: # sample is EFT ttbar sample
        if "postsym" in event_type:
            if customweight is None:
                return e.reco_generator_weights[WC_index]*e.weight_pileup*e.weight_leptonSF*e.weight_globalLeptonTriggerSF*e.weight_jvt
            else:
                return e.mc_generator_weights[WC_index]*e.weight_pileup*e.weight_leptonSF*e.weight_globalLeptonTriggerSF*e.weight_bTagSF_DL1r_85*e.weight_jvt*customweight
        elif "particleLevel" in event_type or "truth" in event_type:
            if customweight is None:            
                return e.mc_generator_weights[WC_index]
            else:
                return e.mc_generator_weights[WC_index]*customweight

        #return e.weight_mc
        #return e.weight_mc*e.weight_pileup
        #return e.weight_mc*e.weight_pileup*e.weight_leptonSF
        #return e.weight_mc*e.weight_pileup*e.weight_leptonSF*e.weight_globalLeptonTriggerSF
        #return e.weight_mc*e.weight_pileup*e.weight_leptonSF*e.weight_jvt*e.weight_bTagSF_MV2c10_77*e.weight_globalLeptonTriggerSF


def true_dilepton(truth, acceptance = None, debug = None):

    """
    Function to ascertain if the truth event is a dilepton event. Dilepton
    is defined here by the string 'acceptance'. The fuction returns a bool.
    There may be exceptions due to the W decay sometimes having pdgId = 0.
    As yet this is not understood. This function exists to help avoid such
    events for truth level analysis.

    """

    dilepton_ids = []

    # set dilepton selection in terms of pdgid
    if acceptance is None:
        dilepton_ids = [11, -11, 12, -12, 13, -13, 14, -14, 15, -15, 16, -16]
    else:
        if "e" in acceptance:
            dilepton_ids.append(11) # electron
            dilepton_ids.append(-11)# positron
            dilepton_ids.append(12) # electron neutrino
            dilepton_ids.append(-12)# positron neutrino
        if "mu" in acceptance:
            dilepton_ids.append(13) # muon
            dilepton_ids.append(-13)# anti-muon
            dilepton_ids.append(14) # muon neutrino
            dilepton_ids.append(-14)# muon anti-neutrino
        if "tau" in acceptance:
            dilepton_ids.append(15) # tau
            dilepton_ids.append(-15)# anti-tau
            dilepton_ids.append(16) # tau neutrino
            dilepton_ids.append(-16)# tau anti-neutrino

    W_decay_ids = []
    # W_decay_ids.append(truth.MC_Wdecay1_from_t_pdgId)
    # W_decay_ids.append(truth.MC_Wdecay2_from_t_pdgId)
    # W_decay_ids.append(truth.MC_Wdecay1_from_tbar_pdgId)
    # W_decay_ids.append(truth.MC_Wdecay2_from_tbar_pdgId)
    W_decay_ids.append(truth.d_l_pdgid)
    W_decay_ids.append(truth.d_nubar_pdgid)
    W_decay_ids.append(truth.d_lbar_pdgid)
    W_decay_ids.append(truth.d_nu_pdgid)


    #    if any(particleid == 0 for particleid in W_decay_ids):
    for wdid in W_decay_ids:
        if wdid in dilepton_ids:
            continue
        else:
            if debug == True:
                print "WARNING: at least one truth W decay pdgid", W_decay_ids ,"not defined as dilepton in event number:", truth.eventNumber
            return False

    return True
