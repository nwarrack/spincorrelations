def sum_histos(compiled_hists, hist_total):

    """ function to add all non-data hists together to provide full yields info """

    hist_total = compiled_hists['ttbar'].Clone()

    empty_hist = 1

    # do not consider the following hists
    ignored_hists = {'ttbar', 'migration', 'data', 'ttbarNNLO', 'truth', 'signalsyst'}

    for sample_type in compiled_hists:
        if sample_type in ignored_hists: continue

        empty_hist *= compiled_hists[sample_type].Integral()

    if empty_hist == 0:
        print "WARNING: ROOT is about to add 1 (or more) hist(s) with an integral"
        print "         of zero which may output warnings. However, this is only"
        print "         for yields/entries print-out and does not effect and hist"
        print "         saving (empty hists will be saved as such)."

    for sample_type in compiled_hists:
        if sample_type in ignored_hists: continue
        hist_total.Add(compiled_hists[sample_type])

    return hist_total
