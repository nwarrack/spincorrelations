from ROOT import TH1, TH1D, TH2D

class hist_collection:

    """
    a class to store everything related to one intended plot (signal &
    backg hists). This includes the configuration of the plot (nBins,
    x-range, etc.)
    """

    TH1.SetDefaultSumw2()
    def __init__(self, channel, period, systematic, hist_config, wc):

        ### unpack list of common histogram configurables
        self.hist_info  = hist_config
        self.variable   = hist_config[0]
        self.xaxis      = hist_config[1]
        self.bins       = hist_config[4]
        self.xmin       = hist_config[5]
        self.xmax       = hist_config[6]
        self.varname    = hist_config[7]

        ### common atributes
        self.systematic = systematic
        self.period     = period
        self.channel    = channel
        self.wc         = wc

        if wc == 666:
            self.histname   = period + "_" + channel + "_" + self.varname + "_" + systematic
        else:
            self.histname   = period + "_" + channel + "_" + self.varname + "_" + systematic + '_' + str(self.wc)


        ### Histograms ###
        TH1D.SetDefaultSumw2(True)
        # temp hists are filled and scaled for each sample (then added to a 'master')
        self.temphist = TH1D(self.histname, self.xaxis, self.bins, self.xmin, self.xmax)

        # master histograms
        h_ttbar   = TH1D()
        h_ttx     = TH1D()
        h_mt      = TH1D()
        h_st      = TH1D()
        h_z       = TH1D()
        h_w       = TH1D()
        h_db      = TH1D()
        h_data    = TH1D()
        h_fake    = TH1D()
        h_sigsyst = TH1D()
        h_eft     = TH1D()

        self.master = master_hists = {
            'ttbar'     : h_ttbar,
            'ttx'       : h_ttx,
            'mt'        : h_mt,
            'singletop' : h_st,
            'zjets'     : h_z,
            'wjets'     : h_w,
            'diboson'   : h_db,
            'data'      : h_data,
            'fakes'     : h_fake,
            'signalsyst': h_sigsyst,
            'EFT'       : h_eft,
        }


    ### Functions to add, fill and keep histograms
    # Target hist
    def fill_temphist(self, x, weight):
        self.temphist.Fill(x, weight)
    def keep_temphist(self, sample_type):
        self.temphist.SetDirectory(0)
        if self.master[sample_type].GetEntries()==0:
            self.master[sample_type] = self.temphist.Clone()
            self.master[sample_type].SetDirectory(0)
        else:
            self.master[sample_type].Add(self.temphist)
        self.temphist.Reset("CEI")

    # ttbar reco when event exists at particle-level (possibly with additional cuts)
    def add_ttbarrecowhenpl(self):
        self.temphist_ttbarrecowhenpl = TH1D(self.histname + "_ttbarrecowhenpltmp", self.xaxis, self.bins, self.xmin, self.xmax)
        self.master['ttbarrecowhenpl'] = TH1D(self.histname + "_ttbarrecowhenpl", self.xaxis, self.bins, self.xmin, self.xmax)
    def fill_tempttbarrecowhenpl(self, x, weight):
        self.temphist_ttbarrecowhenpl.Fill(x, weight)
    def keep_tempttbarrecowhenpl(self):
        self.temphist_ttbarrecowhenpl.SetDirectory(0)
        if self.master['ttbarrecowhenpl'].GetEntries()==0:
            self.master['ttbarrecowhenpl'] = self.temphist_ttbarrecowhenpl.Clone()
            self.master['ttbarrecowhenpl'].SetDirectory(0)
        else:
            self.master['ttbarrecowhenpl'].Add(self.temphist_ttbarrecowhenpl)
        self.temphist_ttbarrecowhenpl.Reset("CEI")

    # ttbar reco when event exists at truth-level (possibly with additional cuts)
    def add_ttbarrecowhentr(self):
        self.temphist_ttbarrecowhentr = TH1D(self.histname + "_ttbarrecowhentrtmp", self.xaxis, self.bins, self.xmin, self.xmax)
        self.master['ttbarrecowhentr'] = TH1D(self.histname + "_ttbarrecowhentr", self.xaxis, self.bins, self.xmin, self.xmax)
    def fill_tempttbarrecowhentr(self, x, weight):
        self.temphist_ttbarrecowhentr.Fill(x, weight)
    def keep_tempttbarrecowhentr(self):
        self.temphist_ttbarrecowhentr.SetDirectory(0)
        if self.master['ttbarrecowhentr'].GetEntries()==0:
            self.master['ttbarrecowhentr'] = self.temphist_ttbarrecowhentr.Clone()
            self.master['ttbarrecowhentr'].SetDirectory(0)
        else:
            self.master['ttbarrecowhentr'].Add(self.temphist_ttbarrecowhentr)
        self.temphist_ttbarrecowhentr.Reset("CEI")

    # NNLO hist
    def add_NNLOhist(self):
        self.temphist_nnlo = TH1D(self.histname + "_nnlotmp", self.xaxis, self.bins, self.xmin, self.xmax)
        self.master['ttbarNNLO'] = TH1D(self.histname + "_nnlo", self.xaxis, self.bins, self.xmin, self.xmax)
    def fill_tempNNLO(self, x, weight):
        self.temphist_nnlo.Fill(x, weight)
    def keep_tempNNLO(self):
        self.temphist_nnlo.SetDirectory(0)
        if self.master['ttbarNNLO'].GetEntries()==0:
            self.master['ttbarNNLO'] = self.temphist_nnlo.Clone()
            self.master['ttbarNNLO'].SetDirectory(0)
        else:
            self.master['ttbarNNLO'].Add(self.temphist_nnlo)
        self.temphist_nnlo.Reset("CEI")

    # Truth hist
    def add_truthhist(self):
        self.temphist_tr = TH1D(self.histname + "_trtmp", self.xaxis, self.bins, self.xmin, self.xmax)
        self.master['truth'] = TH1D(self.histname + "_tr", self.xaxis, self.bins, self.xmin, self.xmax)
    def fill_temptruth(self, x, weight):
        self.temphist_tr.Fill(x, weight)
    def keep_temptruth(self):
        self.temphist_tr.SetDirectory(0)
        if self.master['truth'].GetEntries()==0:
            self.master['truth'] = self.temphist_tr.Clone()
            self.master['truth'].SetDirectory(0)
        else:
            self.master['truth'].Add(self.temphist_tr)
        self.temphist_tr.Reset("CEI")

    # Same-sign fakes hist
    def add_sshist(self):
        self.temphist_ss = TH1D(self.histname + "_sstmp", self.xaxis, self.bins, self.xmin, self.xmax)
        self.master['ss'] = TH1D(self.histname + "_ss", self.xaxis, self.bins, self.xmin, self.xmax)
    def fill_tempss(self, x, weight):
        self.temphist_ss.Fill(x, weight)
    def keep_tempss(self):
        self.temphist_ss.SetDirectory(0)
        if self.master['ss'].GetEntries()==0:
            self.master['ss'] = self.temphist_ss.Clone()
            self.master['ss'].SetDirectory(0)
        else:
            self.master['ss'].Add(self.temphist_ss)
        self.temphist_ss.Reset("CEI")

    # Migration matrix
    def add_migration(self):
        self.temphist_mig = TH2D(self.histname + "_migtmp", self.xaxis, self.bins, self.xmin, self.xmax, self.bins, self.xmin, self.xmax)
        self.master['migration'] = TH2D(self.histname + "_mig", self.xaxis, self.bins, self.xmin, self.xmax, self.bins, self.xmin, self.xmax)
    def fill_tempmig(self, x, y, weight = None):
        if weight is not None:
            self.temphist_mig.Fill(x, y, weight)
        else:
            self.temphist_mig.Fill(x, y)
    def keep_tempmig(self):
        self.temphist_mig.SetDirectory(0)
        if self.master['migration'].GetEntries()==0:
            self.master['migration'] = self.temphist_mig.Clone()
            self.master['migration'].SetDirectory(0)
        else:
            self.master['migration'].Add(self.temphist_mig)
        self.temphist_mig.Reset("CEI")
