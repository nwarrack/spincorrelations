import string
import random

data_lumi = {"16a":3219.56+32988.1, "16d":44307.4, "16e":58450.1}
data_lumi["all"] = data_lumi["16a"] + data_lumi["16d"] + data_lumi["16e"]

illegal_hists = {
    "ee"  : ["mu_pt", "mu_eta", "mu_phi", "mu_e"],
    "mumu": ["el_pt", "el_eta", "el_phi", "el_e"],
    "emu" : ["el_pt", "el_eta", "el_phi", "el_e", "mu_pt", "mu_eta", "mu_phi", "mu_e"],
}

def compile_histograms(WC_index, hists, sample_collection, period, systematic, save_truth, info, debug):

    # system imports:
    import os
    import sys
    import resource
    import gc
    # 3rd party imports:
    from ROOT import TFile, gROOT, TH1D
    # project imports:
    from debug import print_component_integrals, print_component_entries
    from histtools import event_loop, add_overflow_to_last_bin, file_chainer, save_histograms

    gROOT.SetBatch(True)

    for sample in sample_collection:

        file_list_name = sample[0] 
        x_sec          = sample[1]
        k_factor       = sample[2]
        sample_number  = sample[3]
        sample_type    = sample[4]

        file_list = open(file_list_name, 'r')
        file_ctr  = 0
        for line in file_list:
            if line.startswith("#"):
                continue
            file_ctr = file_ctr + 1
        file_list.seek(0)

        if info:
            print ""
            print "New sample:", sample_type
        # if debug:
        #     print "DEBUG: No. of files in:", file_list_name, ":", file_ctr
        #     print "DEBUG: x_sec       :", x_sec
        #     print "DEBUG: k factor    :", k_factor


        # helper for temp hist filling
        # todo: fix this! make better?
        for hist in hists:
            hist.sampletype = sample_type 

        if "data" not in sample_type:
            sample_is_mc = True
        else:
            sample_is_mc = False

        
        if "ttbar" in sample_type:

            tot_ttbar_events = 0
            for line in file_list:

                root_file = TFile.Open(line[:-1],"READ")
                sw = getattr(root_file, "sumWeights")

                for entry in sw:
                    if WC_index == 666:
                        print "entry.totalEventsWeighted:", entry.totalEventsWeighted
                        tot_ttbar_events += entry.totalEventsWeighted
                    else:
                        tot_ttbar_events += entry.totalEventsWeighted_mc_generator_weights[WC_index]

                if debug: print "DEBUG: total ttbar events:", tot_ttbar_events



                ## Loop over events in individual files ##
                systematic_tree = getattr(root_file, systematic)
                if info:
                    print "INFO: sample type:", sample_type
                    print "INFO: Looping over events in file:", root_file.GetName()
                    print "INFO: No. of events in file:", systematic_tree.GetEntries()
                if save_truth:
                    event_loop(debug, systematic_tree, WC_index, hists, sample_type, period, root_file.truth)
                else:
                    event_loop(debug, systematic_tree, WC_index, hists, sample_type, period)
                for hist in hists:
                    hist.keep_temphist("ttbar")
                    hist.keep_tempss()
                    hist.keep_tempNNLO()
                    hist.keep_temptruth()
                    hist.keep_tempmig()
                    hist.keep_tempttbarrecowhenpl()
                    hist.keep_tempttbarrecowhentr()
                    print_component_integrals(hist.master, False)
                    print_component_entries(hist.master, False)

                root_file.Delete()
                gc.collect()
                #print "INFO: MRSS:", resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
                #gROOT.cd()
            file_list.seek(0)
            ## end of file loop ##


            # Scale and add overflows to last bin
            sf = data_lumi[period]/(tot_ttbar_events/(x_sec*k_factor))
            if debug: print "total sum weights: ", tot_ttbar_events
            for hist in hists:
                hist.master["ttbar"].Scale(sf)
                hist.master["ttbarrecowhenpl"].Scale(sf)
                hist.master["ttbarrecowhentr"].Scale(sf)
                hist.master["ttbarNNLO"].Scale(sf)
                hist.master["ss"].Scale(sf)
                hist.master["truth"].Scale(sf)
                add_overflow_to_last_bin(hist.master["ttbar"])
                add_overflow_to_last_bin(hist.master["ttbarrecowhenpl"])
                add_overflow_to_last_bin(hist.master["ttbarrecowhentr"])
                add_overflow_to_last_bin(hist.master["ttbarNNLO"])
                add_overflow_to_last_bin(hist.master["ss"])
                add_overflow_to_last_bin(hist.master["truth"])
            
            if debug:
                print "DEBUG: Finished ttbar file-by-file loop. MRSS:" 
                print resource.getrusage(resource.RUSAGE_SELF).ru_maxrss


        else: # do event loop for non-ttbar mc and data

            n_events = 0
            for line in file_list:
                evts = 0
                root_file = TFile.Open(line[:-1],"READ")
                sw = getattr(root_file, "sumWeights")
                for entry in sw: evts += entry.totalEventsWeighted
                n_events += evts
            file_list.seek(0)

            sf = data_lumi[period]/(n_events/(x_sec*k_factor))
            
            # make ROOT chain of all files in sample
            chain = file_chainer(file_list, systematic, sample_type)

            if chain.GetEntries() == 0:
                if info: print "WARNING: No", chain.GetName(), "tree in any files in sample!"
                continue

            event_loop(debug, chain, WC_index, hists, sample_type, period)

            for hist in hists:
                # Scale mc hists
                if sample_is_mc:
                    hist.temphist.Scale(sf)
                    hist.temphist_ss.Scale(sf)
                add_overflow_to_last_bin(hist.temphist)                
                add_overflow_to_last_bin(hist.temphist_ss)
                hist.keep_temphist(sample_type)
                hist.keep_tempss()
            gROOT.cd()


            print_component_integrals(hist.master, False)
            print_component_entries(hist.master, False)

        gc.collect()

        #print "DEBUG: Finished sample. MRSS:", resource.getrusage(resource.RUSAGE_SELF).ru_maxrss


    #print "DEBUG: Finished sample loop. MRSS:", resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

    ## end of sample loop ##




def find_lep(chan, hist):

    """ Function to find the string "lep" in variable. If it is present then a loop is performed over electrons and muons """

    chan_parts = {'ee':'el','mumu':'mu'}
    if hist.find('lep') > -1 :
        quant = hist[hist.index('_')+1:]
        if chan=='emu' or chan=='all': return [chan_parts['ee']+'_'+quant, chan_parts['mumu']+'_'+quant]
        if chan=='ee' or chan=='mumu': return [chan_parts[chan]+'_'+quant]
    else : return [hist]

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    """ Function to return random string of letters and numbers the range may be specified by the first argument """

    return ''.join(random.choice(chars) for x in range(size))


def getScaledErrors(hist):

    """ Function to return the scaled entries of a histogram """

    from ROOT import TH1, TH1D, TH1F
    import math
    TH1.SetDefaultSumw2()

    scaled_error = 0;
    number = hist.GetNbinsX()
    i = 0
    while i <= number:

        scaled_error += pow(hist.GetBinError(i),2)
        i = i + 1

    #if scaled_error==0: print "WARNING no error in histogram ",hist
    return math.sqrt(scaled_error)


def find_MCDataSFs(hist_sum, data, SFs):

    from ROOT import TH1, TH1D, TH1F

    nbins = hist_sum.GetNbinsX()

    # for item, value in hists.iteritems():
    #     if not "data" in item:
    #         if hists[item].Integral() > 0:
    #             mc.Add(hists[item])

    for bin in range(1, nbins + 1):

        # print "bin :", bin
        # print "data:", hists["data"].GetBinContent(bin)
        # print "Mc  :", mc.GetBinContent(bin)

        if data.GetBinContent(bin) == 0:
            SFs.append(0)
        else:
            SFs.append(data.GetBinContent(bin)/hist_sum.GetBinContent(bin))
        # print "SF  :", SFs[bin -1]

    return SFs
