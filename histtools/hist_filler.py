gev_quantities = ["_pt", "_E", "met_met", "_mass"]
pt_ordered_objects = ["lep", "jet"]

def fill_histograms(hists, is_mc, r_event, r_chan, r_success, WC_index, z_axis=None, t_event=None, t_chan=None):

    """
    a per-event function that fills all histograms passed in 'hists'.
    """

    debug = True
        
    # project imports
    from spin import cos_theta, boosted_objects, boosted_objects_truth, boosted_objects_pl
    from histtools import true_dilepton, NNLOcorrection, construct_mcweight
    from math import pi
    # define some variables and bools
    r_hmatch = False # hist channel = event's reco-lev channel
    t_hmatch = False # hist channel = event's truth-lev channel

    #     # check status of 'truth dilepton'
    #     if not true_dilepton(truth_tree, "emu", printWarnings):
    #         return

    # make cos thetas if any are needed
    if z_axis is not None: # <- implies boosted objects are needed
        if t_chan is not None: # <- 'event' is the truth event
            tr_t, tr_tbar, tr_t_lep, tr_tbar_lep = boosted_objects_truth(t_event)
            if tr_t_lep.X() == 0.0 and tr_t_lep.Y() == 0 and tr_t_lep.Z() == 0:
                print "ERROR: something is wrong!"
            cta_t = {}
            cta_t["k"] = cos_theta(tr_t, tr_t_lep, z_axis, "k", "a")
            cta_t["n"] = cos_theta(tr_t, tr_t_lep, z_axis, "n", "a")
            cta_t["r"] = cos_theta(tr_t, tr_t_lep, z_axis, "r", "a")
            ctb_t = {}
            ctb_t["k"] = cos_theta(tr_tbar, tr_tbar_lep, z_axis, "k", "b")
            ctb_t["n"] = cos_theta(tr_tbar, tr_tbar_lep, z_axis, "n", "b")
            ctb_t["r"] = cos_theta(tr_tbar, tr_tbar_lep, z_axis, "r", "b")
        if r_chan is not None:
            if r_success["NW"]:
                t, tbar, t_lep, tbar_lep = boosted_objects(event, "NW")
                cta_NW = {}
                cta_NW["k"] = cos_theta(t, t_lep, z_axis, "k", "a")
                cta_NW["n"] = cos_theta(t, t_lep, z_axis, "n", "a")
                cta_NW["r"] = cos_theta(t, t_lep, z_axis, "r", "a")
                ctb_NW = {}
                ctb_NW["k"] = cos_theta(tbar, tbar_lep, z_axis, "k", "b")
                ctb_NW["n"] = cos_theta(tbar, tbar_lep, z_axis, "n", "b")
                ctb_NW["r"] = cos_theta(tbar, tbar_lep, z_axis, "r", "b")
            if r_success["SN"]:
                t, tbar, t_lep, tbar_lep = boosted_objects(event, "SN")
                cta_SN = {}
                cta_SN["k"] = cos_theta(t, t_lep, z_axis, "k", "a")
                cta_SN["n"] = cos_theta(t, t_lep, z_axis, "n", "a")
                cta_SN["r"] = cos_theta(t, t_lep, z_axis, "r", "a")
                ctb_SN = {}
                ctb_SN["k"] = cos_theta(tbar, tbar_lep, z_axis, "k", "b")
                ctb_SN["n"] = cos_theta(tbar, tbar_lep, z_axis, "n", "b")
                ctb_SN["r"] = cos_theta(tbar, tbar_lep, z_axis, "r", "b")
            if r_success["EM"]:
                t, tbar, t_lep, tbar_lep = boosted_objects(event, "EM")
                cta_EM = {}
                cta_EM["k"] = cos_theta(t, t_lep, z_axis, "k", "a")
                cta_EM["n"] = cos_theta(t, t_lep, z_axis, "n", "a")
                cta_EM["r"] = cos_theta(t, t_lep, z_axis, "r", "a")
                ctb_EM = {}
                ctb_EM["k"] = cos_theta(tbar, tbar_lep, z_axis, "k", "b")
                ctb_EM["n"] = cos_theta(tbar, tbar_lep, z_axis, "n", "b")
                ctb_EM["r"] = cos_theta(tbar, tbar_lep, z_axis, "r", "b")


    # Get weight(s) for event
    if is_mc:
        if r_chan is not None:
            w_r = construct_mcweight(r_event, "postsym", WC_index)
        if t_chan is not None:
            w_t = construct_mcweight(t_event, "truth", WC_index)
    else:
        w_r = 1.0 # <-- data


    # create a NNLO weight based on pT of reconstructed top
    # todo: ADD NNLO REWEIGHTING (There should be NNLOW weights in ntups)


    ### Fill the histograms ###
    for hist in hists:
        #print hist.channel, hist.variable, r_chan, t_chan, hist.systematic, r_event.GetName()
        if hist.systematic != r_event.GetName():
            continue

        # reset some bools
        r_hmatch = False
        t_hmatch = False
        p_hmatch = False

        v = hist.variable

        # check hist channel against truth, reco and particle-level channel
        if hist.channel == r_chan:
            r_hmatch = True
        if hist.channel == t_chan:
            t_hmatch = True

        ## Fill hist that don't depend on ttbar reco
        if not (v.startswith("NW_") or v.startswith("SN_") or v.startswith("EM_")):
            ############# all events now come here - com05 #############

            # set observable-dependent scaling for easy plotting
            sf = 1
            if any(quantity in v for quantity in gev_quantities):
                sf *= 1
            elif "_phi" in v:
                sf *= 1/pi

            if r_hmatch:
                hist.fill_temphist(getattr(r_event, "reco_" + v)*sf, w_r)
                if t_hmatch:
                    hist.fill_tempmig(getattr(r_event, "reco_" + v)*sf, getattr(t_event, "parton_" + v), w_r)
            if t_hmatch:
                hist.fill_temptruth(getattr(t_event, "parton_" + v)*sf, w_t)


        ## Fill NW hists
        elif v.startswith("NW_"):
            # NW top kinomatics
            if v == "NW_t_pt":
                if r_hmatch:
                    fill_kinematics(hist, w_r, r_event, "r")
                


            # spin polarizations (+) (e.g. "NW_b_kplus" or "NW_b_kplus_resolution")
            if v[3] == "b" and v[6] == "p":
                a = v[5]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(cta_NW[a]-cta_p[a], w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(cta_p[a], w_p)
                    if r_hmatch:
                        hist.fill_temphist(cta_NW[a], w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(cta_NW[a], cta_p[a], w_r)
                            hist.fill_tempttbarrecowhenpl(cta_NW[a], w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(cta_NW[a], w_r)


            # spin polarizations (-) (e.g. "NW_b_kminus" or "NW_b_kminus_resolution")
            elif v[3] == "b" and v[6] == "m":
                b = v[5]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(ctb_NW[b]-ctb_p[b], w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(ctb_p[b], w_p)
                    if r_hmatch:
                        hist.fill_temphist(ctb_NW[b], w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(ctb_NW[b], ctb_p[b], w_r)
                            hist.fill_tempttbarrecowhenpl(ctb_NW[b], w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(ctb_NW[b], w_r)

            # spin correlations (e.g. "NW_c_kk", or "NW_c_kk_resolution")
            elif v[3] == "c" and (len(v) == 7 or len(v) == 7+11):
                a = v[5]
                b = v[6]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(cta_NW[a]*ctb_NW[b]-cta_p[a]*ctb_p[b], w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(cta_p[a]*ctb_p[b], w_p)
                    if r_hmatch:
                        hist.fill_temphist(cta_NW[a]*ctb_NW[b], w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(cta_NW[a]*ctb_NW[b], cta_p[a]*ctb_p[b], w_r)
                            hist.fill_tempttbarrecowhenpl(cta_NW[a]*ctb_NW[b], w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(cta_NW[a]*ctb_NW[b], w_r)

            # cross-correlations (e.g. "NW_c_nk_p_kn" or "NW_c_nk_p_kn_resolution")
            elif v[3] == "c"and (len(v) == 12 or len(v) == 12+11):
                a = v[5]
                b = v[6]
                if v[8] == "p": # plus
                    if t_hmatch:
                        val_t = cta_t[a]*ctb_t[b]+cta_t[b]*ctb_t[a]
                    if p_hmatch:
                        val_p = cta_p[a]*ctb_p[b]+cta_p[b]*ctb_p[a]
                    if r_hmatch:
                        val_r = cta_NW[a]*ctb_NW[b]+cta_NW[b]*ctb_NW[a]
                else: # minus
                    if t_hmatch:
                        val_t = cta_t[a]*ctb_t[b]-cta_t[b]*ctb_t[a]
                    if p_hmatch:
                        val_p = cta_p[a]*ctb_p[b]-cta_p[b]*ctb_p[a]
                    if r_hmatch:
                        val_r = cta_NW[a]*ctb_NW[b]-cta_NW[b]*ctb_NW[a]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(val_r - val_p, w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(val_p, w_p)
                    if r_hmatch:
                        hist.fill_temphist(val_r, w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(val_r, val_p, w_r)
                            hist.fill_tempttbarrecowhenpl(val_r, w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(val_r, w_r)


        ## Fill SN hists
        elif v.startswith("SN_"):
            # spin polarizations (+) (e.g. "SN_b_kplus" or "SN_b_kplus_resolution")
            if v[3] == "b" and v[6] == "p":
                a = v[5]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(cta_SN[a]-cta_p[a], w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(cta_p[a], w_p)
                    if r_hmatch:
                        hist.fill_temphist(cta_SN[a], w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(cta_SN[a], cta_p[a], w_r)
                            hist.fill_tempttbarrecowhenpl(cta_SN[a], w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(cta_SN[a], w_r)


            # spin polarizations (-) (e.g. "SN_b_kminus" or "SN_b_kminus_resolution")
            elif v[3] == "b" and v[6] == "m":
                b = v[5]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(ctb_SN[b]-ctb_p[b], w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(ctb_p[b], w_p)
                    if r_hmatch:
                        hist.fill_temphist(ctb_SN[b], w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(ctb_SN[b], ctb_p[b], w_r)
                            hist.fill_tempttbarrecowhenpl(ctb_SN[b], w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(ctb_SN[b], w_r)

            # spin correlations (e.g. "SN_c_kk", or "SN_c_kk_resolution")
            elif v[3] == "c" and (len(v) == 7 or len(v) == 7+11):
                a = v[5]
                b = v[6]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(cta_SN[a]*ctb_SN[b]-cta_p[a]*ctb_p[b], w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(cta_p[a]*ctb_p[b], w_p)
                    if r_hmatch:
                        hist.fill_temphist(cta_SN[a]*ctb_SN[b], w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(cta_SN[a]*ctb_SN[b], cta_p[a]*ctb_p[b], w_r)
                            hist.fill_tempttbarrecowhenpl(cta_SN[a]*ctb_SN[b], w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(cta_SN[a]*ctb_SN[b], w_r)

            # cross-correlations (e.g. "SN_c_nk_p_kn" or "SN_c_nk_p_kn_resolution")
            elif v[3] == "c"and (len(v) == 12 or len(v) == 12+11):
                a = v[5]
                b = v[6]
                if v[8] == "p": # plus
                    if t_hmatch:
                        val_t = cta_t[a]*ctb_t[b]+cta_t[b]*ctb_t[a]
                    if p_hmatch:
                        val_p = cta_p[a]*ctb_p[b]+cta_p[b]*ctb_p[a]
                    if r_hmatch:
                        val_r = cta_SN[a]*ctb_SN[b]+cta_SN[b]*ctb_SN[a]
                else: # minus
                    if t_hmatch:
                        val_t = cta_t[a]*ctb_t[b]-cta_t[b]*ctb_t[a]
                    if p_hmatch:
                        val_p = cta_p[a]*ctb_p[b]-cta_p[b]*ctb_p[a]
                    if r_hmatch:
                        val_r = cta_SN[a]*ctb_SN[b]-cta_SN[b]*ctb_SN[a]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(val_r - val_p, w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(val_p, w_p)
                    if r_hmatch:
                        hist.fill_temphist(val_r, w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(val_r, val_p, w_r)
                            hist.fill_tempttbarrecowhenpl(val_r, w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(val_r, w_r)


        ## Fill EM hists
        elif v.startswith("EM_"):
            # spin polarizations (+) (e.g. "EM_b_kplus" or "EM_b_kplus_resolution")
            if v[3] == "b" and v[6] == "p":
                a = v[5]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(cta_EM[a]-cta_p[a], w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(cta_p[a], w_p)
                    if r_hmatch:
                        hist.fill_temphist(cta_EM[a], w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(cta_EM[a], cta_p[a], w_r)
                            hist.fill_tempttbarrecowhenpl(cta_EM[a], w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(cta_EM[a], w_r)


            # spin polarizations (-) (e.g. "EM_b_kminus" or "EM_b_kminus_resolution")
            elif v[3] == "b" and v[6] == "m":
                b = v[5]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(ctb_EM[b]-ctb_p[b], w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(ctb_p[b], w_p)
                    if r_hmatch:
                        hist.fill_temphist(ctb_EM[b], w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(ctb_EM[b], ctb_p[b], w_r)
                            hist.fill_tempttbarrecowhenpl(ctb_EM[b], w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(ctb_EM[b], w_r)

            # spin correlations (e.g. "EM_c_kk", or "EM_c_kk_resolution")
            elif v[3] == "c" and (len(v) == 7 or len(v) == 7+11):
                a = v[5]
                b = v[6]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(cta_EM[a]*ctb_EM[b]-cta_p[a]*ctb_p[b], w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(cta_p[a]*ctb_p[b], w_p)
                    if r_hmatch:
                        hist.fill_temphist(cta_EM[a]*ctb_EM[b], w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(cta_EM[a]*ctb_EM[b], cta_p[a]*ctb_p[b], w_r)
                            hist.fill_tempttbarrecowhenpl(cta_EM[a]*ctb_EM[b], w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(cta_EM[a]*ctb_EM[b], w_r)

            # cross-correlations (e.g. "EM_c_nk_p_kn" or "EM_c_nk_p_kn_resolution")
            elif v[3] == "c"and (len(v) == 12 or len(v) == 12+11):
                a = v[5]
                b = v[6]
                if v[8] == "p": # plus
                    if t_hmatch:
                        val_t = cta_t[a]*ctb_t[b]+cta_t[b]*ctb_t[a]
                    if p_hmatch:
                        val_p = cta_p[a]*ctb_p[b]+cta_p[b]*ctb_p[a]
                    if r_hmatch:
                        val_r = cta_EM[a]*ctb_EM[b]+cta_EM[b]*ctb_EM[a]
                else: # minus
                    if t_hmatch:
                        val_t = cta_t[a]*ctb_t[b]-cta_t[b]*ctb_t[a]
                    if p_hmatch:
                        val_p = cta_p[a]*ctb_p[b]-cta_p[b]*ctb_p[a]
                    if r_hmatch:
                        val_r = cta_EM[a]*ctb_EM[b]-cta_EM[b]*ctb_EM[a]
                if "resolution" in v:
                    if r_hmatch and p_hmatch:
                        hist.fill_temphist(val_r - val_p, w_r)
                else:
                    if p_hmatch:
                        hist.fill_temptruth(val_p, w_p)
                    if r_hmatch:
                        hist.fill_temphist(val_r, w_r)
                        if p_hmatch:
                            hist.fill_tempmigration(val_r, val_p, w_r)
                            hist.fill_tempttbarrecowhenpl(val_r, w_r)
                        if t_hmatch:
                            hist.fill_tempttbarrecowhentr(val_r, w_r)






def fill_kinematics(hist, weight, event, event_type):

    from math import pi
    from ROOT import TLorentzVector
    from spin import leptons_pl, leptons

    v  = hist.variable
    sf = 1
    
    # set bools
    found_bjets = False

    # set multiplicative factors
    if any(quantity in v for quantity in gev_quantities):
        sf *= 1
    elif "_phi" in v:
        sf *= 1/pi

    # Fill any 'leading' and 'subleading' hists
    if any(pt_obj in v for pt_obj in pt_ordered_objects):
        # b-jets
        if "jetb" in hist.varname:
            if not found_bjets:
                bjet = (idx for idx, jet in enumerate(event.d_jet_isbtagged_MV2c10_77) if jet==1)
                b0 = next(bjet,0)
                b1 = next(bjet,0)
                found_bjets = True
            if "b0" in hist.varname:
                if event_type == "r":
                    hist.fill_temphist(getattr(event, v)[b0]*sf, weight)
                if event_type == "p":
                    hist.fill_temptruth(getattr(event, v)[b0]*sf, weight)
            elif "b1" in hist.varname:
                if event_type == "r":
                    hist.fill_temphist(getattr(event, v)[b1]*sf, weight)
                if event_type == "p":
                    hist.fill_temptruth(getattr(event, v)[b1]*sf, weight)


        # all other (i.e. not b-jets) pt ordered kinematics (e.g. lep stuff)
        elif "0" in hist.varname:
            if event_type == "r":
                hist.fill_temphist(getattr(event, v)[0]*sf, weight)
            if event_type == "p":
                hist.fill_temptruth(getattr(event, v)[0]*sf, weight)
        elif "1" in hist.varname:
            if event_type == "r":
                hist.fill_temphist(getattr(event, v)[1]*sf, weight)
            if event_type == "p":
                hist.fill_temptruth(getattr(event, v)[1]*sf, weight)
        elif "delta" in hist.varname:
            if "_phi" in v:
                deltaphi = abs(getattr(event, v)[0]-getattr(event, v)[1])*sf
                if event_type == "r":
                    hist.fill_temphist(deltaphi, weight) if deltaphi < 1 else hist.fill_temphist(2-deltaphi, weight)
                if event_type == "p":
                    hist.fill_temptruth(deltaphi, weight) if deltaphi < 1 else hist.fill_temptruth(2-deltaphi, weight)
            else:
                if event_type == "r":
                    hist.fill_temphist(abs(getattr(event, v)[0]-getattr(event, v)[1])*sf, weight)
                if event_type == "p":
                    hist.fill_temptruth(abs(getattr(event, v)[0]-getattr(event, v)[1])*sf, weight)

        elif "pair" in hist.varname:
            if "lep_" in hist.varname:
                lep_top  = TLorentzVector()
                lep_tbar = TLorentzVector()
                llbar    = TLorentzVector()
                if event_type == "r":
                    if len(event.d_lep_pdgid) != 2:
                        print "see histtools/hist_filler.py -- whaat (reco)???"
                    lep_top, lep_tbar = leptons(event)
                if event_type == "p":
                    if len(event.d_lep_pdgid) != 2:
                        print "see histtools/hist_filler.py -- whaat (pl)???"
                    lep_top, lep_tbar = leptons_pl(event)
                llbar = lep_top + lep_tbar
                if "_pt" in hist.varname:
                    if event_type == "r":
                        hist.fill_temphist(llbar.Pt()*sf, weight)
                    if event_type == "p":
                        hist.fill_temptruth(llbar.Pt()*sf, weight)
                if "_m" in hist.varname:
                    if event_type == "r":
                        hist.fill_temphist(llbar.M()*sf, weight)
                    if event_type == "p":
                        hist.fill_temptruth(llbar.M()*sf, weight)


    # Fill all non-pt-ordered kinematic hists
    # NB: variable string should be identicle to a branch name with an array length of 1
    else:
        # NW outputs
        if v.startswith("NW_"):
            hist.fill_temphist(getattr(event, "reco_t_pt")*sf, weight)

        # Everything else... e.g. 'met'
        else:
            if event_type == "r":
                hist.fill_temphist(getattr(event, "reco_" + v)*sf, weight)
            if event_type == "t":
                hist.fill_temptruth(getattr(event, "parton_" + v)*sf, weight)
