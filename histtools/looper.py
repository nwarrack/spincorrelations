check_reco_event_chan = {
    "ee"  : "(abs(e.reco_lep_pdgid[0]) + abs(e.reco_lep_pdgid[1]))==22",
    "emu" : "(abs(e.reco_lep_pdgid[0]) + abs(e.reco_lep_pdgid[1]))==24",
    "mumu": "(abs(e.reco_lep_pdgid[0]) + abs(e.reco_lep_pdgid[1]))==26",
    }
check_truth_event_chan = {
    "ee"  : "(abs(e.parton_lbar_pdgid) + abs(e.parton_l_pdgid)) == 22",
    "emu" : "(abs(e.parton_lbar_pdgid) + abs(e.parton_l_pdgid)) == 24",
    "mumu": "(abs(e.parton_lbar_pdgid) + abs(e.parton_l_pdgid)) == 26",
    }

def fake_event_selection_no_ttbar_reco(e, period, channels):
    if e.d_fakeEvent == 0:
        return None
    else:
        return reco_level_event_selection_no_ttbar_reco(e, period, channels)

def reco_level_event_selection_no_ttbar_reco(e, period, channels):

    reco_chan = None
    is_os = False
    is_2b = False

    # same sign event
    if e.reco_lep_pdgid[0] * e.reco_lep_pdgid[1] < 0:
        is_os = True

    # B-jets todo, re-add bjet check
    #if sum(e.d_jet_isbtagged_DL1r_77) > 1:
    #    is_2b = True
    is_2b = True

    if is_os and is_2b:
        for ch in channels:
            if eval(check_reco_event_chan[ ch ]):
                reco_chan = ch

    return reco_chan


def reco_level_event_selection(e, reco_success, period, channels):

    reco_chan  = None
    is_os = False
    is_2b = False

    # same sign event
    if e.d_lep_charge[0] * e.d_lep_charge[1] < 0:
        is_os = True
    # B-jets
    if sum(e.d_jet_isbtagged_DL1r_77) > 1:
        is_2b = True
    # reco success
    if e.d_NW_weight >= 0:
        reco_success["NW"] = True
    if e.d_SN_t_m > 0:
        reco_success["SN"] = True
    if e.d_EM_t_m > 0:
        reco_success["EM"] = True

    # Save event selection (ee/emu/mumu/None)
    if is_2b and is_os:
        if reco_success["NW"] or reco_success["SN"] or reco_success["EM"]:
            # Record event's detector level channel
            for channel in channels:
                if eval(check_reco_event_chan[ period + channel ]):
                    reco_chan = channel

    return reco_chan


def particle_level_event_selection(e, channels):

    pl_chan = None
    is_os       = False
    leps_in_eta = False
    leps_in_pt  = False

    if e.d_lep_pdgid[0] * e.d_lep_pdgid[1] < 0:
        is_os = True
    if abs(e.d_lep_eta[0]) < 2.5 and abs(e.d_lep_eta[1]) < 2.5:
        leps_in_eta = True
    if e.d_lep_pt[0] > 25 and e.d_lep_pt[1] > 25:
        leps_in_pt = True

    # save event selection (ee/emu/mumu/None)
    if is_os and leps_in_eta and leps_in_pt:
        for channel in channels:
            if eval(check_pl_chan[ channel ]):
                pl_chan = channel

    return pl_chan


def truth_level_event_selection(e, channels):
    
    truth_chan  = None
    good_bs = False
    good_leps = False

    # B-jets
    if e.MC_b_from_tbar_pt > 25 and e.MC_b_from_t_pt > 25 and abs(e.MC_b_from_tbar_eta) < 2.5 and abs(e.MC_b_from_t_eta) < 2.5: good_bs = True

    # Leptons
    if e.parton_l_pt > 25 and e.parton_lbar_pt > 25 and abs(e.parton_l_eta) < 2.5 and abs(e.parton_lbar_eta) < 2.5: good_leps = True

    # Save event selection (ee/emu/mumu/None)
    if good_bs and good_leps:
        for channel in channels:
            if eval(check_truth_event_chan[ channel ]):
                truth_chan = channel

    return truth_chan # this will be 'None' if the channel is not requested by create_hists.py


def event_loop(debug, syst, WC_index, hists, sample_type, period, truth=None, dsid=None):


    if debug: debug_Nevents = 200 # event loop will exit after debug_Nevents

    # system imports
    import resource

    # 3rd party imports
    from ROOT import TVector3, TTreeIndex

    # project imports
    from histtools import fill_histograms, true_dilepton

    # set some bools, lists, etc.
    reco_success          = {"NW" : False, "SN" : False, "EM" : False }
    boosted_tops_required = False
    evt_ctr               = 0
    channels              = []
    z_axis                = None
    is_mc = True

    if "data" in sample_type:
        is_mc = False
        
    # Find which channels and objects are needed
    for hist in hists:
        if "ee" in hist.channel and "ee" not in channels:
            channels.append("ee")
        if "emu" in hist.channel and "emu" not in channels:
            channels.append("emu")
        if "mumu" in hist.channel and "mumu" not in channels:
            channels.append("mumu")

        # Check if the boosted tops are required (for spin sensitive obs)
        if hist.variable.startswith("NW") or hist.variable.startswith("SN") or hist.variable.startswith("EM"):
            # Then I assume you need the z-axis for spin sensitive observables
            if boosted_tops_required == False:
                boosted_tops_required = True
                z_axis = TVector3(0,0,1)


    if truth is not None:
        # Build the reco<->truth association if needed    
        syst.BuildIndex("eventNumber", "runNumber")
        if debug: print "DEBUG: reco<->truth index built"

        for event in truth:

            evt_ctr += 1            
            # handy for testing:
            #if evt_ctr > 20000: break
            if debug:
                if evt_ctr % 100 == 0 and  evt_ctr != 0:
                    print "DEBUG (looper.py): Events done:", evt_ctr
                if evt_ctr > debug_Nevents: break
        
            # Reset some per-event bools
            r_chan = None # reco level channel
            t_chan = None # truth level channel
            reco_success["NW"]  = False # NW reco was successful
            reco_success["SN"]  = False # SN reco was successful
            reco_success["EM"]  = False # EM reco was successful


            # get the corresponding reco event
            syst.GetEntryWithIndex(event.eventNumber, event.runNumber)
            if event.eventNumber != syst.eventNumber: # i.e.: event doesnt exist at (ttbar) reco level
                continue
                
            # Reco event selection (i.e. aditional cuts beyond those made by AnalysisTop)
            #r_chan = reco_level_event_selection(syst, reco_success, period, channels)
            r_chan = reco_level_event_selection_no_ttbar_reco(syst, period, channels)
            t_chan = truth_level_event_selection(event, channels)
            if r_chan == None or t_chan == None:
                continue
            else:
                fill_histograms(hists, is_mc, syst, r_chan, reco_success, WC_index, z_axis, event, t_chan)


    else:
        for event in syst:
            
            evt_ctr += 1

            if debug:
                if evt_ctr % 100 == 0 and  evt_ctr != 0:
                    print "DEBUG (looper.py): Events done:", evt_ctr
                if evt_ctr > debug_Nevents:
                    break

            if evt_ctr % 10000 == 0 and evt_ctr != 0:
                print "looper.py: Events done:", evt_ctr

            # Reset some per-event bools
            r_chan = None # reco level channel
            t_chan = None # truth level channel
            reco_success["NW"]  = False # NW reco was successful
            reco_success["SN"]  = False # SN reco was successful
            reco_success["EM"]  = False # EM reco was successful


            #r_chan = reco_level_event_selection(event, reco_success, period, channels)
            if sample_type == "fakes":
                r_chan = fake_event_selection_no_ttbar_reco(event, period, channels)
            else:
                r_chan = reco_level_event_selection_no_ttbar_reco(event, period, channels)

            fill_histograms(hists, is_mc, event, r_chan, reco_success, WC_index, z_axis)

