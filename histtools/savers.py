def save_histograms(eft,
                    master,
                    channel,
                    variable_name,
                    syst_type,
                    period):

    """ Function to save related histograms to root files """

    from ROOT import TH1D, gROOT, TH1, THStack, SetOwnership, gPad, TFile

    gROOT.SetBatch(True)
    TH1.SetDefaultSumw2()

    ## ttbar stuff
    if eft:
        h_ttbar        = master['EFT']
    else:
        h_ttbar        = master["ttbar"]      # nominal ttbar samples
    h_ttbar_truth      = master["truth"]      # nominal ttbar samples (truth)
    h_ttbar_recowhenpl = master["ttbarrecowhenpl"] # nom ttbar (reco when event exists at particle level)
    h_ttbar_recowhentr = master["ttbarrecowhentr"] # nom ttbar (reco when event exists at truth level)
    h_ttbar_NNLO       = master["ttbarNNLO"]  # NNLO reweighted ttbar nominal samples
    h_sigsyst          = master["signalsyst"] # ttbar systematic samples
    migration          = master['migration']  # nominal ttbar truth to reco migration matrix

    ## background
    h_ttx              = master["ttx"]        # tt + X
    h_mt               = master["mt"]         # multi top
    h_st               = master["singletop"]  # singletop samples
    h_z                = master["zjets"]      # drell yann processes
    h_w                = master["wjets"]      # w+jets samples
    h_db               = master["diboson"]    # diboson samples
    h_fake             = master["fakes"]      # fakes from MC truth clasifier
    h_ss               = master["ss"]         # events with Same Sign leptons

    ## data
    h_data             = master["data"]       # data samples


    targetdir = "./user/Hists/" + period + "/"
    #filename = channel + "_" + variable_name + "_" + syst_type + ".root"
    filename = h_ttbar.GetName().replace(period + "_", "", 1) + ".root"
    output_file = TFile(targetdir + filename, "RECREATE")

    print "-----------------------", h_ttbar.Integral()

    h_data.SetName("data")
    h_fake.SetName("fake")
    h_mt.SetName("mt")
    h_ttx.SetName("ttx")
    h_ss.SetName("ss")
    h_db.SetName("diboson")
    h_z.SetName("zjets")
    h_w.SetName("wjets")
    h_st.SetName("singletop")
    h_ttbar.SetName("ttbar")
    h_ttbar_truth.SetName("ttbar_truth")
    h_ttbar_recowhenpl.SetName("ttbar_rwhenpl")
    h_ttbar_recowhentr.SetName("ttbar_rwhentr")
    h_ttbar_NNLO.SetName("ttbar_NNLO")
    h_sigsyst.SetName("sigsyst")
    migration.SetName("migration")


    h_data.Write()
    h_ttx.Write()
    h_mt.Write()
    h_fake.Write()
    h_ss.Write()
    h_db.Write()
    h_z.Write()
    h_w.Write()
    h_st.Write()
    h_ttbar.Write()
    h_ttbar_recowhenpl.Write()
    h_ttbar_recowhentr.Write()
    h_ttbar_truth.Write()
    h_ttbar_NNLO.Write()
    h_sigsyst.Write()
    migration.Write()

    output_file.Close()


def save_histogram(hist,
                   channel,
                   variable_name,
                   syst_type,
                   period):

    """ Function to save single 1D hist """

    from ROOT import TH1, gROOT, TFile

    gROOT.SetBatch(True)
    TH1.SetDefaultSumw2()

    targetdir = "./user/Hists/" + period + "/"
    filename = channel + "_" + variable_name + "_" + syst_type + ".root"
    output_file = TFile(targetdir + filename, "RECREATE")

    hist.SetName(variable_name)
    hist.Write()
    output_file.Close()
