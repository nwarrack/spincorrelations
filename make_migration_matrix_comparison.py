from ROOT import TFile, TTree, TCanvas, TH2D, gROOT, TH1, gStyle, kBlue, kRed, TH1D
gROOT.SetBatch(True)
gStyle.SetOptStat(0)
TH1.SetDefaultSumw2()

def normalise_by_row(matrix, generator =False, reco =False):

    # If reco/particle histograms are not provided, retrieve them (assumes no acceptance effect!)
    if not generator: generator = matrix.ProjectionY()
    if not reco: reco = matrix.ProjectionX()

    acceptance = []
    # Compute acceptance
    for i in range(1, matrix.GetNbinsY()+1):
        #if not generator.GetBinContent(i) == 0: acceptance.append(matrix.ProjectionY().GetBinContent(i)/generator.GetBinContent(i))
        if not reco.GetBinContent(i) == 0: acceptance.append(matrix.ProjectionX().GetBinContent(i)/reco.GetBinContent(i))
        else: acceptance.append(0.)

    sum_of_rows = []
    # Compute normalisation by row of the matrix
    for row in range(1, matrix.GetNbinsY()+1):
        sum_of_this_row = 0.
        for column in range(1, matrix.GetNbinsX()+1):
            sum_of_this_row += matrix.GetBinContent(column, row)
        sum_of_rows.append(sum_of_this_row)

    # Get normalised and acceptance-corrected  output
    for row in range(1, matrix.GetNbinsY()+1):
        for column in range(1, matrix.GetNbinsX()+1):
            bin_content = 0.
            if not sum_of_rows[row-1] == 0 and not acceptance[column-1] == 0:
                bin_content = round((matrix.GetBinContent(column, row)/sum_of_rows[row-1]), 4) * acceptance[column-1]
                bin_error = round((matrix.GetBinError(column, row)/sum_of_rows[row-1]), 4) * acceptance[column-1]
            matrix.SetBinContent(column, row, bin_content)
            matrix.SetBinError(column, row, bin_error)

    return matrix



inputfile = "504488.DAOD_TOPQ1.e8276_a875_r10724_p4346.root"

f = TFile.Open(inputfile, "READ")
r = f.Get("nominal")
t = f.Get("truth")

index = 173 # cqu8 +16.0
sumweights_sm = 252927479.875
sumweights_eft= 358212524.25
nbins = 4

selection_truth = "( (abs(d_l_pdgid)==11 && abs(d_lbar_pdgid)==13) || (abs(d_l_pdgid)==13 && abs(d_lbar_pdgid)==11) )"
selection_reco  = "( d_NW_t_pt>0 && !d_fakeEvent && !d_truth_tau_event && d_lep_n==2 && nBJets>=2 && ( (abs(pdgid_1lep)==11 && abs(pdgid_2lep)==13) || (abs(pdgid_1lep)==13 && abs(pdgid_2lep)==11) ) )"

th1r_sm  = TH1D("th1r_sm","",nbins,-1,1)
th1r_eft = TH1D("th1r_eft","",nbins,-1,1)
th1t_sm  = TH1D("th1t_sm","",nbins,-1,1)
th1t_eft = TH1D("th1t_eft","",nbins,-1,1)
th2_sm  = TH2D("th2_sm","",nbins,-1,1,nbins,-1,1)
th2_eft = TH2D("th2_eft","",nbins,-1,1,nbins,-1,1)

th2_sm.GetXaxis().SetTitle("reco")
th2_sm.GetYaxis().SetTitle("truth")
th2_sm.SetTitle("SM")

th2_eft.GetXaxis().SetTitle("reco")
th2_eft.GetYaxis().SetTitle("truth")
th2_eft.SetTitle("EFT")

print(t.GetEntries(), t.GetEntries(selection_truth))
print(r.GetEntries(), r.GetEntries(selection_reco))

t.BuildIndex("eventNumber")
r.BuildIndex("eventNumber")
r.AddFriend(t.GetName())

r.Project(th1r_sm.GetName(), "cos_raxis_m", "mc_generator_weights[0]/"+str(sumweights_sm)+"*"+selection_reco)
r.Project(th1r_eft.GetName(), "cos_raxis_m", "mc_generator_weights["+str(index)+"]/"+str(sumweights_eft)+"*"+selection_reco)

t.Project(th1t_sm.GetName(), "cos_raxis_m", "mc_generator_weights[0]/"+str(sumweights_sm)+"*"+selection_truth)
t.Project(th1t_eft.GetName(), "cos_raxis_m", "mc_generator_weights["+str(index)+"]/"+str(sumweights_eft)+"*"+selection_truth)

r.Project(th2_sm.GetName(), "truth.cos_raxis_m:cos_raxis_m", "mc_generator_weights[0]/"+str(sumweights_sm)+"*"+selection_reco+"*"+selection_truth)
r.Project(th2_eft.GetName(), "truth.cos_raxis_m:cos_raxis_m", "mc_generator_weights["+str(index)+"]/"+str(sumweights_eft)+"*"+selection_reco+"*"+selection_truth)

c = TCanvas("c","c",800,800)
c.cd()

acc_sm = th2_sm.ProjectionX()
acc_sm.Divide(th1r_sm)
acc_eft = th2_eft.ProjectionX()
acc_eft.Divide(th1r_eft)
acc_sm.SetLineColor(kBlue)
acc_eft.SetLineColor(kRed)
acc_sm.SetMaximum(1.012)
acc_sm.SetMinimum(0.988)
acc_sm.SetTitle("Acceptance")
for x in range(1,acc_sm.GetNbinsX()+1):
    print(acc_sm.GetBinContent(x), acc_eft.GetBinContent(x))
    print(acc_sm.GetBinError(x), acc_eft.GetBinError(x))

eff_sm = th2_sm.ProjectionY()
eff_sm.Divide(th1t_sm)
eff_eft = th2_eft.ProjectionY()
eff_eft.Divide(th1t_eft)
eff_sm.SetLineColor(kBlue)
eff_eft.SetLineColor(kRed)
eff_sm.SetMaximum(1.5)
eff_sm.SetMinimum(-0.5)
eff_sm.SetTitle("Efficiency")
for x in range(1,eff_sm.GetNbinsX()+1):
    print(eff_sm.GetBinContent(x), eff_eft.GetBinContent(x))

c.Clear()
acc_sm.Draw("HIST E")
acc_eft.Draw("HIST SAME")
c.SaveAs("acceptance.png")
c.Clear()
eff_sm.Draw()
eff_eft.Draw("SAME")
c.SaveAs("efficiency.png")


th2_sm  = normalise_by_row(th2_sm)
th2_eft = normalise_by_row(th2_eft)

th2_diff = th2_eft.Clone("th2_diff")
th2_diff.Add(th2_sm,-1)
th2_diff.SetTitle("EFT-SM")

th2_corr = th2_diff.Clone("th2_corr")
for bin_x in range(1, th2_corr.GetNbinsX()+1):
    for bin_y in range(1, th2_corr.GetNbinsY()+1):
        if abs(th2_corr.GetBinError(bin_x,bin_y)) > abs(th2_corr.GetBinContent(bin_x,bin_y)):
            th2_corr.SetBinContent(bin_x,bin_y,0)
th2_corr.SetTitle("EFT-SM (significant bins only)")


gStyle.SetPadTopMargin(    0.12)
gStyle.SetPadRightMargin(  0.18)
gStyle.SetPadBottomMargin( 0.15)
gStyle.SetPadLeftMargin(   0.15)
gStyle.SetPaintTextFormat(".3f")

th2_sm.Draw("COLZ TEXT E")
c.SaveAs("migration_SM.png")
c.Clear()
th2_eft.Draw("COLZ TEXT E")
c.SaveAs("migration_EFT.png")
c.Clear()
th2_diff.Draw("COLZ TEXT E")
c.SaveAs("migration_DIFF.png")
c.Clear()
th2_corr.Draw("COLZ TEXT E")
c.SaveAs("migration_DIFFcorrected.png")


f.Close()
