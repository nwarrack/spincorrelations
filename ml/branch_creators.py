def create_branch(tree, branch_name, typ):
    """Function to create a new branch for tree based on a branch_name string and
       a typ string that specifies the data type of the branch"""
    from array import array
    if typ == "i":
        type_init = [[0], "/I"]
    if typ == "d":
        type_init = [[0.0], "/D"]
    branch = array(typ, type_init[0])
    tree.Branch(branch_name, branch, str(branch_name) + type_init[1])
    return branch

def create_ptEtaPhi_branches(tree, object_name, typ):
    """Function to create pt eta and phi branches for a physics object"""
    pt  = object_name + "_pt"
    eta = object_name + "_eta"
    phi = object_name + "_phi"
    pt_branch  = create_branch(tree, pt , typ)
    eta_branch = create_branch(tree, eta, typ)
    phi_branch = create_branch(tree, phi, typ)
    return pt_branch, eta_branch, phi_branch
