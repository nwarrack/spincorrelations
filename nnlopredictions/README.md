Some instructions to get the values using uproot (python3)
```python3
import uproot
myfile = uproot.open("truth_4j2b_ljet_lhoodcut_klfitter_bestPerm_topHad_pt_nnlo_NNPDF31_nnlo_as_0118_ljet.root")
myfile.keys()
myfile["TheoryXs_abs"].show()
```
this will return:
```
                     0                                                    5.4872
                     +---------------------------------------------------------+
[-inf, 0)   0        |                                                         |
[0, 50)     2.6823   |****************************                             |
[50, 100)   5.2259   |******************************************************   |
[100, 160)  3.9973   |******************************************               |
[160, 225)  1.8761   |*******************                                      |
[225, 300)  0.6702   |*******                                                  |
[300, 360)  0.24182  |***                                                      |
[360, 475)  0.077814 |*                                                        |
[475, 1000) 0.005797 |                                                         |
[1000, inf] 0        |                                                         |
                     +---------------------------------------------------------+
```
To get the exact number you can do:
```
myfile["TheoryXs_abs"].numpy()
```
