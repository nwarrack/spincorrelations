from ROOT import TH1D

def make_dict(infile):

    """
    Function returns a dictionary of the hists in a _standard_ root file produced
    by the create_hists.py script (this function defines the standard)
    """

    master = {}
    
    # ttbar stuff
    master["ttbar"]       = infile.Get("ttbar")
    master["ttbar_rwhenpl"] = infile.Get("ttbar_rwhenpl")
    master["ttbar_rwhentr"] = infile.Get("ttbar_rwhentr")
    master["ttbar_truth"] = infile.Get("ttbar_truth")
    master["ttbar_NNLO"]  = infile.Get("ttbar_NNLO")
    master["sigsyst"]   = infile.Get("sigsyst")
    master["migration"]   = infile.Get("migration")

    # backgrounds
    master["zjets"]       = infile.Get("zjets")
    master["wjets"]       = infile.Get("wjets")
    master["diboson"]     = infile.Get("diboson")
    master["singletop"]   = infile.Get("singletop")
    master["fakes"]       = infile.Get("fake") # note change of name

    # data
    master["data"]        = infile.Get("data")


    return master

def combine_dicts(in1, in2, in3):

    """
    Function accepts three dictionaries of root file histograms and
    returns a combined dictionary. Because there are three channels
    and three periods this can be used to combine channels or periods
    """
    
    master = {}
    
    for key in in1:
        master[key] = in1[key].Clone()
        master[key].Add(in2[key])
        master[key].Add(in3[key])
    
    return master
