def make_mean(x, y, mean, mean_type, size, colour):

    """ Function to make a mean TLatex Box """

    from ROOT import TLatex

    mean = "  " + str(mean_type) + " = " + str(round(mean,5))

    meantext = TLatex()
    meantext.SetNDC()
    meantext.SetTextColor(1)
    meantext.SetTextSize(size)
    meantext.DrawLatex(x, y, mean)

    dot = "--"
    meandot = TLatex()
    meandot.SetNDC()
    meandot.SetTextColor(colour)
    meandot.SetTextSize(size+0.01)
    meandot.DrawLatex(x-0.01, y, dot)

def make_delta_mean(x, y, mean1, mean2, arg1, arg2, size):

    """ Function to make a delta mean TLatex Box """

    from ROOT import TLatex

    deltamean = arg1 +"-" + arg2 + " = "+ str(round(mean1-mean2, 5))

    meantext = TLatex()
    meantext.SetNDC()
    meantext.SetTextColor(1)
    meantext.SetTextSize(size)
    meantext.DrawLatex(x, y, deltamean)


def make_chi2(x, y, chi2, size, name1 = None, name2 = None):

    """ Function to make a delta mean TLatex Box """

    from ROOT import TLatex
    if name1 == None:
        chi2legend = "#chi^{2} = "+ str(chi2)
    else:
        chi2legend = "#chi^{2}(" + str(name1) +"," +str(name2) + ") = "+ str(chi2)

    chi2text = TLatex()
    chi2text.SetNDC()
    chi2text.SetTextColor(1)
    chi2text.SetTextSize(size)
    chi2text.DrawLatex(x, y, chi2legend)

def make_generic(x, y, intext, size):

    """ Function to make a generic TLatex Box """

    from ROOT import TLatex

    text = TLatex()
    text.SetNDC()
    text.SetTextColor(1)
    text.SetTextSize(size)
    text.DrawLatex(x, y, intext)


def make_lumi(x, y, lumi, size):

    """ Function to make an luminosity TLatex Box """

    from ROOT import TLatex

    luminosity = "#int L dt = " + str(lumi) + " fb^{-1}"

    lumitext = TLatex()
    lumitext.SetNDC()
    lumitext.SetTextColor(1)
    lumitext.SetTextSize(size)
    lumitext.DrawLatex(x, y, luminosity)

def make_energy(x, y, energy, size):

    """ Function to make an ATLAS sqrt(s) TLatex Box """

    from ROOT import TLatex

    col_energy = "#bf{#sqrt{s} = " + str(energy) + " TeV}"

    energytext = TLatex()
    energytext.SetNDC()
    energytext.SetTextColor(1)
    energytext.SetTextSize(size)
    energytext.DrawLatex(x, y, col_energy)

def make_ATLAS_signal_region( x, y, text, color, size):

    """ Function to make an ATLAS signal region TLatex Box """

    from ROOT import TLatex

    energytext = TLatex()
    energytext.SetNDC()
    energytext.SetTextColor(color)
    energytext.SetTextSize(size)
    energytext.DrawLatex(x, y, text)
