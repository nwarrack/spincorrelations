LEGEND_TOPLEFT_X           = 0.2
LEGEND_TOPLEFT_Y           = 0.70
LEGEND_TOPRIGHT_X          = 0.649
LEGEND_TOPRIGHT_Y          = 0.91
LEGEND_WIDTH               = 0.30
LEGEND_HEIGHT              = 0.30


def make_mig_plot(master,
                  channel,
                  histogram,
                  syst_type,
                  luminosity,
                  period):

    if "resolution" in histogram[0]:
        print "ERROR: non-resolution plot passed: ", histogram[0]
        return 0
    else:
        from ROOT import TH2F, TLegend, TLatex, TCanvas, TPad, TStyle, gROOT, TH1, THStack, TLine, SetOwnership, gPad, TFile, kRed, kBlue, kBlue, kGreen, TGaxis
        from plot import atlas_style, make_ATLAS_label_work, make_lumi, make_energy, make_mean, make_delta_mean, make_chi2, make_generic
        from histtools import divide_by_bin_width

        gROOT.SetBatch(True)
        AtlasStyle = atlas_style()
        gROOT.SetStyle("ATLAS")
        gROOT.ForceStyle()
        TH2F.SetDefaultSumw2()
        TGaxis.SetMaxDigits(2)

        histname = histogram[7]
        ttbar  = master["migration"]
        c1 = TCanvas("c1","demo bin labels",10,10,600,600);

        #ttbar = divide_by_bin_width(ttbar)

        # draw resolution values
        mainPad =  TPad("mainPad", "top",    0.1, 0.26, 1.0, 1.0)
        #mainPad.SetBottomMargin(0.0)
        #mainPad.SetRightMargin(500)
        #mainPad.SetLeftMargin(1.0)
        #mainPad.SetTopMargin(0.065)
        mainPad.SetLogy(1)
        mainPad.Draw()
        ttbar.Draw("COLZ")

        ### Set Axis labels
        ttbar.GetXaxis().SetTitle(histogram[1])
        ttbar.GetYaxis().SetTitle(histogram[1]+ " (reconstructed)")
        ttbar.GetYaxis().SetLabelSize(0.04)
        ttbar.GetYaxis().SetTitleSize(0.04)
        ttbar.GetYaxis().SetTitleOffset(1.8)
        ttbar.GetXaxis().SetLabelSize(0.04)
        ttbar.GetXaxis().SetTitleSize(0.04)
        ttbar.GetXaxis().SetTitleOffset(1.1)

        # save plot
        name = "./user/Plots/" + period + "/" + channel + "_" + histname +"_"+ syst_type +"_mig.pdf"
        #name = "/eos/user/n/nwarrack/Plots/" + period + "/" + channel + "_" + histname +"_"+ syst_type +"_mig.pdf"
        c1.Print(name)
        return 1



def make_resolution_plot(master,
                         channel,
                         histogram,
                         syst_type,
                         legend_location, # 1 = top left, 2 = top right
                         luminosity,
                         period):

    if "resolution" in histogram[0]:
        print "ERROR: non-resolution plot passed: ", histogram[0]
        return 0
    else:
        from ROOT import TH1D, TLegend, TLatex, TCanvas, TPad, TStyle, gROOT, TH1, THStack, TLine, SetOwnership, gPad, TFile, kRed, kBlue, kBlue, kGreen, TGaxis
        from plot import atlas_style, make_ATLAS_label_work, make_lumi, make_energy, make_mean, make_delta_mean, make_chi2, make_generic
        from histtools import divide_by_bin_width

        gROOT.SetBatch(True)
        AtlasStyle = atlas_style()
        gROOT.SetStyle("ATLAS")
        gROOT.ForceStyle()
        TH1.SetDefaultSumw2()
        TGaxis.SetMaxDigits(2)

        histname = histogram[7]
        ttbar  = master["ttbar"]
        c1     = TCanvas()
        ttbar = divide_by_bin_width(ttbar)

        # draw resolution values
        mainPad =  TPad("mainPad", "top",    0.1, 0.26, 1.0, 1.0)
        mainPad.SetBottomMargin(0.0)
        mainPad.SetRightMargin(1.0)
        mainPad.SetLeftMargin(1.0)
        mainPad.SetTopMargin(0.065)
        mainPad.SetLogy(1)
        mainPad.Draw()
        ttbar.Draw("HIST")

        # draw line at mean value
        mean      = ttbar.GetMean()
        mean_line = TLine(mean, 0, mean, 1.05*ttbar.GetBinContent(ttbar.GetMaximumBin()))
        SetOwnership(mean_line, False)
        mean_line.SetLineColor(kRed)
        mean_line.SetLineWidth(2)
        mean_line.SetLineStyle(2)
        mean_line.Draw("SAME")
        make_mean(0.2, 0.72, mean, "Mean",  0.03, 2) # 2=kGreen

        # add standard deviation info
        stddev = round(ttbar.GetStdDev(),3)
        make_generic(0.2, 0.68, "#sigma = " + str(stddev), 0.03)

        # add titles
        if legend_location == 2:
            make_ATLAS_label_work( 0.2, 0.88, 1, mainPad, 0.04, " Internal")
            make_lumi(             0.2, 0.83, round(luminosity/1000,2), 0.03)
            make_energy(           0.23, 0.79, 13, 0.03)
        elif legend_location == 1:
            make_ATLAS_label_work( 0.63, 0.85, 1, mainPad, 0.05, "Internal")
            make_lumi(       0.63, 0.78, round(luminosity/1000,2),   0.05)
            make_energy(     0.72, 0.7, 13,     0.05)


        ### Set Axis labels
        yaxis_label = "Events / Bin Width"
        if "KIN" in histname:
            ttbar.GetXaxis().SetTitle(histogram[1]+ "(KIN Reco-Truth)")
        elif "NW" in histname:
            ttbar.GetXaxis().SetTitle(histogram[1]+ "(NW Reco-Truth)")
        else:
            ttbar.GetXaxis().SetTitle(histogram[1]+ "(Reco-Truth)")
        ttbar.GetYaxis().SetTitle(yaxis_label)
        ttbar.GetYaxis().SetLabelSize(0.04)
        #ttbar.GetXaxis().SetLabelSize(0.0)
        ttbar.GetYaxis().SetTitleSize(0.04)
        ttbar.GetYaxis().SetTitleOffset(1.8)

        ttbar.GetXaxis().SetLabelSize(0.04)
        ttbar.GetXaxis().SetTitleSize(0.04)
        ttbar.GetXaxis().SetTitleOffset(1.1)

        # save plot
        name = "./user/Plots/" + period + "/" + channel + "_" + histname +"_"+ syst_type +".pdf"
        c1.Print(name)
        return 1

def make_eft_plot(master,
                  channel,
                  histogram,
                  syst_type,
                  legend_location, # 1 = top left, 2 = top right
                  luminosity,
                  period):

    if "resolution" not in histogram[0]:
        from ROOT import TH1D, TLegend, TLatex, TCanvas, TPad, TStyle, gROOT, TH1, THStack, TLine, SetOwnership, gPad, TFile, kRed, kBlue, kBlue, kGreen, TGaxis
        from plot import atlas_style, make_ATLAS_label_work, make_lumi, make_energy, make_mean, make_delta_mean, make_chi2, make_generic
        from histtools import divide_by_bin_width

        gROOT.SetBatch(True)
        AtlasStyle = atlas_style()
        gROOT.SetStyle("ATLAS")
        gROOT.ForceStyle()
        TH1.SetDefaultSumw2()
        TGaxis.SetMaxDigits(2)

        histname = histogram[7]
        ttbar  = master["ttbar"]
        c1     = TCanvas()
        ttbar = divide_by_bin_width(ttbar)

        # draw resolution values
        mainPad =  TPad("mainPad", "top",    0.1, 0.26, 1.0, 1.0)
        mainPad.SetBottomMargin(0.0)
        mainPad.SetRightMargin(1.0)
        mainPad.SetLeftMargin(1.0)
        mainPad.SetTopMargin(0.065)
        mainPad.SetLogy(1)
        mainPad.Draw()
        ttbar.Draw("HIST")

        # draw line at mean value
        mean      = ttbar.GetMean()
        mean_line = TLine(mean, 0, mean, 1.05*ttbar.GetBinContent(ttbar.GetMaximumBin()))
        SetOwnership(mean_line, False)
        mean_line.SetLineColor(kRed)
        mean_line.SetLineWidth(2)
        mean_line.SetLineStyle(2)
        mean_line.Draw("SAME")
        make_mean(0.2, 0.72, mean, "Mean",  0.03, 2) # 2=kGreen

        # add standard deviation info
        stddev = round(ttbar.GetStdDev(),3)
        make_generic(0.2, 0.68, "#sigma = " + str(stddev), 0.03)

        # add titles
        if legend_location == 2:
            make_ATLAS_label_work( 0.2, 0.88, 1, mainPad, 0.04, " Internal")
            make_lumi(             0.2, 0.83, round(luminosity/1000,2), 0.03)
            make_energy(           0.23, 0.79, 13, 0.03)
        elif legend_location == 1:
            make_ATLAS_label_work( 0.63, 0.85, 1, mainPad, 0.05, "Internal")
            make_lumi(       0.63, 0.78, round(luminosity/1000,2),   0.05)
            make_energy(     0.72, 0.7, 13,     0.05)


        ### Set Axis labels
        yaxis_label = "Events / Bin Width"
        if "KIN" in histname:
            ttbar.GetXaxis().SetTitle(histogram[1]+ "(KIN)")
        elif "NW" in histname:
            ttbar.GetXaxis().SetTitle(histogram[1]+ "(NW)")
        else:
            ttbar.GetXaxis().SetTitle(histogram[1])
        ttbar.GetYaxis().SetTitle(yaxis_label)
        ttbar.GetYaxis().SetLabelSize(0.04)
        #ttbar.GetXaxis().SetLabelSize(0.0)
        ttbar.GetYaxis().SetTitleSize(0.04)
        ttbar.GetYaxis().SetTitleOffset(1.8)

        ttbar.GetXaxis().SetLabelSize(0.04)
        ttbar.GetXaxis().SetTitleSize(0.04)
        ttbar.GetXaxis().SetTitleOffset(1.1)

        # save plot
        name = "./user/Plots/" + period + "/" + channel + "_" + histname +"_"+ syst_type +".pdf"
        c1.Print(name)
        return 1
    else:
        print "ERROR: resolution plot passed: ", histogram[0]
        return 0


def make_simple_mc_plot(master,
                        channel,
                        histogram,
                        syst_type,
                        legend_location, # 1 = top left, 2 = top right
                        draw_mean_value,
                        luminosity,
                        period):

    if "resolution" not in histogram[0]:
        from ROOT import TH1D, TLegend, TLatex, TCanvas, TPad, TStyle, gROOT, TH1, THStack, TLine, SetOwnership, gPad, TFile, kRed, kBlue, kBlue, kGreen, TGaxis
        from plot import atlas_style, make_ATLAS_label_work, make_lumi, make_energy, make_mean, make_delta_mean, make_chi2, make_generic
        from histtools import divide_by_bin_width

        gROOT.SetBatch(True)
        AtlasStyle = atlas_style()
        gROOT.SetStyle("ATLAS")
        gROOT.ForceStyle()
        TH1.SetDefaultSumw2()
        TGaxis.SetMaxDigits(2)
        print "entries:", master["ttbar"].GetEntries()
        histname = histogram[7]
        ttbar  = master["ttbar"]
        c1     = TCanvas()
        ttbar = divide_by_bin_width(ttbar)

        # draw resolution values
        mainPad =  TPad("mainPad", "top",    0.1, 0.26, 1.0, 1.0, 5)
        mainPad.SetBottomMargin(0.0) # look in atlas_labels.py to edit these??
        mainPad.SetRightMargin(0.15)
        mainPad.SetLeftMargin(0.0)
        mainPad.SetTopMargin(0.065)
        mainPad.SetLogy(1)
        mainPad.Draw()
        ttbar.Draw("HIST")

        # draw line at mean value
        if draw_mean_value:
            mean      = ttbar.GetMean()
            mean_line = TLine(mean, 0, mean, 1.05*ttbar.GetBinContent(ttbar.GetMaximumBin()))
            SetOwnership(mean_line, False)
            mean_line.SetLineColor(kRed)
            mean_line.SetLineWidth(2)
            mean_line.SetLineStyle(2)
            mean_line.Draw("SAME")
            make_mean(0.2, 0.72, mean, "Mean",  0.03, 2) # 2=kGreen

            # add standard deviation info
            stddev = round(ttbar.GetStdDev(),3)
            make_generic(0.2, 0.68, "#sigma = " + str(stddev), 0.03)

        # add titles
        if legend_location == 2:
            # make_ATLAS_label_work( 0.2, 0.88, 1, mainPad, 0.04, " Internal")
            # make_lumi(             0.2, 0.83, round(luminosity/1000,2), 0.03)
            # make_energy(           0.23, 0.79, 13, 0.03)
            #make_ATLAS_label_work( 0.63, 0.85, 1, mainPad, 0.05, "Internal")
            make_ATLAS_label_work( 0.56, 0.85, 1, mainPad, 0.05, "Internal.") # x,y,colour,pad,size,word
            make_lumi(       0.56, 0.78, round(luminosity/1000,2),   0.05)
            make_energy(     0.62, 0.7, 13,     0.05)

        elif legend_location == 1:
            make_ATLAS_label_work( 0.63, 0.85, 1, mainPad, 0.05, "Internal")
            make_lumi(       0.63, 0.78, round(luminosity/1000,2),   0.05)
            make_energy(     0.72, 0.7, 13,     0.05)


        ### Set Axis labels
        yaxis_label = "Events / Bin Width"
        if "KIN" in histname:
            ttbar.GetXaxis().SetTitle(histogram[1]+ "(KIN)")
        elif "NW" in histname:
            ttbar.GetXaxis().SetTitle(histogram[1]+ "(NW)")
        else:
            ttbar.GetXaxis().SetTitle(histogram[1])
        ttbar.GetYaxis().SetTitle(yaxis_label)
        ttbar.GetYaxis().SetLabelSize(0.04)
        #ttbar.GetXaxis().SetLabelSize(0.0)
        ttbar.GetYaxis().SetTitleSize(0.04)
        ttbar.GetYaxis().SetTitleOffset(1.8)

        ttbar.GetXaxis().SetLabelSize(0.04)
        ttbar.GetXaxis().SetTitleSize(0.04)
        ttbar.GetXaxis().SetTitleOffset(1.1)

        # save plot
        name = "./user/Plots/" + period + "/" + channel + "_" + histname +"_"+ syst_type +".pdf"
        c1.Print(name)
        return 1
    else:
        print "ERROR: resolution plot passed: ", histogram[0]
        return 0



def make_datamc_plot(master,
                     channel,
                     histogram,
                     syst_type,
                     draw_uncertainty_band,
                     draw_mean_value_line,
                     plot_NNLO,
                     remove_backgrounds,
                     normalise_MC_to_data,
                     legend_location, # 1 = top left, 2 = top right
                     luminosity,
                     period):

    """ Function to plot the double histograms """

    debug = True


    histname = histogram[7]
    if "resolution" in histname:
        print "ERROR: resolution hist passed, skipping plot..."
        return 0

    # third party imports:
    from ROOT import TH1D, TLegend, TLatex, TCanvas, TPad, TStyle, gROOT, TH1, THStack, TLine, SetOwnership, gPad, TFile, kRed, kBlue, kBlue, kGreen

    # project imports:
    from plot import TEXTSIZE, LINEWIDTH
    from plot import atlas_style, make_ATLAS_label_work, make_lumi, make_energy, make_mean, make_delta_mean, make_chi2
    from histtools import divide_by_bin_width


    zjets       = master["zjets"]
    wjets       = master["wjets"]
    diboson     = master["diboson"]
    singletop   = master["singletop"]
    fakes       = master["fakes"]
    data        = master["data"]

    if plot_NNLO:
        ttbar   = master["ttbar_NNLO"]
    else:
        ttbar   = master["ttbar"]

    if data.Integral() == 0:
        if debug: print "data entires :", data.GetEntries()
        print "ERROR: data hist has integral of zero... aborting."
        return 0
    else:
        print "data entires :", data.GetEntries()

    if debug:
        for x in master:
            print master[x]
            print " -> entries  :", master[x].GetEntries()
            print " -> integral :", master[x].Integral()



    gROOT.SetBatch(True)
    AtlasStyle = atlas_style()
    gROOT.SetStyle("ATLAS")
    gROOT.ForceStyle()
    TH1.SetDefaultSumw2()

    # Make kBlue transparent
    blue = gROOT.GetColor(kBlue-10)
    blue.SetAlpha(0.6)

    if remove_backgrounds:
        emptyhist = TH1D()
        data.Add(fakes,-1)
        data.Add(zjets,-1)
        data.Add(wjets,-1)
        data.Add(diboson,-1)
        data.Add(singletop,-1)
        fakes     = emptyhist
        zjets     = emptyhist
        wjets     = emptyhist
        diboson   = emptyhist
        singletop = emptyhist
        print "data integral after bckgrm :", data.Integral()

    # Divide by bin width
    yaxis_label = "Events / Bin Width"
    data      = divide_by_bin_width(data)
    fakes     = divide_by_bin_width(fakes)
    diboson   = divide_by_bin_width(diboson)
    zjets     = divide_by_bin_width(zjets)
    wjets     = divide_by_bin_width(wjets)
    singletop = divide_by_bin_width(singletop)
    ttbar     = divide_by_bin_width(ttbar)


    background = TH1D("background","background",data.GetNbinsX(), data.GetXaxis().GetBinLowEdge(1),data.GetXaxis().GetBinUpEdge(data.GetNbinsX()))
    if fakes.Integral() > 0:
        background.Add(fakes)
    if diboson.Integral() > 0 :
        background.Add(diboson)
    if zjets.Integral() > 0:
        background.Add(zjets)
    if wjets.Integral() > 0:
        background.Add(wjets)
    if singletop.Integral() > 0:
        background.Add(singletop)

    Stack = THStack("Stack", "Stack")
    sum_all = ttbar.Clone("sum_all")

    fakes.SetLineWidth(LINEWIDTH)
    diboson.SetLineWidth(LINEWIDTH)
    zjets.SetLineWidth(LINEWIDTH)
    wjets.SetLineWidth(LINEWIDTH)
    singletop.SetLineWidth(LINEWIDTH)
    ttbar.SetLineWidth(LINEWIDTH)

    zjets.SetFillColor(95)
    wjets.SetFillColor(kGreen)
    singletop.SetFillColor(62)
    diboson.SetFillColor(5)
    fakes.SetFillColor(9)


    ### Hard coded cross section/normalisation uncertainties ###
    ttbar_uncert      = 0.05
    dy_uncert         = 0.10
    zjets_uncert      = 0.34
    singletop_uncert  = 0.054
    diboson_uncert    = 0.34

    if normalise_MC_to_data:
        if (zjets.Integral() > 0):
            sum_all.Add(zjets)
        if (wjets.Integral() > 0):
            sum_all.Add(wjets)
        if (ztau.Integral() > 0):
            sum_all.Add(ztau)
        if (diboson.Integral() > 0):
            sum_all.Add(diboson)
        if (singletop.Integral() > 0):
            sum_all.Add(singletop)
        if (fakes.Integral() > 0):
            sum_all.Add(fakes)

        scale = (data.Integral()/sum_all.Integral())
        zjets.Scale(scale)
        wjets.Scale(scale)
        ztau.Scale(scale)
        diboson.Scale(scale)
        singletop.Scale(scale)
        fakes.Scale(scale)
        ttbar.Scale(scale)
        background.Scale(scale)
    sum_all.Reset()

    if (ttbar.Integral() > 0):
        sum_all.Add(ttbar)
    if (zjets.Integral() > 0):
        sum_all.Add(zjets)
    if (wjets.Integral() > 0):
        sum_all.Add(wjets)
    if (diboson.Integral() > 0):
        sum_all.Add(diboson)
    if (singletop.Integral() > 0):
        sum_all.Add(singletop)
    if (fakes.Integral() > 0):
        sum_all.Add(fakes)

    # Don't add stuff to the Stack if it's empty. Root doesn't like it
    if (zjets.Integral() > 0):
        Stack.Add(zjets)
    if (wjets.Integral() > 0):
        Stack.Add(wjets)
    if (diboson.Integral() > 0):
        Stack.Add(diboson)
    if (singletop.Integral() > 0):
        Stack.Add(singletop)
    if (fakes.Integral() > 0):
        Stack.Add(fakes)
    if (ttbar.Integral() > 0):
        Stack.Add(ttbar)


    upwards_uncertainties            = []
    downwards_uncertainties          = []
    upwards_uncertainties_baseline   = []
    downwards_uncertainties_baseline = []

    bin = 1
    while bin <= ttbar.GetNbinsX():

        uncert = zjets.GetBinContent(bin)*dy_uncert + diboson.GetBinContent(bin)*diboson_uncert + singletop.GetBinContent(bin)*singletop_uncert + ttbar.GetBinContent(bin)*ttbar_uncert
        sum_all.SetBinError(bin, uncert)

        bin = bin + 1

    #sum_all.SetFillStyle(3002)
    #sum_all.SetFillStyle(3004)
    #sum_all.SetFillStyle(1)
    sum_all.SetLineWidth(3)
    sum_all.SetFillColor(kBlue-10)
    sum_all.SetMarkerSize(0)
    SetOwnership(sum_all, 0)

    ks    = 99.9
    chi   = 99.9
    do_ks = False

    kstest_sumall = ttbar.Clone()
    kstest_sumall.SetName("sumall")
    if diboson.Integral()>0:
        kstest_sumall.Add(diboson)
    if zjets.Integral()>0:
        kstest_sumall.Add(zjets)
    if wjets.Integral()>0:
        kstest_sumall.Add(wjets)
    if singletop.Integral()>0:
        kstest_sumall.Add(singletop)
    if fakes.Integral()>0:
        kstest_sumall.Add(fakes)

    if do_ks:
        ks  = data.KolmogorovTest(sumall)
        chi = data.Chi2Test(sumall, "CHI2/NDF")


    ### Now Plot everything ###

    c1 = TCanvas()
    mainPad =  TPad("mainPad", "top",    0.0, 0.26, 1.0, 1.00)
    ratioPad = TPad("ratioPad","bottom", 0.0, 0.02, 1.0, 0.26)

    mainPad.SetBottomMargin(0.0)
    mainPad.SetRightMargin(1.0)
    mainPad.SetTopMargin(0.065)
    ratioPad.SetRightMargin(1.0)
    ratioPad.SetTopMargin(0.015)
    ratioPad.SetBottomMargin(0.5)

    mainPad.Draw()
    mainPad.cd()

    # Make the Legend
    legend_x = 0.
    legend_y = 0.
    if legend_location == 1:
        legend_x = LEGEND_TOPLEFT_X
        legend_y = LEGEND_TOPLEFT_Y
    elif legend_location == 2:
        legend_x = LEGEND_TOPRIGHT_X
        legend_y = LEGEND_TOPRIGHT_Y

    legend = TLegend(legend_x, legend_y - LEGEND_HEIGHT, legend_x + LEGEND_WIDTH, legend_y)

    if channel == "ee":
        channel_string = "#font[12]{e}^{+}#font[12]{e}^{-}"
    if channel == "mumu":
        channel_string = "#mu^{+}#mu^{-}"
    if channel == "emu":
        channel_string = "#font[12]{e}^{#pm}#mu^{#mp}"
    if channel == "all":
        channel_string = "#font[12]{l}^{+}#font[12]{l}^{-}"

    sum_all.SetLineColor(0)
    sum_all.SetLineWidth(0)

    legend.SetHeader(channel_string + " channel")
    legend.AddEntry(data, "data","LEP")
    legend.AddEntry(ttbar, "#scale[1.1]{#font[12]{t#bar{t}}}","F")

    if zjets.Integral() > 0.1:
        legend.AddEntry(zjets,"Z/#gamma^{*}#rightarrow #font[12]{e}^{+}#font[12]{e}^{-}/#mu^{+}#mu^{-}","F")
    if wjets.Integral() > 0.1:
        legend.AddEntry(wjets,"W/#rightarrow ...","F")
    if singletop.Integral() > 0.1:
        legend.AddEntry(singletop,"Single top","F")
    if diboson.Integral() > 0.1:
        legend.AddEntry(diboson, "Diboson","F")
    if fakes.Integral() > 0.1:
        legend.AddEntry(fakes, "Fake leptons","F")

    if draw_uncertainty_band:
        legend.AddEntry(sum_all, "Xsec uncert.","F")


    legend.SetTextSize(TEXTSIZE)
    #legend.SetFillColor(BLANK)
    legend.SetFillStyle(0)
    legend.SetLineColor(0)

    ### Set Axis labels
    data.GetXaxis().SetTitle(histogram[1])
    data.GetYaxis().SetTitle(yaxis_label)
    data.GetYaxis().SetLabelSize(0.06)

    # show '0' in the plots
    if histogram[2]:
        mainPad.SetLogy(1)
        data.SetMinimum(1.001)
    else:
        data.SetMinimum(0.001)

    data.GetXaxis().SetLabelSize(0.0)
    data.GetYaxis().SetTitleSize(0.06)
    data.GetYaxis().SetTitleOffset(1.2)
    data.SetMaximum((ttbar.GetMaximum()*histogram[3]))
    #data.SetMaximum((data.GetMaximum()*histogram[3]))
    data.Draw("PE1X0")


    #    if draw_uncertainty_band:
    #    sum_all.Draw("E2 SAME")

    Stack.Draw("HIST SAME")
    legend.Draw("SAME")
    gPad.RedrawAxis();

    # draw lines at the mean
    if draw_mean_value_line:
        mean_data   = data.GetMean()
        mean_ttbar  = ttbar.GetMean()
        line_height = data.GetBinContent(data.GetMaximumBin())

        if histogram[2]: # log plot
            mean_line_data  = TLine(mean_data,  0, mean_data,  5*line_height)
            mean_line_ttbar = TLine(mean_ttbar, 0, mean_ttbar, 5*line_height)
        else:
            mean_line_data  = TLine(mean_data,   0, mean_data, 1.2*line_height)
            mean_line_ttbar = TLine(mean_ttbar, 0, mean_ttbar, 1.2*line_height)


        SetOwnership(mean_line_data,  False)
        mean_line_data.SetLineColor(kBlue)
        mean_line_data.SetLineWidth(2)
        mean_line_data.SetLineStyle(2)
        mean_line_data.Draw("SAME")
        make_mean(0.2, 0.68, mean_data,  "Data",  0.04, 4) # 2=red, 3=Green, 4=Blue

        SetOwnership(mean_line_ttbar, False)
        mean_line_ttbar.SetLineColor(kRed)
        mean_line_ttbar.SetLineWidth(2)
        mean_line_ttbar.SetLineStyle(2)
        mean_line_ttbar.Draw("SAME")
        make_mean(0.2, 0.72, mean_ttbar, "Reco",  0.04, 2) # 2=red, 3=Green, 4=Blue

        make_delta_mean(0.22, 0.64, mean_data, mean_ttbar, "#mu_D", "#mu_R", 0.04)

    thesis_plots = False

    if thesis_plots:
        if legend_location == 2:
            make_lumi(       0.20, 0.84, round(luminosity/1000,2),   0.05)
            make_energy(     0.47, 0.84, 13,    0.05)
        elif legend_location == 1:
            make_lumi(       0.50, 0.84, round(luminosity/1000,2),   0.05)
            make_energy(     0.76, 0.84, 13,    0.05)
    else:
        if legend_location == 2:
            make_ATLAS_label_work( 0.20, 0.87, 1, mainPad, 0.05, "Internal")
            make_lumi(       0.20, 0.79, round(luminosity/1000,2), 0.045)
            make_energy(     0.46, 0.79, 13,     0.048)
        elif legend_location == 1:
            make_ATLAS_label_work( 0.50, 0.87, 1, mainPad, 0.05, "Internal")
            make_lumi(       0.50, 0.79, round(luminosity/1000,2),   0.05)
            make_energy(     0.76, 0.79, 13,     0.05)


    # Draw Ratio Plot
    c1.cd()

    ratioPad.Draw()
    ratioPad.cd()

    ttbar_clone = ttbar.Clone("ttbar_clone")

    ratio_baseline = data.Clone("ratio_baseline")

    ratio_baseline.GetYaxis().SetLabelSize((data.GetYaxis().GetLabelSize())*3)
    ratio_baseline.GetYaxis().CenterTitle(0) #was 1
    ratio_baseline.GetYaxis().SetTitleOffset(0.4)
    ratio_baseline.GetYaxis().SetTitleColor(1)
    ratio_baseline.GetYaxis().SetTitleSize(0.18)

    ratio_baseline.GetXaxis().SetLabelSize((data.GetYaxis().GetLabelSize())*3)
    ratio_baseline.GetXaxis().SetLabelSize(0.06*3)
    ratio_baseline.GetXaxis().SetTitleSize(0.18)
    ratio_baseline.GetXaxis().SetTitleOffset(1.01)

    if "b-jet" in histogram[1] and "Multiplicity" in histogram[1]:
        ratio_baseline.SetMaximum(1.9)
        ratio_baseline.SetMinimum(0.7001)
    else:
        ratio_baseline.SetMaximum(1.2999)
        ratio_baseline.SetMinimum(0.7001)

    # create ticks etc and 'draw' invisible
    ratio_baseline.GetYaxis().SetNdivisions(003)
    ratio_baseline.SetLineColor(0)
    ratio_baseline.Draw("HIST")

    # collect all MC and fakes by adding background to ttbar
    if background.Integral()>0:
        ttbar_clone.Add(background)

    data_clone  = data.Clone("data_clone")
    data_clone.Divide(ttbar_clone)

    #ratio_baseline.Divide(data)
    #ratio_baseline.SetLineWidth(1)
    #ratio_baseline.SetLineColor(14)

    ratio_baseline.GetYaxis().SetTitle("Data/MC")

    sum_all_clone = sum_all.Clone("sum_all_clone")
    sum_all_clone.Divide(sum_all)
    sum_all_clone.SetFillColor(kBlue-10)

    data_clone.Draw("P0 SAME E0 ")
    if draw_uncertainty_band:
        sum_all_clone.Draw("E2 SAME")
        data_clone.Draw("P0 SAME E0 ")

    # draw dotted horizontal line at 1
    line = TLine(ratio_baseline.GetXaxis().GetXmin(),1,ttbar_clone.GetXaxis().GetXmax(),1)
    line.SetLineWidth(2)
    line.SetLineStyle(2)
    line.Draw("SAME")

    c1.cd()
    c1.Draw()

    ################
    mainPad.cd()
    gPad.RedrawAxis();
    ##################





    if data.Integral() != 0:
        #name = "/eos/user/n/nwarrack/Plots/" + period + "/" + channel + "_" + histname +"_"+ syst_type +".pdf"
        name = "./user/Plots/" + period + "/" + channel + "_" + histname +"_"+ syst_type +".pdf"
        c1.Print(name)


    c1.Close()

    return 1

def make_signal_comparison_plot(master,
                                channel,
                                histogram,
                                syst_type,
                                draw_mean_value_line,
                                compare_truth,
                                compare_NNLO,
                                legend_location, # 1 = top left, 2 = top right
                                luminosity,
                                period):

    """ Function to compare truth level to reco or reco to NNLO reweighted Reco"""

    histname = histogram[7]
    if "resolution" in histname:
        print "ERROR: resolution hist passed, skipping plot..."
        return 0

    # third party imports:
    from ROOT import TH1D, TLegend, TLatex, TCanvas, TPad, TStyle, gROOT, TH1, THStack, TLine, SetOwnership, gPad, kBlue, kRed, kMagenta, kGreen, TColor

    # project imports:
    from plot import TEXTSIZE, LINEWIDTH
    from plot import atlas_style, make_ATLAS_label_work, make_lumi, make_energy, make_mean, make_delta_mean, make_chi2, make_generic
    from histtools import divide_by_bin_width

    #kRed = kRed+1
    kMagenta = kMagenta-7
    #kMagenta = kMagenta.GetColorTransparent(0.01)
    
    testcol = TColor(gROOT.GetColor(26))
    testcol.SetAlpha(0.01);

    ttbar       = master["ttbar"]
    ttbar_NNLO  = master["ttbar_NNLO"]
    ttbar_truth = master["ttbar_truth"]
    data        = master["data"]

    if compare_truth and compare_NNLO:
        print "WARNING: you can only do one of `compare_truth` and `compare_NNLO`..."
        print "         continuing with TRUTH Vs. RECO comparison plots only..."
        compare_NNLO = False

    # skip if missing hists
    if compare_truth and ttbar_truth.Integral() == 0:
        print "ERROR: ttbar_truth histogram has integral of zero... aborting!"
        return 0
    if compare_NNLO and ttbar_NNLO.Integral() == 0:
        print "ERROR: ttbar_NNLO histogram has integral of zero... aborting!"
        return 0

    if compare_NNLO and ttbar_truth.Integral() != 0:
        print "WARNING: ttbar_truth histogram has integral of zero. Plotting without truth."
        include_truth = True
    else:
        include_truth = False

    if histogram[2]:
        log_plots = True
        line_scale = 5
    else:
        log_plots = False
        line_scale = 1.2

    gROOT.SetBatch(True)
    AtlasStyle = atlas_style()
    gROOT.SetStyle("ATLAS")
    gROOT.ForceStyle()
    TH1.SetDefaultSumw2()

    # Divide by bin widths and set line properties
    yaxis_label = "Events/bin"
    ttbar = divide_by_bin_width(ttbar)
    ttbar.SetLineWidth(LINEWIDTH)
    if compare_truth:
        ttbar.SetLineColor(kRed)
    else:
        ttbar.SetLineColor(1)
    ttbar_truth = divide_by_bin_width(ttbar_truth)
    ttbar_truth.SetLineWidth(LINEWIDTH)

    if compare_NNLO:
        ttbar_NNLO  = divide_by_bin_width(ttbar_NNLO)
        ttbar_NNLO.SetLineWidth(LINEWIDTH)
        #ttbar_NNLO.SetLineColor(kMagenta)
        ttbar_NNLO.SetLineColorAlpha(kRed, 0.9);
        ttbar_truth.SetLineColor(kGreen)
    else:
        ttbar_truth.SetLineColor(kBlue)

    # Hard coded cross section/normalisation uncertainties
    ttbar_uncert = 0.05
    bin = 1
    while bin <= ttbar.GetNbinsX():
        uncert = ttbar.GetBinContent(bin)*ttbar_uncert
        ttbar.SetBinError(bin, uncert)
        bin = bin + 1

    # Create and configure canvas and pads
    c1 = TCanvas()
    mainPad =  TPad("mainPad", "top",    0.0, 0.26, 1.0, 1.00)
    ratioPad = TPad("ratioPad","bottom", 0.0, 0.02, 1.0, 0.26)

    mainPad.SetBottomMargin(0.0)
    mainPad.SetRightMargin(1.0)
    mainPad.SetTopMargin(0.065)
    ratioPad.SetRightMargin(1.0)
    ratioPad.SetTopMargin(0.015)
    ratioPad.SetBottomMargin(0.5)

    mainPad.Draw()
    mainPad.cd()

    # Make the Legend
    legend_x = 0
    legend_y = 0
    if legend_location == 1:
        legend_x = LEGEND_TOPLEFT_X
        legend_y = LEGEND_TOPLEFT_Y
    elif legend_location == 2:
        legend_x = LEGEND_TOPRIGHT_X
        legend_y = LEGEND_TOPRIGHT_Y

    legend = TLegend(legend_x, legend_y - LEGEND_HEIGHT, legend_x + LEGEND_WIDTH, legend_y)

    if channel == "ee":
        channel_txt = "#font[12]{e}^{+}#font[12]{e}^{-}"
    if channel == "mumu":
        channel_txt = "#mu^{+}#mu^{-}"
    if channel == "emu":
        channel_txt = "#font[12]{e}^{#pm}#mu^{#mp}"
    if channel == "all":
        channel_txt = "#font[12]{l}^{+}#font[12]{l}^{-}"

    legend.SetHeader(channel_txt + " channel")

    if compare_truth:
        if "NW" in histname:
            legend.AddEntry(ttbar, "#scale[1.1]{#font[12]{t#bar{t}}} NW reco","F")
        elif "KIN" in histname:
            legend.AddEntry(ttbar, "#scale[1.1]{#font[12]{t#bar{t}}} KIN reco","F")
        else:
            legend.AddEntry(ttbar, "#scale[1.1]{#font[12]{t#bar{t}}} reco","F")

        legend.AddEntry(ttbar_truth,"#scale[1.1]{#font[12]{t#bar{t}}} truth","F")
    if compare_NNLO:
        legend.AddEntry(ttbar, "#scale[1.1]{k-factor}","F")
        legend.AddEntry(ttbar_NNLO, "#scale[0.9]{NNLO(top pT)}","F")
        if include_truth:
            legend.AddEntry(ttbar_truth,"#scale[0.9]{#font[12]{t#bar{t}}} truth","F")

    legend.SetTextSize(TEXTSIZE)
    legend.SetFillStyle(0)
    legend.SetLineColor(0)

    ### Set Axis labels
    ttbar.GetXaxis().SetTitle(histogram[1])
    ttbar.GetYaxis().SetTitle(yaxis_label)
    ttbar.GetYaxis().SetLabelSize(0.06)

    # show '0' in the plots
    if log_plots:
        mainPad.SetLogy(1)
        ttbar.SetMinimum(1.001)
    else:
        ttbar.SetMinimum(0.001)

    ttbar.GetXaxis().SetLabelSize(0.0)
    ttbar.GetYaxis().SetTitleSize(0.06)
    ttbar.GetYaxis().SetTitleOffset(1.2)
    ttbar.SetMaximum((ttbar.GetMaximum()*histogram[3]))

    if compare_truth:
        ttbar.Draw("HIST")
        ttbar_truth.Draw("HIST SAME")
    if compare_NNLO:
        ttbar.Draw("HIST")
        ttbar_NNLO.Draw("HIST SAME")
        if include_truth:
            ttbar_truth.SetLineStyle(7)
            ttbar_truth.Draw("HIST SAME")
            
    legend.Draw("SAME")
    gPad.RedrawAxis();

    # draw lines at the mean
    if draw_mean_value_line:
        line_height = ttbar.GetBinContent(ttbar.GetMaximumBin())
        line_height_truth = ttbar_truth.GetBinContent(ttbar_truth.GetMaximumBin())

        mean_ttbar  = ttbar.GetMean()
        mean_ttbar_line = TLine(mean_ttbar, 0, mean_ttbar, line_scale*line_height)
        #mean_ttbar_line = TLine(mean_ttbar, 0, mean_ttbar, line_height)
        SetOwnership(mean_ttbar_line, False)
        mean_ttbar_line.SetLineColor(kRed)
        mean_ttbar_line.SetLineWidth(2)
        mean_ttbar_line.SetLineStyle(2)
        mean_ttbar_line.Draw("SAME")
        
        if compare_truth or include_truth:
            mean_truth  = ttbar_truth.GetMean()
            #mean_truth_line = TLine(mean_truth, 0.5*line_scale*line_height, mean_truth, line_scale*line_height)
            mean_truth_line = TLine(mean_truth, 0.0, mean_truth, line_scale*line_height_truth)
            SetOwnership(mean_truth_line, False)
            mean_truth_line.SetLineWidth(2)
            mean_truth_line.SetLineStyle(2)
            if compare_truth:
                mean_truth_line.SetLineColor(kBlue)
                mean_truth_line.Draw("SAME")
            if include_truth:
                mean_truth_line.SetLineColor(kGreen)
                mean_truth_line.Draw("SAME")

        if compare_NNLO:
            mean_NNLO  = ttbar_NNLO.GetMean()
            mean_NNLO_line = TLine(mean_NNLO, line_height, mean_NNLO, line_scale*line_height)
            SetOwnership(mean_NNLO_line, False)
            #mean_NNLO_line.SetLineColor(kMagenta)
            mean_NNLO_line.SetLineColor(2)
            #mean_NNLO_line.SetLineColorAlpha(2, 0.5)
            mean_NNLO_line.SetLineWidth(2)
            mean_NNLO_line.SetLineStyle(2)
            mean_NNLO_line.Draw("SAME")


        # make mean legend (x, y, mean, mean_type, size, colour)

        if compare_truth:
            make_mean(0.22, 0.72, mean_ttbar, "Reco",  0.04, 2) # 2=kRed
            make_mean(0.22, 0.68, mean_truth, "Truth", 0.04, 4) # 4=kBlue
            make_delta_mean(0.2, 0.64, mean_truth, mean_ttbar, "#mu_{T}", "#mu_{R}", 0.04)
        if compare_NNLO:
            make_mean(0.22, 0.72, mean_ttbar, "#mu_{k}",  0.04, 1) # 2=kRed
            make_mean(0.22, 0.68, mean_NNLO,  "#mu_{pT}",  0.04, 2) # 4=kBlue
            if include_truth:
                make_mean(0.22, 0.64, mean_truth, "Truth", 0.04, 3) # 3=kGreen
                make_delta_mean(0.22, 0.59, mean_NNLO, mean_ttbar, "#mu_{k}", "#mu_{pT}", 0.04)
            else:
                make_delta_mean(0.22, 0.63, mean_NNLO, mean_ttbar, "#mu_{k}", "#mu_{pT}", 0.04)




    if legend_location == 2:
        make_ATLAS_label_work( 0.20, 0.87, 1, mainPad, 0.05, "Internal")
        make_lumi(       0.20, 0.79, round(luminosity/1000,2), 0.045)
        make_energy(     0.46, 0.79, 13,     0.048)
    elif legend_location == 1:
        make_ATLAS_label_work( 0.50, 0.87, 1, mainPad, 0.05, "Internal")
        make_lumi(       0.50, 0.79, round(luminosity/1000,2),   0.05)
        make_energy(     0.76, 0.79, 13,     0.05)

    # Draw Ratio Plot
    c1.cd()

    ratioPad.Draw()
    ratioPad.cd()

    ttbar_clone = ttbar.Clone("ttbar_clone")

    ratio_baseline = ttbar.Clone("ratio_baseline")

    ratio_baseline.GetYaxis().SetLabelSize((ttbar.GetYaxis().GetLabelSize())*3)
    ratio_baseline.GetYaxis().CenterTitle(0)
    ratio_baseline.GetYaxis().SetTitleOffset(0.4)
    ratio_baseline.GetYaxis().SetTitleColor(1)
    ratio_baseline.GetYaxis().SetTitleSize(0.18)

    ratio_baseline.GetXaxis().SetLabelSize((ttbar.GetYaxis().GetLabelSize())*3)
    ratio_baseline.GetXaxis().SetLabelSize(0.06*3)
    ratio_baseline.GetXaxis().SetTitleSize(0.18)
    ratio_baseline.GetXaxis().SetTitleOffset(1.01)

    ratio_baseline.SetMaximum(1.2999)
    ratio_baseline.SetMinimum(0.7001)

    # create ticks etc and 'draw' invisible
    ratio_baseline.GetYaxis().SetNdivisions(003)
    ratio_baseline.SetLineColor(0)
    ratio_baseline.Draw("HIST")

    if compare_NNLO:
        ttbar_NNLO_clone = ttbar_NNLO.Clone("NNLO_clone")
        #chi2_NNLO = ttbar_NNLO_clone.Chi2Test(ttbar_clone, "CHI2 WW")
        ttbar_NNLO_clone.Divide(ttbar_clone)
        # maxerror = 0
        # for b in range(ttbar_NNLO_clone.GetNbinsX()):
        #     if ttbar_NNLO_clone.GetBinError(b) > maxerror:
        #         maxerror = ttbar_NNLO_clone.GetBinError(b)

        #ratio_baseline.SetMaximum(ttbar_NNLO_clone.GetMaximum() + maxerror)
        #ratio_baseline.SetMinimum(ttbar_NNLO_clone.GetMinimum() - maxerror)
        ratio_baseline.SetMaximum(1.059999)
        ratio_baseline.SetMinimum(0.939999)
        ratio_baseline.GetYaxis().SetTitle("pT/K-fac")
        ttbar_NNLO_clone.Draw("P0 SAME E0 ")
        ttbar_clone.Draw("P0 SAME E0 ")

        # draw on main pad
        mainPad.cd()
        # make_chi2(x, y, chi2, size, [name1], [name2])
        #make_chi2(0.2, 0.59, chi2_truth, 0.04, "truth", "reco") #<- options to add function info
        #make_chi2(0.2, 0.58, round(chi2_NNLO,3), 0.04, "NNLO","NLO")
        ratioPad.cd()
        #print period ,channel, histname, syst_type, "chi2 =", chi2_truth
    if compare_truth:
        truth = ttbar_truth.Clone("truth_clone")
        chi2_truth = truth.Chi2Test(ttbar_clone, "CHI2 WW")
        ttbar_clone.Divide(truth)
        ratio_baseline.GetYaxis().SetTitle("Reco/Truth")
        ttbar_clone.Draw("P0 SAME E0 ")

        # draw on main pad
        mainPad.cd()
        # make_chi2(x, y, chi2, size, [name1], [name2])

        if draw_mean_value_line:
            make_chi2(0.2, 0.58, chi2_truth, 0.04)
            #make_chi2(0.2, 0.59, chi2_truth, 0.04, "truth", "reco")
        else:
            make_chi2(0.2, 0.69, chi2_truth, 0.04)
        ratioPad.cd()


    # draw dotted horizontal line at 1
    line = TLine(ratio_baseline.GetXaxis().GetXmin(),1,ttbar_clone.GetXaxis().GetXmax(),1)
    line.SetLineWidth(2)
    line.SetLineStyle(2)
    line.Draw("SAME")

    c1.cd()
    c1.Draw()

    ################
    mainPad.cd()
    gPad.RedrawAxis();
    ##################


    # save
    #name = "/eos/user/n/nwarrack/Plots/" + period + "/" + channel + "_" + histname +"_"+ syst_type +".pdf"
    name = "./user/Plots/" + period + "/" + channel + "_" + histname +"_"+ syst_type +".pdf"
    print name
    c1.Print(name)
    c1.Close()
    return 1
