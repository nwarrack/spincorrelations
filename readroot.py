#!/usr/bin/env python
import uproot
import argparse
import os.path as ospath





parser = argparse.ArgumentParser(description="quickly read root files without ROOT", formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument("-r", "--rootfile", help='ROOTFILE = myfile.root', required=True)
parser.add_argument("-a", "--action", help='ACTION = mig/keys', required=True)


args = parser.parse_args()
#config = vars(args)
#print(config)
rf = args.rootfile
ac = args.action

if not ospath.isfile(rf): 
    print(rf, "is not a file!")
    print ("use, e.g. user/Hists/16e/emu_cos_raxis_p_nominal_61.root")    
    exit()


f = uproot.open(rf)


if ac == "mig":
    mig = f["migration"]
    print(mig.values())

elif ac == "keys":
    print("KEYS:\n", f.keys())
    print("KEYS (truth):\n"     , f["truth"].keys())
    print("KEYS (nominal):\n"   , f["nominal"].keys())

elif ac == "weights":
    sumWeights = f["sumWeights"]
    print("KEYS (sumWeights):\n", sumWeights.classnames())
    #print("CLASSNAMES (sumWeights):\n", sumWeights.classnames())
    for i in range(len(sumWeights["totalEventsWeighted"])):
        print("check", i)
    
