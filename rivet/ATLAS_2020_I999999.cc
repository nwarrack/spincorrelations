// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/PartonicTops.hh"
namespace Rivet {


  /// @brief ttbar spin correlations full density matrix at 13 TeV
  class ATLAS_2020_I999999 : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(ATLAS_2020_I999999);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {


      // Get running mode or set to emu channel
      _mode = 3;
      if ( getOption("LMODE") == "EE" )   _mode = 1;
      if ( getOption("LMODE") == "MUMU" ) _mode = 2;
      if ( getOption("LMODE") == "EMU" )  _mode = 3;
      if ( getOption("LMODE") == "ALL" )  _mode = 4;

      // Initialise and register projections
      const FinalState fs(Cuts::abseta < 5.0);

      // Photons to dress leptons
      FinalState photons(fs, Cuts::abspid == PID::PHOTON);

      // leptons
      Cut lepton_cuts = Cuts::abseta < 2.5 && Cuts::pT > 25*GeV;
      PromptFinalState bare_leps(Cuts::abspid == PID::MUON || Cuts::abspid == PID::ELECTRON);
      DressedLeptons dressed_leps(photons, bare_leps, 0.1, lepton_cuts);
      declare(dressed_leps, "Leptons");

      // missing momentum
      declare(MissingMomentum(fs), "MET");

      // jets
      FastJets jets(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      declare(jets, "Jets");


      PartonicTops partonTops;
      declare(partonTops, "PartonicTops");


      // Book histograms
      //book(_h["b_kplus"], "bkp", {-1.0, -0.5, 0.0, 0.5, 1.0});
      book(_h["lep1_pt"], "lep1_pt", 10, 0.0, 500.0); // 10 even bins [0,500]
      //book(_h["b_kplus"], "b_kplus", 10, -1, 1);
      book(_h["lep1_boosted_pt"], "lep1_boosted_pt", 10, 0.0, 500.0);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      // retrieve objects
      vector<DressedLepton> leptons = apply<DressedLeptons>(event, "Leptons").dressedLeptons();
      const Particles partonicTops = apply<PartonicTops>( event, "PartonicTops").particlesByPt();
      Jets jets = apply<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 25*GeV);

      // TODO: add overlap removal
      // remove all jets within dR < 0.2 of a dressed lepton??
      // idiscardIfAnyDeltaRLess(jets, leptons, 0.2);

      // select b jets
      Jets bjets = filter_select(jets, [](const Jet& jet) {
        return  jet.bTagged(Cuts::pT > 5*GeV && Cuts::abseta < 2.5);
      });

      // veto event if there is not 2 b-jets
      if (bjets.size() != 2)  vetoEvent;

      // apply a missing-momentum cut
      //if (apply<MissingMomentum>(event, "MET").missingPt() < 30*GeV)  vetoEvent;
      if (leptons.size() != 2) vetoEvent;


      // -> check lepton charge here <- //
      bool pos_lep_set = false;
      bool neg_lep_set = false;

      for (int i = 0; i < 2; i++){
        if (leptons[i].pid > 0.0){
          lepton_pos = leptons[i];
          pos_lep_set = true
        }
        if (leptons[i].pid > 0.0){
          lepton_pos = leptons[i];
          neg_lep_set = true
        }
      }
      
      if (!pos_lep_set || !neg_lep_set) vetotEvent;

      // ttbar reconstruction
      int nsmears  = 1;
      double mtop  = 172.5;
      double mtbar = 172.5;
      double mWpos = 80.379;
      double mWneg = 80.379;
      for (int i = 0; i < nsmears; ++i){
        
        // should loops over:
        // - top mass (from gaussian with mean 172.5 and stdDev 1.48)
        // - W mass (from gaussian with mean 80.379 and stdDev 2.085)
        // - combinatoric choice of b-jets for b/bbar.
        
        Particles neutrino_solutions = ReconstructNW(lepton_pos, lepton_neg, b, bbar, met_ex, met_ey, mtop, mtbar, mWpos, mWneg);
      }

      // check boosting:
      // Fill pre-boost histo
      _h["lep1_pt"]->fill(leptons[0].pT()/GeV );
      LorentzTransform LT_lep1 = LorentzTransform::mkFrameTransform(leptons[0]);
      FourMomentum boostedLep1;
      boostedLep1 = LT_lep1.transform(leptons[0]);
      //_h["lep1_boosted_pt"]->fill(boostedLep1.pT()/GeV);

      if (partonicTops.size() == 2) {
        FourMomentum ttbar = partonicTops[0].mom() + partonicTops[1].mom();
        BoostedSystem examp(partonicTops[0], leptons[0], ttbar);
        _h["lep1_boosted_pt"]->fill(examp.bt().pT()/GeV);



        auto costheta = [](const FourMomentum & boostedtop, const FourMomentum & boostedlep, Vector3 z, int axis, int ref) {
          cout << boostedtop.pT() << endl;
        };

        Vector3 z_axis(0.0, 0.0, 1.0);
        int axis = 1;
        int ref = 1;

        //        for (auto top : partonicTops){
    
        costheta( examp.bt(), leptons[0].mom(), z_axis, axis, ref);
            
      }
      


      /* python code:
def cos_theta(t, l, z, axis, ref):

    """
    Function calculates cos of the angle between the lepton three-mom in
    its parent top's rest frame and a reference vector
    """

    from ROOT import TMath

    topDirection = t.Unit()
    yp = z.Dot(topDirection)
    rp = TMath.Sqrt(1.0 - yp*yp)

    # define the k helicity (AKA 'k') axis in the helicity basis
    if "k" in axis:
        quant_axis = topDirection
        mult_factor = 1

    # define the transverse (AKA 'n') axis in the helicity basis
    if "n" in axis:
        quant_axis = 1.0/rp * z.Cross(topDirection)
        mult_factor = TMath.Sign(1,yp)
        
    # define the r axis in the helicity basis
    if "r" in axis:
        quant_axis = 1.0/ rp * (z - yp*topDirection)
        mult_factor = TMath.Sign(1,yp)

    if ref == "b":
        mult_factor = -1*mult_factor

    return TMath.Cos(l.Angle(mult_factor*quant_axis))
    */


      // get tops
      /*
      FourMomentum top, tbar, ttbar;

      for (const Particle& t : partonicTops) {
        if (t.pid() == PID::TQUARK) {
          top = t.mom();
        } else {
          tbar = t.mom();
        }
      }

      ttbar = top + tbar;

      // Boost leptons into ttbar ZMF
      FourMomentum l_p, l_m;
      LorentzTransform LT_ttbar = LorentzTransform::mkFrameTransform(ttbar);
      for (const Particle& l : leptons) {
        if (l.charge() > 0.0){
          l_p = LT_ttbar.transform(l); 
        } else {
          l_m = LT_ttbar.transform(l);
        }
      }
      
      */
    }


    /// Normalise histograms etc., after the run
    void finalize() {
      
      double sf = crossSection() / picobarn / sumOfWeights();
      for (auto& hist : _h) { scale(hist.second, sf); }
    }

    //@}
  private:
    map<string, Histo1DPtr> _h;
    int _mode;

    Particles ReconstructNW(Particle lepton_pos, 
                            Particle lepton_neg, 
                            Particle b, 
                            Particle bbar, 
                            double met_ex, 
                            double met_ey, 
                            double mtop,
                            double mtbar,
                            double mWpos,
                            double mWneg){

  uint nsmears = 10;
  bool sol_nu_success = false;
  bool sol_nubar_success = false;
  std::vector<Particle> solution_nu;
  std::vector<Particle> solution_nubar;
  double nu_eta_mean    = lepton_pos.Eta()*0.72 - 0.018*lepton_pos.Eta()*lepton_pos.Eta()*lepton_pos.Eta();
  double nubar_eta_mean = lepton_neg.Eta()*0.72 - 0.018*lepton_neg.Eta()*lepton_neg.Eta()*lepton_neg.Eta();

  double width = 1.16;
  TRandom3 random = TRandom3(0);

  for(uint i = 0; i < nsmears; ++i){

    if (!sol_nu_success){
      double nu_eta = random.Gaus(nu_eta_mean, width);
      solution_nu = NW_solveForNeutrinoEta(&lepton_pos, &b, nu_eta, mtop, mWpos);
      if (solution_nu.size() > 0) {
        sol_nu_success = true ;
      }
    }

    if (!sol_nubar_success){
      double nubar_eta = random.Gaus(nubar_eta_mean, width);
      solution_nubar = NW_solveForNeutrinoEta(&lepton_neg, &bbar, nubar_eta, mtbar, mWneg);
      if (solution_nubar.size() > 0) {
        sol_nubar_success = true ;
      }
    }

    if( sol_nu_success && sol_nubar_success) break;
  }

  if(solution_nu.size()    == 0) return;
  if(solution_nubar.size() == 0) return;

  Particle top, tbar, ttbar, Wpos, Wneg, nu, nubar;

  for(uint index_nu = 0; index_nu < solution_nu.size(); ++index_nu){
    for(uint index_nubar = 0; index_nubar < solution_nubar.size(); ++index_nubar){

      nu    = solution_nu.at(index_nu);
      nubar = solution_nubar.at(index_nubar);
      Wpos  = lepton_pos + nu;
      Wneg  = lepton_neg + nubar;  
      top   = Wpos + b;
      tbar  = Wneg + bbar;

      m_NW_tops.push_back(top);
      m_NW_tbars.push_back(tbar);
      m_NW_nus.push_back(nu);
      m_NW_nubars.push_back(nubar);
      m_NW_Wposs.push_back(Wpos);
      m_NW_Wnegs.push_back(Wneg);

      double weight = NW_get_weight(nu, nubar, met_ex, met_ey);
      m_NW_weights.push_back(weight);
      if(weight > NWhighestWeight && weight > 0.00001 && top.E() > 0 && tbar.E() > 0){
      	NWhighestWeight = weight;
      	m_NW_highestWeightTop   = top;
      	m_NW_highestWeightTbar  = tbar;
      	m_NW_highestWeightNu    = nu;
      	m_NW_highestWeightNubar = nubar;
      	m_NW_highestWeightWpos  = Wpos;
      	m_NW_highestWeightWneg  = Wneg;
      }
    }
  }
  return;
}


    struct BoostedSystem : public Particle {
      BoostedSystem() { }
      BoostedSystem(Particle top, Particle lep, FourMomentum ttbar) : _t(top), _l(lep), _tt(ttbar) { }

      Particle _t, _l;
      FourMomentum _tt;

      FourMomentum bt() const {
        // returns boosted top
        LorentzTransform LT_top = LorentzTransform::mkFrameTransform(_tt);
        FourMomentum boostedTop;
        boostedTop = LT_top.transform(_t);
        return boostedTop;
      }

    };


  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2020_I999999);


}
