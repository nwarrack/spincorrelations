### Rivet routine

_Every Rivet-able ATLAS analysis needs one!_

[ATLAS Physics Modeling Group Rivet tutorial](https://gitlab.cern.ch/atlas-physics/pmg/tutorials/tree/master/Rivet).

A brief example of running the rivet routine on a couple of _locally stored_ EVNT files (all this info is in the link above!)


```bash
# Firstly lets setup rucio and rivet
setupATLAS
asetup 21.6.33,AthGeneration,here
source setupRivet.sh
lsetup rucio -w

# Using rucio download some ttbar EVNT MC files (e.g. from mc15_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.evgen.EVNT.e6348)
rucio download mc16_13TeV:EVNT.12526783._001887.pool.root.1

# Build the rivet routine
rivet-build ATLAS_2020_I999999.cc

# Use Athena to run the ATLAS EVNT files through Rivet
athena local_jO.py
```
_NB: you can edit the `local_jO.py` file to run on less events. e.g.:_
```bash
theApp.EvtMax = 100
```