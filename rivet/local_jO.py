theApp.EvtMax = 100
import AthenaPoolCnvSvc.ReadAthenaPool

# Choose your directory
datadir="./mc16_13TeV/"

# Name your file(s) in a list
svcMgr.EventSelector.InputCollections = [
    datadir + 'EVNT.12526783._001887.pool.root.1',
]

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'ATLAS_2020_I999999' ]
rivet.RunName = ''
rivet.HistoFile = 'output.yoda'
rivet.CrossSection = 1.0
#rivet.IgnoreBeamCheck = True
#rivet.SkipWeights=True
job += rivet

svcMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.infoLimit = 10
