# source this file in bash to set up software on systems with cvmfs mounted.

# Make sure script is sourced from correct location
[[ -f setup.sh ]] || { echo >&2 "Exiting... You should source setup.sh from within the directory containing setup.sh!"; return 1; }

# set up ATLAS software access
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export setupATLAS=${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
setupATLAS -2

# set up ROOT
lsetup "root 6.14.08-x86_64-centos7-gcc8-opt"

# Rucio set up (if you don't need grid access you can comment these lines out)
export X509_USER_PROXY=~/my_proxy_file
lsetup rucio

# Make some required directories
[[ -d user/Hists/16a ]] || { mkdir -p user/Hists/16a; }
[[ -d user/Hists/16d ]] || { mkdir -p user/Hists/16d; }
[[ -d user/Hists/16e ]] || { mkdir -p user/Hists/16e; }
[[ -d user/Plots/16a ]] || { mkdir -p user/Plots/16a; }
[[ -d user/Plots/16d ]] || { mkdir -p user/Plots/16d; }
[[ -d user/Plots/16e ]] || { mkdir -p user/Plots/16e; }
[[ -d user/Plots/all ]] || { mkdir -p user/Plots/all; }
[[ -d user/addresses ]] || { mkdir -p user/addresses; }

# export env variable for us in python scripts
export SPINHOME="$PWD"

echo ""
echo " ++++++ TTBAR +++ SPINCORRELATIONS +++ RUN2 +++++++"
echo " ++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " There are guides for this code in the READMEs"
echo " However, it may be easier to read them on the web:"
echo ""
echo " https://gitlab.cern.ch/nwarrack/spincorrelations/"
echo " ++++++++++++++++++++++++++++++++++++++++++++++++++"
echo ""
