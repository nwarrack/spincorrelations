""" module contains the machinery to boost tops and leptons and to calculate spin correlations, cross-correlations and polarizations """

def boosted_objects(event, reco_method):

    """
    function to get the reconstructed leptons and tops and
    boost them in order to calculate spin correlatins,
    cross-correlations and polarizations.
    """

    from spin import make_lorentzvector_mass, leptons

    # collect leptons
    lep_top, lep_tbar = leptons(event)

    # collect tops
    t_string    = "d_" + reco_method + "_t"
    tbar_string = "d_" + reco_method + "_tbar"
    top   = make_lorentzvector_mass(event, t_string)
    tbar  = make_lorentzvector_mass(event, tbar_string)

    # return boosted_objects
    return boost_topleps(top, tbar, lep_top, lep_tbar)


def boosted_objects_pl(event):

    """
    function to get the particle level leptons and tops and
    boost them in order to calculate spin correlatins,
    cross-correlations and polarizations.
    """

    from spin import make_lorentzvector_mass, leptons_pl

    # collect leptons
    lep_top, lep_tbar = leptons_pl(event)

    top   = make_lorentzvector_mass(event, "d_t")
    tbar  = make_lorentzvector_mass(event, "d_tbar")

    # return boosted_objects
    return boost_topleps(top, tbar, lep_top, lep_tbar)


def boosted_objects_truth(truthevent):

    """
    function to get the truth-level leptons and tops and
    boost them in order to calculate spin correlatins,
    cross-correlations and polarizations
    """

    from spin import leptons_truth, tops_truth

    # create new four vector objects from branch values
    top, tbar = tops_truth(truthevent)
    lep_top, lep_tbar = leptons_truth(truthevent)

    return boost_topleps(top, tbar, lep_top, lep_tbar)


def boost_topleps(top, tbar, lep_top, lep_tbar):

    """ function to perform the lorentz boosts on the
    tops and leptons, needed for calculating spin
    correlations, cross correlations and polarizations.
    The returned objects are three-momenta
    """

    from ROOT import TVector3 as TV3

    # boost tops into ttbar zero momentum frame (ZMF)
    ttbar = top + tbar
    top.Boost(-ttbar.BoostVector())
    tbar.Boost(-ttbar.BoostVector())

    # boost leptons into ttbar ZMF
    lep_top.Boost(-ttbar.BoostVector())
    lep_tbar.Boost(-ttbar.BoostVector())

    # boost lepton to its parent top's rest frame
    lep_top.Boost(-top.BoostVector())
    lep_tbar.Boost(-tbar.BoostVector())

    # return lepton and top momentum three vectors
    return TV3(top.Vect()), TV3(tbar.Vect()), TV3(lep_top.Vect()), TV3(lep_tbar.Vect())
