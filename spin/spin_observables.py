def calc_cos_thetas(top, tbar, t_lep, tbar_lep, z_axis):

    # Find all the k, n and r cos(theta) values for a and b
    cta = {}
    cta["k"] = cos_theta(top, t_lep, z_axis, "k", "a")
    cta["n"] = cos_theta(top, t_lep, z_axis, "n", "a")
    cta["r"] = cos_theta(top, t_lep, z_axis, "r", "a")
    ctb = {}
    ctb["k"] = cos_theta(tbar, tbar_lep, z_axis, "k", "b")
    ctb["n"] = cos_theta(tbar, tbar_lep, z_axis, "n", "b")
    ctb["r"] = cos_theta(tbar, tbar_lep, z_axis, "r", "b")

    return cta, ctb

def cos_theta(t, l, z, axis, ref):

    """
    Function calculates cos of the angle between the lepton three-mom in
    its parent top's rest frame and a reference vector
    """

    from ROOT import TMath

    topDirection = t.Unit()
    yp = z.Dot(topDirection)
    rp = TMath.Sqrt(1.0 - yp*yp)

    # define the k helicity (AKA 'k') axis in the helicity basis
    if "k" in axis:
        quant_axis = topDirection
        mult_factor = 1

    # define the transverse (AKA 'n') axis in the helicity basis
    if "n" in axis:
        quant_axis = 1.0/rp * z.Cross(topDirection)
        mult_factor = TMath.Sign(1,yp)
        
    # define the r axis in the helicity basis
    if "r" in axis:
        quant_axis = 1.0/ rp * (z - yp*topDirection)
        mult_factor = TMath.Sign(1,yp)

    if ref == "b":
        mult_factor = -1*mult_factor

    return TMath.Cos(l.Angle(mult_factor*quant_axis))


def calc_spin_obs(name, cta, ctb):

    """
    function to calculate spin cor/pol sensitive observables
    from cosTheta_a (cta) and cosTheta_b (ctb) dictionaries
    """

    # calculate spin correlation sensitive obs
    if name.startswith("c_") and len(name) == 4:
        spin_obs = cta[name[2]]*ctb[name[3]]

    # calculate cross correlations sensitive obs
    elif name.startswith("c_") and len(name) > 4:
        if name[5] == "p":
            spin_obs = cta[name[2]]*ctb[name[3]]+cta[name[3]]*ctb[name[2]]
        elif name[5] == "m":
            spin_obs = cta[name[2]]*ctb[name[3]]-cta[name[3]]*ctb[name[2]]

    # calculate spin polerization sensitive obs
    elif name.startswith("b_") and name[3] == "p":
        spin_obs = cta[name[2]]
    elif name.startswith("b_") and name[3] == "m":
        spin_obs = ctb[name[2]]

    return spin_obs
