from ROOT import TLorentzVector, Math
import random


def leptons(event):

    lep_top  = TLorentzVector()
    lep_tbar = TLorentzVector()
    
    # sstags = [0, 1]
    # if ss: # same sign lepton event
    #     random.shuffle(tags)
    #     lep_top.SetPtEtaPhiE(event.d_lep_pt[sstags[0]],event.d_lep_eta[sstags[0]],event.d_lep_phi[sstags[0]],event.d_lep_e[sstags[0]])
    #     lep_tbar.SetPtEtaPhiE(event.d_lep_pt[sstags[1]],event.d_lep_eta[sstags[1]],event.d_lep_phi[sstags[1]],event.d_lep_e[sstags[1]])

    for i in range(2):
        if event.d_lep_pdgid[i] < 0:
            lep_top.SetPtEtaPhiE(event.d_lep_pt[i],event.d_lep_eta[i],event.d_lep_phi[i],event.d_lep_e[i])
        else:
            lep_tbar.SetPtEtaPhiE(event.d_lep_pt[i],event.d_lep_eta[i],event.d_lep_phi[i],event.d_lep_e[i])

    return lep_top, lep_tbar


def true_leptons(event):

    """hunt for true stuff that is saved in nominal tree for some reason"""
    
    lep_top  = TLorentzVector()
    lep_tbar = TLorentzVector()

    for i in range(2):
        if event.d_lep_true_pdgid[i] < 0:
            lep_top.SetPtEtaPhiE(event.d_lep_true_pt[i],event.d_lep_true_eta[i],event.d_lep_true_phi[i],event.d_lep_true_e[i])
        else:
            lep_tbar.SetPtEtaPhiE(event.d_lep_true_pt[i],event.d_lep_true_eta[i],event.d_lep_true_phi[i],event.d_lep_true_e[i])

    return lep_top, lep_tbar


def leptons_pl(event):

    """Particle level tree has no d_lep_e vector so need to set this pl leptons function"""
    lep_top  = TLorentzVector()
    lep_tbar = TLorentzVector()

    for i in range(2):
        if event.d_lep_pdgid[i] < 0:
            lep_top.SetPtEtaPhiM(event.d_lep_pt[i],event.d_lep_eta[i],event.d_lep_phi[i],event.d_lep_m[i])
        else:
            lep_tbar.SetPtEtaPhiM(event.d_lep_pt[i],event.d_lep_eta[i],event.d_lep_phi[i],event.d_lep_m[i])

    return lep_top, lep_tbar


def leptons_mumu(event):


    lep_top  = TLorentzVector()
    lep_tbar = TLorentzVector()

    if event.mu_charge[0] > 0:
        lep_top.SetPtEtaPhiE(event.mu_pt[0],event.mu_eta[0],event.mu_phi[0],event.mu_e[0])
        lep_tbar.SetPtEtaPhiE(event.mu_pt[1],event.mu_eta[1],event.mu_phi[1],event.mu_e[1])
    else:
        lep_top.SetPtEtaPhiE(event.mu_pt[1],event.mu_eta[1],event.mu_phi[1],event.mu_e[1])
        lep_tbar.SetPtEtaPhiE(event.mu_pt[0],event.mu_eta[0],event.mu_phi[0],event.mu_e[0])

    return lep_top, lep_tbar



def leptons_emu(event):

    lep_top  = TLorentzVector()
    lep_tbar = TLorentzVector()

    if event.el_charge[0] > 0:
        lep_top.SetPtEtaPhiE(event.el_pt[0],event.el_eta[0],event.el_phi[0],event.el_e[0])
        lep_tbar.SetPtEtaPhiE(event.mu_pt[0],event.mu_eta[0],event.mu_phi[0],event.mu_e[0])
    else:
        lep_top.SetPtEtaPhiE(event.mu_pt[0],event.mu_eta[0],event.mu_phi[0],event.mu_e[0])        
        lep_tbar.SetPtEtaPhiE(event.el_pt[0],event.el_eta[0],event.el_phi[0],event.el_e[0])

    return lep_top, lep_tbar

def leptons_ee(event):

    lep_top  = TLorentzVector()
    lep_tbar = TLorentzVector()

    if event.el_charge[0] > 0:
        lep_top.SetPtEtaPhiE(event.el_pt[0],event.el_eta[0],event.el_phi[0],event.el_e[0])
        lep_tbar.SetPtEtaPhiE(event.el_pt[1],event.el_eta[1],event.el_phi[1],event.el_e[1])
    else:
        lep_top.SetPtEtaPhiE(event.el_pt[1],event.el_eta[1],event.el_phi[1],event.el_e[1])
        lep_tbar.SetPtEtaPhiE(event.el_pt[0],event.el_eta[0],event.el_phi[0],event.el_e[0])

    return lep_top, lep_tbar


def leptons_truth(truth):

    lep_top  = make_lorentzvector_mass(truth, "d_lbar")
    lep_tbar = make_lorentzvector_mass(truth, "d_l")
    return lep_top, lep_tbar



def neutrinos_truth(truth):

    if dilepton_truth(truth):
        nu_top = make_lorentzvector_mass(truth, "d_nu")
        nu_tbar = make_lorentzvector_mass(truth, "d_nubar")
        return nu_top, nu_tbar
    else:
        return TLorentzVector(), TLorentzVector()


def tops_truth(truth):

        top  = make_lorentzvector_mass(truth, "d_t_afterFSR")
        tbar = make_lorentzvector_mass(truth, "d_tbar_afterFSR")

        return top, tbar

def leptons_truthAT21284(truth):

    if truth.MC_Wdecay1_from_t_pdgId%2 == 0:
        lep_top = make_lorentzvector_mass(truth, "MC_Wdecay1_from_t")
    else:
        lep_top = make_lorentzvector_mass(truth, "MC_Wdecay1_from_t")

    if truth.MC_Wdecay1_from_tbar_pdgId%2 == 0:
        lep_tbar = make_lorentzvector_mass(truth, "MC_Wdecay2_from_tbar")
    else:
        lep_tbar = make_lorentzvector_mass(truth, "MC_Wdecay1_from_tbar")

    return lep_top, lep_tbar



def neutrinos_truthAT21284(truth):

    if dilepton_truth(truth):
        if truth.MC_Wdecay1_from_t_pdgId%2 == 0:
            nu_top = make_lorentzvector_mass(truth, "MC_Wdecay1_from_t")
        else:
            nu_top = make_lorentzvector_mass(truth, "MC_Wdecay2_from_t")
        if truth.MC_Wdecay1_from_tbar_pdgId%2 == 0:
            nu_tbar = make_lorentzvector_mass(truth, "MC_Wdecay1_from_tbar")
        else:
            nu_tbar = make_lorentzvector_mass(truth, "MC_Wdecay2_from_tbar")
        return nu_top, nu_tbar
    else:
        return TLorentzVector(), TLorentzVector()

    
def tops_truth_AT21284(truth):

        top  = make_lorentzvector_mass(truth, "MC_t_afterFSR")
        tbar = make_lorentzvector_mass(truth, "MC_tbar_afterFSR")

        return top, tbar




def boost_AtoCviaB(A, B, C):
    """ Boost A->B->C. i.e. boost the fourvector A to the restframe of object C via the rest frame of object B"""
    boostedA = A
    boostedA.Boost( - B.BoostVector() )
    return boostedA


def get_PtEtaPhiM(obj):
    
    pt  = obj.Pt()
    eta = obj.Eta()
    phi = obj.Phi()
    m   = obj.M()

    return pt, eta, phi, m


def make_fourvector(event, obj):

    obj_four_vector = Math.PtEtaPhiMVector()

    pt  = obj + "_pt"
    eta = obj + "_eta"
    phi = obj + "_phi"
    m   = obj + "_m"

    obj_four_vector.SetPt(  getattr(event, str(pt)  ))
    obj_four_vector.SetEta( getattr(event, str(eta) ))
    obj_four_vector.SetPhi( getattr(event, str(phi) ))
    obj_four_vector.SetM(   getattr(event, str(m)   ))


    return obj_four_vector

def make_lorentzvector_mass(event, obj):

    pt  = obj + "_pt"
    eta = obj + "_eta"
    phi = obj + "_phi"
    m   = obj + "_m"

    obj_4vec = TLorentzVector()
    
    # nTuple missing d_lbar_m in truth tree! Hacky workaround!
    # TODO: remove when this is fixed!
    if obj == "d_lbar":
        if event.d_lbar_pdgid == -11:
            obj_4vec.SetPtEtaPhiM(getattr(event,str(pt)), getattr(event,str(eta)), getattr(event,str(phi)), 0.5)
        elif event.d_lbar_pdgid == -13:
            obj_4vec.SetPtEtaPhiM(getattr(event,str(pt)), getattr(event,str(eta)), getattr(event,str(phi)), 105.7)
        elif event.d_lbar_pdgid == -15:
            obj_4vec.SetPtEtaPhiM(getattr(event,str(pt)), getattr(event,str(eta)), getattr(event,str(phi)), 1776.8)
        else:
            print "something has gone wrong!"
    else:
        obj_4vec.SetPtEtaPhiM(getattr(event,str(pt)), getattr(event,str(eta)), getattr(event,str(phi)), getattr(event,str(m)))

    return obj_4vec

def make_lorentzvector_energy(event, obj):

    # used in particle-level pseudo top creation

    pt  = obj + "_pt"
    eta = obj + "_eta"
    phi = obj + "_phi"
    e   = obj + "_e"

    obj_4vec = TLorentzVector()

    obj_4vec.SetPtEtaPhiE(getattr(event,str(pt)), getattr(event,str(eta)), getattr(event,str(phi)), getattr(event,str(e)))

    return obj_4vec
