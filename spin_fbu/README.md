This is some instructions to get Theo's pyFBU to go!


# Links 
[The publicly avaliable OG FBU](https://github.com/gerbaudo/fbu)  
[Theo's spin correlations FBU implementation](https://gitlab.cern.ch/AtlasKe/FBU_unfolding)  

# Setup
## First time
Make a new shell for each of the following three commands:
```bash
source install.sh x86_64-centos7-gcc8-opt # <- sets up a virtual env
source install_batch.sh # <- this sents up another new env for batch stuff
source init.sh  # <- puts you into fbuenv and does a check to see if anything needs recompiling
```
